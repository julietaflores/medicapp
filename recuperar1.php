<!DOCTYPE html>
<html lang="en">
<head>
<?php include "headerm.php";?>
<title>Clinica Smart | </title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/logo.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<!--<script src="js/sigin.js"></script>-->
<!--===============================================================================================-->
</head>



<script type="text/javascript">				
			function enviarClave(){
				var user = $("#correo").val();
               if(user==""){

				new PNotify({
                 title: 'Señor Usuario !!!',
                 text: 'Necesita llenar el campo correo !!!',
                 type: 'info',
                 hide: false,
                })

               }else{

                $.ajax({
                   type: "GET",
                    contentType: "application/json; charset=utf-8",
                    url:"ws/api_rest.php/recuperar_correo/"+user,
                    dataType: "json",
                    data: { },
                    success: function (data){
                     // alert(data);
                      if(data==1){
                     
					 
				new PNotify({
                 title: 'Señor Usuario !!!',
                 text: 'Se  le envio un Correo verifique porfavor !!!',
                 type: 'info',
                 hide: false,
                })
                    setTimeout( "window.location.href='login.php'", 2000 );

                      }else{
                     
            if(data==0 || data==2 ){
               
				new PNotify({
                 title: 'Señor Usuario !!!',
                 text: 'El correo no existe en la Base de datos !!!',
                 type: 'error',
                 hide: false,
                })
			}else{
				if(data == 400){
					new PNotify({
                 title: 'Señor Usuario !!!',
                 text: 'No se pudo enviar el Correo !!!',
                 type: 'error',
                 hide: false,
                })
				}
			}
			
                      }
                     }
                 });

               }
			}		
    </script>  
    

<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">


				<form class="login100-form validate-form">

					<span class="login100-form-title">
						<img src="images/logo.png" style="border: 0px; width:60%; border-radius: 50%; height:60%;">
					</span>

					<br> 
					<h3  style="color:#1fb9d6;font-weight: bold; font-size: 25px; text-align: center;">
					   Recuperar Clave </h3>
            <p style=" color:#1fb9d6; font-size: 19px;padding: 15px; text-align: center;">
            Ingrese el correo electrónico con el que se registró:
            </p>
                       <br>
					<div class="wrap-input100 validate-input" >
			    		<span class="btn-show-pass">
							<i class="zmdi zmdi-account"></i>
						</span>
						<input class="input100" type="text" name="correo" id="correo">
						<span class="focus-input100" data-placeholder="juanperez@mail.com"></span>
					</div>
				
					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<a class="login100-form-btn" href="#" onclick="enviarClave();">Enviar Clave</a>
						</div>
					</div>

		
					
					<br>
					<div>
					   <div id="custom_notifications" class="custom-notifications dsp_none">
						<ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group"></ul>
						<div class="clearfix"></div>
					    <div id="notif-group" class="tabbed_notifications"></div>
					   </div>
                    </div>
				</form>


			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
	<script type="text/javascript" src="js/notify/pnotify.core.js"></script>
	<script type="text/javascript" src="js/notify/pnotify.buttons.js"></script>
  <script type="text/javascript" src="js/notify/pnotify.nonblock.js"></script>



</body>
</html>