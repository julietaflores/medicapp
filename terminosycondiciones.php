<!DOCTYPE html>
<html lang="en">
<head>
<title>Terminos y Condiciones </title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
<link rel="icon" type="image/png" href="images/logo.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<!--<script src="js/sigin.js"></script>-->
<!--===============================================================================================-->
</head>


<body lang="EN-US" link="blue" vlink="purple">
	
<br>
<span class="login100-form-title">
						<img src="images/logo.png" style="border: 0px; width:10%; border-radius: 15%; height:5%;">
					</span>
<p class="MsoNormal" align="center" style="text-align:center"><b><span lang="ES-MX" style="font-family:Arial">Términos y Condiciones de Uso del Sitio Web </span></b><span lang="ES-MX" style="font-family:Arial"><b>sistema.tuclinicapp.com</b></span></p>


<p class="MsoNormal" align="center" style="text-align:center"><b><span lang="ES-MX" style="font-size:14.0pt;font-family:Arial">&nbsp;</span></b></p>


<p  style="text-align:justify"><span lang="ES-MX" style="font-family:Arial;">1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>Derechos de Propiedad. </u>Entre usted
y Time Inc., Time Inc. es dueño único y exclusivo, de todos los derechos,
título e intereses en y del Sitio Web, de todo el contenido (incluyendo, por
ejemplo, audio, fotografías, ilustraciones, gráficos, otros medios visuales,
videos, copias, textos, software, títulos, archivos de Onda de choque, etc.),
códigos, datos y materiales del mismo, el aspecto y el ambiente, el diseño y la
organización del Sitio Web y la compilación de los contenidos, códigos, datos y
los materiales en el Sitio Web, incluyendo pero no limitado a, cualesquiera
derechos de autor, derechos de marca, derechos de patente, derechos de base de
datos, derechos morales, derechos sui generis y otras propiedades intelectuales
y derechos patrimoniales del mismo. Su uso del Sitio Web no le otorga propiedad
de ninguno de los contenidos, códigos, datos o materiales a los que pueda acceder
en o a través del Sitio Web. </span></p>




	<div id="dropDownSelect1"></div>
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
	<script type="text/javascript" src="js/notify/pnotify.core.js"></script>

</body>
</html>