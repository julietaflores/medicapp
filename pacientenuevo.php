<?php 
session_start();
		 if (!isset($_SESSION['csmart']['idusuario'])) {
		 Header ("Location: login.php");
		 }
		 
		include 'headercondicion.php';
		include 'classes/cpaciente.php';
		include 'classes/cdepartamento.php';
		include 'classes/chistorial_medico.php';
		

		$idusuario=$_SESSION['csmart']['idusuario'];
		$opaciente = new paciente();
		$odepto = new departamento();
		$ohistorial = new historial_medico();
		$accion 	= "Crear";
		$option 	= "n";

		$idpaciente 	= "";
	        $nombre 	= "";
	        $apellido 	= "";
	        $identificacion 	= "";
	        $genero 	= "";
	        $imagen 	= "";
	        $direccion 	= "";
	        $telefono1 	= "";
	        $telefono2 	= "";
	        $correo 	= "";
	        $fecha_nac 	= "";
	        $fecha_mod 	= "";
	        $fecha_cre 	= "";
	        $notas 	= "";
	        $idestado 	= "";
	        $iddepartamento 	= "";
	        $responsable 	= "";
	        $telefono_resp 	= "";
	        $medico_fam 	= "";
			$ocupacion 	= "";
			$nenferemedad="";
			$nmedicamento="";
			$nfractura="";
			$fecha_ult_control="";
	    $imagen   = "user.png";
    
	        
	    if(isset($_REQUEST['opt']) && $_REQUEST['opt']== "m"){
		$option = $_REQUEST['opt'];
		$idObj	= $_REQUEST['id'];


	}

	if ($option == "m") {
		$vpaciente = $opaciente->getOne($idObj);
		if($vpaciente){
			$accion 	= "Modificar";
			
			   foreach ($vpaciente AS $id => $info){ 
			   	  
			   	  $idpaciente		= $info["idpaciente"];
		        $nombre		= $info["nombre"];
		        $apellido		= $info["apellido"];
		        $identificacion		= $info["identificacion"];
		        $genero		= $info["genero"];
		        $imagen		= $info["imagen"];
		        $direccion		= $info["direccion"];
		        $telefono1		= $info["telefono1"];
		        $telefono2		= $info["telefono2"];
		        $correo		= $info["correo"];
		        $fecha_nac		= $info["fecha_nac"];
		        $fecha_mod		= $info["fecha_mod"];
		        $fecha_cre		= $info["fecha_cre"];
		        $notas		= $info["notas"];
		        $idestado		= $info["idestado"];
		        $iddepartamento		= $info["iddepartamento"];
		        $responsable		= $info["responsable"];
		        $telefono_resp		= $info["telefono_resp"];
		        $medico_fam		= $info["medico_fam"];
		        $ocupacion		= $info["ocupacion"];

		        
		         }
				$vhistorial = $ohistorial->getOne($idObj);
				if($vhistorial){
					foreach ($vhistorial AS $id => $info){ 
						$cenfermedad		= $info["cenfermedad"];
						$nenferemedad		= $info["nenferemedad"];
						$calergia		= $info["calergia"];
						$cdesmayos		= $info["cdesmayos"];
						$cdiabetes		= $info["cdiabetes"];
						$chepatitis		= $info["chepatitis"];
						$cartritis		= $info["cartritis"];
						$cpresion		= $info["cpresion"];
						$cmedicamento		= $info["cmedicamento"];
						$nmedicamento		= $info["nmedicamento"];
						$cembarazo		= $info["cembarazo"];
						$nembarazo		= $info["nembarazo"];
						$cfuma		= $info["cfuma"];
						$ctoma		= $info["ctoma"];
						$fecha_ult_control		= $info["fecha_ult_control"];
						$cfractura		= $info["cfractura"];
						$nfractura		= $info["nfractura"];
						$cchupeteo		= $info["cchupeteo"];
						$clabio		= $info["clabio"];
						$csuccion		= $info["csuccion"];
						$cortodoncia		= $info["cortodoncia"];
					}
				}
		        
		
		}else{
			header("Location: paciente.php");
			exit();
		}
	}
  
	$fecha_nac= date("Y-m-d", strtotime($fecha_nac));
    $vDepto			= $odepto->getAll();
?>
<!DOCTYPE HTML>
<html lang="es">
<head>
 	 <?php include 'header.php'; ?>
	
		<style>
		

		.cus {
            cursor: pointer;
            margin-top: 10px;
            display: inline-block;
            padding: 3px;
            padding-right: 15px;
            padding-left: 15px;
            transition: all 1s ease;
            color: #fff;
            text-transform: capitalize;
            font-size: 15px;
            z-index: 1;
            position: relative;
            border-radius: 25px;
            background-color: #46595D;
            border: 2px solid #46595D;
         }

			
		

		
		</style>
<script type="text/javascript">	
//guardar paciente	no se edita		//////////////////////////////////////////////////////////////////////////////////////////


function guardarformulario(){
				debugger;   
       var file_data = $('#imagenS').prop('files')[0];   
		var form_data = new FormData(); 
		var fsize="";   
		var idpaciente="";              
        form_data.append('file', file_data);

			var form = $("#form").serializeJSON(); 
				$.ajax({
				url:'actions/actionpaciente.php',
				type:'POST',
				data: { valor: form }
				}).done(function( data ){
				console.log(data);
          
          if(data > 0){
            new PNotify({
                 title: 'Datos Guardados',
                text: 'Todos los datos fueron guardados. Puede continuar.',
                type: 'info'
             });
			 idpaciente=data;
			 if(file_data){ // SI EXISTE IMAGEN PARA SUBIR o ACTUALIZAR
           fsize = $('#imagenS')[0].files[0].size; //get file size
          //$('#img').val($('#imagenS').val());
		  if(fsize>1048576) 
          {
            new PNotify({
                 title: 'Error en Imagen!',
                 text: 'Excede el tamaño máximo permitido. Tamaño permitido menor a 1 Mb.'
             });
          }else{
			$(".loaderp").fadeIn("fast");
			$.ajax({
                      url: 'processUpload.php?id='+idpaciente + '&tipo=paciente', // point to server-side PHP script 
                      dataType: 'text',  // what to expect back from the PHP script, if anything
                      cache: false,
                      contentType: false,
                      processData: false,
                      data: form_data,                         
                      type: 'post',
                      success: function(val){
						  //console.log(val); // display response from the PHP script, if any
						  $(".loaderp").fadeOut("slow");
						  window.setTimeout("document.location.href='pacientes.php';",500);
                      }
              });
		  }
		}else{
			window.setTimeout("document.location.href='pacientes.php';",2500);
		}
            
            
          }else{
            new PNotify({
                 title: 'Error en datos',
                text: 'Falta información!'
             });
          }
          
        });

		
	
        
        
                                  
        
      }

			function cancelar(){
				window.setTimeout("document.location.href='pacientes.php';",500);

			}
			$(window).load(function() {
					$(".loaderp").fadeOut("slow");
				});
			
	</script>



</head>

<body class="nav-md">
<div class="loaderp" style="position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('images/cargando.webp') 50% 50% no-repeat rgb(249,249,249);
    opacity: .8;"></div>
<div  id="btn3" class="loader" style="display: none"></div>	

  <div class="container body">


    <div class="main_container">

      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          
          <!-- sidebar menu -->
            <?php include "menu.php" ?>
          <!-- /sidebar menu -->

         
        </div>
      </div>

      <!-- top navigation -->
           <?php include "top_nav.php" ?>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">

        <div class="">
          
         
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
								<div class="x_title">
                  <h2>Datos de Paciente</h2>
									<div class="clearfix"></div>
								</div>
                <div class="x_content">

                
                      
                        
                  <form id="form" class="form-horizontal form-label-left" novalidate>
                   
                    <?php
                          if ($option == "m") {
                        ?>
                            <input type="hidden" name="idpaciente" value="<?=$idpaciente?>"/>
							<input type="hidden" name="opt" id="opt" value="m" />
							
                        <?php
                          }else{
                        ?>
                            <input type="hidden" name="opt" id="opt" value="n" />
                        <?php
                          }
                        ?>

										

										<!--aqui empieza el contenido-->
										<div class="col-md-3" style="text-align:center;">  

																	<? if ($imagen =="") { ?>
																		<img src="images/user.png" alt="" class="img-circle img-responsive" style="display: inline-block;">
																	<? }else{ ?>


																		<?
																		if(file_exists("images/paciente/".$idpaciente."/".$imagen)){
																			$aqui="images/paciente/".$idpaciente."/".$imagen;
																							
																		} else {
																			$aqui="images/user.png";
																						
																				}
																						//var_dump($idusuario);?>

																						

																		<img id="imagenCambiar" src="<?=$aqui?>" alt="" class="img-circle img-responsive" style="border-radius: 50%;
																		height:150px;
																		width: 150px;
																		border: 2px solid;
																		display: inline-block;">
																<? } ?>
													<br>
													<br>
													

												<label  class="cus" for="imagenS">Seleccionar Foto</label>
                                                <input id="imagenS" type="file" multiple="multiple" name="imagenS" accept="image/*" style="visibility: hidden">

												<input name="img" id="img" type="hidden" value="<?=$imagen?>" />
										</div>
										<div class="col-md-9" > 
														<div id="step-0">
															<!--<h2 class="StepTitle">Paso 1 No </h2>-->
															
																<span class="section">Información Personal </span>

																		
																<div class="item form-group">
																	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombre">Nombre <span class="required">*</span>
																	</label>
																	<div class="col-md-6 col-sm-6 col-xs-12">
																		<input id="nombre" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="nombre" value="<?=$nombre?>"  required="required" type="text">
																	</div>
																</div>							
												
																<div class="item form-group">
																	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="apellido">Apellido <span class="required">*</span>
																	</label>
																	<div class="col-md-6 col-sm-6 col-xs-12">
																		<input id="apellido" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="apellido" value="<?=$apellido?>" required="required" type="text">
																	</div>
																</div>				
												
																<div class="item form-group">	
																	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="identificacion">Carnet de Identidad <span class="required">*</span>
																	</label>
																	<div class="col-md-6 col-sm-6 col-xs-12">
																		<input id="identificacion" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="identificacion" value="<?=$identificacion?>" required="required" type="text">
																	</div>
																</div>

																<div class="form-group">
																	<label class="control-label col-md-3 col-sm-3 col-xs-12">Género <span class="required">*</span></label>
																	<div class="col-md-6 col-sm-6 col-xs-12">

																		<div id="gender" class="btn-group" data-toggle="buttons">
																			<label class="btn btn-default <?php if($genero=='Hombre'){ echo active;} ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
																				<input type="radio" name="gender" value="Hombre" >  Hombre
																			</label>
																			<label class="btn btn-default <?php if($genero=='Mujer' or $genero==''){ echo active;} ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
																				<input type="radio" name="gender" value="Mujer" > Mujer &nbsp;
																			</label>
																		</div>

																		<input id="genero" name="genero" value="<?=$genero?>" type="hidden">
												
																	</div>
																</div>
											
																							
																<div class="item form-group">
																	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fecha_nac">Fecha de Nacimiento 
																	</label>
																	<div class="col-md-6 col-sm-6 col-xs-12">
																		<input id="fecha_nac" class="form-control col-md-7 col-xs-12"  name="fecha_nac" value="<?=$fecha_nac?>" type="date">
																		<!--<input id="fecha_nac" class="form-control col-md-7 col-xs-12"  name="fecha_nac" value="<?php echo date('Y-m-d');?>" type="date">-->
																	</div>
																</div>

															


																<div class="item form-group">
																	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="ocupacion">Ocupación 
																	</label>
																	<div class="col-md-6 col-sm-6 col-xs-12">
																		<input id="ocupacion" class="form-control col-md-7 col-xs-12" name="ocupacion" value="<?=$ocupacion?>"   type="text">
																	</div>
																</div>


															<span class="section">Datos de contacto </span>

											
												
																<div class="item form-group">
																	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono2">Celular  <span class="required">*</span>
																	</label>
																	<div class="col-md-6 col-sm-6 col-xs-12">
																		<input id="telefono2" class="form-control col-md-7 col-xs-12"  name="telefono2" value="<?=$telefono2?>"  required="required" type="number">
																	</div>
																</div>

																	
																<div class="item form-group">
																	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono1">Teléfono </label>
																	<div class="col-md-6 col-sm-6 col-xs-12">
																		<input id="telefono1" class="form-control col-md-7 col-xs-12"  name="telefono1" value="<?=$telefono1?>" type="number">
																	</div>
																</div>


																<div class="item form-group">
																	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="direccion">Dirección 
																	</label>
																	<div class="col-md-6 col-sm-6 col-xs-12">
																		<input id="direccion" class="form-control col-md-7 col-xs-12"  name="direccion" value="<?=$direccion?>" type="text">
																	</div>
																</div>

																<div class="form-group">
																	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="iddepartamento">Departamento 
																	</label>
																	<div class="col-md-6 col-sm-6 col-xs-12">
																		<select id="iddepartamento" name="iddepartamento"  class="form-control">
																			<option>Elegir una opción</option>
																			<?php if($vDepto){
																						foreach ($vDepto AS $id => $array) {?>

																						<option value="<?=$array['iddepartamento'];?>" <?php if($iddepartamento==$id){echo"selected='selected'";}?>><?=$array['nombredepto'];?></option>
																			<?php } } ?>
																		</select>
																	</div>
																</div>


											
												
																<div class="item form-group">
																	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="correo">Correo 
																	</label>
																	<div class="col-md-6 col-sm-6 col-xs-12">
																		<input id="correo" class="form-control col-md-7 col-xs-12"  name="correo" value="<?=$correo?>" type="email">
																	</div>
																</div>

																<span class="section">En caso de emergencia </span>

																	<div class="item form-group">
																	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="responsable">Persona responsable <span class="required">*</span>
																	</label>
																	<div class="col-md-6 col-sm-6 col-xs-12">
																		<input id="responsable" class="form-control col-md-7 col-xs-12"  name="responsable" value="<?=$responsable?>" required="required" type="text">
																	</div>
																</div>

											
												
																<div class="item form-group">
																	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono_resp">Teléfono del Responsable  
																	</label>
																	<div class="col-md-6 col-sm-6 col-xs-12">
																		<input id="telefono_resp" class="form-control col-md-7 col-xs-12"  name="telefono_resp" value="<?=$telefono_resp?>"  type="number">
																	</div>
																</div>

											
												
																<div class="item form-group">
																	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="medico_fam">Médico familiar 
																	</label>
																	<div class="col-md-6 col-sm-6 col-xs-12">
																		<input id="medico_fam" class="form-control col-md-7 col-xs-12"  name="medico_fam" value="<?=$medico_fam?>" type="text">
																	</div>
																</div>


																<span class="section">Datos Finales </span>

															
																
																<div class="item form-group">
																		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="notas">Notas 
																		</label>
																		<div class="col-md-6 col-sm-6 col-xs-12">
																			<textarea id="notas" name="notas" class="form-control col-md-7 col-xs-12"><?=$notas?> </textarea>
																		</div>
																</div>        


										</div>

										<input type="hidden" id="idestado" name="idestado" value="<?php $idestado ?>" />
										<input type="hidden" id="fecha_cre" name="fecha_cre" value="<?php $fecha_cre ?>" />
									
								 		</div>							
										 
										<div class="col-md-9" >  </div>
										<div class="col-md-3" > 
										
																				<input type="button" style="border-radius: 20px;" onClick="cancelar();" class="btn btn-primary" value="Cancelar">
																				<input type="submit" style="border-radius: 20px;" class="btn btn-success" value="Guardar">
												
										</div>
				        	</form>
									
								
                     
                    
                  </div>


                </div>
              </div>
            </div>
		  </div>
		  <div style="height: 100px;"></div>
		 <?php include "piep.php" ?>
		</div>

 

      </div>

    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>
  <script src="js/custom.js"></script>
  <!-- form validation -->
  <script src="js/validator/validator.js"></script>
  <!-- PNotify -->
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  <script type="text/javascript" src="js/notify/pnotify.buttons.js"></script>
  <script type="text/javascript" src="js/notify/pnotify.nonblock.js"></script>
 <!-- input mask -->
  <script src="js/input_mask/jquery.inputmask.js"></script>
<!-- form wizard -->
  <script type="text/javascript" src="js/wizard/jquery.smartWizard.js"></script>
  <script>


    $('form').submit(function(e) {
      e.preventDefault();
      var submit = true;
   // evaluate the form using generic validaing
   if (!validator.checkAll($(this))) {
        submit = false;
      }
      if (submit)
        guardarformulario();
      return false;
    });

  </script>
  <!-- pace -->
        <script src="js/pace/pace.min.js"></script>  
         <script type="text/javascript">
          $(document).ready(function() {
		      $('#wizard_verticle').smartWizard({
		        transitionEffect: 'slide'
		      });
            
          });
        </script>
        <!-- input_mask -->
  <script>
    $(document).ready(function() {
      $(":input").inputmask();
      $('#fecha_nac').change(function(){
      	fnac = $('#fecha_nac').val();
      	fecha = new Date(fnac);
		hoy = new Date();
		ed = parseInt((hoy - fecha)/365/24/60/60/1000);
		$('#edad').val(ed);
      });


	  $('#correo').change(function(){
		var email=$("#correo").val();
           if(email==""){
			new PNotify({
                     title: 'Error en el  Correo',
                    text: 'El campo Correo esta vacio!!!',
                    type: 'error'
                 });
		   }

		var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if(regex.test(email) ){
					new PNotify({
                     title: 'Correo Correcto',
                    text: 'El correo es correcto !!!',
                    type: 'info'
                 });
				}else{
					new PNotify({
                     title: 'Error en el  Correo',
                    text: 'El correo es invalido !!!',
                    type: 'error'
                 });
				}
	  });



      $('#gender').change(function(){
      	var valor = $("input:radio[name ='gender']:checked").val();
		$('#genero').val(valor);
      });
      $('#cenfermedad').change(function(){
      	var valor = $("input:radio[name ='cenfermedad']:checked").val();
		$('#cenfermedad1').val(valor);
      });
      $('#cmedicamento').change(function(){
      	var valor = $("input:radio[name ='cmedicamento']:checked").val();
		$('#cmedicamento1').val(valor);
      });
      $('#calergia').change(function(){
      	var valor = $("input:radio[name ='calergia']:checked").val();
		$('#calergia1').val(valor);
      });
      $('#cdesmayos').change(function(){
      	var valor = $("input:radio[name ='cdesmayos']:checked").val();
		$('#cdesmayos1').val(valor);
      });
      $('#cpresion').change(function(){
      	var valor = $("input:radio[name ='cpresion']:checked").val();
		$('#cpresion1').val(valor);
      });
      $('#cembarazo').change(function(){
      	var valor = $("input:radio[name ='cembarazo']:checked").val();
		$('#cembarazo1').val(valor);
      });
      $('#cfuma').change(function(){
      	var valor = $("input:radio[name ='cfuma']:checked").val();
		$('#cfuma1').val(valor);
      });
      $('#ctoma').change(function(){
      	var valor = $("input:radio[name ='ctoma']:checked").val();
		$('#ctoma1').val(valor);
      });
      $('#cdiabetes').change(function(){
      	var valor = $("input:radio[name ='cdiabetes']:checked").val();
		$('#cdiabetes1').val(valor);
      });
      $('#cartritis').change(function(){
      	var valor = $("input:radio[name ='cartritis']:checked").val();
		$('#cartritis1').val(valor);
      });
      $('#chepatitis').change(function(){
      	var valor = $("input:radio[name ='chepatitis']:checked").val();
		$('#chepatitis1').val(valor);
      });
      $('#cfractura').change(function(){
      	var valor = $("input:radio[name ='cfractura']:checked").val();
		$('#cfractura1').val(valor);
      });
      $('#cchupeteo').change(function(){
      	var valor = $("input:radio[name ='cchupeteo']:checked").val();
		$('#cchupeteo1').val(valor);
      });
      $('#clabio').change(function(){
      	var valor = $("input:radio[name ='clabio']:checked").val();
		$('#clabio1').val(valor);
      });
      $('#csuccion').change(function(){
      	var valor = $("input:radio[name ='csuccion']:checked").val();
		$('#csuccion1').val(valor);
      });
      $('#cortodoncia').change(function(){
      	var valor = $("input:radio[name ='cortodoncia']:checked").val();
		$('#cortodoncia1').val(valor);
      });
      

      $('#imagenS').change(function() {
                var filename = $(this).val();
                var lastIndex = filename.lastIndexOf("\\");
                if (lastIndex >= 0) {
                    filename = filename.substring(lastIndex + 1);
                } 
                $('#img').val(filename);
            });
    });
	$('#imagenS').change(function(e) {
	debugger;
      addImage(e); 
     });
function addImage(e){
	
      var file = e.target.files[0],
      imageType = /image.*/;
    
      if (!file.type.match(imageType))
       return;
  
      var reader = new FileReader();
      reader.onload = fileOnload;
      reader.readAsDataURL(file);
     }
  
     function fileOnload(e) {
      var result=e.target.result;
      $('#imagenCambiar').attr("src",result);
     }
  </script>
</body>

</html>

