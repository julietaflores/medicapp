<title>Aula Virtual - Embol</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link href="assets/css/estilo.css" rel="stylesheet" />
 <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
   <script src="controller.js"></script>
<link rel="apple-touch-icon" sizes="57x57" href="assets/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="assets/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="assets/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    
    <link href="assets/css/animate.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="assets/css/perfect-scrollbar.min.css">

    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

    <script src="assets/js/plugins/jquery-1.10.2.js" type="text/javascript"></script>
    
	<script src="assets/js/plugins/bootstrap.min.js" type="text/javascript"></script>
	<!--  Notifications Plugin    -->
    <script src="assets/js/plugins/bootstrap-notify.js"></script>
    <script src="assets/js/notifications.js"></script>

    <script type="text/javascript">				
    
			function enviarClave(){
				var user = $("#user").val();
               if(user==""){
                 $.notify({
                    icon: 'pe-7s-bell',
        			message: "Necesita llenar el campo correo"
        		},{
        			type: 'danger',
                    timer: 2000
         		});
               }else{
                $.ajax({
                   type: "GET",
                    contentType: "application/json; charset=utf-8",
                    url:"ws/api_rest.php/recuperar_correo/"+user,
                    dataType: "json",
                    data: { },
                    success: function (data){
                     // alert(data);
                      if(data==1){
                        $.notify({
                       icon: 'pe-7s-bell',
        		    	message: "Correo enviado correctamente"
        	    	},{
        		    	type: 'info',
                       timer: 2000
         		   });
                    setTimeout( "window.location.href='ingreso.php'", 2000 );

                      }else{
                     
                        $.notify({
                    icon: 'pe-7s-bell',
        			message: "El Email no existe en la Base de datos"
        		},{
        			type: 'danger',
                    timer: 2000
         		});


              
                      }
                       

                     },
                    error: function (data){
                    
                        $.notify({
                    icon: 'pe-7s-bell',
        			message: "Nose pudo enviar el correo"
        		},{
        			type: 'danger',
                    timer: 2000
         		});
                     }
                 });

               }
			}		
    </script>  
    


<body ng-app="mv">
<!-- Navbar (sit on top) -->
<div class="w3-top">
  <div class="w3-bar w3-white w3-wide w3-padding w3-card-2">
    <a href="index.php" class="w3-bar-item w3-button"><img src="images/logoEmbol-2.png" alt="Embol" style="width: 150px;"></a>
    <div class="w3-right w3-hide-small">
      <a href="index.php" class="w3-bar-item w3-button" style="letter-spacing: 0px;border-radius: 10px;">Inicio</a>
      <a href="registro.php" class="w3-bar-item w3-button" style="letter-spacing: 0px;border-radius: 10px;">Registrarse</a>
    </div>
  </div>
</div>


<div class="w3-content w3-padding" style="max-width:1564px;     margin-top: 80px;">
    <div class="w3-row-padding" style="text-align: center">

<div class="logoppl">
<img src="images/logoEmbol.png" alt="Embol"   style="height: 25%;"  >
</div>
        <div class="w3-col l12 m12">
            <h3 class="w3-border-bottom w3-border-light-grey w3-padding-16" style="color: black;font-weight: bold;">
            Recuperar clave </h3>
            <p style="font-size: 19px;padding: 15px;color: black;">
            Ingrese el correo electrónico con el que se registró:
            </p>
             <Br>
            <input type="email" id="user" name="user" placeholder="Email de EMBOL" style="width: 400px;padding: 5px;border-radius: 5px;">
             <br><br>
            <input type="button" class="btnLog" value="Enviar clave" onclick="enviarClave();" /> 
            <br><br>
        </div>  
    </div>
</div>



<div class="w3-container w3-padding-32" id="projects">
    <h3 class="w3-border-bottom w3-border-light-grey w3-padding-16"></h3>
  </div>
<!-- Page content -->
<div class="w3-content w3-padding" style="margin-top: 10px;">
   
   
</div>


<footer class="w3-center w3-light-grey w3-padding-16">
    <div style="float:left;margin-left: 40px;position: absolute; font-size: 13px; color: #607d8b;text-align: left;">Acerca de <br> Soporte</div>
  <p>Derechos Reservados - Embol S.A. </p>
</footer>


</body>
</html>
