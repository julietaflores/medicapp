<?php 
	 session_start();
   if (!isset($_SESSION['csmart']['idusuario'])) {
   Header ("Location: login.php");
   }

   include 'headercondicion.php';
  
    include 'classes/cpaciente.php';
    
    include 'classes/ccita.php';
    include 'classes/ctipocita.php';
    include 'classes/ctipoLesion.php';
    include 'classes/cpacienteNotas.php';
    include 'classes/clesiones.php';
    include 'classes/cpagos.php';
    include 'classes/cpagocredito.php';
    include 'classes/cmembresia.php'; 
   
    $omembresia = new Membresia();
   
 
    $oCita  = new cita();
    $oTipocita  = new tipocita();
    $otipo = new tipoLesion();
    $oNotas = new pacienteNotas();
    $oLesiones = new lesiones();
    $opagos = new pago();
    $opagocredito=new pagocredito();
    $opaciente = new paciente();
    $accion   = "Crear";
    $option   = "n";
    $idpaciente   = "";
          $nombre   = "";
          $apellido   = "";
          $identificacion   = "";
          $genero   = "";
          $imagen   = "";
          $direccion  = "";
          $telefono1  = "";
          $telefono2  = "";
          $correo   = "";
          $fecha_nac  = "";
          $fecha_mod  = "";
          $fecha_cre  = "";
          $notas  = "";
          $idestado   = "";
          $iddepartamento   = "";
          $responsable  = "";
          $telefono_resp  = "";
          $medico_fam   = "";
          $ocupacion  = "";
          $idtipocita="";
  if(isset($_REQUEST['id'])){
    $idObj  = $_REQUEST['id'];

    $vpaciente = $opaciente->getOne($idObj);
    if($vpaciente){
      $accion   = "Modificar";
      
         foreach ($vpaciente AS $id => $info){ 
            
            $idpaciente   = $info["idpaciente"];
            $id   = $info["idpaciente"];
            $nombre   = $info["nombre"];
            $apellido   = $info["apellido"];
            $identificacion   = $info["identificacion"];
            $genero   = $info["genero"];
            $imagen   = $info["imagen"];
            $direccion    = $info["direccion"];
            $telefono1    = $info["telefono1"];
            $telefono2    = $info["telefono2"];
            $correo   = $info["correo"];
            $fecha_nac    = $info["fecha_nac"];
            $fecha_mod    = $info["fecha_mod"];
            $fecha_cre    = $info["fecha_cre"];
            $notas    = $info["notas"];
            $idestado   = $info["idestado"];
            $iddepartamento   = $info["iddepartamento"];
            $responsable    = $info["responsable"];
            $telefono_resp    = $info["telefono_resp"];
            $medico_fam   = $info["medico_fam"];
            $ocupacion    = $info["ocupacion"];
             }
       $vcita = $oCita->getAllpaciente($idObj);
    }else{
      header("Location: pacientes.php");
      exit();
    }
  }else{
    header("Location: pacientes.php");
    exit();
  }

  $vtipo    = $otipo->getAll();
  $vNotas    = $oNotas->getAll($idpaciente );
  $vLesiones    = $oLesiones->getAll($idpaciente);
  $vGaleria  = $opaciente->getGaleria($idpaciente);
  $vpagos=$opagos->getAll($idpaciente);
  $vpagostotal=$opagos->getAlltotal($idpaciente);
  $vPagocredito=$opagocredito->getAll($idpaciente);
  $vienedecita=isset($_REQUEST['cita']) ? $_REQUEST['cita']: 0;
  //$vcitas    =$oCita->getAll($_SESSION['csmart']['clinica']);
  $vtcita    =$oTipocita->getAll();
  $vUsuarios =$oUser->getAllxClinica($_SESSION['csmart']['clinica']);
  
 // var_dump($vtcita);
 //var_dump($vpagostotal) ;
 
 
?>
<!DOCTYPE html>
<html ng-app="clinicapp">

<head>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">

  <?php include 'header.php'; ?>
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  <script type="text/javascript"> 
      $(window).on('popstate', function(event) {
 alert("no regreses...");
  return false;
});
      function subir(id){
        debugger;
        var file_data3 = $('#imagenS3').prop('files')[0];   
        var form_data3 = new FormData();                  
        form_data3.append('file', file_data3);
    
        if(file_data3){ // SI EXISTE IMAGEN PARA SUBIR o ACTUALIZAR
              var fsize3 = $('#imagenS3')[0].files[0].size; //get file size
              if(fsize3>548576 ) 
              {
                valido = false;
                new PNotify({
                     title: 'Error en Imagen 3!',
                     text: 'Excede el tamaño máximo permitido. Tamaño permitido menor a 500 Kb.'
                 });
              }else{
                var nombre = $('#imagenS3').prop('files')[0].name;

$.ajax({
  url:'actions/actionpaciente.php?opt=subir&id='+id + '&nombre=' + nombre,
  type:'POST',
  data: {  }
}).done(function( data ){
   
  if(data > 0){

    $.ajax({
                  url: 'processUpload.php?id='+id + '&tipo=paciente', // point to server-side PHP script 
                  dataType: 'text',  // what to expect back from the PHP script, if anything
                  cache: false,
                  contentType: false,
                  processData: false,
                  data: form_data3,                         
                  type: 'post',
                  success: function(val){

                    var codigo = $('#newGal').html();
                    $('#newGal').html(codigo +
                     '<div class="col-md-55" id="'+data+
                     '"><div class="thumbnail"><div class="image view-first fotoGal"><img style="width: 100%;height:90px; display: block;" data-toggle="modal" data-target="#myModalFoto" onclick="onClick(this)" src="images/paciente/'+id+'/'+nombre+'" alt="image" /><div class="mask"><p style="color:white">clic para ampliar</p></div></div><div class="caption"><p><a href="" onClick="eliminarFoto('+data+','+id+',\''+nombre+'\')"> <i class="fa fa-times"></i></a> '+nombre+
                     '</p></div></div></div>');
                    
                     new PNotify({
                         title: 'Datos Guardados',
                         text: 'Todos los datos fueron guardados. Puede continuar.',
                         type: 'success'
                      });

                    // var codigo = $('#newGal').html();
                    // $('#newGal').html(codigo + '<div class="col-md-55" id="'+data+'"><div class="thumbnail"><div class="image view-first fotoGal"><img style="width: 100%; display: block;" onclick="onClick(this)" src="images/paciente/'+id+'/'+nombre+'" alt="image" /></div><div class="caption"><p><a href="" onClick="eliminarFoto()"> <i class="fa fa-times"></i></a> '+nombre+'</p></div></div></div>');
                      //alert(val); // display response from the PHP script, if any
                  }
            });
           /* <img style="width: 100%; display: block;height:100px;" onclick="onClick(this)" data-toggle="modal" data-target="#myModalFoto" src="images/paciente/<?=$array["idpaciente"];?>/<?=$array["nombre"];?>" alt="image" />
                                      <div class="mask"><p style="color:white">clic para ampliar</p></div>*/
                                    
    
    //window.setTimeout("document.location.href='pacienteperfil.php?id="+id+"';",2500);
  }
  
                            else{
                              new PNotify({
                                        title: 'Error en formulario',
                                        text: 'No se puedieron guardar los datos, intente de nuevo.',
                                        type: 'error'
                                    });
                                    window.setTimeout("location.reload(true);",2500);
                            }
                            
                          });
              }
            }
        
        //var form = $("#form").serializeJSON(); 
       //var nombre = $("#img3").val();
        
      }
      
      function eliminarFoto(id,px,archivo) {
        
          if (confirm("Atencion! Va a proceder eliminar este registro. Desea continuar?")) {
            var form = "valor"; 
            $.ajax({
              url: 'actions/actionpaciente.php?opt=efoto&id='+id+'&px='+px+'&archivo='+archivo,
              type:'POST',
              data: { }
            }).done(function( data ){
              
              if(data == 0){
                $("#gal" + id).html(''); 

                new PNotify({
                           title: 'Datos Eliminados',
                            text: 'Todos los datos fueron guardados. Puede continuar.!',
                            type: 'success'
                          });

              //window.setTimeout("document.location.href='paciente.php';",2500);
              }
              else if(data == 1){
                msg = "Error en idpaciente.";
                showWarning(msg,5000);
              }
              else{
                new PNotify({
                             title: 'Error en formulario',
                            text: 'No se puedieron guardar los datos, intente de nuevo.',
                            type: 'error'
                         });
               // window.setTimeout("location.reload(true);",2500);
              }
              
            });
          }
        
      }

      
  </script>

</head>

 

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          

         <!-- sidebar menu -->
            <?php include "menu.php" ?>
          <!-- /sidebar menu -->

          
        </div>
      </div>

      <!-- top navigation -->
           <?php include "top_nav.php" ?>
      <!-- /top navigation -->


      <!-- page content -->
      <div class="right_col" role="main">

        <div class="">
          <div class="page-title">
            <!-- <div class="title_left">
              <h3>Perfil de Paciente</h3>
            </div> -->

            
          </div>
          <div class="clearfix"></div>

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <div class="row">
                      <div class="col-md-6">
                        <h2>Perfil de Paciente <?=$nombre.' '.$apellido;?></h2></div>
                      <div class="col-md-6" style="text-align: right">
                        <a href="" data-toggle="modal" data-target="#CalenderModalNew" class="btn btn-default">Crear Cita</a>
                      </div>
                  </div>
                  
                  <input id="userID" type="hidden" value="<?=$_SESSION['csmart']['idusuario']?>">
                  <input id="nombreusuario" type="hidden" value="<?=$_SESSION['usuario']?>">
                                      
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <div class="col-md-3 col-sm-3 col-xs-12 profile_left">

                    <div class="profile_img">
                        <div class="avatar-view" title="Foto de paciente">
                        <?if($imagen==''){?>
                          <img id="fotoPerfil" src="images/<?=$imagen;?>" alt="Avatar">
                                         
														<?}else{
                              if(file_exists("images/paciente/".$idpaciente."/".$imagen)){?>
                                <img id="fotoPerfil" src="images/paciente/<?=$idpaciente;?>/<?=$imagen;?>" alt="Avatar">
                                      
                             <? }else{?>
                               <img id="fotoPerfil" src="images/user.png" alt="Avatar">
                             <?}
                              ?>
                          	<?}?>
                        
                          <input type="hidden" id="idpaciente" value="<?=$idpaciente;?>" />
                        </div>
                     

                    </div>
                    <h3><?=$nombre.' '.$apellido;?></h3>

                    <ul class="list-unstyled user_data">
                      <li><i class="fa fa-map-marker user-profile-icon"></i> <?=$direccion;?>
                      </li>

                      <li>
                        <i class="fa fa-briefcase user-profile-icon"></i> <?=$ocupacion?>
                      </li>
                       <li>
                        <i class="fa fa-phone user-profile-icon"></i> <?=$telefono1?>
                      </li>
                      <li class="m-top-xs">
                        <i class="fa fa-envelope user-profile-icon"></i>
                        <a href="#" target="_blank"><?=$correo;?></a>
                      </li>
                    </ul>

                    <a href="pacientenuevo.php?opt=m&id=<?=$id?>" class="btn btn-success"><i class="fa fa-edit m-right-xs"></i>Editar Paciente</a>
                    <br />

                    

                  </div>
                  <div class="col-md-9 col-sm-9 col-xs-12">

                    <!-- <div class="profile_title">
                      <div class="col-md-6">
                        <h2>User Activity Report</h2>
                      </div>
                      <div class="col-md-6">
                        <div id="reportrange" class="pull-right" style="margin-top: 5px; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #E6E9ED">
                          <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                          <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
                        </div>
                      </div>
                    </div> -->
                    <!-- start of user-activity-graph -->
                    <!-- <div id="graph_bar" style="width:100%; height:280px;"></div> -->
                    <!-- end of user-activity-graph -->

                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <!-- <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Citas Recientes</a>
                        </li> -->
                        <li role="presentation" class="active"><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="true">Historial Citas</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Notas</a>
                        </li>
                         <li role="presentation" class=""><a href="#tab_content4" onClick="javascript:ocultardiente()"  role="tab" id="profile-tab4" data-toggle="tab" aria-expanded="false">Odontograma</a>
                        </li> 
                        <!--<li role="presentation" class=""><a href="#tab_content5" onClick="javascript:ocultardienten()"  role="tab" id="profile-tab5" data-toggle="tab" aria-expanded="false">Odontograma Niño</a>
                        </li>-->
                        <li role="presentation" class=""><a href="#tab_content6" role="tab" id="profile-tab6" data-toggle="tab" aria-expanded="false">Galería</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content7" role="tab" id="profile-tab7" data-toggle="tab" aria-expanded="false">Pagos</a>
                        </li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
                      
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content2" aria-labelledby="profile-tab">

                          <!-- start user projects -->
                          <table class="data table table-striped no-margin">

                            <thead>
                              <tr>
                              <input type="hidden" id="citaprueba" value="<?=$vienedecita?>">
                                <th>#</th>
                                <th>Tipo</th>
                                <th>Detalle</th>
                                <th>Fecha</th>
                              </tr>
                            </thead>
                            <tbody>

                              <?php if($vcita){
                                
                                $i=1;
                                foreach ($vcita AS $id => $array) {?>
                                              
                                  <tr>
                                    
                                    <td><?=$i;?></td>
                                    <td><?=$array['tipo'];?></td>
                                    <td><?=$array['comentario'];?></td>
                                    <td><?php $date = date_create($array['start']); echo date_format($date, 'd/m/Y H:i'); ?></td>
                                    <td align="left">
                                      <?php if($array['idestado']==0){ ?> <span class="label label-danger cita" data-toggle="modal" data-target="#ModalCita" data-id="<?=$array['id']?>" data-ms="<?=$array['maxSup']?>" data-mi="<?=$array['maxInf']?>" data-obs="<?=$array['observaciones']?>" data-valor="<?=$array['valor']?>" data-fact="<?=$array['factura']?>">Cancelada</span> 
                                       <?php }else if($array['idestado']==1){ ?>
                                                  <span class="label label-warning cita" id="cita<?=$array['id']?>" data-toggle="modal" data-target="#ModalCita" data-id="<?=$array['id']?>"  data-ms="<?=$array['maxSup']?>" data-mi="<?=$array['maxInf']?>" data-obs="<?=$array['observaciones']?>" data-valor="<?=$array['valor']?>"  data-fact="<?=$array['factura']?>">Programada</span>
                                       <?php }else { ;?>
                                                  <a class="label label-success cita " data-toggle="modal" data-target="#ModalCita" data-id="<?=$array['id']?>" data-ms="<?=$array['maxSup']?>" data-mi="<?=$array['maxInf']?>" data-obs="<?=$array['observaciones']?>" data-valor="<?=$array['valor']?>"  data-fact="<?=$array['factura']?>">Finalizada</a>
                                        <?php } ;?></td>

                                  </tr>
                                
                                
                            <?php $i++; } } ?>
                              <!-- <tr>
                                <td>1</td>
                                <td>New Company Takeover Review</td>
                                <td>Deveint Inc</td>
                                <td class="hidden-phone">18</td>
                                <td class="vertical-align-mid">
                                  <div class="progress">
                                    <div class="progress-bar progress-bar-success" data-transitiongoal="35"></div>
                                  </div>
                                </td>
                              </tr> -->
                              
                            </tbody>
                          </table>
                          <!-- end user projects -->

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                          <p>Registro de la evolución:</p>
                           
                          <div class="x_content">
                            <div class="dashboard-widget-content">
                              <input id="nota" name="nota" type="text" style="width:50%;" /> 
                              <button type="button"  class="btn btn-success btn-xs" onclick="guardarnota()"> Agregar <i class="fa fa-chevron-right"></i> </button>
                                  <ul class="list-unstyled timeline widget">
                                     
                                    <div id="nnotas">

                                    </div>
                                    <?php if($vNotas){
                                        foreach ($vNotas AS $id => $array) {?>
                                          
                                        <li>
                                          <div class="block">
                                            <div class="block_content">
                                              
                                              <p class="excerpt">  <?=$array['detalle'];?>
                                              </p>
                                              <div class="byline">
                                                <span><?$source = $array['fecha'];
                                                $date = new DateTime($source);
                                                echo $date->format('d-m-Y');
                                                ?></span> por <?=$array['idusuario'];?>
                                              </div>
                                            </div>
                                          </div>
                                        </li>
                                    
                                    <?php } } ?>
                                  </ul>
                          </div>
                        </div>
                          

                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                        <div class="odonto">
                          

                            
                          <?php include 'dentaduraN.php'; ?>

            </div>

            <br>
            <br> 

            
                          <div class="odonto">
                          
<!--
                            <div class="diente">
                              <a href="#" class="enlace"  data-toggle="modal" data-target="#myModal" data-id="18" >
                                <img src="images/18.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="17" >
                                <img src="images/17.png" />
                              </a>
                            </div>


                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="16" >
                                <img src="images/16.png" />
                              </a>
                            </div>


                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="15" >
                                <img src="images/15.png" />
                              </a>
                            </div>


                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="14" >
                                <img src="images/14.png" />
                              </a>
                            </div>
                            
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="13" >
                                <img src="images/13.png" />
                              </a>
                            </div>


                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="12" >
                                <img src="images/12.png" />
                              </a>
                            </div>


                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="11" >
                                <img src="images/11.png" />
                              </a>
                            </div>


                            

                          
                            
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="21" >
                                <img src="images/21.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="22" >
                                <img src="images/22.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="23" >
                                <img src="images/23.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="24" >
                                <img src="images/24.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="25" >
                                <img src="images/25.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="26" >
                                <img src="images/26.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="27" >
                                <img src="images/27.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="28" >
                                <img src="images/28.png" />
                              </a>
                            </div>

                            <bR><br><bR><br><bR><br><bR><br><bR><br><bR><br>

                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="48" >
                                <img src="images/48.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="47" >
                                <img src="images/47.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="46" >
                                <img src="images/46.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="45" >
                                <img src="images/45.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="44" >
                                <img src="images/44.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="43" >
                                <img src="images/43.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="42" >
                                <img src="images/42.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="41" >
                                <img src="images/41.png" />
                              </a>
                            </div>
                            

                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="31" >
                                <img src="images/31.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="32" >
                                <img src="images/32.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="33" >
                                <img src="images/33.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="34" >
                                <img src="images/34.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="35" >
                                <img src="images/35.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="36" >
                                <img src="images/36.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="37" >
                                <img src="images/37.png" />
                              </a>
                            </div>
                            <div class="diente">
                              <a href="#" class="enlace" data-toggle="modal" data-target="#myModal" data-id="38" >
                                <img src="images/38.png" />
                              </a>
                            </div>
                                        -->
                                        <?php include 'dentadura.php'; ?>

                          </div>

                          <br>
                          <br> 

                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                              
                              <div class="x_content">
                                <ul class="list-unstyled timeline">
                                  
                                  <div id="nlesiones">

                                    </div>
                                    <?php if($vLesiones){
                                        foreach ($vLesiones AS $id => $array) {?>

                                        
                                        <li>
                                          <div class="block">
                                            <div class="tags">
                                              <a href="" class="tag">
                                                <span><?=$array['idtipoLesion'];?></span>
                                              </a>
                                            </div>
                                            <div class="block_content">
                                              <h2 class="title">
                                                              <a>Pieza <?=$array['pieza'];?></a>
                                                          </h2>
                                              <div class="byline">
                                                <span><?=$array['fecha'];?> por <?=$array['nombre'];?></span> 
                                              </div>
                                              <p class="excerpt"><?=$array['nota'];?>
                                              </p>
                                            </div>
                                          </div>
                                        </li>
                                    
                                    <?php } } ?>

                                 
                                  <!-- <li>
                                    <div class="block">
                                      <div class="tags">
                                        <a href="" class="tag">
                                          <span>Ausencia</span>
                                        </a>
                                      </div>
                                      <div class="block_content">
                                        <h2 class="title">
                                                        <a>Pieza 18</a>
                                                    </h2>
                                        <div class="byline">
                                          <span>13/04/2016 por Dr. Medina</span> 
                                        </div>
                                        <p class="excerpt">Observaciones realizadas a la pieza
                                        </p>
                                      </div>
                                    </div>
                                  </li> -->
                                </ul>

                              </div>
                            </div>
                          </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="profile-tab">
                          
                          <div class="odonto">
                          

                            
                                        <?php include 'dentaduraN.php'; ?>

                          </div>

                          <br>
                          <br> 

                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                      
                              <div class="x_content">
                                <ul class="list-unstyled timeline">
                                  
                                  <div id="nlesiones">

                                    </div>
                                    <?php if($vLesiones){
                                        foreach ($vLesiones AS $id => $array) {?>
                                        
                                        <li>
                                          <div class="block">
                                            <div class="tags">
                                              <a href="" class="tag">
                                                <span><?=$array['idtipoLesion'];?></span>
                                              </a>
                                            </div>
                                            <div class="block_content">
                                              <h2 class="title">
                                                              <a>Pieza <?=$array['pieza'];?></a>
                                                          </h2>
                                              <div class="byline">
                                                <span><?=$array['fecha'];?> por <?=$array['nombre'];?></span> 
                                              </div>
                                              <p class="excerpt"><?=$array['nota'];?>
                                              </p>
                                            </div>
                                          </div>
                                        </li>
                                    
                                    <?php } } ?>

                                 
                                  
                                </ul>

                              </div>
                            </div>
                          </div>
                        </div>
    <!--pagos------------------------------------------------------>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content7" aria-labelledby="profile-tab">
                             <div class="container">
                                          <a id="botonpago" class="btn btn-success" href="javascript:verpago(2)">Pago Completo</a>
                                          <a id="botoncredito" class="btn btn-default" href="javascript:verpago(1)" >Pago Credito</a>
                                          <?include 'pagos.php'?>
                             </div>             
                            
                        </div>
    <!-----------------                                          -->
                        <div role="tabpanel" class="tab-pane fade" id="tab_content6" aria-labelledby="profile-tab">
                          <div class="row">
                            <p>Galería de imagenes del paciente.</p>

                            <br>
                            
                             
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="imagenS3"> Subir imagen:
                                </label>

                                <div class="col-md-3"> 
                                  
                                  <input name="imagenS3" class="fileupload " id="imagenS3" type="file" />
                                  <input name="img3" id="img3" type="hidden" value="<?=$imagen3?>" />
                                 
                                </div><br>

                                <div class="col-md-3"> 
                                   <a href="" onClick="subir(<?=$idpaciente?>)" class="btn btn-success btn-xs"><i class="fa fa-upload m-right-xs"></i> Guardar</a>
                                </div>
                            </div>

                            <br><br><br><br>
                            
                              
                                <?php if($vGaleria){
                                        foreach ($vGaleria AS $id => $array) {?>

                                          <div class="col-md-55" id="gal<?=$array["idimagen"];?>">
                                          <!--<a id="ampliara" data-toggle="modal" data-target="#myModalFoto">ampliar</a>-->
                                            <div  class="thumbnail">  
                                            <div class="image view-first fotoGal">
                                              <img style="width: 100%; display: block;height:90px;" onclick="onClick(this)" data-toggle="modal" data-target="#myModalFoto" src="images/paciente/<?=$array["idpaciente"];?>/<?=$array["nombre"];?>" alt="image" />
                                              <div class="mask">
                                                <p style="color:white">clic para ampliar</p>
                                                
                                               
                                              </div>
                                            </div>
                                            <div class="caption">
                                              <p><a href="" onClick="eliminarFoto(<?=$array["idimagen"];?>,<?=$array["idpaciente"];?>,'<?=$array["nombre"];?>')"> <i class="fa fa-times"></i></a> <?=$array["nombre"];?></p>
                                            </div>
                                            </div>
                                            </div>
                                        <? } 
                                      }?>

                                  <div id="newGal">

                                  </div>
                             
                            

                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
                
          </div>
          
        </div>
        

          <!-- Modal Lesion -->
          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <!-- <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Agregar registro</h4>
                </div>
                 --><div class="modal-body">
                  <label for="">Pieza<input type="text" name="bookId" id="bookId" value=""/></label>
                  <!-- <div class="well">
                    <label for="">Procedimento</label>
                  </div> -->
                <!--  <div class="form-group">
                        <label class="col-sm-3 control-label">Tipo de lesión </label>
                        <div class="col-sm-9">
                          <select id="idtipoLesion" name="idtipoLesion" class="form-control">
                            <option>Elegir una opción</option>
                            <?php if($vtipo){
                                  foreach ($vtipo AS $id => $array) {?>

                                  <option value="<?=$array['idtipoLesion'];?>" <?php if($array['idtipoLesion']==$id){echo"selected='selected'";}?>><?=$array['nombre'];?></option>
                            <?php } } ?>
                          </select>
                        </div>
                      </div>
                  <textarea class="form-control" cols="20" id="nota" rows="6"></textarea>-->
                  
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default closemodal" data-dismiss="modal">Cancelar</button>
                  <button type="button" class="btn btn-success guardaLesion">Guardar</button>
                </div>
              </div>
            </div>
          </div>

        <!-- Modal Foto -->
        <div class="modal fade" id="myModalFoto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">

                <label for="">Foto Galeria</label>

                <img id="img01" style="width: 100%; display: block;" alt="image" />
                                              
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default closemodal" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>

        <!-- Modal Cita -->
        <div class="modal fade" id="ModalCita" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"> Control de cita</h4>
              </div>
              <div class="modal-body">
                
                <div class="item form-group">
                     
                Maxilar Superior
                <textarea class="form-control" cols="20" id="maxSup" rows="3"></textarea>
                Maxilar Inferior
                <textarea class="form-control" cols="20" id="maxInf" rows="3"></textarea>
                Observaciones
                <textarea class="form-control" cols="20" id="observaciones" rows="2"></textarea>
                
                <br>
                <!--ocultar factura-->
                <div style="display: none">
                <label class="col-sm-3 control-label">Costo de Consulta </label>
                      <div class="col-sm-1">
                        <input type="text" name="valor" id="valor" value="0"/>
                        <input type="hidden" name="idcita" id="idcita" value="0"/>
                      </div>

                <label class="col-sm-3 control-label"> </label>
                      <div id="facturaDiv" class="col-md-3 col-sm-3 col-xs-12">
                        <div class="checkbox"  >
                            <label>
                              <input id="factura" name="factura" type="checkbox" class=""> Generar Factura
                              <input id="idfactura" name="idfactura" type="hidden" value="0"  > 
                            </label>
                        </div>
                      </div> 
                </div>
                <!---->
                
                </div>
              </div>
              <br>
              <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-danger ">Cancelar</button>
                <button type="button" data-dismiss="modal" class="btn btn-success guardarResultado">Guardar</button>
              </div>
            </div>
          </div>
        </div>
<!--------------------------------------------------------------------------modal citas--->
<div id="CalenderModalNew" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title" id="myModalLabel">Nueva cita</h4>
            </div>
            <div class="modal-body">
              <div id="testmodal" style="padding: 5px 20px;">
                <form id="antoform" class="form-horizontal calender" role="form">
                <div class="form-group">
                      <label class="col-sm-3 control-label">Tipo de cita </label>
                      <div class="col-sm-9">
                        <select id="idtipocita" name="idtipocita" class="form-control">
                        <?php if($vtcita){
                        
                                foreach ($vtcita AS $id => $array) {
                                  if($array['idtipocita']!=4){?>
                                    <option value="<?=$array['idtipocita'];?>" <?php if($idtipocita==$id){echo"selected='selected'";}?>><?=$array['tipo'];?></option>
                         
                                  <?}?>
                                  
                          <?php } } ?>
                        </select>
                      </div>
                    </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Titulo </label>
                    <div class="col-sm-9">
                      
                        <input type="text" class="form-control"  id="pacientec" name="pacientec" /> 
                      
                    </div>
                   </div>
                  <div class="form-group" id="camposinpaciente" style="display: none">
                    <label class="col-sm-3 control-label">Paciente</label>
                    <div class="col-sm-9" >
                    
                   
                          <?php if($vpaciente){
                                foreach ($vpaciente AS $id => $array) {?>
                                <input type="text" id="paci" value="<?=$array['idpaciente'];?>">
                                
                          <input type="hidden" id="idpacientec" name="idpacientec" value="<?=$array['idpaciente'];?>"/>
                          <?php } } ?>
                  
                   
                    
                    </div>
                     
                  </div>
                  
                  <div class="form-group" style="display: none">
                      <label class="col-sm-3 control-label">Usuario </label>
                      <div class="col-sm-9">
                        
                        <input type="hidden" id="idusuario" value="<?=$_SESSION['csmart']['idusuario']?>">
                      </div>
                    </div>

                 
                <div class="form-group" >   
                <label class="col-sm-3 control-label">Color </label> 
                <div class="col-sm-9">
                      <input id="rojoc" style="display:none ;" checked="checked" name="coloresc" type="radio" value="FF0000">
                      <label class="labelcolo" for="rojoc" style="background: red; width: 20px;height: 20px;border-radius:15px; border:3px solid;"></label>
                      <input id="verdec" style="display:none ;" name="coloresc" type="radio" value="228B22">
                      <label class="labelcolo" for="verdec" style="background: green; width: 20px;height: 20px;border-radius:15px;"></label>
                      <input id="azulc" style="display:none ;" name="coloresc" type="radio" value="0000FF">
                      <label class="labelcolo" for="azulc" style="background: blue; width: 20px;height: 20px;border-radius:15px;"></label>
                      <input id="amarilloc" style="display:none ;" name="coloresc" type="radio" value="ffdf00">
                      <label class="labelcolo" for="amarilloc" style="background: #ffdf00 ; width: 20px;height: 20px;border-radius:15px;"></label>
                      <input  id="cieloc" style="display:none ;" name="coloresc" type="radio" value="CCEEFF">
                      <label class="labelcolo" for="cieloc" style="background: #CCEEFF ; width: 20px;height: 20px;border-radius:15px;"></label>
                </div>            
                     
                </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Fecha / Hora</label>
                    <div class="col-sm-4">
                      <input type="date" class="form-control" value="<?php echo date('Y-m-d');?>" id="fechacita" name="fechacita">
                     </div>
                    <div class="col-sm-3">
                      <select id="horac" name="horac" class="form-control">
                          <option value="08:00" >8:00</option>
                          <option value="08:30" >8:30</option>
                          <option value="09:00" >9:00</option>
                          <option value="09:30" >9:30</option>
                          <option value="10:00" >10:00</option>
                          <option value="10:30" >10:30</option>
                          <option value="11:00" >11:00</option>
                          <option value="11:30" >11:30</option>
                          <option value="12:00" >12:00</option>
                          <option value="12:30" >12:30</option>
                          <option value="13:00" >13:00</option>
                          <option value="13:30" >13:30</option>
                          <option value="14:00" >14:00</option>
                          <option value="14:30" >14:30</option>
                          <option value="15:00" >15:00</option>
                          <option value="15:30" >15:30</option>
                          <option value="16:00" >16:00</option>
                          <option value="16:30" >16:30</option>
                          <option value="17:00" >17:00</option>
                          <option value="17:30" >17:30</option>
                          
                         </select>
                    </div>

                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Nota</label>
                    <div class="col-sm-9">
                      <textarea class="form-control" style="height:55px;" id="descrc" name="descrc"></textarea>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-primary antosubmit">Guardar</button>
            </div>
          </div>
        </div>
      </div>
<!----------------------------------------->
        <script type="text/javascript">
                    
          $(document).ready(function() {
            
          
              if($("#citaprueba").val()>0){
                var citaprueba=$("#citaprueba").val();
                $("#cita"+citaprueba).click();
              }
              $('#factura').change(function(){
                  if ($('#factura').is(":checked")){
                    $('#idfactura').val('1');
                  }else{
                    $('#idfactura').val('0');
                    
                  }
                  console.log('3');
              });
            
                      
            });
       </script>

        <!-- footer content -->
        <center> <?php include "piep.php" ?> </center>
        <!-- /footer content -->


      </div>
      <!-- /page content -->
    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="js/bootstrap.min.js"></script>
  
  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>

  <script src="js/custom.js"></script>

  <!-- image cropping -->
  <script src="js/cropping/cropper.min.js"></script>
  <script src="js/cropping/main.js"></script>

  <!-- daterangepicker -->
  <script type="text/javascript" src="js/moment/moment.min.js"></script>
  <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>

  <!-- chart js -->
  <script src="js/chartjs/chart.min.js"></script>

  <!-- moris js -->
  <script src="js/moris/raphael-min.js"></script>
  <script src="js/moris/morris.min.js"></script>

  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>

  

   <script>

      function onClick(element) {
        document.getElementById("img01").src = element.src;
        //$('#myModalFoto').show();
        $("#ampliara").click();
        //document.getElementById("myModalFoto").style.display = "block";
      }
//formato fecha
function formatDate(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  /*var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;*/
  var strTime = hours + ':' + minutes ;
  return date.getDate() + "/" + date.getMonth()+1 + "/" + date.getFullYear() + " " + strTime;
}
      function guardarnota(){
       
        var d = new Date();
        var e = formatDate(d);
        //alert(e);


        var detalle = $('#nota').val();
        
        var idpaciente = $('#idpaciente').val();
        var user=$("#nombreusuario").val();
        $.ajax({
          url:'actions/actionpacienteNotas.php',
          type:'POST',
          data: { opt: 'n', val: detalle, idpaciente: idpaciente  }
        }).done(function( data ){
            var num = data;
            var codigo = $('#nnotas').html();
            $('#nnotas').html('<li><div class="block"><div class="block_content"><p class="excerpt">  '+detalle+' </p><div class="byline"><span>'+e+'</span> por  '+user+'</div></div></div></li>' + codigo)
            //$('#nnotas').html(codigo + '<li id="'+num+'"><p><button type="button" class="btn btn-xs" onclick="borrarnota('+num+')"><i class="fa fa-square-o"></i> </button>'+detalle +'  </p></li>');
            //$('#nnotas').load('divs/div_notas.php');
          
        });

        $("#nota").val('');
      }

      function borrarnota(id){
        var cod = id;
        $.ajax({
          url:'actions/actionnotas.php',
          type:'POST',
          data: { opt: 'b', val: cod  }
        }).done(function( data ){
          
            //var codigo = $('#nnotas').html();
            //$('#nnotas').html(codigo + '<li><p><button type="button" class="btn btn-xs" onclick="borrarnota()"><i class="fa fa-square-o"></i> </button>'+detalle +'  </p></li>');
            //$('#nnotas').load('divs/div_notas.php');
            $('#'+id+'').hide();
            
        });

      }
  </script>

  <!-- datepicker -->
  <script type="text/javascript">
    $(document).on("click", ".enlace", function () {
         var myBookId = $(this).data('id');
         $(".modal-body #bookId").val( myBookId );
         // As pointed out in comments, 
         // it is superfluous to have to manually call the modal.
         // $('#addBookDialog').modal('show');
    });

    $(document).on("click", ".cita", function () {
         var myBookId = $(this).data('id');
         $(".modal-body #idcita").val( myBookId );
         var ms = $(this).data('ms');
         $(".modal-body #maxSup").val( ms );
         var mi = $(this).data('mi');
         $(".modal-body #maxInf").val( mi);
         var obs = $(this).data('obs');
         $(".modal-body #observaciones").val( obs );
         var valor = $(this).data('valor');
         $(".modal-body #valor").val( valor );
         var fact = $(this).data('fact');
         if(fact==1){
            $(".modal-body #facturaDiv").hide( );
         }
         // As pointed out in comments, 
         // it is superfluous to have to manually call the modal.
         // $('#addBookDialog').modal('show');
    });

   
    $(document).on("click", ".guardarResultado", function() {
            
            var id = $("#idcita").val();
            
            if (id) {
                   var maxSup =  $("#maxSup").val();
                   var maxInf = $("#maxInf").val();
                   var observaciones = $("#observaciones").val();
                   var valor = $("#valor").val();
                   var factura = $("#idfactura").val();
                   var idpaciente = $("#idpaciente").val();
                  $.ajax({
                       url: 'actions/actioncita.php?opt=control',
                       data: 'id='+ id +'&maxSup='+ maxSup +'&maxInf='+ maxInf +'&observaciones='+ observaciones +'&valor='+ valor +'&factura='+ factura +'&idpaciente='+ idpaciente ,
                       type: "POST",
                       success: function(json) {
                         //alert('Added Successfully: '+json); 
                         
                         $("#maxSup").val('');
                         $("#maxInf").val('');
                         $("#observaciones").val('');
                         $("#valor").val('');
                         location.reload();
                         //$('.close').click();
                          //alert(json);
                         new PNotify({
                             title: 'Datos Guardados',
                             text: 'El seguimiento y control de la cita fueron guardados.',
                             type: 'success'
                          });
                         //return false;
                     }
                   });             
               }
      });
    // $(document).on("click", ".fotoGal", function () {
    //      //var myBookId = $(this).data('id');
    //      //$(".modal-body #bookId").val( myBookId );
    //      // As pointed out in comments, 
    //      // it is superfluous to have to manually call the modal.
    //       //document.getElementById("img01").src = element.src;
    //       $('#myModalFoto').modal('show');
    // });


    $(document).ready(function() {
    /*  n =  new Date();
      y = n.getFullYear();
      m = n.getMonth() + 1;
      d = n.getDate();
      document.getElementById("fechap").innerHTML = m + "/" + d + "/" + y;
*/
      
      $('#imagenS3').change(function() {
                var filename = $(this).val();
                var lastIndex = filename.lastIndexOf("\\");
                if (lastIndex >= 0) {
                    filename = filename.substring(lastIndex + 1);
                } 
                
                $('#img3').val(filename);
                addImage(this);
            });

            function addImage(e){
      var file = e.target.files[0],
      imageType = /image.*/;
    
      if (!file.type.match(imageType))
       return;
  
      var reader = new FileReader();
      reader.onload = fileOnload;
      reader.readAsDataURL(file);
     }
  
     function fileOnload(e) {
      var result=e.target.result;
      $('#imgSalida').attr("src",result);
     }
      var cb = function(start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
      }

      var optionSet1 = {
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2015',
        dateLimit: {
          days: 60
        },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'MM/DD/YYYY',
        separator: ' to ',
        locale: {
          applyLabel: 'Submit',
          cancelLabel: 'Clear',
          fromLabel: 'From',
          toLabel: 'To',
          customRangeLabel: 'Custom',
          daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
          monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
          firstDay: 1
        }
      };
      $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
      $('#reportrange').daterangepicker(optionSet1, cb);
      $('#reportrange').on('show.daterangepicker', function() {
        console.log("show event fired");
      });
      $('#reportrange').on('hide.daterangepicker', function() {
        console.log("hide event fired");
      });
      $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
      });
      $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
        console.log("cancel event fired");
      });
      $('#options1').click(function() {
        $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
      });
      $('#options2').click(function() {
        $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
      });
      $('#destroy').click(function() {
        $('#reportrange').data('daterangepicker').remove();
      });
    });
  </script>
  <!-- /datepicker ////////////////////////////////////////////////////////////////////////////////-->
 
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
 
  <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/maphilight/1.4.0/jquery.maphilight.min.js"></script>-->
  <script src="js/jquery.maphilight.js"></script>
<script src="js/jquery.rwdImageMaps.min.js" ></script>

<script>
      var dientes = Array.from(document.querySelectorAll('.dientepieza'));
      var colores= Array.from(document.querySelectorAll('.dientecolor'));
      
      function agregarDientes(pieza,color){
        debugger;
        dientes.push(pieza);
        colores.push(color);
        console.log(dientes);
      }
     
   var tablea= $("#tablaa").DataTable({
                "searching": true,
                "info": false,
                "lengthChange": false,
                "oLanguage": {
                        "sSearch": "Buscar:",
                        "sLengthMenu": "Mostrar _MENU_ registros pagina",
                        "sZeroRecords": "Nothing found - sorry",
                        "sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
                        "sInfoEmpty": "Showing 0 to 0 of 0 records",
                        "sInfoFiltered": "(filtered from _MAX_ total records)",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                                    }
                      }
              });
   var tablen= $("#tablani").DataTable({
                "searching": true,
                "info": false,
                "lengthChange": false,
                "oLanguage": {
                        "sSearch": "Buscar:",
                        "sLengthMenu": "Mostrar _MENU_ registros pagina",
                        "sZeroRecords": "Nothing found - sorry",
                        "sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
                        "sInfoEmpty": "Showing 0 to 0 of 0 records",
                        "sInfoFiltered": "(filtered from _MAX_ total records)",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                                    }
                      }
              });

   $(document).on("click", ".guardaLesion", function() {
           
           var id = $("#diente0").val();
           var color = $("input[name='colores']:checked").val();
           var userID=$("#userID").val();
          debugger;
                  var idpaciente =  $("#idpaciente").val();
                  var idtipo = $("#idtipoLesion").val();
                  var nota = $("#notad").val();
                 var pieza={value: id};
                 var colorpieza={value: color};
                 
                  $.ajax({
                      url: 'actions/actionlesiones.php?opt=np',
                      data: 'idtipoLesion='+ idtipo +
                      '&idpaciente='+ idpaciente +
                      '&pieza='+ id +
                      '&notad='+ nota +
                      '&color='+ color+
                      '&user='+userID+
                      '&adulto='+ 0  ,
                      type: "POST",
                      success: function(json) {
                        console.log(json);
                       darcolor(json);

                        agregarDientes(pieza,colorpieza);
                        $("#nota").val('');
                        
                        //$('.closemodal').click();
                         //alert(json);
           
                        //return false;
                    }
                  });
                  
             
      });
   
//diente niño////////////////////////////////////////////////////////
      var dientesn = Array.from(document.querySelectorAll('.dientepiezan'));
      var coloresn= Array.from(document.querySelectorAll('.dientecolorn'));
      function agregarDientesn(pieza,color){
        
        dientesn.push(pieza);
        coloresn.push(color);
        console.log(dientesn);
      }
     
$(document).on("click", ".guardaLesionn", function() {
           
           var id = $("#dienten").val();
           var color = $("input[name='coloresn']:checked").val();
           var userID=$("#userID").val();
          debugger;
                  var idpaciente =  $("#idpaciente").val();
                  var idtipo = $("#idtipoLesionn").val();
                  var nota = $("#notan").val();
                  var pieza={value: id};
                 var colorpieza={value: color};
                 
                  $.ajax({
                      url: 'actions/actionlesiones.php?opt=np',
                      data: 'idtipoLesion='+ idtipo +
                      '&idpaciente='+ idpaciente +
                      '&pieza='+ id +
                      '&notad='+ nota +
                      '&color='+ color+
                      '&user='+userID+
                      '&adulto='+ 1  ,
                      type: "POST",
                      success: function(json) {
                        //alert('Added Successfully: '+json); 
                        //$("#idtipoLesion").val('0');
                        darcolorn(json);
                        agregarDientesn(pieza,colorpieza)
                        $("#notan").val('');
                        
                        //$('.closemodal').click();
                         //alert(json);
           
                        //return false;
                    }
                  });
                  
             
      });

      function eliminarLn(id,diente){
        //debugger;
        if(confirm("Atención! Va a proceder a eliminar este registro. Desea continuar?"))
                {
                  $('#tablani tbody').on( 'click', 'a', function () {
                   tablen.row( $(this).parents('tr') )
                    .remove()
                    .draw();
                  } );
   
                
                  debugger;
                      $.ajax({
                      url: 'actions/actionlesiones.php?opt=e',
                      data: 'idlesion='+ id  ,
                      type: "POST",
                      success: function(json) {
                        quitarcolorn(diente);
                        $("#notan").val('');
                        $('#'+diente).data('maphilight', {"stroke":0, "alwaysOn": false});
                        $('#mapasn').maphilight();
                     
                    }
                  });
                }
      }

      function eliminarL(id,diente){
        //debugger;
        if(confirm("Atención! Va a proceder a eliminar este registro. Desea continuar?"))
                {
                  $('#tablani tbody').on( 'click', 'a', function () {
                   tablen.row( $(this).parents('tr') )
                    .remove()
                    .draw();
                  } );
   
                $('#tablaa tbody').on( 'click', 'a', function () {
                  tablea.row( $(this).parents('tr') )
                      .remove()
                      .draw();
                    } );
                  debugger;
                      $.ajax({
                      url: 'actions/actionlesiones.php?opt=e',
                      data: 'idlesion='+ id  ,
                      type: "POST",
                      success: function(json) {
                        quitarcolor(diente);
                        $("#notan").val('');
                        $('#'+diente).data('maphilight', {"stroke":0, "alwaysOn": false});
                        $('#mapas').maphilight();
                     
                    }
                  });
                }
      }
      function ocultardienten(){
        $("#idbotondienten").show('slow');
        $("#divdienten").hide("slow");
  
}

function verdienten(){
     rellenardienten();
     $("#idbotondienten").hide('slow');
      $("#divdienten").show('slow');
      
       //$('#mapasn').rwdImageMaps();
       $('#mapasn').maphilight();
       ocultardiente();
    }
    
    function rellenardienten(){
      
         for (let index = 0; index < dientesn.length; index++) {
          //dientes[index].setAttribute('visible', 'false');
          var obj2=dientesn[index].value;
          var color=coloresn[index].value;
          //console.log(dientes[index].value);
          $('#'+obj2).data('maphilight', {"stroke":0, "alwaysOn": true, "fillColor":color,  "fillOpacity":0.3});
         }
    }


    //////////////////////////////////
    $('.nino').each(function(){
        $('#'+this.id).click(function(){
          var dientes=Array.from(document.querySelectorAll('.nino'));
          var datos;
          
          var diente=this.id;
        for (let index = 0; index < dientes.length; index++) {
          const element = dientes[index];
          
           $('#'+element.id).data('maphilight', {"stroke":false});
        }
        rellenardienten();
       var data = $('#'+diente).mouseout().data('maphilight') || {};
        if(!data.alwaysOn){
                data.alwaysOn=!data.alwaysOn;
                data.stroke = !data.stroke;
                var datos=   $('#'+diente).data('maphilight', data).trigger('stroke.maphilight');
        
              }
              else{
                data.stroke = !data.stroke;
                var datos=   $('#'+diente).data('maphilight', data).trigger('stroke.maphilight');
        
              }
          console.log(data);
            $('#mapasn').maphilight();
            formularn($(this));
            $( "#btncolorn" ).removeClass( "disabled" );
        });
    });
    /////////////////////////////////////

    
    /*$('.nino').each(function(){
        $(this).click(function(){
           // alert($(this).attr("id"));
           console.log(this.id);
            formularn($(this));
            $( "#btncolorn" ).removeClass( "disabled" );
        });
    });*/
        $(".colorninos").click(function(){
        var radio= Array.from(document.querySelectorAll(".colorninos"));
        debugger;
        for (let index = 0; index < radio.length; index++) {
          const element = radio[index];
                $(element).css('border','0px solid');
            }
        $(this).css('border','3px solid');
            
            console.log();
        });
    

    function formularn(obj){
        //rojo=FF0000 , verde=228B22,azul=0000FF,gris=708090
        $("#dienten").val(obj.attr("id"));
       // alert(obj.attr("id"));
        $('#formulario').show();
       
            //alert(obj.attr("id"));
           
      
        
    }
    
    function quitarcolorn(id){
      debugger;
      for( var i = 0; i < dientesn.length; i++){ 
          if ( dientesn[i].value ==id ) {
            dientesn.splice(i, 1); 
            coloresn.splice(i,1);
        }
        console.log(dientesn);
        }
        }
    function darcolorn(nuevoid){
        var id=$("#dienten").val();
      
        var descripcion=$("#idtipoLesionn option:selected").text();
        var color = $("input[name='coloresn']:checked").val();
       
        var nota=$("#notan").val();
        
        var obj2=$("#dienten").val();
            $('#'+obj2).data('maphilight', {"stroke":0, "alwaysOn": true, "fillColor":color, "fillOpacity":0.3});
            $('#mapasn').maphilight();
            $( "#btncolorn" ).addClass( "disabled" );

            var colo=""+id+" <label class='dientecolor' value="+color+" style='background:#"+color+";width:20px;height:10px;border-radius:15px;'></label>";
          var link="<a class='dientepieza' value="+id+" onclick='javascript:eliminarLn("+nuevoid+","+id+")' ><i style='font-size:18px;' class='fa fa-trash-o' aria-hidden='true'></i></a>";
          tablen.row.add( [
            colo,
            descripcion,
            nota,
            
            link
           
        ] ).draw( false );
          
    }

//diente adulto///////////////////////////////////////////////////////
$(".coloradul").click(function(){
        var radio= Array.from(document.querySelectorAll(".coloradul"));
        debugger;
        for (let index = 0; index < radio.length; index++) {
          const element = radio[index];
                $(element).css('border','0px solid');
            }
        $(this).css('border','3px solid');
            
            console.log();
        });
   
function ocultardiente(){
  $("#idbotondiente").show('slow');
  $("#divdiente").hide("slow");
 
}
    function verdiente(){
      $("#idbotondiente").hide('slow');
      $("#divdiente").show("slow");
      rellenardiente();
      $('#mapas').maphilight({ fillColor: 'ffffff'});
       //$('#mapas').rwdImageMaps();
       ocultardienten();
    }
   
      
    
      
      $('.adul').each(function(){
        $('#'+this.id).click(function(){
          var dientes=Array.from(document.querySelectorAll('.adul'));
          var datos;
          
          var diente=this.id;
        for (let index = 0; index < dientes.length; index++) {
          const element = dientes[index];
          
           $('#'+element.id).data('maphilight', {"stroke":false});
        }
        rellenardiente();
       var data = $('#'+diente).mouseout().data('maphilight') || {};
        if(!data.alwaysOn){
                data.alwaysOn=!data.alwaysOn;
                data.stroke = !data.stroke;
                var datos=   $('#'+diente).data('maphilight', data).trigger('stroke.maphilight');
        
              }
              else{
                data.stroke = !data.stroke;
                var datos=   $('#'+diente).data('maphilight', data).trigger('stroke.maphilight');
        
              }
          console.log(data);
            $('#mapas').maphilight();
            formular($(this));
            $( "#btncolor" ).removeClass( "disabled" );
        });
    });
  

    
    function quitarcolor(id){
      debugger;
      for( var i = 0; i < dientes.length; i++){ 
          if ( dientes[i].value ==id ) {
            dientes.splice(i, 1); 
            colores.splice(i,1);
        }
        console.log(dientes);
          }
    }
    function rellenardiente(){
      debugger;
         for (let index = 0; index < dientes.length; index++) { 
          var obj2=dientes[index].value;
          var color=colores[index].value;
          $('#'+obj2).data('maphilight', {"stroke":false, "alwaysOn": true, "fillColor":color,  "fillOpacity":0.3});
         }
    }
    /////////////////////////////prueba de maph/////////////////////////////////////////////////
   
        ///////////////////////////////////////////////////////////
   

    function formular(obj){
        //rojo=FF0000 , verde=228B22,azul=0000FF,gris=708090
        $("#diente0").val(obj.attr("id"));
       // alert(obj.attr("id"));
        //$('#formulario').show();
        
            //alert(obj.attr("id"));
           
      
        
    }
    

    function darcolor(nuevoid){
      debugger;
        var id=$("#diente0").val();
       
        
        var descripcion=$("#idtipoLesion option:selected").text();
        var color = $("input[name='colores']:checked").val();
        var nota=$("#notad").val();
     
        var obj2=$("#diente0").val();
            $('#'+obj2).data('maphilight', {"stroke":0, "alwaysOn": true, "fillColor":color, "fillOpacity":0.3});
            $('#mapas').maphilight();
            $( "#btncolor" ).addClass( "disabled" );
          var colo=""+id+" <label style='background:#"+color+";width:20px;height:10px;border-radius:15px;'></label><input type='hidden' value="+color+" class='dientecolor'> ";
          var link="<a onclick='javascript:eliminarL("+nuevoid+","+id+")'><i style='font-size:18px;' class='fa fa-trash-o' aria-hidden='true'></i></a><input type='hidden' value="+id+" class='dientepieza'>";
          tablea.row.add( [
         
            colo,
            descripcion,
            nota,
            link
           
        ] ).draw( false );



    }
    function ver(id){
        var mostrar= id;
        alert(mostrar);
    }
 
////////////////////////////////////////////////////////////////pagos

var tablapagos= $("#tablapagos").DataTable();
var tablacreditos= $("#tablacredito").DataTable();
function verpago(tipo){
  if(tipo==1){
    $("#tabladecreditos").show('slow');
    
    $("#tabladepagos").hide('slow');
   
    $("#formularioPagos").hide('slow');
    
    $("#formularioCreditos").hide('slow');
    
    $("#forpagocreditos").hide('slow');
   
   // $("#botonpago").hide('slow');
   $( "#botonpago" ).removeClass( "btn-success" ).addClass( "btn-default" );
   $( "#botoncredito" ).removeClass( "btn-default" ).addClass( "btn-success" );
    
  }
  else{
    
    //$("#botonpago").show('slow');
    $("#tabladepagos").show('slow');
    $("#tabladecreditos").hide('slow');
    $("#formularioPagos").hide('slow');
    $("#formularioCreditos").hide('slow');
    
    $("#forpagocreditos").hide('slow');
    
   // $("#botoncredito").hide('slow');
   $( "#botoncredito" ).removeClass( "btn-success" ).addClass( "btn-default" );
   $( "#botonpago" ).removeClass( "btn-default" ).addClass( "btn-success" );
    
  }
  
  
  
  
}

function verformulario(tipo){
  if(tipo==1){
    $("#formularioPagos").show('slow');
    $("#forcredito").hide();
    $("#actualizarFormpago").hide();
    $("#guadarFormPago").show();
    $("#forpagocreditos").hide('slow');
    $("#idtipopago").val(1);
    $("#tratamientopago").val('');
    $("#monto").val(0);
    $("#notapago").val('');
    $("#modaldepago").text('Nuevo');
    
  }else{
    $("#forpagocreditos").hide('slow');
    $("#forcredito").show();
    $("#formularioPagos").show('slow');
    $("#actualizarFormpago").hide();
    $("#guadarFormPago").show();
    $("#idtipopago").val(2);
    $("#tratamientopago").val('');
    $("#monto").val(0);
    $("#notapago").val('');
    $("#modaldepago").text('Nuevo');
  }
}

function verfomcreditopago(id){
  var saldo=$("#d"+id).text();
  $('#idpago').val(id);
  $("#idsaldo").val(saldo);
  $("#modalpagocredito").text('Saldo: '+saldo);
  

  $("#forpagocreditos").show('slow');
  
  $("#formularioPagos").hide('slow');
  $("#formularioCreditos").hide('slow');
 
}
function vertablacreditospago(id){
  $('#idpago').val(id);
  $("#forpagocreditos").hide('slow');
  $("#tablacreditopagos").show('show');
  $("#formularioPagos").hide('slow');
 $("#tablaCredito").empty();
 
 var tabla= $("#tablaCredito");
 debugger;
  var tr;
                    $.ajax({
                      url: 'actions/actionpagocredito.php?opt=todos',
                      data: 'pago='+id ,
                      type: "POST",
                      dataType:'json',
                      success: function(data) {

                        $.each(data, function(i, item) {
                      
                            $("#nombretratamiento").text(item.tratamiento);
                          $("#saldo").text("Saldo: "+item.debe);
                          $("#pagado").text('Se cancelo: '+item.pagado);
                          if(item.credito1==1){
                            tr="<tr><td>"+item.idpagoTraramiento+"</td>"+
                           "<td>"+item.fecha+"</td>"+
                           "<td>"+item.monto+"</td>"+
                           "<td>"+item.observaciones+"</td>"+               
                           "</tr>";
                          }else{
                            tr="<tr><td>"+item.idpagoTraramiento+"</td>"+
                           "<td>"+item.fecha+"</td>"+
                           "<td>"+item.monto+"</td>"+
                           "<td>"+item.observaciones+"</td>"+

                            "<td><a href='javascript:eliminarCredito("+item.idpagoTraramiento+")'><i class='fa fa-trash' aria-hidden='true'></i></a></td>"
                          
                         
                           "</tr>";
                          }
                          
                           $(tabla).append(tr);
                    
                          
                       
                        });
                        console.log(data);
                       
                    }
                  });
 
}
function eliminarCredito(id){
  debugger;
  var idpaciente = $('#idpaciente').val();
                if(confirm("Atención! Va a proceder a eliminar este registro. Desea continuar?"))
                {
                  $.ajax({
                      url: 'actions/actionpagocredito.php?opt=eli',
                      data: 'id='+id ,
                      type: "POST",
                    
                      success: function(data) {
                              if(data == 0){
                                new PNotify({
                                title: 'Datos Eliminados',
                                  text: 'Todos los datos fueron guardados. Puede continuar.!',
                                  type: 'success'
                                });
                                window.setTimeout("location.reload(true);",2500);
                          }
                          else{
                                new PNotify({
                                    title: 'Error en formulario',
                                    text: 'No se puedieron guardar los datos, intente de nuevo.',
                                    type: 'error'
                                });
                                  window.setTimeout("location.reload(true);",2500);
                          }                     
                    }
                  });
                }
}

function eliminarCreditoTotal(id){
  var idpaciente = $('#idpaciente').val();
                if(confirm("Atención! Va a proceder a eliminar este registro. Desea continuar?"))
                {
                  $.ajax({
                      url: 'actions/actionpagocredito.php?opt=eliminarTotal',
                      data: 'id='+id ,
                      type: "POST",
                    
                      success: function(data) {
                              if(data == 0){
                                new PNotify({
                                title: 'Datos Eliminados',
                                  text: 'Todos los datos fueron guardados. Puede continuar.!',
                                  type: 'success'
                                });
                                window.setTimeout("location.reload(true);",2500);
                          }
                          else{
                                new PNotify({
                                    title: 'Error en formulario',
                                    text: 'No se puedieron guardar los datos, intente de nuevo.',
                                    type: 'error'
                                });
                                  window.setTimeout("location.reload(true);",2500);
                          }                     
                    }
                  });
                }
}

function cerrarpagos(){
  $("#formularioPagos").hide('slow');
  $("#forpagocreditos").hide('slow');

  
}
function agregarcredito(){
debugger;
                  var userID=$("#userID").val();
                  var fecha=$("#fechapc").val();
                  var idpaciente = $('#idpaciente').val();
                  var monto =  $("#montoc").val();
                  var pago = $("#idpago").val();
                  var nota = $("#notapagoc").val();
               
                  var sald=$("#idsaldo").val();
                  var subtotal=sald-monto;
                 var cambio=Math.abs(subtotal);
                 if(subtotal<=0){
                  monto=monto-Math.abs(subtotal);
                  subtotal=0;
                  
                   $("#idsaldo").val('0');
                   $("#abrirformulariopagos"+pago).hide();
                   new PNotify({
                                title: 'Devolver',
                                  text: cambio,
                                  type: 'success'
                                });
                 }
                 $('#idsaldo').val(subtotal);
                  $.ajax({
                      url: 'actions/actionpagocredito.php?opt=np',
                      data: 'usuario='+userID+
                            '&fecha='+fecha+ 
                            '&paciente='+idpaciente+
                            '&monto='+monto+
                            '&nota='+nota+
                            '&credito1='+0+
                            '&idpago='+pago
                            ,
                      type: "POST",
                      success: function(json) {
                        $("#d"+pago).text(subtotal);
                        //alert('Added Successfully: '+json); 
                        //$("#idtipoLesion").val('0');
                        $("#formularioPagos").hide('slow');
                        $("#forpagocreditos").hide('slow');
                        $("#notapagoc").val('');
                        $("#montoc").val(0);
                        $("#notan").val('');
                        new PNotify({
                                title: 'Datos almacenando',
                                  text: 'Los datos fueron guardados correctamente',
                                  type: 'success'
                                });
                        console.log(json);
                        //$('.closemodal').click();
                         //alert(json);
           
                        //return false;
                    }
                  });
                 
                
                     
}

////nuevo pago
function pagocredito0(id,fecha,monto,tipopago,tratamiento){
  debugger;
  if(tipopago==1){
          tipo="efectivo";
        }else{
          tipo="tarjeta";
        }
        var usuario=$("#nombreusuario").val();
                  var userID=$("#userID").val();
                  var credito1=1;
                  var idpaciente = $('#idpaciente').val();
                  var pago=(id);
                  var montoc =  $("#montocr").val();
                  var subtotal=monto-montoc;
                  var debe="<span id='d"+id+"'>"+subtotal+"</span>";
                  var nota = $("#notapago").val();
                  var opcion="<a onclick='javascript:verfomcreditopago("+id+")'"+
                  " data-toggle='modal' data-target='#modalFormularioCredito'"+
                   "title='Agregar pagos'><i class='fa fa-plus' aria-hidden='true'></i></a>"+
                   "<a title='Ver pagos' data-toggle='modal' data-target='#tablapagoscredito' onclick='javascript:vertablacreditospago("+id+")' ><i class='fa fa-search' aria-hidden='true'></i></a>"+
                   "<a title='Imprimir' href='pagosImprimir.php?pago="+pago+"'><i class='fa fa-print'></i></a>"+
                   "<a title='Eliminar' href='javascript:eliminarCreditoTotal("+pago+")'><i class='fa fa-trash-o'></i></a>"
                      
                  tablacreditos.row.add( [ 
                    tratamiento,
                    fecha,
                    monto,
                    debe,
                    tipo,
                    nota,
                    usuario,
                    opcion
                    ] ).draw( false );
                   $.ajax({
                      url: 'actions/actionpagocredito.php?opt=np',
                      data: 'usuario='+userID+
                            '&fecha='+fecha+ 
                            '&paciente='+idpaciente+
                            '&monto='+montoc+
                            '&nota='+nota+
                            '&credito1='+1+
                            '&idpago='+pago
                            ,
                      type: "POST",
                      success: function(json) {
                       
                        new PNotify({
                                title: 'Datos almacenando',
                                  text: 'Los datos fueron guardados correctamente',
                                  type: 'success'
                                });
                        $("#notan").val('');
                        console.log(json);
                       
                    }
                  });   
}

$(document).on("click", ".guardapago", function() {
          debugger;
                  var userID=$("#userID").val();
                  var fecha=$("#fechap").val();
                  var idpaciente = $('#idpaciente').val();
                  var monto =  $("#monto").val();
                  var formapago = $("#formapagoid").val();
                  var nota = $("#notapago").val();
                  var tratamiento=$("#tratamientopago").val();
                  var tipopago=$("#idtipopago").val();
                  if(validar()){
                    $.ajax({
                      url: 'actions/actionpagos.php?opt=np',
                      data: 'usuario='+userID+'&fecha='+fecha+ '&paciente='+idpaciente+
                            '&monto='+monto+'&formapago='+formapago+'&nota='+nota+
                            '&tratamiento='+tratamiento+
                            '&tipopago='+tipopago ,
                      type: "POST",
                     dataType:'json',
                      success: function(data) {
                        $("#formularioPagos").hide('slow');
                        if(tipopago!=1){
                          pagocredito0(data,fecha,monto,formapago,tratamiento);
                          new PNotify({
                                title: 'Almacenando Datos',
                                  text: 'Aguarde un momento',
                                  type: 'success'
                                });
                        }else{
                          new PNotify({
                                title: 'Almacenando Datos',
                                  text: 'Todos los datos fueron almacenados correctamente',
                                  type: 'success'
                                });
                          llenartablapago(tratamiento,fecha,monto,formapago,nota)
                        $("#notapago").val('');
                        }
                        $("#tratamientopago").val('');
                        $("#monto").val(0);
                       
                                             
                    }
                  });
                  }else
                  {
                    new PNotify({
                                title: 'Faltan Datos',
                                  text: 'No puede almacenar',
                                  type: 'error'
                                });
                  }
                  
                 
                  
             
      });
    function llenartablapago(tratamiento,fecha,monto,tipopago,nota){
      debugger;
        var fechampagos=convertirfecha(fecha);
        var tipo;
        var usuario=$("#nombreusuario").val();
        var opcion="<a><i class='fa fa-minus-circle' aria-hidden='true'></i></a>"
        if(tipopago==1){
          tipo="efectivo";
        }else{
          tipo="tarjeta";
        }
              tablapagos.row.add( [ 
                    tratamiento,
                    fechampagos,
                    monto,
                   
                    tipo,
                    nota,
                    usuario,
                    opcion
                    ] ).draw( false );
      }

  function eliminarPago(id){
                var idpaciente = $('#idpaciente').val();
                if(confirm("Atención! Va a proceder a eliminar este registro. Desea continuar?"))
                {
                  $.ajax({
                      url: 'actions/actionpagos.php?opt=eli',
                      data: 'id='+id ,
                      type: "POST",
                    
                      success: function(data) {
                              if(data == 0){
                                new PNotify({
                                title: 'Datos Eliminados',
                                  text: 'Todos los datos fueron guardados. Puede continuar.!',
                                  type: 'success'
                                });
                                window.setTimeout("location.reload(true);",2500);
                          }
                          else{
                                new PNotify({
                                    title: 'Error en formulario',
                                    text: 'No se puedieron guardar los datos, intente de nuevo.',
                                    type: 'error'
                                });
                                  window.setTimeout("location.reload(true);",2500);
                          }                     
                    }
                  });
                }
  }
function actualizarPagoform(id){
  $("#idpago").val(id);
  $("#formularioPagos").show();
  $("#idtipopago").val('1');
  $("#modaldepago").text('Actualizar');
  $("#actualizarFormpago").show();
  $("#guadarFormPago").hide();
  debugger;
                $.ajax({
                      url: 'actions/actionpagos.php?opt=uno',
                      data: 'idpago='+id ,
                      type: "POST",
                     dataType:'json',
                      success: function(data) {
                        $.each(data, function(i, item) {
                          $("#monto").val(item.monto);
                          //$("#formapagoid").val();
                          $("#notapago").val(item.observaciones);
                          $("#tratamientopago").val(item.tratamiento);
                        console.log(data);
                        })
                       
                                             
                    }
                  });

}
function actualizarPago(){
  debugger;
                  var id=$("#idpago").val();
                  var userID=$("#userID").val();
                  var fecha=$("#fechap").val();
                  var idpaciente = $('#idpaciente').val();
                  var monto =  $("#monto").val();
                  var formapago = $("#formapagoid").val();
                  var nota = $("#notapago").val();
                  var tratamiento=$("#tratamientopago").val();
                  var tipopago=$("#idtipopago").val();
                  
                  $.ajax({
                      url: 'actions/actionpagos.php?opt=up',
                      data: 'usuario='+userID+'&fecha='+fecha+ '&paciente='+idpaciente+
                            '&monto='+monto+'&formapago='+formapago+'&nota='+nota+
                            '&tratamiento='+tratamiento+
                            '&tipopago='+tipopago+
                            '&idpago='+id ,
                      type: "POST",
                     dataType:'json',
                      success: function(data) {
                        if(data != 0){
                                new PNotify({
                                title: 'Datos Actualizados',
                                  text: 'Todos los datos fueron guardados. Puede continuar.!',
                                  type: 'success'
                                });
                                window.setTimeout("location.reload(true);",1500);
                          }
                          else{
                                new PNotify({
                                    title: 'Error en formulario',
                                    text: 'No se puedieron guardar los datos, intente de nuevo.',
                                    type: 'error'
                                });
                                  window.setTimeout("location.reload(true);",1500);
                          }
                       
                                             
                    }
                  });
}
function imprimir(){
  //$("#tablapagoscredito").print();

  var printContents = document.getElementById('tablapagoscredito').innerHTML;
			var originalContents = document.body.innerHTML;
			document.body.innerHTML = printContents;
			window.print();
			document.body.innerHTML = originalContents;
}
function convertirfecha(fecha){
  var today = new Date(fecha);
var dd = today.getDate()+1;

var mm = today.getMonth()+1; 
var yyyy = today.getFullYear();
    if(dd<10) 
    {
        dd='0'+dd;
    } 

    if(mm<10) 
    {
        mm='0'+mm;
    } 
return today = dd+'-'+mm+'-'+yyyy;
}

function validar(){
  debugger;
var paso=true;
var tratamiento=$("#tratamientopago").val();
var monto=$("#monto").val();
var montocr=$("#montocr").val();
if( tratamiento == null || tratamiento.length == 0 || /^\s+$/.test(tratamiento) ){
  paso=false;

}else{
  if(isNaN(monto)){
    paso=false;
  }else{
    if(isNaN(montocr)){
      paso=false;
    }else{
      paso=true;
    }
  }
}
  return paso;
}

//////////////////////////////////citas---modal
$(".labelcolo").click(function(){
        var radio= Array.from(document.querySelectorAll(".labelcolo"));
        debugger;
        for (let index = 0; index < radio.length; index++) {
          const element = radio[index];
                $(element).css('border','0px solid');
            }
        $(this).css('border','3px solid');
            
            console.log();
        });

        $(".antosubmit").on("click", function() {
             debugger;
                var title = $("#pacientec").val();
                if (title) {
                  var idpaciente =  $("#idpacientec").val();
                   var idusuario =  $("#idusuario").val();
                  if(idtipocita==4){
                    idpaciente=1;
                   
                   }
                   
                   var idtipocita = $("#idtipocita").val();
                   var detalle = $("#descrc").val();
                   var colores='#'+$("input[name='coloresc']:checked").val();
                   var start = $('#fechacita').val() + " " + $('#horac').val();
                   var hora = $('#horac').val().charAt(0) + $('#horac').val().charAt(1);
                   var min = $('#horac').val().charAt(3) + $('#horac').val().charAt(4);
                   hora = parseInt(hora) + 1;
                   var end = $('#fechacita').val() + " " +  hora + ":" + min ;
                   var url = '';
                   $.ajax({
                       url: 'actions/actioncita.php?opt=ja',
                       data: 'title='+ title+
                       '&start='+ start +
                       '&end='+ end +
                       '&url='+ url +
                       '&tipocita=' + idtipocita +
                       '&idpaciente=' + idpaciente +
                       '&detalle=' + detalle + 
                       '&idusuario=' + idusuario + 
                       '&color='+colores+
                       '&allDay='  ,
                       type: "POST",
                       success: function(json) {
                        new PNotify({
                                title: 'Se agrego correctamente',
                                  text: 'todos los cambios fueron guardados!',
                                  type: 'success'
                                });
                                $('.antoclose').click();
                     }
                   });
                  
               }
             

               return false;

           });
</script>
</body>

</html>
