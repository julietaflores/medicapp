<?php
// DataBase
include_once '../data/dataBase.php';
// Classes
include_once '../classes/cmembresiaClinica.php';
include_once '../classes/cclinica.php';
include_once '../classes/creferidos.php';
include_once '../classes/cmembresia.php';
include_once '../classes/cproducto.php';

$oproducto  	= new producto();
$omembresia  	= new Membresia();
$oMembresiaClinica  	= new MembresiaClinica();
$oclinica  	= new clinica();
$oreferidos = new referido();
$option = '';



if (isset($_REQUEST['valor'])){
  $data 	= $_REQUEST['valor'];
  $option 	= $data['opt'];
}else{
	$option =  $_REQUEST['opt'];
}


if ( $option == 'n' ) {
	try{
		    $idmembresia		= $_REQUEST['idmembresia'];
		    $fechaCompra		=  date('Y-m-d HH:MM:SS');
		    $idformaPago		= $_REQUEST['idformaPago'];
		    $idestado		= $_REQUEST['idestado'];  
			$idclinica		=  $_REQUEST['idclinica'];
			$valor		=  $_REQUEST['valor'];
			$idusuario		=  $_REQUEST['idusuario'];
			$params = array($idmembresia,$fechaCompra,$idformaPago,$idestado,$idclinica,$valor,$idusuario);
			$id   = $oMembresiaClinica->nuevo($params);
			if ( $id ) {
				echo $id;
			} else {
				echo 0;
			}
	}catch (Exception $e){
		echo  $e;
	}
}




if($option=="actestado"){
	try{
		$id		= $_REQUEST['id'];
		$idc		= $_REQUEST['idc'];
		$idm		= $_REQUEST['idm'];
		$referidosclinica=$oreferidos->getAllsiexiste($idc);
	    $dd=$omembresia->getOnedesc($idm);
		$upd   = $oclinica->habilitar($idc);
		$fechainicio   = date('Y-m-d');
		$duracionm=$omembresia->getOneduracion($idm);
		$tiemponm=$omembresia->getOneTiempo($idm);
        if($tiemponm==1){
			$nuevafecha = strtotime ( '+'.$duracionm.' days' , strtotime ( $fechainicio ) ) ;
		}else{
			if($tiemponm==2){
				$nuevafecha = strtotime ( '+'.$duracionm.' month' , strtotime ( $fechainicio ) ) ;
			}else{
				if($tiemponm==3){
					$nuevafecha = strtotime ( '+'.$duracionm.' year' , strtotime ( $fechainicio ) ) ;
				}
			}
		}
		$fechafin = date ( 'Y-m-d' , $nuevafecha );
		$update   = $oMembresiaClinica->habilitar($id,$fechainicio,$fechafin);

		$cv= $oMembresiaClinica->getTotalusuariomcl($idc);
		if($cv==0){
			$params = array($idc,'consulta',50,0,2,0,0);
			$pro=$oproducto->nuevo($params);
		 }         
		
	
		if ( $update ) {
			if($dd==1){
				if($referidosclinica){
					$c=$oreferidos->actualizar($idc);
				}
				echo $id;
			}else{
				echo $id;
			}
			
		} else {
			echo "0";
		}
	}catch(Exception $e){
		echo "error";
	}
}


if($option=="actestadob"){
	try{
		$id		= $_REQUEST['id'];
		$idc		= $_REQUEST['idc'];
		$upd   = $oclinica->bloquear($idc);
		$update   = $oMembresiaClinica->deshabilitar($id);
		if ( $update ) {
			echo "1";
		} else {
			echo "0";
		}
	}catch(Exception $e){
		echo "error";
	}
}


if($option=="bloqueoproducto"){
	try{
		$idc		= $_REQUEST['idc'];
		$update   = $oMembresiaClinica->bloqueoproducto($idc);
		if ( $update ) {
			echo "1";
		} else {
			echo "0";
		}
	}catch(Exception $e){
		echo "error";
	}
}

if($option=="desbloqueoproducto"){
	try{
		$idc		= $_REQUEST['idc'];
		$update   = $oMembresiaClinica->desbloqueoproducto($idc);
		if ( $update ) {
			echo "1";
		} else {
			echo "0";
		}
	}catch(Exception $e){
		echo "error";
	}
}



if($option=="actestadob1"){
	try{
		$id		= $_REQUEST['idmembresiaClinica'];
		$idc		= $_REQUEST['idclinica'];
		$upd   = $oclinica->bloquear($idc);
		$update   = $oMembresiaClinica->deshabilitar($id);
		if ( $update ) {
			echo "1";
		} else {
			echo "0";
		}
	}catch(Exception $e){
		echo "error";
	}
}




if ( $option == 'm' ) {
			$idmembresiaClinica	= $data['idmembresiaClinica'];  
			$idmembresia		= $data['idmembresia'];  
			$fechaCompra		= $data['fechaCompra'];  
		    $idformaPago		= $data['idformaPago'];  
			$idestado		= $data['idestado']; 
			$idclinica		=  $data['idclinica'];
			$valor		=  $data['valor'];
			$idusuario		=  $data['idusuario'];
			try {
		    $params = array($idmembresia,$fechaCompra,$idformaPago,$idestado,$idclinica,$valor,$idusuario,$idmembresiaClinica);
			$save   = $oMembresiaClinica->actualizar($params);
			if ($save) {
				echo $idMembresiaClinica;
			} else {
				echo "400";
			}
		    }catch (Exception $e){
		     	echo  $e;
		    }
	
}


if($option=="e"){
	try{
		$id		= $_REQUEST['id'];
		$update   = $oMembresiaClinica->eliminar($id);
		if ( $update ) {
			echo "0";
		} else {
			echo "400";
		}
	}catch(Exception $e){
		echo "error";
	}
}


if($option=="eliminarc"){
	try{
		$id		= $_REQUEST['id'];
		$update   = $oMembresiaClinica->eliminarc($id);
		if ( $update ) {
			echo "0";
		} else {
			echo "400";
		}
	}catch(Exception $e){
		echo "error";
	}
}



if($option=="repetidosm"){
	try{
		$idmembresia    = $_REQUEST['idmembresia'];
		$idclinica		= $_REQUEST['idclinica'];
		$cantidad   = $oMembresiaClinica->repetidosm($idmembresia,$idclinica);
		 echo $cantidad;
	}catch(Exception $e){
		 echo "error";
	}
}



if($option=="repetidosmx"){
	try{
		$idmembresia    = $_REQUEST['idmembresia'];
		$idclinica		= $_REQUEST['idclinica'];
		$cantidad   = $oMembresiaClinica->repetidosmx($idmembresia,$idclinica);
		 echo $cantidad;
	}catch(Exception $e){
		 echo "error";
	}
}




if($option=="repetidosmm"){
	try{
		//$idmembresia    = $_REQUEST['idmembresia'];
		$idclinica		= $_REQUEST['idclinica'];
		$cantidad   = $oMembresiaClinica->repetidosmm($idclinica);
		 echo $cantidad;
	}catch(Exception $e){
		 echo "error";
	}
}



if($option=="repetidosmmc"){
	try{
		$idclinica		= $_REQUEST['idclinica'];
		$idmembresiaClinica   = $oMembresiaClinica->repetidosmmc($idclinica);
		 echo $idmembresiaClinica;
	}catch(Exception $e){
		 echo "error";
	}
}



if($option=="repetidosmmce"){
	try{
		$idclinica		= $_REQUEST['idclinica'];
		$idmembresiaClinica   = $oMembresiaClinica->repetidosmmce($idclinica);
		 echo $idmembresiaClinica;
	}catch(Exception $e){
		 echo "error";
	}
}



if ( $option == 'n1' ) {
	try{
		    $idmembresia		= $_REQUEST['idmembresia'];
		    $fechaCompra		= date('Y-m-d');
		    $idformaPago		= 0;
		    $idestado		= 0;
			$idclinica		=  $_REQUEST['idclinica'];
			$valor		=  $_REQUEST['valorr'];
			$idusuario		=  $_REQUEST['idusuario'];
			$params = array($idmembresia,$fechaCompra,$idformaPago,$idestado,$idclinica,$valor,$idusuario);
			$id   = $oMembresiaClinica->nuevo1($params,$idclinica);
			if ( $id ) {
				echo $id;
			} else {
				echo 0;
			}
	}catch (Exception $e){
		echo  $e;
	}
}


if ( $option == 'n11' ) {
	try{
		    $idmembresia		= $_REQUEST['idmembresia'];
		    $fechaCompra		= date('Y-m-d');
		    $idformaPago		= 0;
		    $idestado		= 0;
			$idclinica		=  $_REQUEST['idclinica'];
			$valor		=  $_REQUEST['valorr'];
			$idusuario		=  $_REQUEST['idusuario'];
			$params = array($idmembresia,$fechaCompra,$idformaPago,$idestado,$idclinica,$valor,$idusuario);
			$id   = $oMembresiaClinica->nuevo($params);
			if ( $id ) {
				echo $id;
			} else {
				echo 0;
			}
	}catch (Exception $e){
		echo  $e;
	}
}


?>