<?php


// DataBase
include_once '../data/dataBase.php';
// Classes
include_once '../classes/cmembresia.php';


$omembresia  	= new Membresia();
$option = '';
session_start();


if (isset($_REQUEST['valor'])){
  $data 	= $_REQUEST['valor'];
  $option 	= $data['opt'];
}else{
	$option =  $_REQUEST['opt'];
}


if ( $option == 'n' ) {
	try{
		    $nombre		= $data['nombre'];
		    $valor		= $data['valor'];
		    $duracion		= $data['duracion'];
		    $idestado		= $data['idestado'];  
			$nusuarios		=  $data['nusuarios'];
			$detalle		=  $data['detalle'];
			$idmonetario		=  $data['idmonetario'];
			$idtiempo		=  $data['idtiempo'];
			$params = array($nombre,$valor,$duracion,$idestado,$nusuarios,$detalle,$idmonetario,$idtiempo);
			$id   = $omembresia->nuevo($params);
			if ( $id ) {
				echo $id;
			} else {
				echo 0;
			}
		
	}catch (Exception $e){
		echo  $e;
	}
}








if ( $option == 'm' ) {
			$idmembresia	= $data['idmembresia'];  
			$nombre		= $data['nombre'];  
			$valor		= $data['valor'];  
		    $duracion		= $data['duracion'];  
			$idestado		= $data['idestado']; 
			$nusuarios		=  $data['nusuarios'];
			$detalle		=  $data['detalle'];
			$idmonetario		=  $data['idmonetario'];
			$idtiempo		=  $data['idtiempo'];
			try {
		    $params = array($nombre,$valor,$duracion,$idestado,$nusuarios,$detalle,$idmonetario,$idtiempo,$idmembresia);
			$save   = $omembresia->actualizar($params);
			if ($save) {
				echo $idmembresia;
			} else {
				echo "400";
			}
		    }catch (Exception $e){
		     	echo  $e;
		    }
	
}


if($option=="e"){
	try{
		$id		= $_REQUEST['id'];
		$update   = $omembresia->eliminar($id);
		if ( $update ) {
			echo "0";
		} else {
			echo "400";
		}
	}catch(Exception $e){
		echo "error";
	}
}



if($option=="ha"){
	try{
		$id		= $_REQUEST['id'];
		$update   = $omembresia->habilitar($id);
		if ( $update ) {
			echo "1";
		} else {
			echo "0";
		}
	}catch(Exception $e){
		echo "error";
	}
}


if($option=="deshabilitado"){
	try{
		$id		= $_REQUEST['id'];
		$update   = $omembresia->deshabilitar($id);
		if ( $update ) {
			echo "1";
		} else {
			echo "0";
		}
	}catch(Exception $e){
		echo "error";
	}
}




if($option=="getcosto"){
	try{
		//$idmembresia    = $_REQUEST['idmembresia'];
		$idmembresia		= $_REQUEST['idmembresia'];
		$valor  = $omembresia->getOneCosto($idmembresia);
		 echo $valor;
	}catch(Exception $e){
		 echo "error";
	}
}


if($option=="getcp"){
	try{
		$idmembresia		= $_REQUEST['idmembresia'];
		$valor  = $omembresia->getnumusuario($idmembresia);
		 echo $valor;
	}catch(Exception $e){
		 echo "error";
	}
}


?>