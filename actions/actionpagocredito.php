<?php 
include_once '../data/dataBase.php';
// Classes
include_once '../classes/cpagocredito.php';
include_once '../classes/cpagos.php';

$opagos 	= new pago();
$opagocredito 	= new pagocredito();
$option = '';
session_start();
date_default_timezone_set ("America/La_Paz");
if (isset($_REQUEST['valor'])){
    $data 	= $_REQUEST['valor'];
    $option 	= $data['opt'];
  }else{
      $option =  $_REQUEST['opt'];
  }
  
  $option 	= $_REQUEST['opt'];

  if ( $option == 'np' ) {
	try{
		//$idlesion		= $data['idlesion']; 
		$idusuario		= $_POST['usuario']; 
		$fecha	= $_POST['fecha']; 
		$paciente		= $_POST['paciente']; 
		$monto		= $_POST['monto']; 
		$credito1		= $_POST['credito1']; 
		//$formapago		=$_POST['formapago'];
		$nota=$_POST['nota'];
		$pago=  $_POST['idpago'];
		$fechaCompleta=$fecha." ". date("H:i:s");

			
            $params = array($monto,$fechaCompleta,$idusuario,$pago,$nota,$credito1);
            if($pago!=0){
                $id   = $opagocredito->nuevo($params);
                if ( $id ) {
                    echo $id;
                } else {
                    echo "400";
                }
            }echo $pago;
			
		
	}catch (Exception $e){
		echo  $e;
	}
}
if($option == 'prueba'){
	
	$idpago=$_GET['pago'];
	$id=$opagocredito->getprueball($idpago);
	echo $id;
	
}
if($option == 'todos'){
	$idpago=$_POST['pago'];

	$id=$opagocredito->getAll($idpago);
	if($id){
		echo $id;
	}else{
		echo '400';
	}
	
	
}
if($option=='eli'){
	try {
		$idpago=$_POST['id'];
	$id = $opagocredito->eliminar($idpago);
		if($id){
			echo "0";
		}else{
			echo "400";
		}
	} catch (\Throwable $th) {
		//throw $th;
	}
	
}
if($option=='eliminarTotal'){
	try {
		$idpago=$_POST['id'];
		$id = $opagocredito->eliminar($idpago);
		if($id){
			$opagos->eliminar($idpago);
			echo "0";
		}else{
			echo "400";
		}
	} catch (\Throwable $th) {
		//throw $th;
	}
	
}

if($option=='reporteIngreso'){
	try {
		$inicio=$_REQUEST['fechainicio'];
		$fin=$_REQUEST['fechafin'];
		$clinica=$_REQUEST['clinica'];
		$id = $opagocredito->reporteIG($inicio,$fin,$clinica);
		if($id){
			echo $id;
		}else{
			echo "400";
		}
	} catch (\Throwable $th) {
		//throw $th;
	}
}
?>