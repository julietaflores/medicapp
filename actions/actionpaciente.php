<?php

session_start();
if (!isset($_SESSION['csmart']['idusuario'])) {
	Header ("Location: login.php");
  }

// DataBase
include_once '../data/dataBase.php';
// Classes
include_once '../classes/cpaciente.php';
include_once '../classes/chistorial_medico.php';


$opaciente  	= new paciente();
$ohistorial_medico = new historial_medico();
$option = '';



if (isset($_REQUEST['valor'])){
  $data 	= $_REQUEST['valor'];
  $option 	= $data['opt'];
}else{
	$option =  $_REQUEST['opt'];
}

//$option 	= $_REQUEST['opt'];

if($option == 'j'){

	$filtro 	= $_REQUEST['q'];
	
	$pac = $opaciente->getAllfiltrado($filtro);
	
	// foreach ($pac AS $id => $array) {
	// 	$val[$array['idpaciente']] = $array['nombre'];
	// }
	
	echo json_encode($pac);
													
										
	//echo "[{\"id\":\"14\",\"title\":\"New Event\",\"start\":\"2016-06-24 16:00:00\",\"allDay\":false}]";
}

if ( $option == 'n' ) {
	try{
		$nombre		= $data['nombre']; 
		$apellido		= $data['apellido']; 
		$identificacion		= $data['identificacion']; 
		$genero		= $data['genero']; 
		$imagen		= $data['img']; 
		$direccion		= $data['direccion']; 
		$telefono1		= $data['telefono1']; 
		$telefono2		= $data['telefono2']; 
		$correo		= $data['correo']; 
		$fecha_nac		= $data['fecha_nac']; 
		$fecha_cre		= $data['fecha_cre']; 
		$notas		= $data['notas']; 
		$idestado		= 1; 
		$idusuario		= $_SESSION['csmart']['idusuario'];
		$idclinica		= $_SESSION['csmart']['clinica'];
		$iddepartamento		= $data['iddepartamento']; 
		$responsable		= $data['responsable']; 
		$telefono_resp		= $data['telefono_resp']; 
		$medico_fam		= $data['medico_fam']; 
		$ocupacion		= $data['ocupacion']; 


			$params = array($nombre,$apellido,$identificacion,$genero,$imagen,$direccion,$telefono1,$telefono2,$correo,$fecha_nac,$fecha_cre,$notas,$idestado,$idusuario,$idclinica,$iddepartamento,$responsable,$telefono_resp,$medico_fam,$ocupacion);
			$id   = $opaciente->nuevo($params);
			if ( $id ) {

				
				echo $id;
			} else {
				echo "0";
			}
		
	}catch (Exception $e){
		echo  $e;
	}
}

if ( $option == 'm' ) {

	$idpaciente		= $data['idpaciente'];  
		$nombre		= $data['nombre'];  
		$apellido		= $data['apellido'];  
		$identificacion		= $data['identificacion'];  
		$genero		= $data['genero'];  
		$imagen		= $data['img'];  
		$direccion		= $data['direccion'];  
		$telefono1		= $data['telefono1'];  
		$telefono2		= $data['telefono2'];  
		$correo		= $data['correo'];  
		$fecha_nac		= $data['fecha_nac'];  
		$fecha_cre		= $data['fecha_cre'];  
		$notas		= $data['notas'];  
		$idestado		= 1;  
		$idusuario		= $_SESSION['csmart']['idusuario'];
		$idclinica		= $_SESSION['csmart']['clinica'];
		$iddepartamento		= $data['iddepartamento'];  
		$responsable		= $data['responsable'];  
		$telefono_resp		= $data['telefono_resp'];  
		$medico_fam		= $data['medico_fam'];  
		$ocupacion		= $data['ocupacion'];  
		
			try { 
			
			$params = array($nombre,$apellido,$identificacion,$genero,$imagen,$direccion,$telefono1,$telefono2,$correo,$fecha_nac,$fecha_cre,$notas,$idestado,$idusuario,$idclinica,$iddepartamento,$responsable,$telefono_resp,$medico_fam,$ocupacion,$idpaciente);
			$save   = $opaciente->actualizar($params);
			
			if ( $save ) {
				
				echo $idpaciente;
			} else {
				echo "0";
			}
		}catch (Exception $e){
			echo  $e;
		}
	
}
if($option=="subir"){
	try{
		$id		= $_REQUEST['id'];
		$nombre		= $_REQUEST['nombre'];
		$params = array($nombre,$id);
		$update   = $opaciente->subirIMG($params);
		if ( $update ) {
			echo $update ;
		} else {
			echo "500";
		}
	}catch(Exception $e){
		echo "400";
	}
}
if($option=="bpaciente"){
	try{
		$id		= $_REQUEST['id'];
		$update   = $opaciente->bloqueo($id);
		if ( $update ) {
			echo "0";
		} else {
			echo "400";
		}
	}catch(Exception $e){
		echo "400";
	}
}
if($option=="hpaciente"){
	try{
		$id		= $_REQUEST['id'];
		$update   = $opaciente->habilitar($id);
		if ( $update ) {
			
			echo "0";
		} else {
			echo "400";
		}
	}catch(Exception $e){
		echo "error";
	}
}
if($option=="e"){
	try{
		$id		= $_REQUEST['id'];
		$update   = $opaciente->eliminar($id);
		if ( $update ) {
			echo "0";
		} else {
			echo "400";
		}
	}catch(Exception $e){
		echo "error";
	}
}
if($option=="efoto"){
	try{
		$id		= $_REQUEST['id'];
		$px		= $_REQUEST['px'];
		$archivo		= $_REQUEST['archivo'];
		$update   = $opaciente->eliminarFoto($id);
		if ( $update ) {
			unlink("../images/paciente/".$px."/".$archivo);
        
			echo "0";
		} else {
			echo "400";
		}
	}catch(Exception $e){
		echo "error";
	}
}
if($option=="buscar"){
	$valor		= $_REQUEST['busqueda'];
	$clinica =		$_REQUEST['clinica'];

	$id = $opaciente->buscar($clinica,$valor);

	if($id){
		if(count($id)>0){
			echo $id;
		}else{
			echo 0;
		}
		
	}else{
		echo 0;
	}
}
if($option=="maspacientes"){
	$numeroPacientes=$_REQUEST['numeropa'];
	$id=$opaciente->paginadorjson($numeroPacientes);

	if($id){
		echo $id;
	}else{
		echo 'no hay mas';
	}
}
if($option=="botonbuscarpaciente"){
	$palabra=$_REQUEST['palabra'];
	$id=$opaciente->buscarPaciente($palabra);

	if($id){
		echo $id;
	}else{
		echo 'no hay mas';
	}
}

?>