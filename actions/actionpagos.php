<?php 
include_once '../data/dataBase.php';
// Classes
include_once '../classes/cpagos.php';

$opagos 	= new pago();
$option = '';
session_start();
date_default_timezone_set ("America/La_Paz");
if (isset($_REQUEST['valor'])){
    $data 	= $_REQUEST['valor'];
    $option 	= $data['opt'];
  }else{
      $option =  $_REQUEST['opt'];
  }
  
  $option 	= $_REQUEST['opt'];

  if ( $option == 'np' ) {
	try{
		// 
		$idusuario		= $_POST['usuario']; 
		$fecha			= $_POST['fecha']; 
		$paciente		= $_POST['paciente']; 
		$monto			= $_POST['monto']; 
		$tratamiento	= $_POST['tratamiento']; 
		$formapago		=$_POST['formapago'];
	 	$formaseguro		=$_POST['formaseguro'];
		$nota			=$_POST['nota'];
		$tipopago		=$_POST['tipopago'];
	
		$fechaHora		=$fecha." ".date("H:i:s");

			$params = array($formapago,$formaseguro,$tipopago,$tratamiento,$paciente,$fechaHora,$monto,$idusuario,$nota);
			$id   = $opagos->nuevo($params);
			if ( $id ) {
				echo $id;
			} else {
				echo "400";
			}
		
	}catch (Exception $e){
		echo  $e;
	}
}


if($option == 'd1seguro'){
	$fecha1 	= $_REQUEST['fecha1'];
	$fecha2 	= $_REQUEST['fecha2'];
	$idseguro 	= $_REQUEST['idseguro'];

     if($idseguro==0){
		$pac = $opagos->getAlltodoslosseguros($fecha1,$fecha2);
		if($pac){
			echo $pac;
		}else{
			echo "400";
		}

	 }else{
		$pac = $opagos->getAllxsxf1xf2($fecha1,$fecha2,$idseguro);
		if($pac){
			echo $pac;
		}else{
			echo "400";
		}
	 }


}



		if($option=='uno'){
			try {
				$idpago=$_POST['idpago'];
			$id = $opagos->getone($idpago);
				if($id){
					echo $id;
				}else{
					echo "400";
				}
			} catch (\Throwable $th) {
				//throw $th;
			}
			
		}
	if($option=='up'){
		try {
			$idpago= $_POST['idpago'];
			$idusuario		= $_POST['usuario']; 
			$fecha	= $_POST['fecha']; 
			$paciente		= $_POST['paciente']; 
			$monto		= $_POST['monto']; 
			$tratamiento		= $_POST['tratamiento']; 
			$formapago		=$_POST['formapago'];
			$nota=$_POST['nota'];
			$tipopago=$_POST['tipopago'];
			$params = array($monto,$formapago,$tratamiento,$fecha,$nota,$idpago);
			$id   = $opagos->actualizar($params);
			if ( $id ) {
				echo $id;
			} else {
				echo "400";
			}
		} catch (\Throwable $th) {
			//throw $th;
		}
	}
	if($option=='eli'){
		try {
			$idpago=$_POST['id'];
		$id = $opagos->eliminar($idpago);
			if($id){
				echo "0";
			}else{
				echo "400";
			}
		} catch (\Throwable $th) {
			//throw $th;
		}
		
	}
	if($option=='reporteCreditos'){
		try {
			$inicio=$_POST['fechainicio'];
			$fin=$_POST['fechafin'];
			$usuario=$_POST['clinica'];
			$id = $opagos->todosloscreditos($inicio,$fin,$usuario);
			if($id){
				
				echo $id;
			}else{
				echo "400";
			}
		} catch (\Throwable $th) {
			//throw $th;
		}
	}
?>