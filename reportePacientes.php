<?php
   session_start();
   if (!isset($_SESSION['csmart']['idusuario'])) {
   Header ("Location: login.php");
   }
    include 'headercondicion.php';
    include 'classes/cpaciente.php';   
    include 'classes/cpagos.php';
    include 'classes/cpagocredito.php';
  
    $opagos = new pago();
    $opagocredito=new pagocredito();
    $opaciente = new paciente();

    $nombre='';
    $apellido='';
    $telefono="";
    $correo='';
    $pagado='';
    $debe="";
    $tratamiento='';
    $vpaciente 		= $opaciente->getAll1();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reporte Pacientes </title>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">

  <?php include 'header.php'; ?>
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  <script type="text/javascript"> </script>
</head>
<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
            <?php include "menu.php" ?>
        </div>
      </div>


           <?php include "top_nav.php" ?>
   
      <div class="right_col" role="main">

<div class="">
  
 
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Administración de Pacientes</h2>
            
           <div class="title_right">
    
    </div><div class="clearfix"></div>
</div>
<?if(!$vpaciente){?>
  <div class="col-md-12" style="height: 400px; text-align: center">
  <h2>Bienvenido Registra tu primer Paciente</h2>
  <a class="btn btn-success " href="pacientenuevo.php">Nuevo Paciente</a>
  </div>
  <?}else{?>		
        <div  class="x_content" >
                <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">
                   
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                    <tr>
                      <th>#</th>
                        <th>nombre</th>
                        <th>apellido</th>
                        <th>identificación</th>
                        <th>teléfono </th>
                        <th>celular 	</th>
                        <th>correo</th>
                        <th>estado</th>
                        
                        
                       
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  if($vpaciente){
                    $i=1;
                    foreach ($vpaciente AS $id => $array) {
                  ?>
                    <tr><td align="left"><?=$i;?></td>
                      <td align="left"><?=$array['nombre'];?></td>
                      <td align="left"><?=$array['apellido'];?></td>
                      <td align="left"><?=$array['identificacion'];?></td>
                      <td align="left"><?=$array['telefono1'];?></td>
                      <td align="left"><?=$array['telefono2'];?></td>
                      <td align="left"><?=$array['correo'];?></td>
                      <td align="left"><?php if($array['idestado']==1){ ?> <span class="label label-success">Activo</span> 
                              <?php }else{ ?>
                                  <span class="label label-warning">Inactivo</span>
                                <?php };?></td>	
                   									
                        
                    </tr>
                  <?php
                    $i++;}
                  }	
                  ?>
                </tbody>
        </table>
                      
        </div>
                </div>
              </div>

    
<!--paciente cartas------------------------------------------------------------------------------------------>
              
             <!------------------------------------------------------->
        


</div>
    <?}?>
      </div>
    </div>
  </div>
</div>
<form style="display: hidden" action="pacienteperfil.php" method="POST" id="formoculto">
                                    <input type="hidden" id="idpacientes" name="id" value=""/>
                                    
</form>
<form style="display: hidden" action="pacientenuevo.php" method="POST" id="formularioeditar">
                                    <input type="hidden" id="idpacienteditar" name="id" value=""/>
                                    <input type="hidden" id="modificarpaciente" name="opt" value="">
                                    
</form>

<div style="height: 100px;"></div>
<?php include "piep.php" ?>

</div>

  <script src="js/bootstrap.min.js"></script>
  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="js/moment/moment.min.js"></script>
  <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>
  
  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>
  <script src="js/custom.js"></script>
  <!-- form validation -->
  <script src="js/validator/validator.js"></script>
  <!-- PNotify -->
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  <script src="js/datatables/jquery.dataTables.min.js"></script>
        <script src="js/datatables/dataTables.bootstrap.js"></script>
        <script src="js/datatables/dataTables.buttons.min.js"></script>
        <script src="js/datatables/buttons.bootstrap.min.js"></script>
        <script src="js/datatables/jszip.min.js"></script>
        <script src="js/datatables/pdfmake.min.js"></script>
        <script src="js/datatables/vfs_fonts.js"></script>
        <script src="js/datatables/buttons.html5.min.js"></script>
        <script src="js/datatables/buttons.print.min.js"></script>
        <script src="js/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="js/datatables/dataTables.responsive.min.js"></script>
        <script src="js/datatables/responsive.bootstrap.min.js"></script>
        <script src="js/datatables/dataTables.scroller.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
  <script>


    

 tabla=$("#datatable-buttons").DataTable({
    dom: 'Bfrtip',
        buttons: [
            'print','excel', 'pdf'
        ], "order": [[ 0, "asc" ]]
                                });

  

  
  </script>
 
   

 

</body>
</html>