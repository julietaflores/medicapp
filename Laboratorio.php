<?php

session_start();
   if (!isset($_SESSION['csmart']['idusuario'])) {
   Header ("Location: login.php");
   }
   include 'headercondicion.php';
    include 'classes/cpaciente.php';
    
    include 'classes/claboratorio.php';
    include 'classes/ctrabajoslab.php';
   
   $oPacientes    =new paciente();
  $olaboratorio = new Laboratorio();
   $otrabajolab   =  new TrabajosLab();

    
    $vlab 		= $olaboratorio->getAll($_SESSION['csmart']['clinica']);
   $vtrablab  = $otrabajolab->getAll($_SESSION['csmart']['clinica']);
   $vpacientes=$oPacientes->getAll1();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laboratorios</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
  
  <?php include 'header.php'; ?>
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  <script type="text/javascript"> </script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/js/select2.min.js"></script>
</head>
<body class="nav-md">

  <div class="container body">


    <div class="main_container">

      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          
          <!-- sidebar menu -->
            <?php include "menu.php" ?>
          <!-- /sidebar menu -->

         
        </div>
      </div>

      <!-- top navigation -->
           <?php include "top_nav.php" ?>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">

<div class="">
  
 
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Administración de Laboratorios</h2>
            
           <div class="title_right">
         
      
            </div>
    <div class="clearfix"></div>
        </div>
		
        <div  class="x_content" >
            <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
               <li id="tab1" role="presentation" class="active"><a href="#tab_content1" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="true">Listado</a>
              </li>
           <li id="tab2" role="presentation" class=""><a href="#tab_content2" id="home-tab" role="tab" data-toggle="tab" aria-expanded="false">Trabajos</a>
              </li>
              
            </ul>
            <div id="myTabContent" class="tab-content">
              <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                <!-- tab 1 -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                 
                    <h2>Laboratorios Registrados</h2>
                    
                    <div class="clearfix"></div>
                      </div>
                    
                  <div class="x_content">
                      <a class="btn btn-default modalnuevolab" data-toggle="modal" data-target="#modalformulariopago" >Agregar</a>
             
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                    <tr>
                      <th>#</th>
                        <th>Laboratorio</th>
                        <th>Teléfono</th>
                        <th>Correo</th>
                        <!--<th>estado</th>-->
                        
                       <th>Opciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  if($vlab){
                    $i=1;
                    foreach ($vlab AS $id => $array) {
                  ?>
                    <tr><td align="left"><?=$i;?></td>
                      <td align="left"><?=$array['nombre'];?></td>
                      <td align="left"><?=$array['telefono'];?></td>
                      <td align="left"><?=$array['correo'];?></td>
                   
                      
                    <td>
                      
                          <a data-toggle="modal" data-target="#modalformulariopago"  onclick="ireditar(<?=$array['idlaboratorio'];?>)" title="Perfil" >
                            <span class="label label-primary" >
                              <i class="fa fa-pencil" title="Modificar"></i>
                            </span>
                          </a>
                          
                           
                          <span class="label label-default" onClick="eliminarlab(<?=$array['idlaboratorio']?>)" >
                            <i class="fa fa-trash-o" title="Eliminar"></i>	
                        
                          </span>	
                          
                      </td>										
                        
                    </tr>
                  <?php
                    $i++;}
                  }	
                  ?>
                </tbody>
        </table>
                      
                  </div>
                </div>
              </div>

    </div>
<!--trabajos ------------------------------------------------------------------------------------------>
        <div role="tabpanel" class="tab-pane fade  in" id="tab_content2" aria-labelledby="home-tab">
          <!-- tab 1 -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                 
                    <h2>Trabajos Registrados</h2>
                    
                    <div class="clearfix"></div>
                      </div>
                     
                  <div class="x_content">
                      <a class="btn btn-default modaltrabajolab" data-toggle="modal" data-target="#modaltrabajo" >Agregar</a>
        
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                    <tr>
                        <th>#</th>
                        <th>Laboratorio</th>
                        <th>Paciente</th>
                        <th>Detalle</th>
                        <th>Precio</th>
                        <th>Fecha Envío</th>
                        <th>Fecha Entrega</th>
                        <th>Estado</th>
                       <th>Opciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  if($vtrablab){
                    $i=1;
                    foreach ($vtrablab AS $id => $array) {
                  ?>
                    <tr><td align="left"><?=$i;?></td>
                    <td align="left"><?=$array['nombrelab'];?></td>
                      <td align="left"><?=$array['nombrepaciente'];?></td>
                      <td align="left"><?=$array['detalle'];?></td>
                      <td align="left"><?=$array['precio']?></td>
                     
                    <td align="left"><? 
                                      $source = $array['fechaEnvio'];
                                      $date = new DateTime($source);
                                      echo $date->format('d-m-Y');      ?></td>
                    <td align="left"><?
                                      $source=$array['fechaEntrega'];
                                      $date = new DateTime($source);
                                      echo $date->format('d-m-Y');?>
                                      </td>
                   <td align="left"><?php if($array['idestado']==1){ ?>
																	<div class="btn-group">
												                    <button type="button" class="btn btn-warning btn-xs">Enviado</button>
												                    <button type="button" class="btn btn-warning btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
												                      <span class="caret"></span>
												                    </button>
												                    <ul class="dropdown-menu" role="menu">
												                      <li><a href="#" onClick="aprobar(<?=$id?>,2)">Recibido</a>
												                      </li>
												                      <li><a href="#" onClick="aprobar(<?=$id?>,0)">Cancelado</a>
												                      </li>
												                    </ul>
												                 </div>
																	
															 	<?php }elseif($array['idestado']=='2'){ ?>
															 		<div class="btn-group">
												                    <button type="button" class="btn btn-success btn-xs">Recibido</button>
												                    <button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
												                      <span class="caret"></span>
												                    </button>
												                    <ul class="dropdown-menu" role="menu">
												                      <li><a href="#" onClick="aprobar(<?=$id?>,1)">Enviado</a>
												                      </li>
												                      <li><a href="#" onClick="aprobar(<?=$id?>,0)">Cancelado</a>
												                      </li>
												                    </ul>
												                 </div>
															 		
															 	<?php }elseif($array['idestado']=='0'){ ?>
																 	<div class="btn-group">
													                    <button type="button" class="btn btn-danger btn-xs">Cancelado</button>
													                    <button type="button" class="btn btn-danger btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
													                      <span class="caret"></span>
													                    </button>
													                    <ul class="dropdown-menu" role="menu">
													                      <li><a href="#" onClick="aprobar(<?=$id?>,1)">Enviado</a>
													                      </li>
													                      <li><a href="#" onClick="aprobar(<?=$id?>,2)">Pendiente</a>
													                      </li>
													                    </ul>
													                 </div>
															 		
															 	<?php }; ?>
                      </td>	
                    <td>
                          <a data-toggle="modal" data-target="#modaltrabajo" onclick="ireditartabajo(<?=$array['idtrabajo'];?>)" title="Perfil" >
                            <span class="label label-primary" >
                              <i class="fa fa-pencil" title="Modificar"></i>
                            </span>
                          </a>
                          
                           
                          <span class="label label-default" onClick="eliminartrabajo(<?=$array['idtrabajo']?>,<?=$array['idcita']?>)" >
                            <i class="fa fa-trash-o" title="Eliminar"></i>	
                        
                          </span>	
                          
                      </td>										
                        
                    </tr>
                  <?php
                    $i++;}
                  }	
                  ?>
                </tbody>
        </table>
                      
                  </div>
                </div>
              </div>
          </div>
             <!------------------------------------------------------->
        </div>
          </div>


        </div>
    
      </div>
    </div>
  </div>
</div>

<!--------------------------------modal laboratorio--->

                <div class="modal fade" id="modalformulariopago" tabindex="-1" role="dialog" aria-labelledby="modalformulariopago" aria-hidden="true">
                   <div class="modal-dialog" role="document">
                      <div class="modal-content">
                         <div class="modal-header">
                                            <h5 class="modal-title" id="modalLab">Nuevo</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                          </div>
                                    <div class="modal-body">
                     <div id="formularioPagos" class="container" >
                        <div class="col-md-12">

                          <div class="form-group">
                                <input id="idlabo" type="hidden" >
                              <label for="nombre">Laboratorio</label>
                              <input id="nombre" class="form-control" type="text">
                              <label for="correo">Correo</label>
                              <input class="form-control" name="correo" id="correo" type="email" >
                              <label for="telefono">Teléfono</label>
                              <input class="form-control" name="telefono" id="telefono" value="0" type="tel" >
                          </div>
                        </div>
                
                      </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div id="guadarFormPago">
                                        
                                        <a class="btn btn-primary guardarLab" >Agregar</a>
                                        <a style="display: none;" class="btn btn-primary actualizarLab" >Actualizar</a>
                                        <a class="btn btn-danger" href="javascript:cerrarpagos() " data-dismiss="modal">Cancelar</a>
                                   
                                        </div>
                                       
                                    </div>
                        </div>
                    </div>
                </div>
                   <!------------------------------------>                 
<!------------------------------modal trabajo----------------------->
<div class="modal fade" id="modaltrabajo" tabindex="-1" role="dialog" aria-labelledby="modalformulariopago" aria-hidden="true">
                   <div class="modal-dialog" role="document">
                      <div class="modal-content">
                         <div class="modal-header">
                                            <h5 class="modal-title" id="modalTrabajo">Nuevo Trabajo</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                          </div>
                                    <div class="modal-body">
                     <div id="formularioPagos" class="container" >
                        <div class="col-md-12">

                          <div class="form-group">
                              <input type="hidden" value="1" id="idtrabajolab">
                              <label for="paciente">Paciente</label>
                              

                              <select class="form-control" name="paciente" id="paciente">
                                  <option value="1" selected="selected">Sin paciente</option>
                                  <?if($vpacientes){
                                      foreach ($vpacientes AS $id => $array) {?>
                                        <option value="<?=$array['idpaciente']?>"><?=$array['nombre']?></option>
                                     

                                  <? }   }?>
                              </select>
                              <label for="Laboratorio">Laboratorio</label>
                              

                              <select class="form-control" name="Laboratorio" id="Laboratorioid">
                                  
                                  <?if($vlab){
                                      foreach ($vlab AS $id => $array) {?>
                                        <option value="<?=$array['idlaboratorio']?>"><?=$array['nombre']?></option>
                                     

                                  <? }   }?>
                              </select>
                              

                              <label for="detalle">detalle</label>
                              <input class="form-control" name="detalle" id="detalle" type="text" >
                              <label for="precio">Precio</label>
                              <input class="form-control" name="precio" id="precio" type="text" >
                              <label for="fechaini">Fecha envio</label>
                              <input class="form-control" name="fechaini" id="fechaini" value="<?php echo date('Y-m-d');?>" type="date" >
                              <label for="fechaent">Fecha entrega</label>
                              <input class="form-control" name="fechaent" id="fechaent" value="<?php echo date('Y-m-d');?>" type="date" >
                          </div>
                        </div>
                
                      </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div id="guadarFormPago">
                                        <a class="btn btn-primary guardarTrabajolab" >Agregar</a>
                                        <a class="btn btn-primary actualizarTrabajolab" >Actualizar</a>
                                        <a class="btn btn-danger" href="javascript:cerrarpagos() " data-dismiss="modal">Cancelar</a>
                                   
                                        </div>
                                      
                                    </div>
                        </div>
                    </div>
                </div>
<!----------------------------------------------------------------------------------->
<!-- footer content -->
<center> <?php include "piep.php" ?> </center>
<!-- /footer content -->

</div>

  <script src="js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="js/moment/moment.min.js"></script>
  <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>
  
  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>
  <script src="js/custom.js"></script>
  <!-- form validation -->
  <script src="js/validator/validator.js"></script>
  <!-- PNotify -->
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  <script src="js/datatables/jquery.dataTables.min.js"></script>
        <script src="js/datatables/dataTables.bootstrap.js"></script>
        <script src="js/datatables/dataTables.buttons.min.js"></script>
        <script src="js/datatables/buttons.bootstrap.min.js"></script>
        <script src="js/datatables/jszip.min.js"></script>
        <script src="js/datatables/pdfmake.min.js"></script>
        <script src="js/datatables/vfs_fonts.js"></script>
        <script src="js/datatables/buttons.html5.min.js"></script>
        <script src="js/datatables/buttons.print.min.js"></script>
        <script src="js/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="js/datatables/dataTables.responsive.min.js"></script>
        <script src="js/datatables/responsive.bootstrap.min.js"></script>
        <script src="js/datatables/dataTables.scroller.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
  <script>




    $(".modalnuevolab").click(function(){
      $(".actualizarLab").hide();
      $('.guardarLab').show();
      $("#nombre").val('');
      $("#telefono").val('');
      $("#correo").val('');
      $("#modalLab").text('Nuevo');
    });
    function ireditar(id){
      $(".actualizarLab").show();
      $('.guardarLab').hide();
        $.ajax({
          url: 'actions/actionlaboratorio.php?opt=uno',
                      data: 'id=' + id  ,
                      type: "POST",
                      dataType:"json",
                      success: function(data) {
                        $.each(data, function(i, item) {
                          $("#nombre").val(item.nombre);
                          $("#telefono").val(item.telefono);
                          $("#correo").val(item.correo);
                          $("#modalLab").text('Actualizar');
                          $("#idlabo").val(item.idlaboratorio);
                        });
                    }
        });
      }
      $(".actualizarLab").click(function(){
          var nombre=  $("#nombre").val();
          var telefono=  $("#telefono").val();
          var correo=  $("#correo").val();
          var clinica=<?=$_SESSION['csmart']['clinica']?>;
          var idlab=$("#idlabo").val();
        $.ajax({
          url: 'actions/actionlaboratorio.php?opt=up',
                data: 'clinica='+clinica +
                      '&nombre='+nombre+
                      '&telefono='+telefono+
                      '&idlab='+idlab+
                      '&correo='+correo,
                      type: "POST",
                      dataType:"json",
                      success: function(data) {
                        new PNotify({
                                title: 'Datos Actualizados',
                                  text: 'Todos los datos fueron guardados. Puede continuar.!',
                                  type: 'info'
                                });
                        window.setTimeout("location.reload(true);",500);
                    }
        });
      });
      function eliminarlab(id){
        if(confirm("Atención! Va a proceder a eliminar este registro. Desea continuar?"))
                {
        $.ajax({
          url: 'actions/actionlaboratorio.php?opt=eli',
                data: '&idlab='+id,
                      
                      type: "POST",
                      dataType:"json",
                      success: function(data) {
                        new PNotify({
                                title: 'Datos Eliminados',
                                  text: 'Todos los datos fueron guardados. Puede continuar.!',
                                  type: 'info'
                                });
                        window.setTimeout("location.reload(true);",500);
                    }
        });
                }
      }
      //Laboratorios
      $(".guardarLab").click(function(){
        debugger;
          var nombre=  $("#nombre").val();
          var telefono=  $("#telefono").val();
          var correo=  $("#correo").val();
          var clinica=<?=$_SESSION['csmart']['clinica']?>;
          $.ajax({
            url: 'actions/actionlaboratorio.php?opt=nuevo',
                      data: 'clinica='+clinica +
                      '&nombre='+nombre+
                      '&telefono='+telefono+
                      '&correo='+correo,
                      type: "POST",
                    
                      success: function(data) {
                        new PNotify({
                                title: 'Datos Insertados',
                                  text: 'Todos los datos fueron guardados. Puede continuar.!',
                                  type: 'info'
                                });
                        window.setTimeout("location.reload(true);",500);
                      } 
          });
      });


    //////////////////////trabajo lab//////////////////////////////////////////////////////////////////
  
    $("#paciente").select2({
        dropdownParent: $("#modaltrabajo"),
        width: '100%'
      });
  
   $(".modaltrabajolab").click(function(){
     debugger;
     var fecha= new Date(Date.now());
      $(".actualizarTrabajolab").hide();
      $('.guardarTrabajolab').show();
        $("#paciente").val(1);
        $("#detalle").val('');
        $("#precio").val(0);
        $("#fechaini").val(fechas(fecha));
        $("#fechaent").val(fechas(fecha));
        $("#Laboratorioid").val(1);
   });
   function ireditartabajo(id){
     debugger;
     var fechaini;
     var fechafin;
  
      $(".actualizarTrabajolab").show();
      $('.guardarTrabajolab').hide();
                    $.ajax({
                      url: 'actions/actiontrabajolab.php?opt=uno',
                      data: 'idtrabajo=' + id  ,
                      type: "POST",
                      dataType:"json",
                      success: function(data) {
                        
                        $.each(data, function(i, item) {
                          //$("#paciente option[value="+item.idpaciente+"]").attr('selected', 'selected');
                          $("#paciente").val(item.idpaciente);
                          $("#detalle").val(item.detalle);
                          fechaini=fechas(item.fechaEnvio);
                          fechafin=fechas(item.fechaEntrega);
                          $("#precio").val(item.precio);
                          $("#idtrabajolab").val(item.idtrabajo);
                          $("#fechaini").val(fechaini);
                          $("#fechaent").val(fechafin);
                          $("#Laboratorioid").val(item.idlaboratorio);
                         
                        });
                        console.log(data);
                    }
        });
   }
   $(".actualizarTrabajolab").click(function(){
          var user=<?=$_SESSION['csmart']['idusuario']?>;
          var paciente=  $("#paciente").val();
          var lab =$("#Laboratorioid").val();
          var detalle=   $("#detalle").val();
          var fechaini=  $("#fechaini").val()+" 10:00:00";
          var fechaent=  $("#fechaent").val()+" 10:00:00";
          var fechafin=$("#fechaent").val()+" 11:00:00";
          var clinica=<?=$_SESSION['csmart']['clinica']?>;
          var idtrabajo=$("#idtrabajolab").val();
          var precio= $("#precio").val();
          $.ajax({
            url: 'actions/actiontrabajolab.php?opt=up',
                      data: 'clinica='+clinica +
                      '&paciente='+paciente+
                      '&detalle='+detalle+
                      '&fechaent='+fechaent+
                      '&fechaini='+fechaini+
                      '&end='+fechafin+
                      '&lab='+lab+
                      '&idtrabajo='+idtrabajo+
                      '&user='+user+
                      '&precio='+precio,
                      type: "POST",
                    
                      success: function(data) {
                        new PNotify({
                                title: 'Datos Insertados',
                                  text: 'Todos los datos fueron guardados. Puede continuar.!',
                                  type: 'info'
                                });
                        window.setTimeout("location.reload(true);",500);
                        console.log(data);
                      }
          });
   });

  
    //guardar trabajo
    $(".guardarTrabajolab").click(function(){
      debugger;
          var user=<?=$_SESSION['csmart']['idusuario']?>;
          var paciente=  $("#paciente").val();
          var lab =$("#Laboratorioid").val();
          var detalle=   $("#detalle").val();
          var fechaini=  $("#fechaini").val()+" 12:00:00";
          var fechaent=  $("#fechaent").val()+" 12:00:00";
          var fechafin=$("#fechaent").val()+" 12:30:00";
          var clinica=<?=$_SESSION['csmart']['clinica']?>;
          var precio= $("#precio").val();
          if(lab>=1){

         
      $.ajax({
            url: 'actions/actiontrabajolab.php?opt=nuevo',
                      data: 'clinica='+clinica +
                      '&paciente='+paciente+
                      '&detalle='+detalle+
                      '&fechaent='+fechaent+
                      '&fechaini='+fechaini+
                      '&end='+fechafin+
                      '&lab='+lab+
                      '&user='+user+
                      '&precio='+precio,
                      type: "POST",
                    
                      success: function(data) {
                        new PNotify({
                                title: 'Datos Insertados',
                                  text: 'Todos los datos fueron guardados. Puede continuar.!',
                                  type: 'info'
                                });
                        window.setTimeout("location.reload(true);",500);
                        console.log(data);
                      }
          });
        }
    });
function eliminartrabajo(id,cita){
  $.ajax({
            url: 'actions/actiontrabajolab.php?opt=eli',
                      data: 'id='+id +
                      
                      '&cita='+cita,
                      type: "POST",
                    
                      success: function(data) {
                        new PNotify({
                                title: 'Datos Eliminados',
                                  text: 'Todos los datos fueron guardados. Puede continuar.!',
                                  type: 'info'
                                });
                        window.setTimeout("location.reload(true);",500);
                        console.log(data);
                      }
          });
}

    /////////////////////////estado trabajo//////////////////////////
    function aprobar(id,estado){
      debugger;
				var form = "valor"; 
						$.ajax({
							url: 'actions/actiontrabajolab.php?opt=aprotrabajo',
							type:'POST',
							data: 'idtrab='+id+
                    '&estado='+estado
						}).done(function( data ){
							
							if(data == 0){
								new PNotify({
				                   title: 'Trabajo Recibido',
				                    text: 'Todos los datos fueron guardados. Puede continuar.!',
				                    type: 'info'
				                  });
                          window.setTimeout("location.reload(true);",2500);
							}
							else{
								new PNotify({
				                     title: 'Error en accion',
				                    text: 'No se puedieron guardar los datos, intente de nuevo.',
				                    type: 'error'
				                 });
                         window.setTimeout("location.reload(true);",2500);
					}				
			  	});
      }
      
       /////fechas
   function fechas(fecha){
    var today = new Date(fecha);
      var dd = today.getDate();

      var mm = today.getMonth()+1; 
      var yyyy = today.getFullYear();
      if(dd<10) 
      {
          dd='0'+dd;
      } 

      if(mm<10) 
      {
          mm='0'+mm;
      } 
      today = yyyy+'-'+mm+'-'+dd;
      return today;
        }
  </script>
 
   

 
 <div style="height: 100px;"></div>
</body>
</html>