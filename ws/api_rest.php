<?php

header("Content-Type: text/html;charset=utf-8");
   header('Content-Type: application/json');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
		
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	    exit(0);
}

	require 'Slim/Slim.php';
	require '../PHPmailer/PHPMailerAutoload.php';
	require '../PHPmailer/class.phpmailer.php';

	$app = new Slim();

	$app->get('/recuperar_correo/:email','recuperar_correo');
	$app->get('/getClinica','getClinica');
	$app->get('/getUser','getUser');
	$app->get('/getProducto','getProducto');
	$app->get('/getPresupuesto','getPresupuesto');
	$app->get('/getPresupuestoDetalle','getPresupuestoDetalle');
	$app->get('/getitemsFactura','getitemsFactura');
	$app->get('/getPaciente','getPaciente');
	$app->get('/getFactura','getFactura');
	$app->get('/getIncidencia','getIncidencia');
	//Enviar confirmacion

	$app->get('/rest_presu','rest_presu');
	$app->get('/rest_presuD','rest_presuD');

//getAvisoIncidenciaA
$app->get('/getAvisoIncidenciaA/:idi','getAvisoIncidenciaA');
$app->get('/getAvisoIncidenciaU/:idi','getAvisoIncidenciaU');
	$app->get('/getAvisoNuevoUsuarioU/:email/:usuario','getAvisoNuevoUsuarioU');
	$app->get('/getAvisoNuevoUsuarioA','getAvisoNuevoUsuarioA');
	$app->get('/getSolicitarMembresia/:idusuario/:membresia/:estado','getSolicitarMembresia');
	$app->get('/getCompraMembresia/:idmc','getCompraMembresia');
	$app->get('/getActivacionUsuario/:idusuario','getActivacionUsuario');
	$app->get('/enviarmail/:idusuario/:email','mailreferido');

	$app->get('/getrecordatorioAdmin/:idusuario/:nombrem','getrecordatorioAdmin');
	$app->response()->header('Content-Type','application/json','charset=ISO-8859-1');
	
//CORREMOS EL API
	$app->run();
	 
//CREAMOS LA CONEXION A LA DB
	function getConexion(){
//192.254.185.66
$dbhost     = "localhost";
$dbuser     = "rigoarri_clinica";
$dbname   = "rigoarri_clinicapp";
$dbpass = 'cl1n1c@pp';

try {
	$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $dbh;
} catch (\Throwable $th) {
	//throw $th;
}
		
	}


   

	function getUser(){
		$callback = isset($_GET['callback']) ? $_GET['callback'] : false;
		$db = getConexion();
		$arrDataTotal = array();
        	
		$sql = "SELECT * from usuario;";
					$stmt	= $db->prepare($sql);
					$stmt->execute();
					if($stmt->rowCount()>0){
						while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
							$arrData['idusuario'] 					= trim($data['idusuario']);
							$arrData['nombre'] 					= trim($data['nombre']);
							$arrData['idestado'] 					= trim($data['idestado']);
							$arrData['idtipoUsuario'] 					= trim($data['idtipoUsuario']);
							$arrData['idclinica'] 					= trim($data['idclinica']);
							array_push($arrDataTotal,$arrData);
						}

					}
		$output = $arrDataTotal;
		$output = json_encode($output);

		if ($callback) {
			echo sprintf('%s(%s)', $callback, $output);
		} else {
			echo $output;
		}
	}


	function getClinica(){
		$callback = isset($_GET['callback']) ? $_GET['callback'] : false;
		$db = getConexion();
		$arrDataTotal = array();
        	
		$sql = "SELECT * from clinica;";
					$stmt	= $db->prepare($sql);
					$stmt->execute();
					if($stmt->rowCount()>0){
						while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
							$arrData['idclinica'] 					= trim($data['idclinica']);
							$arrData['nombre'] 					= trim($data['nombre']);
							$arrData['imagen'] 					= trim($data['imagen']);
							array_push($arrDataTotal,$arrData);
						}

					}
		$output = $arrDataTotal;
		$output = json_encode($output);

		if ($callback) {
			echo sprintf('%s(%s)', $callback, $output);
		} else {
			echo $output;
		}
	}



	function getIncidencia(){
		$callback = isset($_GET['callback']) ? $_GET['callback'] : false;
		$db = getConexion();
		$arrDataTotal = array();
        	
		$sql = "SELECT * from incidencia;";
					$stmt	= $db->prepare($sql);
					$stmt->execute();
					if($stmt->rowCount()>0){
						while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
							$arrData['idusuario'] 					= trim($data['idusuario']);
							$arrData['asunto'] 					= trim($data['asunto']);
							$arrData['detalle'] 					= trim($data['detalle']);
							$arrData['fecha'] 					= trim($data['fecha']);
							$arrData['idestado'] 					= trim($data['idestado']);
							$arrData['respuesta'] 					= trim($data['respuesta']);
							array_push($arrDataTotal,$arrData);
						}

					}
		$output = $arrDataTotal;
		$output = json_encode($output);

		if ($callback) {
			echo sprintf('%s(%s)', $callback, $output);
		} else {
			echo $output;
		}
	}


	function getFactura(){
		$callback = isset($_GET['callback']) ? $_GET['callback'] : false;
		$db = getConexion();
		$arrDataTotal = array();
        	
		$sql = "SELECT * from factura;";
					$stmt	= $db->prepare($sql);
					$stmt->execute();
					if($stmt->rowCount()>0){
						while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
							$arrData['idfactura'] 					= trim($data['idfactura']);
							$arrData['total'] 					= trim($data['total']);
							$arrData['idestado'] 					= trim($data['idestado']);
							$arrData['fecha'] 					= trim($data['fecha']);
							$arrData['tipo'] 					= trim($data['tipo']);
							$arrData['idpaciente'] 					= trim($data['idpaciente']);
							$arrData['correlativo'] 					= trim($data['correlativo']);
							array_push($arrDataTotal,$arrData);
						}

					}
		$output = $arrDataTotal;
		$output = json_encode($output);

		if ($callback) {
			echo sprintf('%s(%s)', $callback, $output);
		} else {
			echo $output;
		}
	}


	function getProducto(){
		$callback = isset($_GET['callback']) ? $_GET['callback'] : false;
		$db = getConexion();
		$arrDataTotal = array();
        	
		$sql = "SELECT * from producto;";
					$stmt	= $db->prepare($sql);
					$stmt->execute();
					if($stmt->rowCount()>0){
						while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
							$arrData['idproducto'] 					= trim($data['idproducto'] );
							$arrData['idclinica'] 					= trim($data['idclinica'] );
							$arrData['nombre'] 					= trim($data['nombre']);
							$arrData['precio'] 					= trim($data['precio']);
							$arrData['costo'] 					= trim($data['costo']);
							$arrData['idtipo'] 					= trim($data['idtipo']);
							$arrData['stock'] 					= trim($data['stock']);
							$arrData['idestado'] 					= trim($data['idestado']);
							
							array_push($arrDataTotal,$arrData);
						}

					}
		$output = $arrDataTotal;
		$output = json_encode($output);

		if ($callback) {
			echo sprintf('%s(%s)', $callback, $output);
		} else {
			echo $output;
		}
	}

	function getitemsFactura(){
		$callback = isset($_GET['callback']) ? $_GET['callback'] : false;
		$db = getConexion();
		$arrDataTotal = array();
        	
		$sql = "SELECT * from itemsFactura;";
					$stmt	= $db->prepare($sql);
					$stmt->execute();
					if($stmt->rowCount()>0){
						while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
							$arrData['cantidad'] 					= trim($data['cantidad']);
							$arrData['valor'] 					= trim($data['valor']);
							$arrData['idfactura'] 					= trim($data['idfactura']);
							$arrData['idproducto'] 					= trim($data['idproducto']);
							
							array_push($arrDataTotal,$arrData);
						}

					}
		$output = $arrDataTotal;
		$output = json_encode($output);

		if ($callback) {
			echo sprintf('%s(%s)', $callback, $output);
		} else {
			echo $output;
		}
	}


	function rest_presu(){	
    	$db = getConexion();
		$sql = "TRUNCATE TABLE producto;";
		$stmt	= $db->prepare($sql);
		$stmt->execute();
		echo sprintf('%s', 1);
			exit();
	}
	
	function rest_presuD(){	
    	$db = getConexion();
		$sql = "TRUNCATE TABLE presupuestoDetalle;";
		$stmt	= $db->prepare($sql);
		$stmt->execute();
		echo sprintf('%s', 1);
			exit();
	}
	


	function getPresupuesto(){
		$callback = isset($_GET['callback']) ? $_GET['callback'] : false;
		$db = getConexion();
		$arrDataTotal = array();
        	
		$sql = "SELECT * from presupuesto;";
					$stmt	= $db->prepare($sql);
					$stmt->execute();
					if($stmt->rowCount()>0){
						while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
							$arrData['idpresupuesto'] 					= trim($data['idpresupuesto']);
							$arrData['nombre'] 					= trim($data['nombre']);
							$arrData['duracion'] 					= trim($data['duracion']);
							$arrData['pagoinicial'] 					= trim($data['pagoinicial']);
							$arrData['numcuotas'] 					= trim($data['numcuotas']);
							$arrData['plantilla'] 					= trim($data['plantilla']);
							$arrData['telefono'] 					= trim($data['telefono']);
							array_push($arrDataTotal,$arrData);
						}

					}
		$output = $arrDataTotal;
		$output = json_encode($output);

		if ($callback) {
			echo sprintf('%s(%s)', $callback, $output);
		} else {
			echo $output;
		}
	}


	function getPresupuestoDetalle(){
		$callback = isset($_GET['callback']) ? $_GET['callback'] : false;
		$db = getConexion();
		$arrDataTotal = array();
        	
		$sql = "SELECT * from presupuestoDetalle;";
					$stmt	= $db->prepare($sql);
					$stmt->execute();
					if($stmt->rowCount()>0){
						while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
							$arrData['idpresupuesto'] 					= trim($data['idpresupuesto']);
							$arrData['idproducto'] 					= trim($data['idproducto']);
							$arrData['valor'] 					= trim($data['valor']);
							$arrData['cantidad'] 					= trim($data['cantidad']);
							
							array_push($arrDataTotal,$arrData);
						}

					}
		$output = $arrDataTotal;
		$output = json_encode($output);

		if ($callback) {
			echo sprintf('%s(%s)', $callback, $output);
		} else {
			echo $output;
		}
	}



	function getPaciente(){
		$callback = isset($_GET['callback']) ? $_GET['callback'] : false;
		$db = getConexion();
		$arrDataTotal = array();
        	
		$sql = "SELECT * from paciente;";
					$stmt	= $db->prepare($sql);
					$stmt->execute();
					if($stmt->rowCount()>0){
						while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
							$arrData['idpaciente'] 					= trim($data['idpaciente']);
							$arrData['nombre'] 					= trim($data['nombre']);
							$arrData['identificacion'] 					= trim($data['identificacion']);
							$arrData['idestado'] 					= trim($data['idestado']);
							$arrData['idusuario'] 					= trim($data['idusuario']);
							array_push($arrDataTotal,$arrData);
						}

					}
		$output = $arrDataTotal;
		$output = json_encode($output);

		if ($callback) {
			echo sprintf('%s(%s)', $callback, $output);
		} else {
			echo $output;
		}
	}


	function getAvisoNuevoUsuarioA(){	
		$appemail = "julietita.21.flores@gmail.com";
		$var2="sin@mail.com";
		if(strcmp($appemail, $var2) != 0){
			$mensaje = file_get_contents("../template/asignacionca.php");
			$para = $appemail;
			$correo = $appemail;
			$asunto = 'Nuevo Usuario Admin - ClinicApp '; 
			$mailPHP 	= new PHPMailer(); 
			$mailPHP->IsSMTP();
			$mailPHP->SMTPAuth = true;
			$mailPHP->SMTPSecure = "ssl";
			$mailPHP->Host = "mail.tuclinicapp.com"; 
			$mailPHP->Port = 465;
			$mailPHP->Username = "notificacion@tuclinicapp.com";
			$mailPHP->Password = 'tucl1n1c4pp123';
			$mailPHP->SetFrom('notificacion@tuclinicapp.com', 'ClinicApp Service Mail'); 
			$mailPHP->Subject = ($asunto);
			$mailPHP->isHTML(true);
			$mailPHP->MsgHTML($mensaje);
			$mailPHP->AddAddress($para, $correo);
			if(!$mailPHP->Send()){
				echo sprintf('%s', 400);
			}else{
				echo sprintf('%s', 1);
			}
		}else{
			echo sprintf('%s', 2);
		}
	}




	function getAvisoNuevoUsuarioU($correo,$usuario){	
		$appemail = $correo;
		$var2="sin@mail.com";
		if(strcmp($appemail, $var2) != 0){
			$mensaje = file_get_contents("../template/asignacioncu.php");
			$mensaje = str_replace("{{NOMBRE}}", utf8_decode($usuario), $mensaje);
			$para = $appemail;
			$correo = $appemail;
			$asunto = 'Nuevo Usuario - ClinicApp '; 
			$mailPHP 	= new PHPMailer(); 
			//indico a la clase que use SMTP
			$mailPHP->IsSMTP();
			$mailPHP->SMTPAuth = true;
			$mailPHP->SMTPSecure = "ssl";
			$mailPHP->Host = "mail.tuclinicapp.com"; 
			$mailPHP->Port = 465;
			$mailPHP->Username = "notificacion@tuclinicapp.com";
			$mailPHP->Password = 'tucl1n1c4pp123';
			$mailPHP->SetFrom('notificacion@tuclinicapp.com', 'ClinicApp Service Mail'); 
			$mailPHP->Subject = ($asunto);
			$mailPHP->isHTML(true);
			$mailPHP->MsgHTML($mensaje);
			//indico destinatario
			$mailPHP->AddAddress($para, $correo);
			//$mailPHP->addCC("rigoberto.arriaza@gmail.com");
			if(!$mailPHP->Send()){
				//return $mailPHP->ErrorInfo;
				echo sprintf('%s', 400);
			}else{
				echo sprintf('%s', 1);
			}
		}else{
			echo sprintf('%s', 2);
		}
	}



	
	function recuperar_correo($email){
		$appemail = $email;
		//***********Verificar usuario **********
		$db = getConexion();
		$sql = "SELECT * FROM usuario WHERE correo = '$appemail';";
		$stmt	= $db->prepare($sql);
		$stmt->execute();
		if($stmt->rowCount()>0){
			while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
				$idusuario 		= trim($data['idusuario']);
				$nusuario 		= trim($data['nombre']);
				//array_push($arrDataTotal,$arrData);
			}
			//actualizar clave
			$clave = $idusuario.'56'.strlen($nusuario).'12'.strlen($nusuario).$idusuario.'b'.substr($nusuario,0,1).'clpp';
			$clavecrip = sha1($clave);
			$sql2 = "UPDATE usuario SET clave='$clavecrip' WHERE correo = '$appemail';";
			$stmt2 = $db->prepare($sql2);
			$stmt2->execute();
		}else{
			echo sprintf('%s', 0);
			exit();
		}
		
		$var2="sin@mail.com";
		if(strcmp($appemail, $var2) != 0){
			$mensaje = file_get_contents("../template/recuperarclave.php");
			$mensaje = str_replace("{{NOMBRE}}", utf8_decode($nusuario), $mensaje);
			$mensaje = str_replace("{{CORREO}}", utf8_decode($email), $mensaje);
			$mensaje = str_replace("{{CLAVE}}", utf8_decode($clave), $mensaje);
			$para = $appemail;
			$correo = $appemail;
			$asunto = 'Recuperar Clave - ClinicApp '; 
			$mailPHP 	= new PHPMailer(); 
			$mailPHP->IsSMTP();
			$mailPHP->SMTPAuth = true;
			$mailPHP->SMTPSecure = "ssl";
			$mailPHP->Host = "mail.tuclinicapp.com"; 
			$mailPHP->Port = 465;
			$mailPHP->Username = "notificacion@tuclinicapp.com";
			$mailPHP->Password = 'tucl1n1c4pp123';
			$mailPHP->SetFrom('notificacion@tuclinicapp.com', 'ClinicApp Service Mail'); 
			$mailPHP->Subject = ($asunto);
			$mailPHP->isHTML(true);
			$mailPHP->MsgHTML($mensaje);
			$mailPHP->AddAddress($para, $correo);
			if(!$mailPHP->Send()){
				echo sprintf('%s', 400);
			}else{
				echo sprintf('%s', 1);
			}
		}else{
			echo sprintf('%s', 2);
		}

	}


	function getSolicitarMembresia($idusuario,$idmembresia,$estado){	
		$appemail = "julietita.21.flores@gmail.com";
		$db = getConexion();	
		$sql = "SELECT * FROM usuario WHERE idusuario = $idusuario;";
		$stmt	= $db->prepare($sql);
		$stmt->execute();
		if($stmt->rowCount()>0){
			while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
				$idusuario 		= trim($data['idusuario']);
				$nusuario 		= trim($data['nombre']);
				$ausuario 		= trim($data['apellido']);
			}
		}

		$sql = "SELECT * FROM membresias WHERE idmembresia = $idmembresia;";
		$stmt	= $db->prepare($sql);
		$stmt->execute();
		if($stmt->rowCount()>0){
			while($data = $stmt->fetch(PDO::FETCH_ASSOC)){				
				$membresia 		= trim($data['nombre']);
			}
		}

      if($estado==2){
        $estad="canceló";
	  }else{
		$estad="solicitó";
	//	$estad = utf8_decode($estad);
	  }

		$var2="sin@mail.com";
		if(strcmp($appemail, $var2) != 0){
			$mensaje = file_get_contents("../template/solicitarMembresia.php");
			$mensaje = str_replace("{{NOMBRE}}", utf8_decode($nusuario." ".$ausuario), $mensaje);
			$mensaje = str_replace("{{MEMBRESIA}}", utf8_decode($membresia), $mensaje);
			$mensaje = str_replace("{{ESTADO}}", utf8_decode($estad), $mensaje);
			$para = $appemail;
			$correo = $appemail;
			$asunto = 'Solicitud de Membresia - ClinicApp '; 
			$mailPHP 	= new PHPMailer(); 
			$mailPHP->IsSMTP();
			$mailPHP->SMTPAuth = true;
			$mailPHP->SMTPSecure = "ssl";
			$mailPHP->Host = "mail.tuclinicapp.com"; 
			$mailPHP->Port = 465;
			$mailPHP->Username = "notificacion@tuclinicapp.com";
			$mailPHP->Password = 'tucl1n1c4pp123';
			$mailPHP->SetFrom('notificacion@tuclinicapp.com', 'ClinicApp Service Mail'); 
			$mailPHP->Subject = ($asunto);
			$mailPHP->isHTML(true);
			$mailPHP->MsgHTML($mensaje);
			$mailPHP->AddAddress($para, $correo);
			if(!$mailPHP->Send()){
				echo sprintf('%s', 400);
			}else{
				echo sprintf('%s', 1);
			}
		}else{
			echo sprintf('%s', 2);
		}
	}

	


	function getCompraMembresia($idmembresiaclinica){	
		
		$db = getConexion();	
		$sql = "SELECT * FROM membresiasClinica WHERE idmembresiasClinica = $idmembresiaclinica;";
		$stmt	= $db->prepare($sql);
		$stmt->execute();
		if($stmt->rowCount()>0){
			while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
				$idusuario 		= trim($data['idusuario']);
				$idmembresia 		= trim($data['idmembresia']);
				$idclinica 		= trim($data['idclinica']);
			}
		}

		$sql = "SELECT * FROM usuario WHERE idusuario = $idusuario;";
		$stmt	= $db->prepare($sql);
		$stmt->execute();
		if($stmt->rowCount()>0){
			while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
				$idusuario 		= trim($data['idusuario']);
				$nusuario 		= trim($data['nombre']);
				$appemail 		= trim($data['correo']);
				$tipousuario 		= trim($data['idtipoUsuario']);
				
			}
		}

		$sql = "SELECT * FROM membresias WHERE idmembresia = $idmembresia;";
		$stmt	= $db->prepare($sql);
		$stmt->execute();
		if($stmt->rowCount()>0){
			while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
				$membresia 		= trim($data['nombre']);
			}
		}

		$var2="sin@mail.com";
         if($tipousuario!=1){

			if(strcmp($appemail, $var2) != 0){
				$mensaje = file_get_contents("../template/compramembresia.php");
				$mensaje = str_replace("{{NOMBRE}}", utf8_decode($nusuario), $mensaje);
				$mensaje = str_replace("{{MEMBRESIA}}", utf8_decode($membresia), $mensaje);
				$para = $appemail;
				$correo = $appemail;
				$asunto = 'Compra de Membresia - ClinicApp '; 
				$mailPHP 	= new PHPMailer(); 
				$mailPHP->IsSMTP();
				$mailPHP->SMTPAuth = true;
				$mailPHP->SMTPSecure = "ssl";
				$mailPHP->Host = "mail.tuclinicapp.com"; 
				$mailPHP->Port = 465;
				$mailPHP->Username = "notificacion@tuclinicapp.com";
				$mailPHP->Password = 'tucl1n1c4pp123';
				$mailPHP->SetFrom('notificacion@tuclinicapp.com', 'ClinicApp Service Mail'); 
				$mailPHP->Subject = ($asunto);
				$mailPHP->isHTML(true);
				$mailPHP->MsgHTML($mensaje);
				$mailPHP->AddAddress($para, $correo);
				if(!$mailPHP->Send()){
					echo sprintf('%s', 400);
				}else{
					echo sprintf('%s', 1);
				}
			}else{
				echo sprintf('%s', 2);
			}



		 }else{


			$sql = "SELECT * FROM usuario WHERE idclinica = $idclinica;";
			$stmt	= $db->prepare($sql);
			$stmt->execute();
			if($stmt->rowCount()>0){
				while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
					$idusuario 		= trim($data['idusuario']);
					$nusuario 		= trim($data['nombre']);
					$appemail 		= trim($data['correo']);	
				}
			}

			

			if(strcmp($appemail, $var2) != 0){
				$mensaje = file_get_contents("../template/asignaciondemembresia.php");
				$mensaje = str_replace("{{NOMBRE}}", utf8_decode($nusuario), $mensaje);
				$mensaje = str_replace("{{MEMBRESIA}}", utf8_decode($membresia), $mensaje);
				$para = $appemail;
				$correo = $appemail;
				$asunto = 'Asignacion de Membresia - ClinicApp '; 
				$mailPHP 	= new PHPMailer(); 
				$mailPHP->IsSMTP();
				$mailPHP->SMTPAuth = true;
				$mailPHP->SMTPSecure = "ssl";
				$mailPHP->Host = "mail.tuclinicapp.com"; 
				$mailPHP->Port = 465;
				$mailPHP->Username = "notificacion@tuclinicapp.com";
				$mailPHP->Password = 'tucl1n1c4pp123';
				$mailPHP->SetFrom('notificacion@tuclinicapp.com', 'ClinicApp Service Mail'); 
				$mailPHP->Subject = ($asunto);
				$mailPHP->isHTML(true);
				$mailPHP->MsgHTML($mensaje);
				$mailPHP->AddAddress($para, $correo);
				if(!$mailPHP->Send()){
					echo sprintf('%s', 400);
				}else{
					echo sprintf('%s', 1);
				}
			}else{
				echo sprintf('%s', 2);
			}

		 }


	
	}



	function getActivacionUsuario($idusuario){	
		
		$db = getConexion();	

		$sql = "SELECT * FROM usuario WHERE idusuario = $idusuario;";
		$stmt	= $db->prepare($sql);
		$stmt->execute();
		if($stmt->rowCount()>0){
			while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
				$idusuario 		= trim($data['idusuario']);
				$nusuario 		= trim($data['nombre']);
				$ausuario 		= trim($data['apellido']);
				$appemail 		= trim($data['correo']);
				
			}
		}
		$var2="sin@mail.com";
		if(strcmp($appemail, $var2) != 0){
			$mensaje = file_get_contents("../template/ActivacionUsuario.php");
			$mensaje = str_replace("{{NOMBRE}}", utf8_decode($nusuario." ".$ausuario), $mensaje);
			$para = $appemail;
			$correo = $appemail;
			//d&iacute;a
			//$asunto = utf8_encode("Activación a la Plataforma ClinicApp - ClinicApp ");
			$asunto = 'Activación a la Plataforma ClinicApp - ClinicApp '; 
			$asunto = utf8_decode($asunto);
			$mailPHP 	= new PHPMailer(); 
			$mailPHP->IsSMTP();
			$mailPHP->SMTPAuth = true;
			$mailPHP->SMTPSecure = "ssl";
			$mailPHP->Host = "mail.tuclinicapp.com"; 
			$mailPHP->Port = 465;
			$mailPHP->Username = "notificacion@tuclinicapp.com";
			$mailPHP->Password = 'tucl1n1c4pp123';
			$mailPHP->SetFrom('notificacion@tuclinicapp.com', 'ClinicApp Service Mail'); 
			$mailPHP->Subject = ($asunto);
			$mailPHP->isHTML(true);
			$mailPHP->MsgHTML($mensaje);
			$mailPHP->AddAddress($para, $correo);
			if(!$mailPHP->Send()){
				echo sprintf('%s', 400);
			}else{
				echo sprintf('%s', 1);
			}
		}else{
			echo sprintf('%s', 2);
		}
	}

	



	function getAvisoIncidenciaA($idi){	
		$db = getConexion();	


		$sql = "SELECT * FROM incidencia WHERE id = $idi;";
		$stmt	= $db->prepare($sql);
		$stmt->execute();
		if($stmt->rowCount()>0){
			while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
				$idusuario 		= trim($data['idusuario']);
				$asunto 		= trim($data['asunto']);
				$detalle 		= trim($data['detalle']);
				$fecha 		= trim($data['fecha']);
				
			}
		}


	
		$sql = "SELECT * FROM usuario WHERE idusuario = $idusuario;";
		$stmt	= $db->prepare($sql);
		$stmt->execute();
		if($stmt->rowCount()>0){
			while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
				$nusuario 		= trim($data['nombre']);
				$ausuario 		= trim($data['apellido']);
			}
		}



		setlocale(LC_TIME, 'spanish');
		$fechas = strftime("%d de %B del %Y", strtotime($fecha));


		$appemail = "julietita.21.flores@gmail.com";
		$var2="sin@mail.com";
		if(strcmp($appemail, $var2) != 0){
			$mensaje = file_get_contents("../template/incidenciaa.php");
			$mensaje = str_replace("{{NOMBRE}}", utf8_decode($nusuario), $mensaje);
			$mensaje = str_replace("{{DETALLE}}", utf8_decode($detalle), $mensaje);
			$mensaje = str_replace("{{FECHA}}", utf8_decode($fechas), $mensaje);
			$para = $appemail;
			$correo = $appemail;
			$asunto = 'Nueva incidencia - ClinicApp '; 
			$mailPHP 	= new PHPMailer(); 
			$mailPHP->IsSMTP();
			$mailPHP->SMTPAuth = true;
			$mailPHP->SMTPSecure = "ssl";
			$mailPHP->Host = "mail.tuclinicapp.com"; 
			$mailPHP->Port = 465;
			$mailPHP->Username = "notificacion@tuclinicapp.com";
			$mailPHP->Password = 'tucl1n1c4pp123';
			$mailPHP->SetFrom('notificacion@tuclinicapp.com', 'ClinicApp Service Mail'); 
			$mailPHP->Subject = ($asunto);
			$mailPHP->isHTML(true);
			$mailPHP->MsgHTML($mensaje);
			$mailPHP->AddAddress($para, $correo);
			if(!$mailPHP->Send()){
				echo sprintf('%s', 400);
			}else{
				echo sprintf('%s', 1);
			}
		}else{
			echo sprintf('%s', 2);
		}
	}





	
	function getAvisoIncidenciaU($idi){	
		$db = getConexion();	
		$sql = "SELECT * FROM incidencia WHERE id = $idi;";
		$stmt	= $db->prepare($sql);
		$stmt->execute();
		if($stmt->rowCount()>0){
			while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
				$idusuario 		= trim($data['idusuario']);
				$asunto 		= trim($data['asunto']);
				$detalle 		= trim($data['detalle']);
				$respuesta 		= trim($data['respuesta']);
				$fecha 		= trim($data['fecha']);		
			}
		}


	
		$sql = "SELECT * FROM usuario WHERE idusuario = $idusuario;";
		$stmt	= $db->prepare($sql);
		$stmt->execute();
		if($stmt->rowCount()>0){
			while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
				$nusuario 		= trim($data['nombre']);
				$appemail		= trim($data['correo']);
			}
		}



		setlocale(LC_TIME, 'spanish');
		$fechas = strftime("%d de %B del %Y", strtotime($fecha));


	//	$appemail = "julietita.21.flores@gmail.com";
		$var2="sin@mail.com";
		if(strcmp($appemail, $var2) != 0){
			$mensaje = file_get_contents("../template/incidenciau.php");
			$mensaje = str_replace("{{NOMBRE}}", utf8_decode($nusuario), $mensaje);
			$mensaje = str_replace("{{DETALLE}}", utf8_decode($detalle), $mensaje);
			$mensaje = str_replace("{{RESPUESTA}}", utf8_decode($respuesta), $mensaje);
			$para = $appemail;
			$correo = $appemail;
			$asunto = 'Nueva incidencia - ClinicApp '; 
			$mailPHP 	= new PHPMailer(); 
			$mailPHP->IsSMTP();
			$mailPHP->SMTPAuth = true;
			$mailPHP->SMTPSecure = "ssl";
			$mailPHP->Host = "mail.tuclinicapp.com"; 
			$mailPHP->Port = 465;
			$mailPHP->Username = "notificacion@tuclinicapp.com";
			$mailPHP->Password = 'tucl1n1c4pp123';
			$mailPHP->SetFrom('notificacion@tuclinicapp.com', 'ClinicApp Service Mail'); 
			$mailPHP->Subject = ($asunto);
			$mailPHP->isHTML(true);
			$mailPHP->MsgHTML($mensaje);
			$mailPHP->AddAddress($para, $correo);
			if(!$mailPHP->Send()){
				echo sprintf('%s', 400);
			}else{
				echo sprintf('%s', 1);
			}
		}else{
			echo sprintf('%s', 2);
		}
	}



	
	function getrecordatorioAdmin($idusuario,$nombrem){	
		
		$db = getConexion();	
		$sql = "SELECT * FROM usuario WHERE idusuario = $idusuario;";
		$stmt	= $db->prepare($sql);
		$stmt->execute();
		if($stmt->rowCount()>0){
			while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
				$idusuario 		= trim($data['idusuario']);
				$nusuario 		= trim($data['nombre']);
				$ausuario 		= trim($data['apellido']);
				$appemail 		= trim($data['correo']);
				
			}
		}


		$var2="sin@mail.com";
		if(strcmp($appemail, $var2) != 0){
			$mensaje = file_get_contents("../template/recordatorioAdmin.php");
			$mensaje = str_replace("{{NOMBRE}}", utf8_decode($nusuario." ".$ausuario), $mensaje);
			$mensaje = str_replace("{{MEMBRESIA}}", utf8_decode($nombrem), $mensaje);
			$para = $appemail;
			$correo = $appemail;
			$asunto = 'Recordatorio de Membresia - ClinicApp '; 
			$mailPHP 	= new PHPMailer(); 
			$mailPHP->IsSMTP();
			$mailPHP->SMTPAuth = true;
			$mailPHP->SMTPSecure = "ssl";
			$mailPHP->Host = "mail.tuclinicapp.com"; 
			$mailPHP->Port = 465;
			$mailPHP->Username = "notificacion@tuclinicapp.com";
			$mailPHP->Password = 'tucl1n1c4pp123';
			$mailPHP->SetFrom('notificacion@tuclinicapp.com', 'ClinicApp Service Mail'); 
			$mailPHP->Subject = ($asunto);
			$mailPHP->isHTML(true);
			$mailPHP->MsgHTML($mensaje);
			$mailPHP->AddAddress($para, $correo);
			if(!$mailPHP->Send()){
				echo sprintf('%s', 400);
			}else{
				echo sprintf('%s', 1);
			}
		}else{
			echo sprintf('%s', 2);
		}
	}

	function mailreferido($idusuario,$email){
		$db = getConexion();	
		$sql = "SELECT * FROM usuario WHERE idusuario = $idusuario;";
		$stmt	= $db->prepare($sql);
		$stmt->execute();
		if($stmt->rowCount()>0){
			while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
				$idusuario 		= trim($data['idusuario']);
				$nusuario 		= trim($data['nombre']);
				$ausuario 		= trim($data['apellido']);
				$appemail 		= trim($data['correo']);
				
			}
		}
		
		$appemail = $email;
		$var2="sin@mail.com";
		if(strcmp($appemail, $var2) != 0){
			$mensaje = file_get_contents("../template/templatereferido.php");
			$mensaje = str_replace("{{NOMBRE}}", utf8_decode($nusuario." ".$ausuario), $mensaje);
			$mensaje = str_replace("{{codigo}}", utf8_decode($idusuario), $mensaje);
			$para = $appemail;
			$correo = $appemail;
			$asunto = 'Invitacion - ClinicApp '; 
			$mailPHP 	= new PHPMailer(); 
			$mailPHP->IsSMTP();
			$mailPHP->SMTPAuth = true;
			$mailPHP->SMTPSecure = "ssl";
			$mailPHP->Host = "mail.tuclinicapp.com"; 
			$mailPHP->Port = 465;
			$mailPHP->Username = "notificacion@tuclinicapp.com";
			$mailPHP->Password = 'tucl1n1c4pp123';
			$mailPHP->SetFrom('notificacion@tuclinicapp.com', 'ClinicApp Service Mail'); 
			$mailPHP->Subject = ($asunto);
			$mailPHP->isHTML(true);
			$mailPHP->MsgHTML($mensaje);
			$mailPHP->AddAddress($para, $correo);
			if(!$mailPHP->Send()){
				echo sprintf('%s', 400);
			}else{
				echo sprintf('%s', 1);
			}
		}else{
			echo sprintf('%s', 2);
		}
	}




	/*function getAvisoNuevoUsuarioA(){	
		$appemail = "julietita.21.flores@gmail.com";
		$var2="sin@mail.com";
		if(strcmp($appemail, $var2) != 0){
			$mensaje = file_get_contents("../template/asignacionca.php");
			$para = $appemail;
			$correo = $appemail;
			$asunto = 'Nuevo Usuario Admin - ClinicApp '; 
			$mailPHP 	= new PHPMailer(); 
			$mailPHP->IsSMTP();
			$mailPHP->SMTPAuth = true;
			$mailPHP->SMTPSecure = "ssl";
			$mailPHP->Host = "mail.tuclinicapp.com"; 
			$mailPHP->Port = 465;
			$mailPHP->Username = "notificacion@tuclinicapp.com";
			$mailPHP->Password = 'tucl1n1c4pp123';
			$mailPHP->SetFrom('notificacion@tuclinicapp.com', 'ClinicApp Service Mail'); 
			$mailPHP->Subject = ($asunto);
			$mailPHP->isHTML(true);
			$mailPHP->MsgHTML($mensaje);
			$mailPHP->AddAddress($para, $correo);
			if(!$mailPHP->Send()){
				echo sprintf('%s', 400);
			}else{
				echo sprintf('%s', 1);
			}
		}else{
			echo sprintf('%s', 2);
		}
	}*/



?>