<?php 
  
    $seguros    =  $oUser->getAllseguros();
    $idseguro="";
 ?>  


            <div id="tabladepagos">
               <a onclick="javascript:verformulario(1)" data-toggle="modal" data-target="#modalformulariopago" class="btn btn-info">Crear Pagos</a>
               <br>
               <bR>
              <!-- <a class="btn btn-default" data-toggle="modal" data-target="#modalformulariopago"  onclick="javascript:verformulario(1)"><i class="fa fa-plus" aria-hidden="true"></i></a>-->
               <table id="tablapagos" class="table table-striped">
                                <thead>
                                          <tr>
                                                    <th>Mótivo</th>
                                                    <th>Fecha</th>
                                                    <th>Monto</th>
                                                    <th>Forma de pago</th>
                                              
                                                    <th>Notas</th>
                                                    <th>Seguro</th>
                                                    <th>opciones</th>
                                          </tr>
                                </thead>
                                <tbody id="tablaPagos">
                                        <?php 
                                        if($vpagostotal){
                                        
                                        foreach ($vpagostotal AS $id => $array) {
                                            if($array['idtipoPago']==1){
                                                if($array['idformaPago']==1){
                                                    $forma='Efectivo';
                                                }else{
                                                    $forma='Tarjeta';
                                                }
                                                ?>
                                            <tr>
                                                
                                                <td><?=$array['tratamiento'];?></td>
                                                <td><? 
                                                       $source = $array['fechaPago'];
                                                       $date = new DateTime($source);
                                                     
                                                       echo $date->format('d-m-Y');
                                                        ?>
                                                </td>

                                                <td><?=$array['monto'];?></td>
                                                
                                                <td><?=$forma?></td>

                                                <td><?=$array['observaciones'];?></td>
                                                
                                                <td><?=$array['nombreseguro'];?></td>



                                                <td ><a style="font-size: 18px;"  onclick="javascript:eliminarPago(<?=$array['idpago']?>)"><i class="fa fa-trash-o" aria-hidden="true"></i> </a>
                                                <a style="font-size: 18px;" data-toggle="modal" data-target="#modalformulariopago" onclick="javascript:actualizarPagoform(<?=$array['idpago']?>,1)"> <i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                <a style='font-size:18px;' title='Imprimir' href='pagosImprimir.php?pago="<?=$id?>"'><i class='fa fa-print'></i></a>
                   
                                                
                                            </td>
                                            </tr>

                                        <?   }?>                
                                    
                                    <?php } } ?> 
                                </tbody>
            </table>
           </div>
            
            

    



    <div class="modal fade" id="modalformulariopago" tabindex="-1" role="dialog" aria-labelledby="modalformulariopago" aria-hidden="true">
         <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myModalLabel">Nuevo Pagó</h4>               
                  </div>
                 
                  <div class="modal-body">
                   <div id="formularioPagos" class="container" style="display:none">   
                     <div id="testmodal" style="padding: 5px 20px;">
                        <form id="antoform" class="form-horizontal calender" role="form">
                          <input type="hidden" value="1" id="idtipopago">
                      

                          <div class="form-group">
                             <label class="col-sm-3 control-label" for="tratamientopago">Mótivo</label>
                             <div class="col-sm-9">
                              <input id="tratamientopago" class="form-control" type="text">
                             </div>
                          </div> 


                          <div class="form-group">   
                            <label class="col-sm-3 control-label" for="fechap">Fecha</label>
                            <div class="col-sm-4">
                              <input class="form-control" name="fechap" id="fechap" type="date" value="<?php echo date('Y-m-d');?>">
                            </div>
       
                            
                            <label class="col-sm-2 control-label" for="monto">Monto</label>
                            <div class="col-sm-3">
                             <input class="form-control" name="monto" id="monto" type="number" >
                            </div>
                        </div> 



                        <div class="form-group">
                           <label class="col-sm-3 control-label" for="formapago">Forma de Pago</label>
                            <div class="col-sm-4">
                             <select class="form-control" name="formapagoid" id="formapagoid">
                                    <option selected="selected" value="1">Efectivo</option>
                                    <option  value="2">Tarjeta</option>
                                    
                             </select>
                            </div>
       
                            
                         <label class="col-sm-1 control-label" style="padding-left: 3px;" for="formaseguro">Seguro</label>
                            <div class="col-sm-4" >

                            <select class="form-control" name="formaseguroid" id="formaseguroid">
                          <option value="1">Elegir una opción</option>
                          <?php if($seguros){
                                foreach ($seguros AS $id => $array) {?>
                                <option value="<?=$array['idseguro'];?>" <?php if($idseguro==$id){echo"selected='selected'";}?>><?=$array['nombre'];?></option>
                          <?php } } ?>
                        </select>

                          
                            </div>
                        </div> 


                        <div class="form-group">

                            <label class="col-sm-3 control-label" for="notad">Notas</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" name="notapago" id="notapago" >
                            </div>
                        </div>

                     

                     
             
                        </form>
                      </div>
                     </div>
                    </div>
                                    <div class="modal-footer">
                                        <div id="guadarFormPago">
                                      
                                        <a class="btn btn-danger" class="close" data-dismiss="modal" aria-hidden="true">Cancelar</a>
                                        <a class="btn btn-primary guardapago"data-dismiss="modal" >Agregar</a>
                                     <!--   <a class="btn btn-danger" href="javascript:cerrarpagos() " data-dismiss="modal">Cancelar</a>-->
                                        </div>

                                        <div id="actualizarFormpago" style="display:none">
                                        <a class="btn btn-primary " onclick="javascript:actualizarPago()"   data-dismiss="modal" >Modificar</a>
                                        <a class="btn btn-danger" href="javascript:cerrarpagos() " data-dismiss="modal">Cancelar</a>
                                        </div>
                                    </div>
                 </div>
              </div>
            </div>













<!--


                    <div class="modal fade" id="tablapagoscredito" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h2 class="modal-title" id="nombretratamiento"></h2>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                 <div id="tablacreditopagos" style="display:none">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Nro</th>
                                                                <th>fecha</th>
                                                                <th>Monto</th>
                                                                
                                                                <th>observación</th>
                                                                <th>opciones</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tablaCredito">
                                                        
                                                        </tbody>
                                                    </table>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-8 " style="text-align: right;"><h2 id="pagado"></h2></div>
                                            <div class="col-md-4"><h2 id="saldo"></h2></div>
                                        </div>
                                    </div>
                                    
                                    
                                   
                            </div>
                            <div class="modal-footer">
                                <button id="cerrarModalcredito" type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            </div>
                            </div>
                        </div>
                    </div>
       
           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalFormularioCredito">

                    </button>

             
                    <div class="modal fade" id="modalFormularioCredito" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="modalpagocredito"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                        <div id="forpagocreditos" class="container" style="display:none">
                                <div class="col-md-12">

                                        <div class="form-group">
                                            <label for="fechapc">Fecha</label>
                                            <input class="form-control" name="fechapc" id="fechapc" type="date" value="<?php echo date('Y-m-d');?>">
                                            <label for="montoc">Monto</label>
                                            <input class="form-control" name="montoc" id="montoc" value="0" type="number" >
                                            <input id="idpago" type="hidden" value="0">
                                            <input id="idsaldo" type="hidden" value="0">
                                            <br>
                                            <label for="notapc">Observación</label>
                                            <input type="text" class="form-control" name="notapagoc" id="notapagoc" >
                                            <br>
                                            
                                        </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a class="btn btn-primary" data-dismiss="modal"  onclick="javascript:agregarcredito()">Agregar</a>
                            <a class="btn btn-danger" data-dismiss="modal" onclick="javascript:cerrarpagos()">Cancelar</a>
                                        
                          
                        </div>
                        </div>
                    </div>
                    </div>
  ------------------------->


            