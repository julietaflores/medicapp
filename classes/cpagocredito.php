<?php
class pagocredito{
    function __construct() {
        global $DATA;
        $this->DATA = $DATA;
    }
    function getprueball($idpago){
        $sql = "SELECT p.monto as totaldeuda, pa.nombre as nombrepaciente, pa.apellido as apellidopa,p.idpago,p.idusuario,
        pa.correo as correopa,pa.telefono1,
        if((select sum(monto) from pagoTratamiento where idPago=p.idpago),(select sum(monto) from pagoTratamiento where idPago=p.idpago),0) as pagado,
        if(p.monto-(select sum(monto) from pagoTratamiento where idPago=p.idpago),(p.monto-(select sum(monto) from pagoTratamiento where idPago=p.idpago)),p.monto) as debe,
        p.tratamiento as tratamiento
        		FROM pago p, paciente pa 
                where  pa.idpaciente=p.idpaciente
                and  p.idpago= $idpago 
                 ;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){

         		$id                  	= $rs->fields['idpago'];
                       $info[$id]['idpago']	= $rs->fields['idpago'];
                       //$info[$id]['monto']	= $rs->fields['monto'];
                       //$info[$id]['fecha']	= $rs->fields['fecha'];
                       $info[$id]['idusuario']	= $rs->fields['idusuario'];
                       //$info[$id]['idPago']	= $rs->fields['idPago'];
                      // $info[$id]['observaciones']	= $rs->fields['observaciones'];
                       $info[$id]['totaldeuda']	= $rs->fields['totaldeuda'];
                       $info[$id]['debe']	= $rs->fields['debe'];
                       $info[$id]['tratamiento']	= $rs->fields['tratamiento'];
                       $info[$id]['pagado']	= $rs->fields['pagado'];		
                       $info[$id]['nombrepaciente']	= $rs->fields['nombrepaciente'];
                       $info[$id]['apellidopa']	= $rs->fields['apellidopa'];
                       $info[$id]['correopa']	= $rs->fields['correopa'];
                       $info[$id]['telefono1']	= $rs->fields['telefono1'];		
                $rs->MoveNext();
            }
            $rs->Close();
            return ( $info);
        } else {
            return false;
        }
    }
    function getAll($idpago){
        $sql = "SELECT pc.*,p.monto as totaldeuda, 
        ((select sum(monto) from pagoTratamiento where idPago=p.idpago)) as pagado,
        (p.monto-(select sum(monto) from pagoTratamiento where idPago=p.idpago)) as debe,
        p.tratamiento as tratamiento
        		FROM pagoTratamiento  pc, pago p 
                where pc.idPago=p.idpago and  pc.idPago= $idpago  ;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){

         		$id                  	= $rs->fields['idpagoTraramiento'];
                       $info[$id]['idpagoTraramiento']	= $rs->fields['idpagoTraramiento'];
                       $info[$id]['monto']	= $rs->fields['monto'];
                       $info[$id]['fecha']	= $rs->fields['fecha'];
                       $info[$id]['idusuario']	= $rs->fields['idusuario'];
                       $info[$id]['idPago']	= $rs->fields['idPago'];
                       $info[$id]['observaciones']	= $rs->fields['observaciones'];
                       $info[$id]['totaldeuda']	= $rs->fields['totaldeuda'];
                       $info[$id]['debe']	= $rs->fields['debe'];
                       $info[$id]['tratamiento']	= $rs->fields['tratamiento'];
                       $info[$id]['pagado']	= $rs->fields['pagado'];
                       $info[$id]['credito1']	= $rs->fields['credito1'];
                       			
								
                $rs->MoveNext();
            }
            $rs->Close();
            return json_encode( $info);
        } else {
            return false;
        }
    }
    function getone($id){
        $sql = "SELECT * FROM pago WHERE idpago = ?;";
        $rs = $this->DATA->Execute($sql, $id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){

                $id                  	= $rs->fields['idpagoTraramiento'];
                      $info[$id]['idpagoTraramiento']	= $rs->fields['idpagoTraramiento'];
                      $info[$id]['monto']	= $rs->fields['monto'];
                      $info[$id]['fecha']	= $rs->fields['fecha'];
                      $info[$id]['idusuario']	= $rs->fields['idusuario'];
                      $info[$id]['idPago']	= $rs->fields['idPago'];
                      $info[$id]['observaciones']	= $rs->fields['observaciones'];
                      
                      
                               
                               
               $rs->MoveNext();
           }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }

    function nuevo($params) 
	{
		try{
		
			$sql = "INSERT INTO pagoTratamiento (monto,fecha,idusuario,idPago,observaciones,credito1) VALUES (?,?,?,?,?,?);";
				
			$save = $this->DATA->Execute($sql, $params);
			$id 	= $this->DATA->Insert_ID();
			if ($save){
				return json_encode($id);
			} else {
				return false;
			}
		}catch(exception $e){
			return false;
		}
        
    }
    function actualizar($params){
        try{
        
            $sql = "UPDATE pagoTratamiento SET monto=?,fecha=?,tratamiento=?,observaciones=? WHERE idpagoTraramiento = ?";
            $save = $this->DATA->Execute($sql, $params);
            if ($save){
                return true;
            } else {
                return false;
            }
        }catch(exception $e){
            return false;
        }
    }
    function eliminar($id){
        $sql = "delete from pagoTratamiento where idpagoTraramiento = ?";
        $update = $this->DATA->Execute($sql, $id);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
    function reporteIG($inicio,$fin,$clinica){
        $sql = "SELECT monto,fecha,tipo,idusuario,clinica,id,detalle from(
            select (select tratamiento from pago where idpago=pa.idPago)as detalle,pa.fecha ,pa.monto,c.idclinica as clinica,pa.idPago as id ,u.idusuario ,'ingreso' as tipo
            from pagoTratamiento pa, usuario u , clinica c
            where pa.idusuario=u.idusuario and c.idclinica=u.idclinica 
            and u.idclinica=$clinica
            
            union all
            
            select p.tratamiento as detalle,p.fechapago, p.monto,c.idclinica as clinica,p.idpago as id,p.idusuario,'ingreso'as tipo
                   
                from pago p,usuario u,clinica c where p.idusuario=u.idusuario and c.idclinica=u.idclinica  and p.idtipoPago=1 
                and u.idclinica=$clinica
                
            union all
            
            select detalle,fecha,monto,idclinica as clinica,idgasto as id ,idusuario,'gasto' as tipo from gasto g
            where idclinica =$clinica)ti
            where fecha between '$inicio 00:00' and '$fin 23:59:59' order by fecha asc  ;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){

         		       $id                  	= $rs->fields['id'];
                       $info[$id]['id']	= $rs->fields['id'];
                       $info[$id]['monto']	= $rs->fields['monto'];
                       $info[$id]['fecha']	= $rs->fields['fecha'];
                       $info[$id]['clinica']	= $rs->fields['clinica'];
                       $info[$id]['tipo']	= $rs->fields['tipo'];
                       $info[$id]['detalle']	= $rs->fields['detalle'];
                     
                       			
								
                $rs->MoveNext();
            }
            $rs->Close();
            return json_encode( $info);
        } else {
            return false;
        }
    }
}
?>