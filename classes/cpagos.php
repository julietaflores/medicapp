<?php
class pago{
    function __construct() {
        global $DATA;
        $this->DATA = $DATA;
    }
    //credito
    function getAll($idpaciente){
        $sql = "SELECT p.*,u.nombre as nombreUsu,pa.nombre as nombrePa ,p.monto as total,
        if((select sum(monto) from pagoTratamiento where idPago=p.idpago)>0,(select sum(monto) from pagoTratamiento where idPago=p.idpago),0) as pagado,
        (p.monto- if((select sum(monto) from pagoTratamiento where idPago=p.idpago)>0,(select sum(monto) from pagoTratamiento where idPago=p.idpago),0) ) as debe,
        p.tratamiento as tratamiento
FROM pago p , usuario u,paciente pa
where p.idusuario=u.idusuario and p.idpaciente=pa.idpaciente and p.idtipoPago=2 and p.idpaciente= $idpaciente;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){

         		$id                  	= $rs->fields['idpago'];
                       $info[$id]['idpago']	= $rs->fields['idpago'];
                       $info[$id]['idformaPago']	= $rs->fields['idformaPago'];
                       $info[$id]['idtipoPago']	= $rs->fields['idtipoPago'];
                       $info[$id]['tratamiento']	= $rs->fields['tratamiento'];
                       $info[$id]['idpaciente']	= $rs->fields['idpaciente'];
                       $info[$id]['fechaPago']	= $rs->fields['fechaPago'];
                       $info[$id]['monto']	= $rs->fields['monto'];
                       $info[$id]['idusuario']	= $rs->fields['idusuario'];
                       $info[$id]['observaciones']	= $rs->fields['observaciones'];
                       $info[$id]['nombreUsu']	= $rs->fields['nombreUsu'];
                       $info[$id]['nombrePa']	= $rs->fields['nombrePa'];
                       $info[$id]['debe']	= $rs->fields['debe'];
                       $info[$id]['total']	= $rs->fields['total']; 
                       $info[$id]['pagado']	= $rs->fields['pagado'];
                       
                       
								
								
                $rs->MoveNext();
            }
            $rs->Close();
            return ( $info);
        } else {
            return false;
        }
    }
    
    //pago total
    function getAlltotal($idpaciente){
        $sql = "SELECT p.*,u.nombre as nombreUsu,pa.nombre as nombrePa 
        
        		FROM pago p , usuario u,paciente pa 
                where p.idusuario=u.idusuario and p.idpaciente=pa.idpaciente 
                and p.idpaciente= $idpaciente  ;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){

         		$id                  	= $rs->fields['idpago'];
                       $info[$id]['idpago']	= $rs->fields['idpago'];
                       $info[$id]['idformaPago']	= $rs->fields['idformaPago'];
                       $info[$id]['idtipoPago']	= $rs->fields['idtipoPago'];
                       $info[$id]['tratamiento']	= $rs->fields['tratamiento'];
                       $info[$id]['idpaciente']	= $rs->fields['idpaciente'];
                       $info[$id]['fechaPago']	= $rs->fields['fechaPago'];
                       $info[$id]['monto']	= $rs->fields['monto'];
                       $info[$id]['idusuario']	= $rs->fields['idusuario'];
                       $info[$id]['observaciones']	= $rs->fields['observaciones'];
                       $info[$id]['nombreUsu']	= $rs->fields['nombreUsu'];
                       $info[$id]['nombrePa']	= $rs->fields['nombrePa'];
                     
								
								
                $rs->MoveNext();
            }
            $rs->Close();
            return ( $info);
        } else {
            return false;
        }
    }



    //pago total
    function getAlltotalconseguro($idpaciente){
        $sql = "SELECT p.*, se.nombre as nombreseguro ,u.nombre as nombreUsu, pa.nombre as nombrePa 
        		FROM pago p 
                 inner join seguro se  on  p.idformaseguro=se.idseguro  
                 inner join usuario u on  p.idusuario=u.idusuario
                 inner join paciente pa on  p.idpaciente=pa.idpaciente 
                where  p.idpaciente= $idpaciente ;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){

         		       $id                  	= $rs->fields['idpago'];
                       $info[$id]['idpago']	= $rs->fields['idpago'];
                       $info[$id]['idformaPago']	= $rs->fields['idformaPago'];
                       $info[$id]['idtipoPago']	= $rs->fields['idtipoPago'];
                       $info[$id]['tratamiento']	= $rs->fields['tratamiento'];
                       $info[$id]['idpaciente']	= $rs->fields['idpaciente'];
                       $info[$id]['fechaPago']	= $rs->fields['fechaPago'];
                       $info[$id]['monto']	= $rs->fields['monto'];
                       $info[$id]['idusuario']	= $rs->fields['idusuario'];
                       $info[$id]['observaciones']	= $rs->fields['observaciones'];
                       $info[$id]['nombreUsu']	= $rs->fields['nombreUsu'];
                       $info[$id]['nombrePa']	= $rs->fields['nombrePa'];
                       $info[$id]['nombreseguro']	= $rs->fields['nombreseguro'];
                     
								
								
                $rs->MoveNext();
            }
            $rs->Close();
            return ( $info);
        } else {
            return false;
        }
    }


    function getAlltodoslosseguros($fecha1,$fecha2)
    {
        $idclinica = $_SESSION['csmart']['clinica'];
        $sql = " SELECT p.*, se.nombre as nombreseguro ,u.nombre as nombreUsu, pa.nombre as nombrePa 
        FROM pago p 
         inner join seguro se  on  p.idformaseguro=se.idseguro  
         inner join usuario u on  p.idusuario=u.idusuario
         inner join paciente pa on  p.idpaciente=pa.idpaciente  
           where u.idclinica=$idclinica and  p.fechaPago BETWEEN '$fecha1 23:59:00' and '$fecha2 23:59:00'  
                order by p.idpago desc;";
$rs = $this->DATA->Execute($sql);
if ( $rs->RecordCount()) {
    while(!$rs->EOF){
                $id                  	= $rs->fields['idpago'];
               $info[$id]['idpago']	= $rs->fields['idpago'];
               $info[$id]['idformaPago']	= $rs->fields['idformaPago'];
               $info[$id]['idtipoPago']	= $rs->fields['idtipoPago'];
               $info[$id]['tratamiento']	= $rs->fields['tratamiento'];
               $info[$id]['idpaciente']	= $rs->fields['idpaciente'];
               $info[$id]['fechaPago']	= $rs->fields['fechaPago'];
               $info[$id]['monto']	= $rs->fields['monto'];
               $info[$id]['idusuario']	= $rs->fields['idusuario'];
               $info[$id]['observaciones']	= $rs->fields['observaciones'];
               $info[$id]['nombreUsu']	= $rs->fields['nombreUsu'];
               $info[$id]['nombrePa']	= $rs->fields['nombrePa'];
               $info[$id]['nombreseguro']	= $rs->fields['nombreseguro'];              
        $rs->MoveNext();
    }
    $rs->Close();
    return json_encode($info);
} else {
    return false;
}   
    }



    function getAllxsxf1xf2($fecha1,$fecha2,$idseguro)
    {
        $idclinica = $_SESSION['csmart']['clinica'];
        $sql = " SELECT p.*, se.nombre as nombreseguro ,u.nombre as nombreUsu, pa.nombre as nombrePa 
        FROM pago p 
         inner join seguro se  on  p.idformaseguro=se.idseguro  
         inner join usuario u on  p.idusuario=u.idusuario
         inner join paciente pa on  p.idpaciente=pa.idpaciente  
           where u.idclinica=$idclinica and  p.idformaseguro=$idseguro and  p.fechaPago BETWEEN '$fecha1 23:59:00' and '$fecha2 23:59:00'  
                order by p.idpago desc;";
$rs = $this->DATA->Execute($sql);
if ( $rs->RecordCount()) {
    while(!$rs->EOF){
                $id                  	= $rs->fields['idpago'];
               $info[$id]['idpago']	= $rs->fields['idpago'];
               $info[$id]['idformaPago']	= $rs->fields['idformaPago'];
               $info[$id]['idtipoPago']	= $rs->fields['idtipoPago'];
               $info[$id]['tratamiento']	= $rs->fields['tratamiento'];
               $info[$id]['idpaciente']	= $rs->fields['idpaciente'];
               $info[$id]['fechaPago']	= $rs->fields['fechaPago'];
               $info[$id]['monto']	= $rs->fields['monto'];
               $info[$id]['idusuario']	= $rs->fields['idusuario'];
               $info[$id]['observaciones']	= $rs->fields['observaciones'];
               $info[$id]['nombreUsu']	= $rs->fields['nombreUsu'];
               $info[$id]['nombrePa']	= $rs->fields['nombrePa'];
               $info[$id]['nombreseguro']	= $rs->fields['nombreseguro'];              
        $rs->MoveNext();
    }
    $rs->Close();
    return json_encode($info);
} else {
    return false;
}   
    }


    function getAlltotalconseguroconpacientepor($idseguro)
    {
        $sql = " SELECT p.*, se.nombre as nombreseguro ,u.nombre as nombreUsu, pa.nombre as nombrePa 
        FROM pago p 
         inner join seguro se  on  p.idformaseguro=se.idseguro  
         inner join usuario u on  p.idusuario=u.idusuario
         inner join paciente pa on  p.idpaciente=pa.idpaciente  
           where p.idformaseguro=$idseguro ;";
$rs = $this->DATA->Execute($sql);
if ( $rs->RecordCount()) {
    while(!$rs->EOF){
                $id                  	= $rs->fields['idpago'];
               $info[$id]['idpago']	= $rs->fields['idpago'];
               $info[$id]['idformaPago']	= $rs->fields['idformaPago'];
               $info[$id]['idtipoPago']	= $rs->fields['idtipoPago'];
               $info[$id]['tratamiento']	= $rs->fields['tratamiento'];
               $info[$id]['idpaciente']	= $rs->fields['idpaciente'];
               $info[$id]['fechaPago']	= $rs->fields['fechaPago'];
               $info[$id]['monto']	= $rs->fields['monto'];
               $info[$id]['idusuario']	= $rs->fields['idusuario'];
               $info[$id]['observaciones']	= $rs->fields['observaciones'];
               $info[$id]['nombreUsu']	= $rs->fields['nombreUsu'];
               $info[$id]['nombrePa']	= $rs->fields['nombrePa'];
               $info[$id]['nombreseguro']	= $rs->fields['nombreseguro'];              
        $rs->MoveNext();
    }
    $rs->Close();
    return json_encode($info);
} else {
    return false;
}   
    }

    //pago total
    function getAlltotalconseguroconpaciente(){
        $sql = "SELECT p.*, se.nombre as nombreseguro ,u.nombre as nombreUsu, pa.nombre as nombrePa 
        		FROM pago p 
                 inner join seguro se  on  p.idformaseguro=se.idseguro  
                 inner join usuario u on  p.idusuario=u.idusuario
                 inner join paciente pa on  p.idpaciente=pa.idpaciente ;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
         		       $id                  	= $rs->fields['idpago'];
                       $info[$id]['idpago']	= $rs->fields['idpago'];
                       $info[$id]['idformaPago']	= $rs->fields['idformaPago'];
                       $info[$id]['idtipoPago']	= $rs->fields['idtipoPago'];
                       $info[$id]['tratamiento']	= $rs->fields['tratamiento'];
                       $info[$id]['idpaciente']	= $rs->fields['idpaciente'];
                       $info[$id]['fechaPago']	= $rs->fields['fechaPago'];
                       $info[$id]['monto']	= $rs->fields['monto'];
                       $info[$id]['idusuario']	= $rs->fields['idusuario'];
                       $info[$id]['observaciones']	= $rs->fields['observaciones'];
                       $info[$id]['nombreUsu']	= $rs->fields['nombreUsu'];
                       $info[$id]['nombrePa']	= $rs->fields['nombrePa'];
                       $info[$id]['nombreseguro']	= $rs->fields['nombreseguro'];              
                $rs->MoveNext();
            }
            $rs->Close();
            return ( $info);
        } else {
            return false;
        }
    }


    function getone($id){
        $sql = "SELECT * FROM pago WHERE idpago = ?;";
        $rs = $this->DATA->Execute($sql, $id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id  = $rs->fields['idpago'];
                $info[$id]['idpago'] = $rs->fields['idpago'];
                $info[$id]['idformaPago']    = $rs->fields['idformaPago'];
                $info[$id]['idtipoPago']  = $rs->fields['idtipoPago'];
                $info[$id]['tratamiento']   = $rs->fields['tratamiento'];
                $info[$id]['idpaciente'] = $rs->fields['idpaciente'];
                $info[$id]['fechaPago'] = $rs->fields['fechaPago']; 
                $info[$id]['idusuario'] = $rs->fields['idusuario']; 
                $info[$id]['monto'] = $rs->fields['monto']; 
                $info[$id]['observaciones'] = $rs->fields['observaciones']; 
               
                $rs->MoveNext();
            }
            $rs->Close();
            return json_encode( $info);
        } else {
            return false;
        }
    }

    function nuevo($params) 
	{
		try{
		
			$sql = "INSERT INTO pago (idformaPago,idformaseguro,idtipoPago,tratamiento,idpaciente,fechaPago,monto,idusuario,observaciones) VALUES (?,?,?,?,?,?,?,?,?);";
				
			$save = $this->DATA->Execute($sql, $params);
			$id 	= $this->DATA->Insert_ID();
			if ($save){
				return json_encode($id);
			} else {
				return false;
			}
		}catch(exception $e){
			return false;
		}
        
    }
    function actualizar($params){
        try{
        
            $sql = "UPDATE pago SET monto=?,idformaPago=?,tratamiento=?,fechaPago=?,observaciones=? WHERE idpago = ?";
            $save = $this->DATA->Execute($sql, $params);
            if ($save){
                return true;
            } else {
                return false;
            }
        }catch(exception $e){
            return false;
        }
    }
    function eliminar($id){
        $sql = "delete from pago where idpago = ?";
        $update = $this->DATA->Execute($sql, $id);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
//reportes
function todosloscreditos($inicio,$fin,$clinica){
    $sql = "SELECT p.*,u.nombre as nombreUsu,CONCAT(pa.nombre,' ',pa.apellido) as nombrePa , p.monto as total,u.idusuario,u.idclinica,
    if((select sum(monto) from pagoTratamiento where idPago=p.idpago),(select sum(monto) from pagoTratamiento where idPago=p.idpago),0)as pagado,
    if((p.monto-(select sum(monto) from pagoTratamiento where idPago=p.idpago)),(p.monto-(select sum(monto) from pagoTratamiento where idPago=p.idpago)),p.monto) as debe
               FROM pago p , usuario u,paciente pa 
               where p.idusuario=u.idusuario and p.idpaciente=pa.idpaciente and idtipoPago=2
               and u.idclinica= $clinica 
               and p.fechapago between '$inicio 00:00' and '$fin 23:59:00' group by p.idpago
             ;";
    $rs = $this->DATA->Execute($sql);
    if ( $rs->RecordCount()) {
        while(!$rs->EOF){

                   $id                  	= $rs->fields['idpago'];
                   $info[$id]['idpago']	= $rs->fields['idpago'];
                   $info[$id]['idformaPago']	= $rs->fields['idformaPago'];
                   $info[$id]['idtipoPago']	= $rs->fields['idtipoPago'];
                   $info[$id]['tratamiento']	= $rs->fields['tratamiento'];
                   $info[$id]['idpaciente']	= $rs->fields['idpaciente'];
                   $info[$id]['fechaPago']	= $rs->fields['fechaPago'];
                   $info[$id]['monto']	= $rs->fields['monto'];
                   $info[$id]['idusuario']	= $rs->fields['idusuario'];
                   $info[$id]['observaciones']	= $rs->fields['observaciones'];
                   $info[$id]['nombreUsu']	= $rs->fields['nombreUsu'];
                   $info[$id]['nombrePa']	= $rs->fields['nombrePa'];
                   $info[$id]['debe']	= $rs->fields['debe'];
                   $info[$id]['total']	= $rs->fields['total']; 
                   $info[$id]['pagado']	= $rs->fields['pagado'];
                   
                   
                            
                            
            $rs->MoveNext();
        }
        $rs->Close();
        return json_encode( $info);
    } else {
        return false;
    }
}

    
}?>