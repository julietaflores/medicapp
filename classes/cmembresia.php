<?php
class Membresia {
    //Constructor
    function __construct() {
        global $DATA;
        $this->DATA = $DATA;
    }


    
    function getAll1()
    {   $e=1;
        $sql = "SELECT * FROM membresias where idestado=? order by idmembresia asc;";
        $rs = $this->DATA->Execute($sql,$e);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                    $id  = $rs->fields['idmembresia'];
                    $info[$id]['idmembresia'] = $rs->fields['idmembresia'];
                    $info[$id]['nombre']    = $rs->fields['nombre'];
                    $info[$id]['valor']  = $rs->fields['valor'];
                    $info[$id]['duracion']  = $rs->fields['duracion'];
                    $info[$id]['idestado'] = $rs->fields['idestado']; 
                    $info[$id]['nusuarios'] = $rs->fields['nusuarios']; 
                    $info[$id]['detalle'] = $rs->fields['detalle']; 
                    $info[$id]['tiempo'] = $rs->fields['tiempo']; 
                    $info[$id]['moneda'] = $rs->fields['moneda']; 
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }


    function getAll()
    {   
        $sql = "SELECT * FROM membresias order by idmembresia asc;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                    $id  = $rs->fields['idmembresia'];
                    $info[$id]['idmembresia'] = $rs->fields['idmembresia'];
                    $info[$id]['nombre']    = $rs->fields['nombre'];
                    $info[$id]['valor']  = $rs->fields['valor'];
                    $info[$id]['duracion']  = $rs->fields['duracion'];
                    $info[$id]['idestado'] = $rs->fields['idestado']; 
                    $info[$id]['nusuarios'] = $rs->fields['nusuarios']; 
                    $info[$id]['actodesc'] = $rs->fields['actodesc']; 
                    $info[$id]['detalle'] = $rs->fields['detalle']; 
                    $info[$id]['tiempo'] = $rs->fields['tiempo']; 
                    $info[$id]['moneda'] = $rs->fields['moneda']; 
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }
    


    function getAllnogratis()
    {   
        $sql = "SELECT * FROM membresias where valor!=0 and idestado=1 order by idmembresia asc ;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                    $id  = $rs->fields['idmembresia'];
                    $info[$id]['idmembresia'] = $rs->fields['idmembresia'];
                    $info[$id]['nombre']    = $rs->fields['nombre'];
                    $info[$id]['valor']  = $rs->fields['valor'];
                    $info[$id]['duracion']  = $rs->fields['duracion'];
                    $info[$id]['idestado'] = $rs->fields['idestado']; 
                    $info[$id]['nusuarios'] = $rs->fields['nusuarios'];
                    $info[$id]['actodesc'] = $rs->fields['actodesc'];
                    $info[$id]['detalle'] = $rs->fields['detalle']; 
                    $info[$id]['tiempo'] = $rs->fields['tiempo']; 
                    $info[$id]['moneda'] = $rs->fields['moneda']; 
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }
	
	function getOne($id)
    {
        $sql = "SELECT * FROM membresias WHERE idmembresia = ?;";
        $rs = $this->DATA->Execute($sql, $id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id  = $rs->fields['idmembresia'];
                $info[$id]['idmembresia'] = $rs->fields['idmembresia'];
                $info[$id]['nombre']    = $rs->fields['nombre'];
                $info[$id]['valor']  = $rs->fields['valor'];
                $info[$id]['duracion']   = $rs->fields['duracion'];
                $info[$id]['idestado'] = $rs->fields['idestado'];
                $info[$id]['nusuarios'] = $rs->fields['nusuarios']; 
                $info[$id]['detalle'] = $rs->fields['detalle']; 
                $info[$id]['tiempo'] = $rs->fields['tiempo']; 
                $info[$id]['moneda'] = $rs->fields['moneda']; 
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }



    function getOnedesc($id)
    {
        $sql = "SELECT * FROM membresias WHERE idmembresia = ? ;";
        $rs = $this->DATA->Execute($sql, $id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
					$info   = $rs->fields['actodesc'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }

    function getOneduracion($id)
    {
        $sql = "SELECT * FROM membresias WHERE idmembresia = ? ;";
        $rs = $this->DATA->Execute($sql, $id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
					$info   = $rs->fields['duracion'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }


    function getOneTiempo($id)
    {
        $sql = "SELECT * FROM membresias WHERE idmembresia = ? ;";
        $rs = $this->DATA->Execute($sql, $id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
					$info   = $rs->fields['tiempo'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }


    function getOneCosto($id)
    {
        $sql = "SELECT * FROM membresias WHERE idmembresia = ? ;";
        $rs = $this->DATA->Execute($sql, $id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
					$info   = $rs->fields['valor'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }



    
    function getnumusuario($id)
    {
        $sql = "SELECT * FROM membresias WHERE idmembresia = ? ;";
        $rs = $this->DATA->Execute($sql, $id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
					$info   = $rs->fields['nusuarios'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }



    function nuevo($params) 
    {
        try{
            $sql = "INSERT INTO membresias (nombre,valor,duracion,idestado,nusuarios,detalle,moneda,tiempo) VALUES (?,?,?,?,?);";     
            $save = $this->DATA->Execute($sql, $params);
            $id     = $this->DATA->Insert_ID();
            if ($save){
                return $id;
            } else {
                return false;
            }
        }catch(exception $e){
            return false;
        }  
    }
    

	

    function actualizar($params) 
    {
        try{
        
            $sql = "UPDATE membresias SET nombre=?,valor=?,duracion=?,idestado=?,nusuarios=?,detalle=?,moneda=?,tiempo=? WHERE idmembresia = ?";
            $save = $this->DATA->Execute($sql, $params);
            if ($save){
                return true;
            } else {
                return false;
            }
        }catch(exception $e){
            return false;
        }
        
    }



    
    function habilitar($id) {
        $e=1;
        $sql = "UPDATE membresias SET actodesc = '$e' WHERE idmembresia = ?;";
        $update = $this->DATA->Execute($sql, $id);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
    function deshabilitar($id) {
        $e=0;
        $sql = "UPDATE membresias SET actodesc = '$e' WHERE idmembresia = ?;";
        $update = $this->DATA->Execute($sql, $id);
        if ($update){
            return true;
        } else {
            return false;
        }
    }

	function eliminar($id) {
        $sql = "delete from membresias where idmembresia = ?";
        $update = $this->DATA->Execute($sql, $id);
        if ($update){
            return true;
        } else {
            return false;
        }
    }

}
?>