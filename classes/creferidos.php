<?php
class referido{
    function __construct() {
        global $DATA;
        $this->DATA = $DATA;
    }
    function getAll($idclinica)
    {   
        $sql = "SELECT r.*,(select nombre from clinica where idclinica=r.idclinica) as nombreclini 
        FROM referido r where idclinicaref=$idclinica;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                    $id  = $rs->fields['idreferido'];
                    $info[$id]['idreferido'] = $rs->fields['idreferido'];
                    $info[$id]['idclinica']    = $rs->fields['idclinica'];
                    $info[$id]['nombre']  = $rs->fields['nombre'];
                    $info[$id]['contacto']  = $rs->fields['contacto'];
                    $info[$id]['idestado'] = $rs->fields['idestado']; 
                    $info[$id]['direccion'] = $rs->fields['direccion']; 
                    $info[$id]['telefono'] = $rs->fields['telefono']; 
                    $info[$id]['correo'] = $rs->fields['correo']; 
                    $info[$id]['comentario'] = $rs->fields['comentario']; 
                    $info[$id]['idclinicaref'] = $rs->fields['idclinicaref']; 
                    $info[$id]['nombreclini'] = $rs->fields['nombreclini']; 
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }
    function getAllsiexiste($idclinica)
    {   
        $sql = "SELECT * from referido where idclinica=$idclinica;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                    $id  = $rs->fields['idreferido'];
                    $info[$id]['idreferido'] = $rs->fields['idreferido'];
                    
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }
    function nuevo($params) 
    {
        try{
            $sql = "INSERT INTO referido (idclinica,nombre,contacto,direccion,telefono,correo,idestado,comentario,idclinicaref) VALUES (?,?,?,?,?,?,?,?,?);";     
            $save = $this->DATA->Execute($sql, $params);
            $id     = $this->DATA->Insert_ID();
            if ($save){
                return $id;
            } else {
                return false;
            }
        }catch(exception $e){
            return false;
        }  
    }
    function actualizar($params) 
    {
        try{
        $r=1;
            $sql = "UPDATE referido SET idestado='$r' WHERE idclinica = ?";
            $save = $this->DATA->Execute($sql, $params);
            if ($save){
                return true;
            } else {
                return false;
            }
        }catch(exception $e){
            return false;
        }
        
    }
    function contardescuento($id,$fecha){
        $sql = "select  r.*,m.fechainicio,m.fechafin,m.idestado 
        from referido r, membresiasClinica m,membresias me
        where r.idclinica=m.idclinica
        and me.idmembresia=m.idmembresia
        and me.actodesc=1
        and r.idclinicaref=$id
        and m.idestado=1
        and m.fechainicio <= '$fecha'";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                    $id  = $rs->fields['idreferido'];
                    $info[$id]['idclinica']=$rs->fields['idclinica'];
                    $info[$id]['fechainicio']    = $rs->fields['fechainicio'];
                     
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }

    }


}
?>