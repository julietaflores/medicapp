<?php

class pacienteNotas{

	function __construct() {
        global $DATA;
        $this->DATA = $DATA;
    }
	
	 function getAll($idpaciente)
    {
        $sql = "SELECT p.idpacienteNota,p.idpaciente,p.detalle,p.fecha,u.nombre FROM pacienteNotas p 
        inner join usuario u on p.idusuario=u.idusuario WHERE p.idpaciente= $idpaciente order by idpacienteNota desc;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){

         		$id                  	= $rs->fields['idpacienteNota'];
				   	$info[$id]['idpacienteNota']	= $rs->fields['idpacienteNota'];
								$info[$id]['idpaciente']	= $rs->fields['idpaciente'];
								$info[$id]['detalle']	= $rs->fields['detalle'];
                                $info[$id]['fecha']	= $rs->fields['fecha'];
                                $info[$id]['start']	= $rs->fields['fecha'];
								$info[$id]['idusuario']	= $rs->fields['nombre'];
								
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }

    function citanotaJson($idpaciente,$pag)
    {
        $sql = "SELECT id, idpaciente, title,fecha,observaciones,idestado,idtipocita,maxSup,maxInf from
        (select id, idpaciente,title,start as fecha,observaciones,idestado,idtipocita,maxSup,maxInf 
        from cita 
        where idpaciente=$idpaciente
        union all
        select idpacienteNota,idpaciente,detalle,fecha,'nota',1 as idestado,1 as idtipocita,'notasup' as maxSup ,'notainf' as maxInf 
        from pacienteNotas 
        where idpaciente=$idpaciente)ti order by fecha desc limit 10 offset $pag";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){

                $id                  	= $rs->fields['id'];
                $info[$id]['idpaciente']	= $rs->fields['idpaciente'];
             $info[$id]['title']	= $rs->fields['title'];
             //$info[$id]['detalle']	= $rs->fields['detalle'];
             $info[$id]['fecha']	= $rs->fields['fecha'];
             $info[$id]['observaciones']	= $rs->fields['observaciones'];
             $info[$id]['idestado']	= $rs->fields['idestado'];
             $info[$id]['idtipocita']	= $rs->fields['idtipocita'];
             $info[$id]['maxSup']	= $rs->fields['maxSup'];
             $info[$id]['maxInf']	= $rs->fields['maxInf'];
								
                $rs->MoveNext();
            }
            $rs->Close();
            return json_encode( $info);
        } else {
            return false;
        }
    }
    function citanotaPhp($idpaciente)
    {
        $sql = "SELECT id, idusuario,idpaciente,(select  u.nombre from usuario u where u.idusuario=ti.idusuario) as nomu, title,fecha,observaciones,idestado,idtipocita,maxSup,maxInf ,  anotaciones,indicaciones,prescripcion,diagnostico 
        from
        (select id, idusuario,idpaciente,title,start as fecha,observaciones,idestado,idtipocita,maxSup,maxInf ,anotaciones,indicaciones,prescripcion,diagnostico 
        from cita 
        where idpaciente=$idpaciente
        union all
        select idpacienteNota,idusuario,idpaciente,detalle,fecha,'nota',1 as idestado,1 as idtipocita,'notasup' as maxSup ,'notainf' as maxInf , 'anotaciones' as anotaciones,'indicaciones' as indicaciones,
                        'prescripcion' as prescripcion, 'diagnostico' as diagnostico 
        from pacienteNotas 
        where idpaciente=$idpaciente)ti order by fecha desc limit 10 offset 0";
        
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){

         		                $id                  	= $rs->fields['id'];
				   	            $info[$id]['idpaciente']	= $rs->fields['idpaciente'];
                                $info[$id]['title']	= $rs->fields['title'];
                                $info[$id]['idusuario']	= $rs->fields['idusuario'];
                                $info[$id]['nomu']	= $rs->fields['nomu'];
								$info[$id]['id']	= $rs->fields['id'];
                                $info[$id]['fecha']	= $rs->fields['fecha'];
                                $info[$id]['observaciones']	= $rs->fields['observaciones'];
                                $info[$id]['idestado']	= $rs->fields['idestado'];
                                $info[$id]['idtipocita']	= $rs->fields['idtipocita'];
                                $info[$id]['maxSup']	= $rs->fields['maxSup'];
                                $info[$id]['maxInf']	= $rs->fields['maxInf'];
                                $info[$id]['anotaciones']	= $rs->fields['anotaciones'];
                                $info[$id]['indicaciones']	= $rs->fields['indicaciones'];
                                $info[$id]['prescripcion']	= $rs->fields['prescripcion'];
                                $info[$id]['diagnostico']	= $rs->fields['diagnostico'];
                                
                $rs->MoveNext();
            }
            $rs->Close();
            return ( $info);
        } else {
            return false;
        }
    }
	
	function getOne($id)
    {
        $sql = "SELECT * FROM pacienteNotas WHERE idpacienteNota = ? ;";

        $rs = $this->DATA->Execute($sql, $id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id                  	= $rs->fields['idpacienteNota'];
				   	$info[$id]['idpacienteNota']	= $rs->fields['idpacienteNota'];
								$info[$id]['idpaciente']	= $rs->fields['idpaciente'];
								$info[$id]['detalle']	= $rs->fields['detalle'];
								$info[$id]['fecha']	= $rs->fields['fecha'];
								$info[$id]['idusuario']	= $rs->fields['idusuario'];
								
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }


	function nuevo($params) 
	{
		try{
		
			$sql = "INSERT INTO pacienteNotas (idpaciente,detalle,idusuario) VALUES (?,?,?);";
				 
			$save = $this->DATA->Execute($sql, $params);
			$id 	= $this->DATA->Insert_ID();
			if ($save){
				return $id;
			} else {
				return false;
			}
		}catch(exception $e){
			return false;
		}
        
    }

	function actualizar($params) 
	{
		try{
		
			$sql = "UPDATE pacienteNotas SET idpaciente=?,detalle=?,fecha=?,idusuario=? WHERE idpacienteNota = ?";
			$save = $this->DATA->Execute($sql, $params);
			if ($save){
				return true;
			} else {
				return false;
			}
		}catch(exception $e){
			return false;
		}
        
    }
	
	function bloqueo($id) {
        $sql = "UPDATE pacienteNotas SET estado = 2 WHERE idpacienteNota = ?";

        $update = $this->DATA->Execute($sql, $id);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	
	
	function habilitar($id) {
        $sql = "UPDATE pacienteNotas SET estado = 1 WHERE idpacienteNota = ?";

        $update = $this->DATA->Execute($sql, $id);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	
	function eliminar($id) {
        $sql = "delete from pacienteNotas where idpacienteNota = ?";

        $update = $this->DATA->Execute($sql, $id);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	
}
?>