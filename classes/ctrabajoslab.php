<?php
class TrabajosLab{
    function __construct() {
        global $DATA;
        $this->DATA = $DATA;
    }
    //credito
    function getAll($idclinica){
        $sql = "SELECT tl.*,(select nombre from paciente where idpaciente =tl.idpaciente )as nombrepaciente ,lab.nombre as nombrelab
        from TrabajosLab tl,Laboratorio lab
        where tl.idlaboratorio=lab.idlaboratorio
        and tl.idclinica=$idclinica";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){

         		        $id                  	= $rs->fields['idtrabajo'];
                       $info[$id]['idtrabajo']	= $rs->fields['idtrabajo'];
                       $info[$id]['idclinica']	= $rs->fields['idclinica'];
                       $info[$id]['idpaciente']	= $rs->fields['idpaciente'];
                       $info[$id]['idestado']	= $rs->fields['idestado'];
                       $info[$id]['detalle']	= $rs->fields['detalle'];
                       $info[$id]['fechaEnvio']	= $rs->fields['fechaEnvio'];
                       $info[$id]['fechaEntrega']	= $rs->fields['fechaEntrega'];
                       $info[$id]['nombrepaciente']	= $rs->fields['nombrepaciente'];
                       $info[$id]['nombrelab']	= $rs->fields['nombrelab'];
                       $info[$id]['idcita']	= $rs->fields['idcita'];
                       $info[$id]['precio']	= $rs->fields['precio'];
            				
                $rs->MoveNext();
            }
            $rs->Close();
            return ( $info);
        } else {
            return false;
        }
    }
  
   
    function getone($id){
        $sql = "SELECT tl.*,(select nombre from paciente where idpaciente =tl.idpaciente )as nombrepaciente ,lab.nombre as nombrelab
        from TrabajosLab tl,Laboratorio lab
        where tl.idlaboratorio=lab.idlaboratorio
        and tl.idtrabajo=$id";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id                  	= $rs->fields['idtrabajo'];
                $info[$id]['idtrabajo']	= $rs->fields['idtrabajo'];
                $info[$id]['idclinica']	= $rs->fields['idclinica'];
                $info[$id]['idpaciente']	= $rs->fields['idpaciente'];
                $info[$id]['idestado']	= $rs->fields['idestado'];
                $info[$id]['detalle']	= $rs->fields['detalle'];
                $info[$id]['fechaEnvio']	= $rs->fields['fechaEnvio'];
                $info[$id]['fechaEntrega']	= $rs->fields['fechaEntrega'];
                $info[$id]['nombrepaciente']	= $rs->fields['nombrepaciente'];
                $info[$id]['nombrelab']	= $rs->fields['nombrelab'];
                $info[$id]['idlaboratorio']	= $rs->fields['idlaboratorio'];
                $info[$id]['precio']	= $rs->fields['precio'];
            			
                $rs->MoveNext();
            }
            $rs->Close();
            return json_encode( $info);
        } else {
            return false;
        }
    }

    function nuevo($params) 
	{
		try{
		
			$sql = "INSERT INTO TrabajosLab (idclinica,idpaciente,detalle,fechaEnvio,fechaEntrega,idlaboratorio,idcita,precio) VALUES (?,?,?,?,?,?,?,?);";
				
			$save = $this->DATA->Execute($sql, $params);
			$id 	= $this->DATA->Insert_ID();
			if ($save){
				return json_encode($id);
			} else {
				return false;
			}
		}catch(exception $e){
			return false;
		}
        
    }
    function actualizar($params){
        try{
        
            $sql = "UPDATE TrabajosLab SET idclinica=?, idpaciente=?,detalle=?,fechaEnvio=?,fechaEntrega=?,idlaboratorio=?,precio=? WHERE idtrabajo = ?";
            $save = $this->DATA->Execute($sql, $params);
            if ($save){
                return true;
            } else {
                return false;
            }
        }catch(exception $e){
            return false;
        }
    }

    function actestadotrabajo($id,$estado){
        try{
        
            $sql = "UPDATE TrabajosLab SET  idestado=$estado WHERE idtrabajo = $id";
            $save = $this->DATA->Execute($sql);
            if ($save){
                return true;
            } else {
                return false;
            }
        }catch(exception $e){
            return false;
        }
    }

    function eliminar($id){
        $sql = "delete from TrabajosLab where idtrabajo = ?";
        $update = $this->DATA->Execute($sql, $id);
        if ($update){
            return true;
        } else {
            return false;
        }
    }



    
}?>