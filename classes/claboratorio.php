<?php
class Laboratorio{
    function __construct() {
        global $DATA;
        $this->DATA = $DATA;
    }
    //credito
    function getAll($idclinica){
        $sql = "SELECT * from Laboratorio where idclinica=$idclinica;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){

         		        $id                  	= $rs->fields['idlaboratorio'];
                       $info[$id]['idlaboratorio']	= $rs->fields['idlaboratorio'];
                       $info[$id]['idclinica']	= $rs->fields['idclinica'];
                       $info[$id]['nombre']	= $rs->fields['nombre'];
                       $info[$id]['telefono']	= $rs->fields['telefono'];
                       $info[$id]['correo']	= $rs->fields['correo'];
                       $info[$id]['estado']	= $rs->fields['estado'];
            				
                $rs->MoveNext();
            }
            $rs->Close();
            return ( $info);
        } else {
            return false;
        }
    }
  
   
    function getone($id){
        $sql = "SELECT * FROM Laboratorio WHERE idlaboratorio = ?;";
        $rs = $this->DATA->Execute($sql, $id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id                  	= $rs->fields['idlaboratorio'];
                $info[$id]['idlaboratorio']	= $rs->fields['idlaboratorio'];
                $info[$id]['idclinica']	= $rs->fields['idclinica'];
                $info[$id]['nombre']	= $rs->fields['nombre'];
                $info[$id]['telefono']	= $rs->fields['telefono'];
                $info[$id]['correo']	= $rs->fields['correo'];
                $info[$id]['estado']	= $rs->fields['estado'];
               
                $rs->MoveNext();
            }
            $rs->Close();
            return json_encode( $info);
        } else {
            return false;
        }
    }

    function nuevo($params) 
	{
		try{
		
			$sql = "INSERT INTO Laboratorio (idclinica,nombre,telefono,correo) VALUES (?,?,?,?);";
				
			$save = $this->DATA->Execute($sql, $params);
			$id 	= $this->DATA->Insert_ID();
			if ($save){
				return json_encode($id);
			} else {
				return false;
			}
		}catch(exception $e){
			return false;
		}
        
    }
    function actualizar($params){
        try{
        
            $sql = "UPDATE Laboratorio SET idclinica=?,nombre=?,telefono=?,correo=? WHERE idlaboratorio = ?";
            $save = $this->DATA->Execute($sql, $params);
            if ($save){
                return true;
            } else {
                return false;
            }
        }catch(exception $e){
            return false;
        }
    }
    function eliminar($id){
        $sql = "delete from Laboratorio where idlaboratorio = ?";
        $update = $this->DATA->Execute($sql, $id);
        if ($update){
            return true;
        } else {
            return false;
        }
    }


    
}?>