<?php
class MembresiaClinica {
    //Constructor
    function __construct() {
        global $DATA;
        $this->DATA = $DATA;
    }


    function getusuclinica($idclinica) {
        $info=0;
        $sql = "SELECT count(*) as cantidad  from usuario where  idclinica='$idclinica' ;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $info	= $rs->fields['cantidad'];									
                $rs->MoveNext();
            }
            $rs->Close();
        } 
        return $info;
    }



    function getAll()
    {
        $sql = "SELECT m.*,c.nombre as nombrec , mm.nombre as nombrem,  
        concat(u.nombre,' ',u.apellido) as nombreu FROM membresiasClinica m 
        join clinica c on m.idclinica=c.idclinica  
        Inner join membresias mm on mm.idmembresia=m.idmembresia 
        Inner join usuario u on u.idusuario=m.idusuario 
        ORDER BY  m.idmembresiasClinica DESC;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                    $id  = $rs->fields['idmembresiasClinica'];
                    $info[$id]['idmembresiasClinica'] = $rs->fields['idmembresiasClinica'];
                    $info[$id]['idmembresia'] = $rs->fields['idmembresia'];
                    $info[$id]['nombrem'] = $rs->fields['nombrem'];
                    $info[$id]['nombreu'] = $rs->fields['nombreu'];
                    $info[$id]['fechaCompra']    = $rs->fields['fechaCompra'];
                    $info[$id]['idformaPago']  = $rs->fields['idformaPago'];
                    $info[$id]['idestado']  = $rs->fields['idestado'];
                    $info[$id]['idclinica'] = $rs->fields['idclinica']; 
                    $info[$id]['nombrec'] = $rs->fields['nombrec']; 
                    $info[$id]['valor'] = $rs->fields['valor']; 
                    $info[$id]['idusuario'] = $rs->fields['idusuario']; 
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }




    function getAllmc($idusuario)
    {
        $sql = "SELECT m.*,mm.nombre as nombrem ,mm.duracion as duracionm 
        FROM membresiasClinica m  join membresias mm on mm.idmembresia=m.idmembresia  
        where m.idusuario=? ORDER BY  m.idmembresiasClinica DESC;";
        $rs = $this->DATA->Execute($sql,$idusuario);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                    $id  = $rs->fields['idmembresiasClinica'];
                    $info[$id]['idmembresiasClinica'] = $rs->fields['idmembresiasClinica'];
                    $info[$id]['idmembresia'] = $rs->fields['idmembresia'];
                    $info[$id]['nombrem'] = $rs->fields['nombrem'];
                    $info[$id]['duracionm'] = $rs->fields['duracionm'];
                    $info[$id]['fechaCompra']    = $rs->fields['fechaCompra'];
                 
                    $info[$id]['fechafin']    = $rs->fields['fechafin'];
                    $info[$id]['fechainicio']    = $rs->fields['fechainicio'];
                    $info[$id]['idformaPago']  = $rs->fields['idformaPago'];
                    $info[$id]['idestado']  = $rs->fields['idestado'];
                    $info[$id]['idclinica'] = $rs->fields['idclinica']; 
                    $info[$id]['valor'] = $rs->fields['valor']; 
                    $info[$id]['idusuario'] = $rs->fields['idusuario']; 
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return "";
        }
    }


    function getAllmcc($idclinica)
    {
        $sql = "SELECT m.*,mm.nombre as nombrem ,mm.duracion as duracionm 
        FROM membresiasClinica m  join membresias mm on mm.idmembresia=m.idmembresia  
        where m.idclinica=? ORDER BY  m.idmembresiasClinica DESC;";
        $rs = $this->DATA->Execute($sql,$idclinica);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                    $id  = $rs->fields['idmembresiasClinica'];
                    $info[$id]['idmembresiasClinica'] = $rs->fields['idmembresiasClinica'];
                    $info[$id]['idmembresia'] = $rs->fields['idmembresia'];
                    $info[$id]['nombrem'] = $rs->fields['nombrem'];
                    $info[$id]['duracionm'] = $rs->fields['duracionm'];
                    $info[$id]['fechaCompra']    = $rs->fields['fechaCompra'];
                 
                    $info[$id]['fechafin']    = $rs->fields['fechafin'];
                    $info[$id]['fechainicio']    = $rs->fields['fechainicio'];
                    $info[$id]['idformaPago']  = $rs->fields['idformaPago'];
                    $info[$id]['idestado']  = $rs->fields['idestado'];
                    $info[$id]['idclinica'] = $rs->fields['idclinica']; 
                    $info[$id]['valor'] = $rs->fields['valor']; 
                    $info[$id]['idusuario'] = $rs->fields['idusuario']; 
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return "";
        }
    }

    function getAll1($idusuario)
    {
        $sql = "SELECT m.*,mm.nombre as nombrem ,mm.duracion as duracionm 
        FROM membresiasClinica m  join membresias mm on mm.idmembresia=m.idmembresia  
        where  m.idestado=1 and m.idusuario=?   ORDER BY  m.idmembresiasClinica DESC;";
        $rs = $this->DATA->Execute($sql,$idusuario);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                    $id  = $rs->fields['idmembresiasClinica'];
                    $info[$id]['idmembresiasClinica'] = $rs->fields['idmembresiasClinica'];
                    $info[$id]['idmembresia'] = $rs->fields['idmembresia'];
                    $info[$id]['nombrem'] = $rs->fields['nombrem'];
                    $info[$id]['duracionm'] = $rs->fields['duracionm'];
                    $info[$id]['fechaCompra']    = $rs->fields['fechaCompra'];
                    $info[$id]['fechafin']    = $rs->fields['fechafin'];
                    $info[$id]['fechainicio']    = $rs->fields['fechainicio'];
                    $info[$id]['idformaPago']  = $rs->fields['idformaPago'];
                    $info[$id]['idestado']  = $rs->fields['idestado'];
                    $info[$id]['idclinica'] = $rs->fields['idclinica']; 
                    $info[$id]['valor'] = $rs->fields['valor']; 
                    $info[$id]['idusuario'] = $rs->fields['idusuario']; 
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return "";
        }
    }

    
    function getAllc($idclinica)
    {
        $sql = "SELECT m.*,mm.nombre as nombrem ,mm.detalle as detallem, mm.duracion as duracionm , mm.nusuarios as nusuariosm,
        mm.nroImg
        FROM membresiasClinica m  join membresias mm on mm.idmembresia=m.idmembresia  
        where  m.idestado=1 and m.idclinica=?  ORDER BY  m.idmembresiasClinica DESC;";
        $rs = $this->DATA->Execute($sql,$idclinica);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                    $id  = $rs->fields['idmembresiasClinica'];
                    $info[$id]['idmembresiasClinica'] = $rs->fields['idmembresiasClinica'];
                    $info[$id]['idmembresia'] = $rs->fields['idmembresia'];
                    $info[$id]['nombrem'] = $rs->fields['nombrem'];
                    $info[$id]['detallem'] = $rs->fields['detallem'];
                    $info[$id]['duracionm'] = $rs->fields['duracionm'];
                    $info[$id]['fechaCompra']    = $rs->fields['fechaCompra'];
                    $info[$id]['fechafin']    = $rs->fields['fechafin'];
                    $info[$id]['fechainicio']    = $rs->fields['fechainicio'];
                    $info[$id]['nusuariosm']    = $rs->fields['nusuariosm'];
                    $info[$id]['idformaPago']  = $rs->fields['idformaPago'];
                    $info[$id]['idestado']  = $rs->fields['idestado'];
                    $info[$id]['idclinica'] = $rs->fields['idclinica']; 
                    $info[$id]['valor'] = $rs->fields['valor']; 
                    $info[$id]['idusuario'] = $rs->fields['idusuario'];
                    $info[$id]['nroImg'] = $rs->fields['nroImg']; 
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return "";
        }
    }
    


    function getAll1p($idusuario)
    {
        $sql = "SELECT m.*,mm.nombre as nombrem ,mm.duracion as duracionm FROM membresiasClinica m  join membresias mm on mm.idmembresia=m.idmembresia  where  m.idestado=0 and m.idusuario=? ;";
        $rs = $this->DATA->Execute($sql,$idusuario);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                    $id  = $rs->fields['idmembresiasClinica'];
                    $info[$id]['idmembresiasClinica'] = $rs->fields['idmembresiasClinica'];
                    $info[$id]['idmembresia'] = $rs->fields['idmembresia'];
                    $info[$id]['nombrem'] = $rs->fields['nombrem'];
                    $info[$id]['duracionm'] = $rs->fields['duracionm'];
                    $info[$id]['fechaCompra']    = $rs->fields['fechaCompra'];
                    $info[$id]['idformaPago']  = $rs->fields['idformaPago'];
                    $info[$id]['idestado']  = $rs->fields['idestado'];
                    $info[$id]['idclinica'] = $rs->fields['idclinica']; 
                    $info[$id]['valor'] = $rs->fields['valor']; 
                    $info[$id]['idusuario'] = $rs->fields['idusuario']; 
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }


	function getTotalusuariomc($idusuario){
		$sql = "SELECT count(m.idusuario) as cantidad 
        FROM membresiasClinica m  join membresias mm on mm.idmembresia=m.idmembresia  
        where  m.idestado=2 and m.idusuario=? ;";
		$rs = $this->DATA->Execute($sql,$idusuario);
		if ( $rs->RecordCount()) {
				while(!$rs->EOF){
					$info	= $rs->fields['cantidad'];									
					$rs->MoveNext();
				}
				$rs->Close();
				return $info;
		} else {
				return false;
		}
	}


    function getTotalusuariomcl($idclinica){
		$sql = "SELECT count(m.idclinica) as cantidad 
        FROM membresiasClinica m  join membresias mm on mm.idmembresia=m.idmembresia  
        where  m.idestado=2 and m.idclinica=? ;";
		$rs = $this->DATA->Execute($sql,$idclinica);
		if ( $rs->RecordCount()) {
				while(!$rs->EOF){
					$info	= $rs->fields['cantidad'];									
					$rs->MoveNext();
				}
				$rs->Close();
				return $info;
		} else {
				return false;
		}
	}

    function getTotalpendiente($idusuario){
		$sql = "SELECT count(m.idusuario) as cantidad 
        FROM membresiasClinica m  join membresias mm on mm.idmembresia=m.idmembresia  
        where  m.idestado=0 and m.idusuario=? ;";
		$rs = $this->DATA->Execute($sql,$idusuario);
		if ( $rs->RecordCount()) {
				while(!$rs->EOF){
					$info	= $rs->fields['cantidad'];									
					$rs->MoveNext();
				}
				$rs->Close();
				return $info;
		} else {
				return false;
		}
	}

 

    function getAllv($idusuario)
    {
        $sql = "SELECT m.*,mm.nombre as nombrem ,mm.duracion as duracionm FROM membresiasClinica m  join membresias mm on mm.idmembresia=m.idmembresia  where  m.idestado=2 and m.idusuario=? ;";
        $rs = $this->DATA->Execute($sql,$idusuario);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                    $id  = $rs->fields['idmembresiasClinica'];
                    $info[$id]['idmembresiasClinica'] = $rs->fields['idmembresiasClinica'];
                    $info[$id]['idmembresia'] = $rs->fields['idmembresia'];
                    $info[$id]['nombrem'] = $rs->fields['nombrem'];
                    $info[$id]['duracionm'] = $rs->fields['duracionm'];
                    $info[$id]['fechaCompra']    = $rs->fields['fechaCompra'];
                    $info[$id]['idformaPago']  = $rs->fields['idformaPago'];
                    $info[$id]['idestado']  = $rs->fields['idestado'];
                    $info[$id]['idclinica'] = $rs->fields['idclinica']; 
                    $info[$id]['valor'] = $rs->fields['valor']; 
                    $info[$id]['idusuario'] = $rs->fields['idusuario']; 
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }


    function bloqueoproducto($idclinica) {
        $sql = "SELECT * from producto where idclinica=?;";
        $rs = $this->DATA->Execute($sql,$idclinica);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                    $dato=   $rs->fields['idproducto'];
                    $sql1 = "UPDATE producto SET idestado = 2 WHERE idproducto = ?";
                    $update= $this->DATA->Execute($sql1, $dato);
                     if ($update){
                        $rs->MoveNext();
                     } else {
                        return false;
                     }
            }
            $rs->Close();
            return true;
        } else {
            return false;
        }
    }

    function desbloqueoproducto($idclinica) {
        $sql = "SELECT * from producto where idclinica=?;";
        $rs = $this->DATA->Execute($sql,$idclinica);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                    $dato=   $rs->fields['idproducto'];
                    $sql1 = "UPDATE producto SET idestado = 1 WHERE idproducto = ?";
                    $update= $this->DATA->Execute($sql1, $dato);
                     if ($update){
                        $rs->MoveNext();
                     } else {
                        return false;
                     }
            }
            $rs->Close();
            return true;
        } else {
            return false;
        }
    }

    function deshabilitar($id) {
        $sql = "UPDATE membresiasClinica SET idestado = 2 WHERE idmembresiasClinica = ?;";
        $update = $this->DATA->Execute($sql, $id);
        if ($update){
            return $update;
        } else {
            return false;
        }
    }



    function getidm($id)
    {
        $sql = "SELECT * FROM membresiasClinica WHERE idmembresiaClinica = ? ;";
        $rs = $this->DATA->Execute($sql, $id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
					$info   = $rs->fields['idmembresia'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }


    function getidmcl($id)
    {
        $sql = "SELECT * FROM membresiasClinica WHERE idmembresiaClinica = ? ;";
        $rs = $this->DATA->Execute($sql, $id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
					$info   = $rs->fields['idclinica'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }

    function habilitar($id,$fechaini,$fechafin) {
        $sql = "UPDATE membresiasClinica SET idestado = 1 ,fechainicio='$fechaini',fechafin='$fechafin' WHERE idmembresiasClinica = $id;";
        $update = $this->DATA->Execute($sql);
        if ($update){
            return $update;
        } else {
            return false;
        }
    }


    function obteneridcli($idmc) {
        $info=0;
        $sql = "SELECT *  from membresiasClinica where  idmembresiasClinica=$idmc ;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $info	= $rs->fields['idclinica'];									
                $rs->MoveNext();
            }
            $rs->Close();
        } 
        return $info;
    }
	





   


    function actualizar($params) 
    {
        try{   
            $sql = "UPDATE membresiasClinica SET idmembresia=?,fechaCompra=?,idformaPago=?,idestado=?,idclinica=?,valor=?,idusuario=? WHERE idmembresiasClinica = ?";
            $save = $this->DATA->Execute($sql, $params);
            if ($save){
                return true;
            } else {
                return false;
            }
        }catch(exception $e){
            return false;
        }
    }


	function eliminar($id) {
        $e=0;
        $sql = "delete from membresiasClinica where  idestado = '$e' and idmembresiasClinica=? ;";
        $update = $this->DATA->Execute($sql, $id);
        if ($update){
            return true;
        } else {
            return false;
        }
    }



    function eliminarc($id) {
        $e=0;
        $sql = "delete from membresiasClinica where idestado = '$e' and idclinica=? ;";
        $update = $this->DATA->Execute($sql, $id);
        if ($update){
            return true;
        } else {
            return false;
        }
    }


    function nuevo1($params,$idclinica) 
    {
        $e=0;
        $sql = "delete from membresiasClinica where idestado = '$e' and idclinica=? ;";
        $update = $this->DATA->Execute($sql, $idclinica);
        if ($update){
            try{
                $sql = "INSERT INTO membresiasClinica (idmembresia,fechaCompra,idformaPago,idestado,idclinica,valor,idusuario) VALUES (?,?,?,?,?,?,?);";     
                $save = $this->DATA->Execute($sql, $params);
                $id     = $this->DATA->Insert_ID();
                if ($save){
                    return $id;
                } else {
                    return false;
                }
            }catch(exception $e){
                return false;
            }  
        } else {
            return false;
        }      
    }



    function nuevo($params) 
    {
        try{
            $sql = "INSERT INTO membresiasClinica (idmembresia,fechaCompra,idformaPago,idestado,idclinica,valor,idusuario) VALUES (?,?,?,?,?,?,?);";     
            $save = $this->DATA->Execute($sql, $params);
            $id     = $this->DATA->Insert_ID();
            if ($save){
                return $id;
            } else {
                return false;
            }
        }catch(exception $e){
            return false;
        }  
    }


    function repetidosm($idmembresia,$idclinica) {
        $info=0;
        $e=0;
        $sql = "SELECT count(idmembresiasClinica) as cantidad  from membresiasClinica where idestado = '$e' and idclinica='$idclinica' and idmembresia='$idmembresia' ;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $info	= $rs->fields['cantidad'];									
                $rs->MoveNext();
            }
            $rs->Close();
        } 
        return $info;
    }




    function repetidosmx($idmembresia,$idclinica) {
        $info=0;
        $e=1;
        $sql = "SELECT count(idmembresiasClinica) as cantidad  from membresiasClinica where idestado = '$e' and idclinica='$idclinica' and idmembresia='$idmembresia' ;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $info	= $rs->fields['cantidad'];									
                $rs->MoveNext();
            }
            $rs->Close();
        } 
        return $info;
    }




    function repetidosmm($idclinica) {
        $info=0;
        $e=1;
        $sql = "SELECT count(idmembresiasClinica) as cantidad  from membresiasClinica where idestado = '$e' and idclinica='$idclinica' ;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $info	= $rs->fields['cantidad'];									
                $rs->MoveNext();
            }
            $rs->Close();
        } 
        return $info;
    }



    function repetidosmmc($idclinica) {
        $info=0;
        $e=1;
        $sql = "SELECT *  from membresiasClinica where idestado = '$e' and idclinica='$idclinica' ;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $info	= $rs->fields['idmembresiasClinica'];									
                $rs->MoveNext();
            }
            $rs->Close();
        } 
        return $info;
    }






    
    function repetidosmmce($idclinica) {
        $info=0;
        $sql = "SELECT count(*) as cantidad  from membresiasClinica where  idclinica='$idclinica' ;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $info	= $rs->fields['cantidad'];									
                $rs->MoveNext();
            }
            $rs->Close();
        } 
        return $info;
    }

    
  
    function getOne($id)
    {
        $sql = "SELECT * FROM membresiasClinica WHERE idmembresiasClinica = ?;";
        $rs = $this->DATA->Execute($sql, $id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id  = $rs->fields['idmembresiasClinica'];
                $info[$id]['idmembresiasClinica'] = $rs->fields['idmembresiasClinica'];
                $info[$id]['idmembresia'] = $rs->fields['idmembresia'];
                $info[$id]['fechaCompra']    = $rs->fields['fechaCompra'];
                $info[$id]['idformaPago']  = $rs->fields['idformaPago'];
                $info[$id]['idestado']  = $rs->fields['idestado'];
                $info[$id]['idclinica'] = $rs->fields['idclinica']; 
                $info[$id]['valor'] = $rs->fields['valor'];
                $info[$id]['fechafin'] = $rs->fields['fechafin'];
                
                $info[$id]['idusuario'] = $rs->fields['idusuario'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }



}
?>