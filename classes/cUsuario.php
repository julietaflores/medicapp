<?php
class Usuario {
    //Constructor
    function __construct() {
        global $DATA;
        $this->DATA = $DATA;
    }
	//FUNCION QUE VERIFICA EL INGRESO DE LOS USUARIOS EN EL ADMINISTRADOR WEB DE LA APLICACION
    function ingreso($usuario, $contrasenia)
    {
		try{
            $sha1pass = ($contrasenia);
            $ide=1;
			$sql = "SELECT u.idusuario,u.nombre,u.idTipoUsuario, u.imagen, u.idclinica,
             c.nombre as nombrecl,
             c.telefono1 as nombreclt,
             c.direccion as nombrecld,
             c.correo as nombreclc,
             t.nombre as nombretu ,
            concat(u.nombre) as nusuario  FROM usuario u   
            join tipoUsuario t on t.idtipoUsuario=u.idtipoUsuario 
            inner join clinica c on c.idclinica=u.idclinica    
            WHERE u.correo=? AND u.clave=? AND u.idestado=? ;";
			$rs = $this->DATA->Execute($sql, array($usuario, $sha1pass,$ide));
			if ( $rs->RecordCount() > 0 ) {
				$_SESSION['csmart']['idusuario']     	= $rs->fields['idusuario'];
                $_SESSION['usuario']   	= $rs->fields['nusuario'];
                $_SESSION['nombretu']   	= $rs->fields['nombretu'];
                $_SESSION['nombrecl']   	= $rs->fields['nombrecl'];

                $_SESSION['nombreclt']   	= $rs->fields['nombreclt'];
                $_SESSION['nombrecld']   	= $rs->fields['nombrecld'];
                $_SESSION['nombreclc']   	= $rs->fields['nombreclc'];

				$_SESSION['csmart']['permisos']     = $rs->fields['idTipoUsuario'];
                $_SESSION['csmart']['status']   = "authenticate";
                $_SESSION['csmart']['clinica']   = $rs->fields['idclinica'];
                $_SESSION['csmart']['iperfil']   = $rs->fields['imagen'];
				$rs->Close();
				return true;
			} else {
				return false; 
			}
		}catch(Exception $e){
			return $e;
		}
    }



    /*
     function ingreso($usuario, $contrasenia)
    {
		try{
            $sha1pass = ($contrasenia);
            $ide=1;
			$sql = "SELECT idusuario,nombre,idTipoUsuario, imagen, idclinica, concat(nombre,' ',apellido) as nusuario FROM usuario WHERE correo=? AND clave=? AND idestado=? ;";
			$rs = $this->DATA->Execute($sql, array($usuario, $sha1pass,$ide));
			if ( $rs->RecordCount() > 0 ) {
				$_SESSION['csmart']['idusuario']     	= $rs->fields['idusuario'];
				$_SESSION['usuario']   	= $rs->fields['nusuario'];
				$_SESSION['csmart']['permisos']     = $rs->fields['idTipoUsuario'];
                $_SESSION['csmart']['status']   = "authenticate";
                $_SESSION['csmart']['clinica']   = $rs->fields['idclinica'];
                $_SESSION['csmart']['iperfil']   = $rs->fields['imagen'];
				$rs->Close();
				return true;
			} else {
				return false; 
			}
		}catch(Exception $e){
			return $e;
		}
    }
    */

	function verificarcorreo($correo)
    {
        $sql = "SELECT count(idusuario) as cantidad FROM usuario WHERE correo = '$correo';";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $cantidad = $rs->fields['cantidad'];
		        $rs->MoveNext();
            }
            $rs->Close();
            return $cantidad;
        } else {
            return 0;
        }
    }


    function ingresoestadoi($usuario, $contrasenia)
    {
		try{
            $sha1pass = ($contrasenia);
            $ide=0;
			$sql = "SELECT idusuario,nombre,idTipoUsuario, imagen, idclinica, concat(nombre,' ',apellido) as nusuario FROM usuario WHERE correo=? AND clave=? AND idestado=? ;";
			$rs = $this->DATA->Execute($sql, array($usuario, $sha1pass,$ide));
			if ( $rs->RecordCount() > 0 ) {
				$_SESSION['csmart']['idusuario']     	= $rs->fields['idusuario'];
				$_SESSION['usuario']   	= $rs->fields['nusuario'];
				$_SESSION['csmart']['permisos']     = $rs->fields['idTipoUsuario'];
                $_SESSION['csmart']['status']   = "authenticate";
                $_SESSION['csmart']['clinica']   = $rs->fields['idclinica'];
                $_SESSION['csmart']['iperfil']   = $rs->fields['imagen'];
				$rs->Close();
				return true;
			} else {
				return false; 
			}
		}catch(Exception $e){
			return $e;
		}
    }


    function ingreso1($usuario, $contrasenia)
    {
		try{
            $sha1pass = ($contrasenia);
            $ide=1;
			$sql = "SELECT idusuario,nombre,idTipoUsuario, imagen, idclinica, concat(nombre,' ',apellido) as nusuario  FROM usuario WHERE usuario=? AND clave=? AND idestado=? ;";
			$rs = $this->DATA->Execute($sql, array($usuario, $sha1pass,$ide));
			if ( $rs->RecordCount() > 0 ) {
				$_SESSION['csmart']['idusuario']     	= $rs->fields['idusuario'];
				$_SESSION['usuario']   	= $rs->fields['nusuario'];
				$_SESSION['csmart']['permisos']     = $rs->fields['idTipoUsuario'];
                $_SESSION['csmart']['status']   = "authenticate";
                $_SESSION['csmart']['clinica']   = $rs->fields['idclinica'];
                $_SESSION['csmart']['iperfil']   = $rs->fields['imagen'];
				$rs->Close();
				return true;
			} else {
				return false; 
			}
		}catch(Exception $e){
			return $e;
		}
		
    }

   // function verSession()
    //{
      //  session_start();
      //  if (!isset($_SESSION['csmart']['idusuario'])) {
        //    return false;
       //   }else{
         //     return true;
        //  }
       
   // }

    
    function getAll()
    {
        $sql = "SELECT u.*, t.nombre as tipo , c.nombre as nombrec 
                FROM usuario u join tipoUsuario t on u.idtipoUsuario=t.idtipoUsuario 
                inner join clinica c on u.idclinica=c.idclinica  
                where u.idtipoUsuario!=1  order by u.idusuario desc;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id                     = $rs->fields['idusuario'];
                    $info[$id]['idusuario'] = $rs->fields['idusuario'];
                                $info[$id]['nombre']    = $rs->fields['nombre'];
                                $info[$id]['apellido']  = $rs->fields['apellido'];
                                $info[$id]['usuario']   = $rs->fields['usuario'];
                                $info[$id]['clave'] = $rs->fields['clave'];
                                $info[$id]['idclinica'] = $rs->fields['idclinica'];
                                $info[$id]['nombrec'] = $rs->fields['nombrec'];
                                $info[$id]['imagen']    = $rs->fields['imagen'];
                                $info[$id]['fecha'] = $rs->fields['fecha'];
                                $info[$id]['tipo'] = $rs->fields['tipo'];
                                $info[$id]['idestado'] = $rs->fields['idestado'];
                                $info[$id]['telefono'] = $rs->fields['telefono'];
                                $info[$id]['correo'] = $rs->fields['correo'];   
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }



    function getAllxclinica1($idclinica,$idusuario)
    {
        $sql = "SELECT u.*, t.nombre as tipo , c.nombre as nombrec 
                FROM usuario u 
                inner join tipoUsuario t on u.idtipoUsuario=t.idtipoUsuario 
                inner join clinica c on u.idclinica=c.idclinica  
                where u.idtipoUsuario!=1 and u.idclinica=$idclinica and u.idusuario!=$idusuario order by u.idusuario desc;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                                $id                     = $rs->fields['idusuario'];
                                $info[$id]['idusuario'] = $rs->fields['idusuario'];
                                $info[$id]['nombre']    = $rs->fields['nombre'];
                                $info[$id]['apellido']  = $rs->fields['apellido'];
                                $info[$id]['usuario']   = $rs->fields['usuario'];
                                $info[$id]['clave'] = $rs->fields['clave'];
                                $info[$id]['idclinica'] = $rs->fields['idclinica'];
                                $info[$id]['nombrec'] = $rs->fields['nombrec'];
                                $info[$id]['imagen']    = $rs->fields['imagen'];
                                $info[$id]['fecha'] = $rs->fields['fecha'];
                                $info[$id]['tipo'] = $rs->fields['tipo'];
                                $info[$id]['idestado'] = $rs->fields['idestado'];
                                $info[$id]['telefono'] = $rs->fields['telefono'];
                                $info[$id]['correo'] = $rs->fields['correo'];   
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }


    function getAllxClinica($idclinica)
    {
        $sql = "SELECT u.*, t.nombre as tipo
                FROM usuario u join tipoUsuario t 
                on u.idtipoUsuario=t.idtipoUsuario where u.idclinica=$idclinica;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id                     = $rs->fields['idusuario'];
                $info[$id]['idusuario'] = $rs->fields['idusuario'];
                $info[$id]['nombre']    = $rs->fields['nombre'];
                $info[$id]['apellido']  = $rs->fields['apellido'];
                $info[$id]['usuario']   = $rs->fields['usuario'];
                                $info[$id]['clave'] = $rs->fields['clave'];
                                $info[$id]['idclinica'] = $rs->fields['idclinica'];
                                $info[$id]['imagen']    = $rs->fields['imagen'];
                                $info[$id]['fecha'] = $rs->fields['fecha'];
                                $info[$id]['tipo'] = $rs->fields['tipo'];                                
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }

    

    function habilitar1($id) {
        $sql = "UPDATE usuario SET idestado = 1 WHERE idusuario = ?;";
        $update = $this->DATA->Execute($sql, $id);
        if ($update){
            return $update;
        } else {
            return false;
        }
    }



	function getRol()
    {
        $sql = "SELECT idrol,titulo FROM rol WHERE estado=1;";

        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id                   	= $rs->fields['idrol'];
				$info[$id]['nombre']	= $rs->fields['titulo'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }


    	


	function getOne($id)
    {
        $sql = "SELECT u.*, c.nombre as nombrec FROM usuario u join clinica c on u.idclinica=c.idclinica WHERE u.idusuario = ?;";
        $rs = $this->DATA->Execute($sql, $id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id                     = $rs->fields['idusuario'];
                    $info[$id]['idusuario'] = $rs->fields['idusuario'];
                                $info[$id]['nombre']    = $rs->fields['nombre'];
                                $info[$id]['apellido']  = $rs->fields['apellido'];
                                $info[$id]['usuario']   = $rs->fields['usuario'];
                                $info[$id]['clave'] = $rs->fields['clave'];
                                $info[$id]['telefono']  = $rs->fields['telefono'];
                                $info[$id]['correo']    = $rs->fields['correo'];
                                $info[$id]['imagen']    = $rs->fields['imagen'];
                                $info[$id]['fecha'] = $rs->fields['fecha'];
                                $info[$id]['notas'] = $rs->fields['notas'];
                                $info[$id]['idtipoUsuario'] = $rs->fields['idtipoUsuario'];
                                $info[$id]['idclinica'] = $rs->fields['idclinica'];
                                $info[$id]['nombrec'] = $rs->fields['nombrec'];
                                $info[$id]['idestado']  = $rs->fields['idestado'];
                                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }



    function getOneSeguro($id)
    {
        $sql = "SELECT * FROM seguro WHERE idseguro = ? ;";
        $rs = $this->DATA->Execute($sql, $id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
					$info   = $rs->fields['nombre'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }


    function getAllseguros()
    {
        $sql = "SELECT idseguro,nombre FROM seguro where  idseguro>1;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
         		    $id = $rs->fields['idseguro'];
				   	$info[$id]['idseguro']	= $rs->fields['idseguro'];
					$info[$id]['nombre']	= $rs->fields['nombre'];			
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }

    function getAllseguros1()
    {
        $sql = "SELECT idseguro,nombre FROM seguro;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
         		    $id = $rs->fields['idseguro'];
				   	$info[$id]['idseguro']	= $rs->fields['idseguro'];
					$info[$id]['nombre']	= $rs->fields['nombre'];			
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }


    function getOne1($id)
    {
        $sql = "SELECT u.*, c.nombre as nombrec , t.nombre as nombretu FROM usuario u 
        join clinica c on u.idclinica=c.idclinica  inner 
        join tipoUsuario t on t.idtipoUsuario=u.idtipoUsuario   WHERE u.idusuario = ?;";
        $rs = $this->DATA->Execute($sql, $id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id                     = $rs->fields['idusuario'];
                    $info[$id]['idusuario'] = $rs->fields['idusuario'];
                                $info[$id]['nombre']    = $rs->fields['nombre'];
                                $info[$id]['apellido']  = $rs->fields['apellido'];
                                $info[$id]['usuario']   = $rs->fields['usuario'];
                                $info[$id]['nombretu']   = $rs->fields['nombretu'];
                                $info[$id]['clave'] = $rs->fields['clave'];
                                $info[$id]['telefono']  = $rs->fields['telefono'];
                                $info[$id]['correo']    = $rs->fields['correo'];
                                $info[$id]['imagen']    = $rs->fields['imagen'];
                                $info[$id]['fecha'] = $rs->fields['fecha'];
                                $info[$id]['notas'] = $rs->fields['notas'];
                                $info[$id]['idtipoUsuario'] = $rs->fields['idtipoUsuario'];
                                $info[$id]['idclinica'] = $rs->fields['idclinica'];
                                $info[$id]['nombrec'] = $rs->fields['nombrec'];
                                $info[$id]['idestado']  = $rs->fields['idestado'];
                                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }


    function nuevo2($params) 
	{
        $sql = "INSERT INTO usuario_web (nombre,idrol,password,estado) VALUES (?,?,?,?);";
			 
        $save = $this->DATA->Execute($sql, $params);
        if ($save){
            return true;
        } else {
            return false;
        }
    }

    function nuevo($params) 
    {
        try{
            $sql = "INSERT INTO usuario (nombre,apellido,usuario,clave,telefono,correo,imagen,notas,idtipoUsuario,idclinica,idestado) VALUES (?,?,?,?,?,?,?,?,?,?,?);";     
            $save = $this->DATA->Execute($sql, $params);
            $id     = $this->DATA->Insert_ID();
            if ($save){
                return $id;
            } else {
                return false;
            }
        }catch(exception $e){
            return false;
        }
        
    }


    function nuevopermiso($params) 
    {
        try{
            $sql = "INSERT INTO permisos (idusuario,agenda,paneldecontrol,miclinica,reporte) VALUES (?,?,?,?,?);";     
            $save = $this->DATA->Execute($sql, $params);
            $id     = $this->DATA->Insert_ID();
            if ($save){
                return $id;
            } else {
                return false;
            }
        }catch(exception $e){
            return false;
        }     
    }





    function getpermiso($idusuario)
    {
        $sql = "SELECT * from permisos  WHERE idusuario = $idusuario;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                   $id                     = $rs->fields['idpermisos'];
                   $info[$id]['idusuario'] = $rs->fields['idusuario'];
                    $info[$id]['agenda'] = $rs->fields['agenda'];
                    $info[$id]['paneldecontrol']    = $rs->fields['paneldecontrol'];
                    $info[$id]['miclinica']  = $rs->fields['miclinica'];
                    $info[$id]['reporte']   = $rs->fields['reporte'];            
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }



    function getperimisos1($idusuario) {
        $info=0;
        $sql = "SELECT count(idusuario) as cantidad  from permisos where  idusuario=$idusuario ;";
        $rs = $this->DATA->Execute($sql);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $info	= $rs->fields['cantidad'];									
                $rs->MoveNext();
            }
            $rs->Close();
        } 
        return $info;
    }


    function nuevoua($params) 
    {
        try{
            $sql = "INSERT INTO usuario (nombre,clave,telefono,correo,imagen,notas,idtipoUsuario,idclinica,idestado) VALUES (?,?,?,?,?,?,?,?,?);";     
            $save = $this->DATA->Execute($sql, $params);
            $id     = $this->DATA->Insert_ID();
            if ($save){
                return $id;
            } else {
                return false;
            }
        }catch(exception $e){
            return false;
        }
        
    }
    

    


  

    
    function nuevor($params) 
    {
        try{
            $sql = "INSERT INTO usuario (nombre,clave,correo) VALUES (?,?,?);";     
            $save = $this->DATA->Execute($sql, $params);
            $id     = $this->DATA->Insert_ID();
            if ($save){
                return $id;
            } else {
                return false;
            }
        }catch(exception $e){
            return false;
        }
        
    }
	

    function actualizar($params) 
    {
        try{
        
            $sql = "UPDATE usuario SET nombre=?,apellido=?,usuario=?,clave=?,telefono=?,correo=?,imagen=?,notas=?,idtipoUsuario=?,idestado=? WHERE idusuario = ?";
            $save = $this->DATA->Execute($sql, $params);
            if ($save){
                return true;
            } else {
                return false;
            }
        }catch(exception $e){
            return false;
        }
        
    }




    function actualizar1($params) 
    {
        try{
            $sql = "UPDATE usuario SET nombre=?,telefono=?,correo=?,imagen=?,notas=?,idtipoUsuario=?,idestado=? WHERE idusuario = ?";
            $save = $this->DATA->Execute($sql, $params);
            if ($save){
                return true;
            } else {
                return false;
            }
        }catch(exception $e){
            return false;
        }
        
    }



    
    function actualizar2($params) 
    {
        try{
            $sql = "UPDATE usuario SET nombre=?,clave=?,telefono=?,correo=?,imagen=?,notas=?,idtipoUsuario=?  WHERE idusuario = ?";
            $save = $this->DATA->Execute($sql, $params);
            if ($save){
                return true;
            } else {
                return false;
            }
        }catch(exception $e){
            return false;
        }
        
    }



    function actualizar22($params) 
    {
        try{
            $sql = "UPDATE usuario SET nombre=?,telefono=?,correo=?,imagen=?  WHERE idusuario = ?";
            $save = $this->DATA->Execute($sql, $params);
            if ($save){
                return true;
            } else {
                return false;
            }
        }catch(exception $e){
            return false;
        }
        
    }


    function actualizarnuevopermiso($params) 
    {
        try{
            $sql = "UPDATE permisos SET agenda=? ,paneldecontrol=? ,miclinica=?,reporte=?  WHERE idusuario = ? ;";     
            $save = $this->DATA->Execute($sql, $params);
            if ($save){
                return true;
            } else {
                return false;
            }
        }catch(exception $e){
            return false;
        }     
    }


    function getidclinica($id)
    {
        $sql = "SELECT idclinica FROM usuario WHERE idusuario=?;";
        $rs = $this->DATA->Execute($sql,$id);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                //$id                   	= $rs->fields['idusuario'];
				$info	= $rs->fields['idclinica'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $info;
        } else {
            return false;
        }
    }



	function modificar($params)
	{
        $sql = "UPDATE usuario_web SET idrol=?,password=?,estado=? WHERE idusuario_web=?;";

        $update = $this->DATA->Execute($sql, $params);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	function modificar2($params)
	{
        $sql = "UPDATE usuario_web SET idrol=?,estado=? WHERE idusuario_web=?;";

        $update = $this->DATA->Execute($sql, $params);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	
	function bloqueo($id) {
        $sql = "UPDATE usuario_web SET estado = 0 "
             . "WHERE idusuario_web = ?";

        $update = $this->DATA->Execute($sql, $id);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	function habilitar($id) {
        $sql = "UPDATE usuario_web SET  estado = 1 "
             . "WHERE idusuario_web = ?";

        $update = $this->DATA->Execute($sql, $id);
        if ($update){
            return true;
        } else {
            return false; 
        }
    }
	function eliminar($id) {
        $sql = "delete from usuario where idusuario = ?";
        $update = $this->DATA->Execute($sql, $id);
        if ($update){
            return true;
        } else {
            return false;
        }
    }
	function validar($titulo)
    {
        $sql = "SELECT COUNT(idusuario_web) AS cantidad FROM usuario_web WHERE nombre = ?;";

        $rs = $this->DATA->Execute($sql, $titulo);
        if ( $rs->RecordCount()) {
            while(!$rs->EOF){
                $id   	= $rs->fields['cantidad'];
                $rs->MoveNext();
            }
            $rs->Close();
            return $id;
        } else {
            return false;
        }
    }
}
?>