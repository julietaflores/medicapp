<?php
header('Cache-Control: no cache'); //no cache
session_cache_limiter('private_no_expire'); // works
session_start();
   if (!isset($_SESSION['csmart']['idusuario'])) {
   Header ("Location: login.php");
   }
    include 'headercondicion.php'; 
    include 'classes/cpagocredito.php';
    $opagocredito=new pagocredito();
    $pago=$_REQUEST['pago'];
    $vpagocredigo = $opagocredito->getprueball($pago);
    $vcreditos=$opagocredito->getAll($pago);
        foreach ($vpagocredigo AS $id => $info){ 
            $nombre		= $info["nombrepaciente"];
            $apellido		= $info["apellidopa"];
            $telefono		= $info["telefono1"];
            $correo		= $info["correopa"];
            $pagado		= $info["pagado"];
            $debe		= $info["debe"];
            $tratamiento		= $info["tratamiento"];
            }


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Imprimir Pago</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
  <?php include 'header.php'; ?>
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  <script type="text/javascript"> </script>
</head>
<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <!-- sidebar menu -->
            <?php include "menu.php" ?>
          <!-- /sidebar menu -->
        </div>
      </div>

      <!-- top navigation -->
           <?php include "top_nav.php" ?>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">

        <div class="">
          
         
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                  <div id="imprimirpagoss">
                  
                    
                <div class="x_title">
                  <h2>Pagó del Tratamiento  <?=$tratamiento?></h2>
                    
                  <div class="clearfix"></div>
                </div>
               
                <div class="x_content">
                        <form id="form" class="form-horizontal form-label-left" novalidate>
                   

                  
           <div class="row">
                   
                      <!-- /.col -->
                    </div>        
           <div class="row">
            <div class="col-md-12 col-xs-12">
              <div class="x_panel">
                
              <div class="row"> 
                <div class="col-sm-6 invoice-col"> 
                 <div class="col-md-12 col-sm-12 col-xs-12"> 
                <div class="x_title">
                  <h2>Datos de Paciente</h2>
                  <div class="clearfix"></div>
                </div>

                </div>
                </div>     
                <div class="col-sm-6 invoice-col"> 
                 <div class="col-md-12 col-sm-12 col-xs-12"> 
                <div class="x_title">
                  <h2>Datos de la Clínica <?=$_SESSION['nombrecl']?> </h2> 
                  <div class="clearfix"></div>
                </div>
                </div>
                </div>
              </div>


                <div class="x_content">
                  <div class="row"> 
                    <div class="col-sm-6 invoice-col">
                    <div class="col-md-12 col-sm-12 col-xs-12"> 
                    <span class="fa fa-user"><b> Nombre: </b></span> <?=$nombre ?> <?=$apellido?>
                     <br>
                    <span class="fa fa-phone "><b> Teléfono:</b></span> <?=$telefono?>
                    <br>
                    <span class="fa fa-envelope "><b> Correo: </b></span> <?=$correo?><br>
                    </div>
                    </div>
                    <div class="col-sm-6 invoice-col">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    
                    <span class="fa fa-phone"><b> Teléfono:</b></span>  <?=$_SESSION['nombreclt']?>
                     <br>
                    <span class="fa fa-home "><b> Dirección:</b> </span> <?=$_SESSION['nombrecld']?>
                    <br>
                    <span class="fa fa-envelope "><b> Correo:</b></span>  <?=$_SESSION['nombreclc']?><br>

                    </div>
                    </div>
                  </div>
                </div>



              </div>

                          

            </div>

            <div class="col-md-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Datos de Pagó</h2>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">
                  <form class="form-horizontal form-label-left">  		
                  	<div class="row">
                  		<div class="col-sm-6 invoice-col">
                  			<div class="col-md-12 col-sm-12 col-xs-12">
                          <b>tratamiento: </b> <?=$tratamiento?>
							             <br>
							            <b>Se pagó: </b>  <?=$pagado?>
		                       <br>
		                      <b>Saldo: </b> <?=$debe?>
		                    </div>
                  		</div>
                  	</div>
                  <br> 
                     		
		              <h2>Detalle de Pagó</h2>
		                    <div class="item form-group">
		                    	<table id="productos" class="table table-striped">
				                           <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>fecha</th>
                                            <th>Monto</th>                        
                                            <th>observación</th>
                                        </tr>
                                    </thead>
                        <?php $i=1;
                        if($vcreditos){
                          foreach ($vcreditos AS $id => $info){    ?>
                          
                            <tr>
                                <td><?=$i;?></td>
                                <td>
                                <?$source = $info['fecha'];
                                $date = new DateTime($source);
                                echo $date->format('d-m-Y');?>
                                </td>
                                <td><?=$info['monto']?></td>
                                <td><?=$info['observaciones']?></td>
                            </tr>    
                           <?  $i++;}
                        }else{?>
                          <tr>
                            <td>No existe pagos</td>
                          </tr>
                        <?}?>
                          
                        
                           
				                </table>
		                    </div>
		           

                </div>
              </div>
            </div>


            
          </div>	
          </div>		 
          </div>
               										
													
								<div class="ln_solid"></div>
				                    <div id="ocultarboton" class="form-group">
				                      <div class="col-md-6 col-md-offset-3">
				                        <button class="btn btn-default" onclick="imprimir();"><i class="fa fa-print"></i> Imprimir</button>
                        				
				                      </div>
				                    </div>
				                  </form>
                          
              </div>
            </div>
          </div>
        </div>

        <!-- footer content -->
        <center> <?php include "piep.php" ?> </center>
        <!-- /footer content -->

      </div>
      <!-- /page content -->
    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="js/moment/moment.min.js"></script>
  <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>
  
  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>
  <script src="js/custom.js"></script>
  <!-- form validation -->
  <script src="js/validator/validator.js"></script>
  <!-- PNotify -->
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>

  <script>

   function imprimir(){
  //$("#tablapagoscredito").print();
debugger;

  var printContents = document.getElementById('imprimirpagoss').innerHTML;
  
			var originalContents = document.body.innerHTML;
			document.body.innerHTML = printContents;
           
			window.print();
      window.location.reload(true);
			document.body.innerHTML = originalContents;
            
}
  </script>
 
   

 
 <div style="height: 100px;"></div>
</body>
</html>