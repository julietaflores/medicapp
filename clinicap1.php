<?php 

session_start();
if (!isset($_SESSION['csmart']['idusuario'])) {
Header ("Location: login.php");
}


include 'headercondicion.php';

   
 
    include 'classes/cclinica.php';
    $oClinica  = new clinica();
      
    $registro		= "";
    $direccion		= "";
    $imagen		= "";

   
     $vclinica = $oClinica->getOne($_SESSION['csmart']['clinica'] );

       if($vclinica){ 
        foreach ($vclinica AS $id => $info){ 
            $idclinica		= $info["idclinica"];
		        $nombre		= $info["nombre"];
		        $registro		= $info["registro"];
		        $direccion		= $info["direccion"];
		        $telefono1		= $info["telefono1"];
		        $telefono2		= $info["telefono2"];
		        $correo		= $info["correo"];
		        $imagen		= $info["imagen"];
            $idestado25		= $info["idestado"];    
            $longitud 	= $info["longitud"];
            $latitud 	= $info["latitud"]; 
          }
        }
 

 
   
 ?>  

 <!DOCTYPE html>
<html >

<head>
  
  <?php include "header.php";?>

  <script type="text/javascript">

      function guardarm(){
        var fsizen ="";
        var i7="";

        var file_data = $('#imagenS').prop('files')[0];   
        var form_data = new FormData();                  
        form_data.append('file', file_data);
        
        if(file_data){ // SI EXISTE IMAGEN PARA SUBIR o ACTUALIZAR
          var fsize = $('#imagenS')[0].files[0].size; //get file size
           fsizen = $('#imagenS')[0].files[0].name;
          if(fsize>1048576) 
          {
            new PNotify({
                 title: 'Error en Imagen!',
                 text: 'Excede el tamaño máximo permitido. Tamaño permitido menor a 1 Mb.'
             });
          }
        }


        if(fsizen!=""){
          i7 = fsizen;         
        }else{
           i7="<?= $imagen?>";
        }

        var i0 = $("#idclinica").val();
        var i1 = $("#nombre").val();
				var i2 = $("#registro").val();
				var i3 = $("#direccion").val();
				var i4 = $("#telefono1").val();
				var i5 = $("#telefono2").val();
        var i6 = $("#correo").val();
        var lat = $("#latitud").val();
        var log = $("#longitud").val();

        $.ajax({
          url:'actions/actionclinica.php',
          type:'POST',
          data: { idclinica:i0,nombre:i1,registro:i2,direccion:i3,telefono1:i4,telefono2:i5,correo:i6,imagen:i7,latitud:lat,longitud:log ,opt: 'mm' }
        }).done(function( data ){
          if(data > 0){
            new PNotify({
                 title: 'Datos Guardados',
                text: 'Todos los datos fueron guardados. Puede continuar.',
                type: 'success'
             });
         
             if(i7!=""){
              $(".loaderp").fadeIn("fast");
              $.ajax({
                      url: 'processUpload.php?id='+data + '&tipo=clinica', // point to server-side PHP script 
                      dataType: 'text',  // what to expect back from the PHP script, if anything
                      cache: false,
                      contentType: false,
                      processData: false,
                      data: form_data,                         
                      type: 'post',
                      success: function(val){
                        $(".loaderp").fadeOut("slow");
                        window.setTimeout("document.location.href='index.php';",500);
                      }
              });  
            }else{
              window.setTimeout("document.location.href='index.php';",500);
            }
          
          }else{
            new PNotify({
                 title: 'Error en datos',
                text: 'Falta información!'
             });
          }        
        });
      }

      function cancelar(){
        window.setTimeout("document.location.href='index.php';",500);

      }
      $(window).load(function() {
					$(".loaderp").fadeOut("slow");
				});
  </script>

</head>


<body class="nav-md">
<div class="loaderp" style="position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('images/cargando.webp') 50% 50% no-repeat rgb(249,249,249);
    opacity: .8;"></div>
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <!-- sidebar menu -->
            <?php include "menu.php" ?>
          <!-- /sidebar menu -->
        </div>
      </div>
      <!-- top navigation -->
           <?php include "top_nav.php" ?>
      <!-- /top navigation -->
      <!-- page content -->
      <div class="right_col" role="main">

        <div class="">
          
         
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Administración de Clínica</h2>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                      <li id="tab1" role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Perfil de tu Clínica</a>
                      </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">


                      <div role="tabpanel" class="tab-pane fade  active in" id="tab_content1" aria-labelledby="profile-tab">
                        
                        <form id="form" class="form-horizontal form-label-left" novalidate>
                   
                    <span class="section">Datos de tu Clínica</span>

        
                    <div class="item form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="idclinica" class="form-control col-md-7 col-xs-12"  name="idclinica" value="<?=$idclinica?>" type="text" style="display:none;">
                      </div>
                    </div>

                    <div class="row">

<div class="col-md-1">
</div>
 <div class="col-md-4">  
  
      <?php if ($imagen =="") { ?>
        <img src="images/hospital.png" alt=""  style="
        height:300px;
        width: 300px;
        border: 2px solid;
        border-color: #58D3F7;">
      <?php }else{ ?>
        <?php
                if(file_exists("images/clinica/".$idclinica."/".$imagen)){
                  $aqui="images/clinica/".$idclinica."/".$imagen;          
                } else {
                  $aqui="images/hospital.png";                     
                }
                ?>
        <img src="<?=$aqui?>" alt=""  style="
        height:300px;
        width:300px;
        border: 2px solid;
        border-color: #58D3F7;">
     <?php } ?>
      
</div>


<div class="col-md-1">
</div>

<div class="col-md-5">
   <div id="googleMap" style="width:100%;height:300px;  border: 2px solid;
        border-color: #58D3F7;"></div>
</div>

<div class="col-md-1">
</div>

</div>
<br>

                        <div class="item form-group">
		                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="imagen">Logo 
		                      </label>
		                      <div class="col-md-6 col-sm-6 col-xs-12">
		                        <input name="imagenS" class="fileupload " id="imagenS" type="file" />
                        		<input name="img" id="img" type="hidden" value="<?=$imagen?>" />
		                      </div>
		                    </div>




                        <div class="item form-group">
		                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombre">Nombre <span class="required">*</span>
		                      </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="nombre" class="form-control col-md-7 col-xs-12" name="nombre" value="<?=$nombre?>" placeholder="nombre" required="required" type="text">
                      </div>
		                    </div>


               
                        <div class="item form-group">
		                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="registro"> NIT 
		                      </label>
		                      <div class="col-md-6 col-sm-6 col-xs-12">
		                        <input id="registro" class="form-control col-md-7 col-xs-12"  name="registro" value="<?=$registro?>"  type="text" placeholder="Nit" required="required"> 
		                      </div>
                        
		                    </div>
        

                        <div class="item form-group">
		                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="direccion">Direccion <span class="required">*</span>
		                      </label>
		                      <div class="col-md-6 col-sm-6 col-xs-12">
		                        <input id="direccion" class="form-control col-md-7 col-xs-12" name="direccion" value="<?=$direccion?>" placeholder="Direccion" required="required" type="text">
		                      </div>
		                    </div>

							
								 
							 <div class="item form-group">
		                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono1">Telefono 1  <span class="required">*</span>
		                      </label>
		                      <div class="col-md-6 col-sm-6 col-xs-12">
		                        <input id="telefono1" class="form-control col-md-7 col-xs-12"  name="telefono1" value="<?=$telefono1?>" placeholder="999-99-999" required="required" type="number">
		                      </div>
		                    </div>




								 
                        <div class="item form-group">
		                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono2">Telefono 2 
		                      </label>
		                      <div class="col-md-6 col-sm-6 col-xs-12">
		                        <input id="telefono2" class="form-control col-md-7 col-xs-12"  name="telefono2" value="<?=$telefono2?>" placeholder="999-99-999"  type="number">
		                      </div>
		                    </div>

							
								 
							 <div class="item form-group">
		                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="correo">Correo <span class="required">*</span>
		                      </label>
		                      <div class="col-md-6 col-sm-6 col-xs-12">
		                        <input id="correo" class="form-control col-md-7 col-xs-12" name="correo" value="<?=$correo?>" placeholder="Correo" required="required" type="email">
		                      </div>
                        </div>
                        

                            
		                    <input id="latitud" type="hidden" class="form-control col-md-7 col-xs-12"  name="latitud" value="<?=$latitud?>" placeholder="latitud" required="required" type="text">
		                
              
                    <input id="longitud" type="hidden" class="form-control col-md-7 col-xs-12"  name="longitud" value="<?=$longitud?>" placeholder="longitud" required="required" type="text">
             
        

                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-md-offset-3">
                        <input type="button" onClick="cancelar();" class="btn btn-primary" value="Cancelar">
                        <input type="button" onClick="guardarm();" class="btn btn-success" value="Guardar">
                      </div>
                    </div>




                  </form>


                      </div>
                     
                    </div>
                  </div>


                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- footer content -->
      <center>  <? include "piep.php"; ?> </center>
        <!-- /footer content -->

      </div>
      <!-- /page content -->
    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>



  <script src="../plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
       <script src="../plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
       <script src="../plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
	   <div class="control-sidebar-bg"></div>

  <script src="js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>
  <script src="js/custom.js"></script>
  <!-- form validation -->
  <script src="js/validator/validator.js"></script>
  <!-- PNotify -->
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  
   <!-- Datatables-->
        <script src="js/datatables/jquery.dataTables.min.js"></script>
        <script src="js/datatables/dataTables.bootstrap.js"></script>
        <script src="js/datatables/dataTables.buttons.min.js"></script>
        <script src="js/datatables/buttons.bootstrap.min.js"></script>
        <script src="js/datatables/jszip.min.js"></script>
        <script src="js/datatables/pdfmake.min.js"></script>
        <script src="js/datatables/vfs_fonts.js"></script>
        <script src="js/datatables/buttons.html5.min.js"></script>
        <script src="js/datatables/buttons.print.min.js"></script>
        <script src="js/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="js/datatables/dataTables.responsive.min.js"></script>
        <script src="js/datatables/responsive.bootstrap.min.js"></script>
        <script src="js/datatables/dataTables.scroller.min.js"></script>


  <script>
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
      .on('blur', 'input[required], input.optional, select.required', validator.checkField)
      .on('change', 'select.required', validator.checkField)
      .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
      .on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function(e) {
      e.preventDefault();
      var submit = true;

      
      // evaluate the form using generic validaing
      if (!validator.checkAll($(this))) {
        submit = false;
      }

      if (submit)
        guardarformulario();
      return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function() {
      $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function() {
      validator.defaults.alerts = (this.checked) ? false : true;
      if (this.checked)
        $('form .alert').remove();
    }).prop('checked', false);
  </script>
  <!-- pace -->
        <script src="js/pace/pace.min.js"></script>
        <script>
          var handleDataTableButtons = function() {
              "use strict";
              0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
                dom: "Bfrtip",
                buttons: [{
                  extend: "csv",
                  className: "btn-sm"
                }, {
                  extend: "excel",
                  className: "btn-sm"
                }, {
                  extend: "pdf",
                  className: "btn-sm"
                }, {
                  extend: "print",
                  className: "btn-sm"
                }],
                responsive: !0
              })
            },
            TableManageButtons = function() {
              "use strict";
              return {
                init: function() {
                  handleDataTableButtons()
                }
              }
            }();
        </script>
         <script type="text/javascript">
          $(document).ready(function() {
            $('#imagenS').change(function() {
                var filename = $(this).val();
                var lastIndex = filename.lastIndexOf("\\");
                if (lastIndex >= 0) {
                    filename = filename.substring(lastIndex + 1);
                } 
                $('#img').val(filename);
            });
            <?php
              if($option == "m"){
            ?>
                $("#tab1").removeClass('active');
                $("#tab2").addClass('active');
                $("#tab_content1").removeClass('active in');
                $("#tab_content2").addClass('active in');
                
            <? } ?>

            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({
              keys: true
            });
            $('#datatable-responsive').DataTable();
            
            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true
            });
          });
          TableManageButtons.init();
        </script>


<script>
			var markers = [];
			function myMap() {
        if((<?=$latitud?>==0) && (<?=$longitud?>==0) ){
        
           var mapOptions= {
					 	center:new google.maps.LatLng(-17.7745714,-63.1842811),
						zoom:15,
						mapTypeId: google.maps.MapTypeId.ROADMAP,
			   	};
           var mapCanvas=document.getElementById("googleMap");
				   var map = new google.maps.Map(mapCanvas, mapOptions);
			   	google.maps.event.addListener(map, 'click', function(event) {		
				   	placeMarker(map, event.latLng);
				  });
          
        }else{
           
          var mapOptions= {
					 	center:new google.maps.LatLng(<?=$latitud?>,<?=$longitud?>),
						zoom:15,
						mapTypeId: google.maps.MapTypeId.ROADMAP,
			   	};
          var mapCanvas=document.getElementById("googleMap");
          var map = new google.maps.Map(mapCanvas, mapOptions);
          var myLatLng = {lat: <?=$latitud?>, lng: <?=$longitud?>};
          var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          });

          var infowindow = new google.maps.InfoWindow({
					content: 'Latitud: ' + <?=$latitud?> + '<br>Longitud: ' + <?=$longitud?>
				  });
				  infowindow.open(map,marker);

          google.maps.event.addListener(map, 'click', function(event) {	
                  markers.push(marker);	
                  markers[0].setMap(null);
 				          markers.splice(0, 1);    
                  placeMarker(map, event.latLng);
				  });
   
      

        }


			
				
     

			 }

			function placeMarker(map, location) {
				 deleteMarkers();
				var marker = new google.maps.Marker({
					position: location,
					map: map
				});
				$('#latitud').val(location.lat());
				$('#longitud').val(location.lng());
				var infowindow = new google.maps.InfoWindow({
					content: 'Latitud: ' + location.lat() + '<br>Longitud: ' + location.lng()
				});
				infowindow.open(map,marker);
				markers.push(marker);
			}
			
			function deleteMarkers() {
				for (var i = 0; i < markers.length; i++) {
                  markers[i].setMap(null);
 				          markers.splice(i, 1);
              }
            }
      
    </script>
    
    <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAD_uDDi0TSvBwIvihAJeyK-N543hMDBKg&callback=myMap"></script>
	


</body>

</html>
