<?php 
    session_start();
    if (!isset($_SESSION['csmart']['idusuario'])) {
    Header ("Location: login.php");
    }
    include 'headercondicion.php';
		include 'classes/cincidencia.php';
    $oincidencia = new incidencia();
		$accion 	= "Crear";
		$option 	= "n";
		$id 	= "";
	        $idusuario 	= "";
	        $asunto 	= "";
	        $detalle 	= "";
	        $fecha 	= "";
	        $idestado 	= "";
	        $respuesta 	= "";
	        
	    if(isset($_REQUEST['opt']) && $_REQUEST['opt']== "m"){
		$option = $_REQUEST['opt'];
		$idObj	= $_REQUEST['id'];
	}

	if ($option == "m") {
		$vincidencia = $oincidencia->getOne($idObj);
		if($vincidencia){
			$accion 	= "Modificar";
			
			   foreach ($vincidencia AS $id => $info){ 
			   	  $id		= $info["id"];
		        $idusuario		= $info["idusuario"];
		        $asunto		= $info["asunto"];
		        $detalle		= $info["detalle"];
		        $fecha		= $info["fecha"];
		        $idestado		= $info["idestado"];
		        $respuesta		= $info["respuesta"];
		         }
		}else{
			header("Location: incidencia.php");
			exit();
		}
	}
  $vincidencia 		= $oincidencia->getAll1($_SESSION['csmart']['idusuario']);
  
?>
<!DOCTYPE HTML><html>
<head>
 	 <?php include 'header.php'; ?>

		<script type="text/javascript">	
			
			function guardarformulario(){
				var form = $("#form").serializeJSON(); 
        var detalle= $("#detalle").val();
        if(detalle!=""){

          $.ajax({
					url:'actions/actionincidencia.php',
					type:'POST',
					data: { valor: form, }
				}).done(function( data ){
					if(data != 0){


            new PNotify({
		                 	title: 'Datos Guardados',
			                text: 'Todos los datos fueron guardados. Puede continuar.',
			                type: 'info'
			             });


            $.ajax({
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        url: "ws/api_rest.php/getAvisoIncidenciaA/"+data,
                        dataType: "json",
                        data: { },
                        success: function (data){
                           if(data==1){
							                new PNotify({
                              title: 'Usuario',
                              text: 'Se envio un correo al Administrador ya ellos se comunicaran con usted gracias por utilizar la plataforma !!!',
                              type: 'info'
                              });  

                              window.setTimeout("document.location.href='incidencia.php';",7000);

                            }else{
                            if(data==400){
							                new PNotify({
                              title: 'Usuario',
                              text: 'Nose pudo enviar el correo !!!',
                              type: 'error'
                               });       
                             }
                            }
                           
                          }
                 });

					
					}
					
					else{
						new PNotify({
			                title: 'Error en formulario',
			                text: 'No se puedieron guardar los datos, intente de nuevo.',
			                type: 'error'
			             });
			   
					}
					
				});
        }else{
          new PNotify({
			                title: 'Error en formulario',
			                text: 'Necesita llenar todos los campos !!!.',
			                type: 'error'
			             });
        }

			
			}
			function cancelar(){
				window.setTimeout("document.location.href='incidencia.php';",500);

			}
			function eliminar(id) {
				
					if (confirm("Atencion! Va a proceder eliminar este registro. Desea continuar?")) {
						var form = "valor"; 
						$.ajax({
							url: 'actions/actionincidencia.php?opt=e&id='+id,
							type:'POST',
							data: { valor1: form, }
						}).done(function( data ){
							
							if(data == 0){
								new PNotify({
				                   title: 'Datos Eliminados',
				                    text: 'Todos los datos fueron guardados. Puede continuar.!',
				                    type: 'success'
				                  });
							window.setTimeout("document.location.href='incidencia.php';",3000);
							}
							else if(data == 1){
								msg = "Error en idincidencia.";
								showWarning(msg,5000);
							}
							else{
								new PNotify({
				                     title: 'Error en formulario',
				                    text: 'No se puedieron guardar los datos, intente de nuevo.',
				                    type: 'error'
				                 });
							
							}
							
						});
					}
				
			}
	</script>



</head>

<body class="nav-md">
<div  id="btn3" class="loader" style="display: none"></div>	

  <div class="container body">


    <div class="main_container">

      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          
          <!-- sidebar menu -->
            <?php include "menu.php" ?>
          <!-- /sidebar menu -->

         
        </div>
      </div>

      <!-- top navigation -->
           <?php include "top_nav.php" ?>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">

        <div class="">
          
         
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Administración de Incidencias</h2>
                    
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                

                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                      <li id="tab2" role="presentation" class="active"><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="true"><?=$accion;?> Incidencia</a>
                      </li>
                      <li id="tab1" role="presentation" class=""><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="false">Historial de Incidencias</a>
                      </li>
                      
                    </ul>
                    <div id="myTabContent" class="tab-content">
                      <div role="tabpanel" class="tab-pane fade " id="tab_content1" aria-labelledby="home-tab">
                        
                        <!-- tab 1 -->
                        <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2>Incidencias Registradas  </h2>
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content">
                         
                            <table id="datatable-buttons" class="table table-striped table-bordered">
                              <thead>
											      <tr>
												        <th>Asunto</th>
												        <th>Detalle</th>
												        <th>Fecha</th>
												        <th>Estado</th>
                                <th>Respuesta</th>
											         <th>Opciones</th>
											      </tr>
											    </thead>
											    <tbody>
													<?php
													if($vincidencia){
														foreach ($vincidencia AS $id => $array) {
													?>
														<tr>
                            
                            <td align="left">
                            <?php 
                              if($array['asunto']==1){ ?>
															  Información
															 <?php } ?>
                               <?php 
                              if($array['asunto']==2){ ?>
															Error del Sistemas
															 <?php } ?>
                               <?php 
                              if($array['asunto']==3){ ?>
																Pagos
															 <?php } ?>
                            
                            
                            </td>


															<td align="left"><?=$array['detalle'];?></td>

                              <?php
                                	setlocale(LC_TIME, 'spanish');
                                 $fechas = strftime("%d de %B del %Y  a las  %H:%M horas" , strtotime( $array['fecha']));
                                ?>

															<td align="left"><?=$fechas;?></td>


                              <td align="left"><?php 
                              if($array['idestado']=='1'){ ?>
																<span class="label label-warning">Pendiente</span>
															 <?php }; ?>
															 	<?php if($array['idestado']=='2'){ ?>
																<span class="label label-success">Resuelta</span>
															 <?php }; ?></td>
                              
                              
                   
                               <td align="left"> 
                               <?php 
                              if($array['respuesta']==""){ ?>
															  Respuesta en espera !!!
															 <?php } else { ?>
                               <?= $array['respuesta'] ?>
                                <?php
                                 }
                                    ?>
                               </td>




														<td>
															
                                <?php if($array['idestado']=='1'){?>

                                 <a href="incidencia.php?opt=m&id=<?=$id?>" title="modificar" >
																		<span class="label label-primary" >
																			<i class="fa fa-pencil" title="Modificar"></i>
																		</span>
																	</a>
																	
                               
																	<span class="label label-default" onClick="eliminar(<?=$id?>)" >
																		<i class="fa fa-trash-o" title="Eliminar"></i>	
																
																	</span>	

                                   <?php 
                                }else{
                                ?>

                                       <span class="label label-success">Incidente resuelto</span>

                                <?php 
                                }

                                ?>

															</td>										
																
														</tr>
													<?php
														}
													}	
													?>
												</tbody>
											  </table>
						                	
						                	</div>
                        </div>
                      </div>

                      </div>
                      <div role="tabpanel" class="tab-pane fade active in" id="tab_content2" aria-labelledby="profile-tab">
                        
                        <form id="form" class="form-horizontal form-label-left" novalidate>
                  <span class="section">Ingresar Información</span>

                    <?php
                          if ($option == "m") {
                        ?>
                            <input type="hidden" name="id" value="<?=$_GET['id']?>"/>
                            <input type="hidden" name="opt" id="opt" value="m" />
                        <?php
                          }else{
                        ?>
                            <input type="hidden" name="opt" id="opt" value="n" />
                        <?php
                          }
                        ?>
                    
                   						
												
								 
							 <div class="item form-group">
		                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="asunto">Asunto <span class="required">*</span>
		                      </label>
		                      <div class="col-md-6 col-sm-6 col-xs-12">
		                        <select id="asunto" name="asunto" class="form-control">
		                          <option value="1" <?php if($asunto==1){echo"selected='selected'";}?>>Información</option>
		                          <option value="2" <?php if($asunto==2){echo"selected='selected'";}?>>Error del Sistema</option>
		                          <option value="3" <?php if($asunto==3){echo"selected='selected'";}?>>Pagos</option>
		                          
		                        </select> 
		                       </div>
		                    </div>

							
								 
							 <div class="item form-group">
		                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="detalle">Detalle <span class="required">*</span>
		                      </label>
		                      <div class="col-md-6 col-sm-6 col-xs-12">
		                      	<textarea id="detalle" name="detalle" class="form-control col-md-7 col-xs-12"><?=$detalle?></textarea>
		                      </div>
		                    </div>

							
														
					
													
													
								<div class="ln_solid"></div>
				                    <div class="form-group">
				                      <div class="col-md-6 col-md-offset-3">
				                        <input style="border-radius: 20px;" type="button" onClick="cancelar();" class="btn btn-primary" value="Cancelar">
				                        <button style="border-radius: 20px;" id="send" class="btn btn-success">Guardar</button>
				                      </div>
				                    </div>
				                  </form>
									
								</div>
                     
                    </div>
                  </div>


                </div>
              </div>
            </div>
          </div>
        </div>
        <div style="height: 100px;"></div>
       <?php include "piep.php" ?> 

      </div>

    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>
  <script src="js/custom.js"></script>
  <!-- form validation -->
  <script src="js/validator/validator.js"></script>
  <!-- PNotify -->
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  
   <!-- Datatables-->
        <script src="js/datatables/jquery.dataTables.min.js"></script>
        <script src="js/datatables/dataTables.bootstrap.js"></script>
        <script src="js/datatables/dataTables.buttons.min.js"></script>
        <script src="js/datatables/buttons.bootstrap.min.js"></script>
        <script src="js/datatables/jszip.min.js"></script>
        <script src="js/datatables/pdfmake.min.js"></script>
        <script src="js/datatables/vfs_fonts.js"></script>
        <script src="js/datatables/buttons.html5.min.js"></script>
        <script src="js/datatables/buttons.print.min.js"></script>
        <script src="js/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="js/datatables/dataTables.responsive.min.js"></script>
        <script src="js/datatables/responsive.bootstrap.min.js"></script>
        <script src="js/datatables/dataTables.scroller.min.js"></script>
       <script src="js/datatables/buttons.colVis.min.js"></script>

  <script>
  

    $('form').submit(function(e) {
      e.preventDefault();
      var submit = true;  
      if (!validator.checkAll($(this))) {
        submit = false;
      }
      if (submit)
        guardarformulario();
      return false;
    });

  

  </script>
  <!-- pace -->
        <script src="js/pace/pace.min.js"></script>
        <script type="text/javascript">

$(document).ready(function() {
           $("#datatable-buttons").DataTable({
                dom: "Bfrtip",
                buttons: [{
                  extend: "csv",
                  exportOptions: {
                       columns: [ 0, 1, 2, 3, 4 ] //Your Colume value those you want
                           }
                }, {
                  extend: "excel",
                  exportOptions: {
                       columns: [ 0, 1, 2, 3, 4 ] //Your Colume value those you want
                           }
                }, {
                  extend: "pdf",
                  exportOptions: {
                       columns: [ 0, 1, 2, 3, 4 ] //Your Colume value those you want
                           }
                }, {
                  extend: "print",
                  exportOptions: {
                       columns: [ 0, 1, 2, 3, 4 ] //Your Colume value those you want
                           }         
                }],      
              });
            });
        </script>



         <script type="text/javascript">
          $(document).ready(function() {
            <?php if($option == "m"){ ?>
                $("#tab1").removeClass('active');
                $("#tab2").addClass('active');
                $("#tab_content1").removeClass('active in');
                $("#tab_content2").addClass('active in');               
            <?php }?>
          });

        </script>
</body>

</html>

