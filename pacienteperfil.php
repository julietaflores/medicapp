<?php 
header('Cache-Control: no cache'); //no cache
session_cache_limiter('private_no_expire'); // works
	 session_start();
   if (!isset($_SESSION['csmart']['idusuario'])) {
   Header ("Location: login.php");
   }
  
    include 'headercondicion.php';
    include 'classes/cpaciente.php';
    include 'classes/ccita.php';
    include 'classes/ctipocita.php';
    include 'classes/ctipoLesion.php';
    include 'classes/cpacienteNotas.php';
    include 'classes/clesiones.php';
    include 'classes/cpagos.php';
    include 'classes/cpagocredito.php';
    include 'classes/cmembresia.php'; 
   
    $omembresia = new Membresia();
    $oCita  = new cita();
    $oTipocita  = new tipocita();
    $otipo = new tipoLesion();
    $oNotas = new pacienteNotas();
    $oLesiones = new lesiones();
    $opagos = new pago();
    $opagocredito=new pagocredito();
    $opaciente = new paciente();
    $accion   = "Crear";
    $option   = "n";
    $idpaciente   = "";
          $nombre   = "";
          $apellido   = "";
          $identificacion   = "";
          $genero   = "";
          $imagen   = "";
          $direccion  = "";
          $telefono1  = "";
          $telefono2  = "";
          $correo   = "";
          $fecha_nac  = "";
          $fecha_mod  = "";
          $fecha_cre  = "";
          $notas  = "";
          $idestado   = "";
          $iddepartamento   = "";
          $responsable  = "";
          $telefono_resp  = "";
          $medico_fam   = "";
          $ocupacion  = "";
          $idtipocita="";
  if(isset($_REQUEST['id'])){
    $idObj  = $_REQUEST['id'];

    $vpaciente = $opaciente->getOne($idObj);
    if($vpaciente){
      $accion   = "Modificar";
      
         foreach ($vpaciente AS $id => $info){ 
            
            $idpaciente   = $info["idpaciente"];
            $id   = $info["idpaciente"];
            $nombre   = $info["nombre"];
            $apellido   = $info["apellido"];
            $identificacion   = $info["identificacion"];
            $genero   = $info["genero"];
            $imagen   = $info["imagen"];
            $direccion    = $info["direccion"];
            $telefono1    = $info["telefono1"];
            $telefono2    = $info["telefono2"];
            $correo   = $info["correo"];
            $fecha_nac    = $info["fecha_nac"];
            $fecha_mod    = $info["fecha_mod"];
            $fecha_cre    = $info["fecha_cre"];
            $notas    = $info["notas"];
            $idestado   = $info["idestado"];
            $iddepartamento   = $info["iddepartamento"];
            $responsable    = $info["responsable"];
            $telefono_resp    = $info["telefono_resp"];
            $medico_fam   = $info["medico_fam"];
            $ocupacion    = $info["ocupacion"];
             }
       $vcita = $oCita->getAllpaciente($idObj);
    }else{
      header("Location: pacientes.php");
      exit();
    }
  }else{
    header("Location: pacientes.php");
    exit();
  }

  $tpusuario=$_SESSION['csmart']['permisos'] ;

  $vtipo    = $otipo->getAll();
  $vNotas    = $oNotas->getAll($idpaciente );
  $vLesiones    = $oLesiones->getAll($idpaciente);
  $vGaleria  = $opaciente->getGaleria($idpaciente);
  $vpagos=$opagos->getAll($idpaciente);
  $vpagostotal=$opagos->getAlltotalconseguro($idpaciente);
  $seguross    =  $oUser->getAllseguros();


  $vPagocredito=$opagocredito->getAll($idpaciente);
  $vienedecita=isset($_REQUEST['cita']) ? $_REQUEST['cita']: 0;
  $vtcita    =$oTipocita->getAll();
  $vUsuarios =$oUser->getAllxClinica($_SESSION['csmart']['clinica']);
  $nroimg=0;
 
 if($vGaleria){
  $nroimg=count($vGaleria);
 }
 


$date = new DateTime();
$date->modify('+1 hours');
$newh= $date->format('H');
$newm= $date->format('i');


$nombredeusuario= $_SESSION['usuario'];





 $citanota=$oNotas->citanotaPhp($idpaciente);



$mesnombre='';
?>
<!DOCTYPE html>
<html ng-app="clinicapp">

<head>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">

  <?php include 'header.php'; ?>

  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  <script type="text/javascript" src="js/notify/pnotify.buttons.js"></script>
  <script type="text/javascript" src="js/notify/pnotify.nonblock.js"></script>
  <script type="text/javascript"> 
    
      function subir(id){

        var limiteimg=parseInt($("#limiteimg").text());
        var limitar=parseInt($("#limitador").text());
        var file_data3 = $('#imagenS3').prop('files')[0];   
        var form_data3 = new FormData();                  
        form_data3.append('file', file_data3);
    
        if(file_data3){ // SI EXISTE IMAGEN PARA SUBIR o ACTUALIZAR
              var fsize3 = $('#imagenS3')[0].files[0].size; //get file size
              if(fsize3>548576 ) 
              {
                valido = false;
                new PNotify({
                     title: 'Error en Imagen 3!',
                     text: 'Excede el tamaño máximo permitido. Tamaño permitido menor a 500 Kb.'
                 });
              }else{
                var nombre = $('#imagenS3').prop('files')[0].name;
                $(".loaderp").fadeIn("slow");
              $.ajax({
                url:'actions/actionpaciente.php?opt=subir&id='+id + '&nombre=' + nombre,
                type:'POST',
                data: {  }
              }).done(function( data ){
                
            if(data > 0){

              $.ajax({
                  url: 'processUpload.php?id='+id + '&tipo=paciente', // point to server-side PHP script 
                  dataType: 'text',  // what to expect back from the PHP script, if anything
                  cache: false,
                  contentType: false,
                  processData: false,
                  data: form_data3,                         
                  type: 'post',
                  success: function(val){
                      limitar++;
                      if(limitar>=limiteimg){
                        document.getElementById("imagenS3").disabled = true;
                        $("#limitarmembresia").show('slow');
                      }
                    $(".loaderp").fadeOut("slow");
                    var codigo = $('#newGal').html();
                    $('#newGal').html(codigo +
                     '<div class="col-md-55" id="gal'+data+
                     '"><div class="thumbnail"><div class="image view-first fotoGal"><img style="width: 100%;height:90px; display: block;" data-toggle="modal" data-target="#myModalFoto" onclick="onClick(this)" src="images/paciente/'+id+'/'+nombre+'" alt="image" /><div class="mask"><p style="color:white">clic para ampliar</p></div></div><div class="caption"><p><a onClick="eliminarFoto('+data+','+id+',\''+nombre+'\')"> <i class="fa fa-times"></i></a> '+nombre+
                     '</p></div></div></div>');
                    $("#limitador").text(limitar);
                     new PNotify({
                         title: 'Datos Guardados',
                         text: 'Todos los datos fueron guardados. Puede continuar.',
                         type: 'info'
                      });

                  }
            });
        
  }
  
                            else{
                              new PNotify({
                                        title: 'Error en formulario',
                                        text: 'No se puedieron guardar los datos, intente de nuevo.',
                                        type: 'error'
                                    });
                                    window.setTimeout("location.reload(true);",2500);
                            }
                            
                          });
              }
            }
        
 
        
      }
      
      
      ////////////////Eliminar
      function eliminarFoto(id,px,archivo) {
        var limitar=parseInt($("#limitador").text());
        var limiteimg=parseInt($("#limiteimg").text());
          if (confirm("Atencion! Va a proceder eliminar este registro. Desea continuar?")) {
            $(".loaderp").fadeIn("slow");
            debugger;
            $.ajax({
              url: 'actions/actionpaciente.php?opt=efoto&id='+id+'&px='+px+'&archivo='+archivo,
              type:'POST',
              data: { },
            success:function(data){
              $(".loaderp").fadeOut("slow");
              if(data == 0){
                $("#gal" + id).hide('fast'); 
                limitar--;
                if(limitar<=limiteimg){
                        document.getElementById("imagenS3").disabled = false;
                        $("#limitarmembresia").hide('slow');
                      }
                $("#limitador").text(limitar);
                new PNotify({
                           title: 'Datos Eliminados',
                            text: 'Todos los datos fueron guardados. Puede continuar.!',
                            type: 'info'
                          });
                         
              }
              else if(data == 1){
                msg = "Error en idpaciente.";
                showWarning(msg,5000);
              }
              else{
                new PNotify({
                             title: 'Error en formulario',
                            text: 'No se puedieron guardar los datos, intente de nuevo.',
                            type: 'error'
                         });
              }
            }
            
            });
          }
      }

      $(window).load(function() {
					$(".loaderp").fadeOut("slow");
				});
      
  </script>

</head>

 

<body class="nav-md">

<div class="loaderp" style="position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('images/cargando.webp') 50% 50% no-repeat rgb(249,249,249);
    opacity: .8;"></div>
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
         <!-- sidebar menu -->
            <?php include "menu.php" ?>
          <!-- /sidebar menu -->
        </div>
      </div>
           <?php include "top_nav.php" ?>
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">

            
          </div>
          <div class="clearfix"></div>

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <div class="row">
                      <div class="col-md-6">
                        <h2>Perfil de Paciente <?=$nombre.' '.$apellido;?></h2></div>
                      <div class="col-md-6" style="text-align: right">
                        <a href="" data-toggle="modal" data-target="#CalenderModalNew" class="btn btn-danger">Crear Cita</a>
                      </div>
                  </div>
                  
                  <input id="userID" type="hidden" value="<?=$_SESSION['csmart']['idusuario']?>">
                  <input id="nombreusuario" type="hidden" value="<?=$_SESSION['usuario']?>">
                                      
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <div class="col-md-3 col-sm-3 col-xs-12 profile_left">

           
                        <div class="avatar-view" title="Foto de paciente">
                        <?if($imagen==''){?>
                          <img id="fotoPerfil" src="images/<?=$imagen;?>" alt="Avatar">
                                         
														<?}else{
                              if(file_exists("images/paciente/".$idpaciente."/".$imagen)){?>
                                <img id="fotoPerfil" src="images/paciente/<?=$idpaciente;?>/<?=$imagen;?>" alt="Avatar">
                                      
                             <? }else{?>
                               <img id="fotoPerfil" src="images/user.png" alt="Avatar">
                             <?}
                              ?>
                          	<?}?>
                        
                          <input type="hidden" id="idpaciente" value="<?=$idpaciente;?>" />
                          <input type="hidden" id="nombreusuario1" value="<?=$nombredeusuario;?>" />    
                        </div>
                     

                 
                    <h3><?=$nombre.' '.$apellido;?></h3>

                    <ul class="list-unstyled user_data">
                      <?if($direccion){?>
                         <li><i class="fa fa-map-marker user-profile-icon"></i> <?=$direccion;?>
                         </li>
                       <? }?>
                     
                        <?if($ocupacion){?>
                           <li>
                           <i class="fa fa-briefcase user-profile-icon"></i> <?=$ocupacion?>
                         </li>
                         <? }?>
                     <?if($telefono1){?>
                        <li>
                        <i class="fa fa-phone user-profile-icon"></i> <?=$telefono1?>
                      </li>
                       <?}?>
                      <?if($correo){?>
                        <li class="m-top-xs">
                        <i class="fa fa-envelope user-profile-icon"></i>
                        <a href="#" target="_blank"><?=$correo;?></a>
                      </li>
                        <?}?>
                     
                    </ul>

                    <a onclick="ireditar(<?=$id?>)" class="btn btn-success"><i class="fa fa-edit m-right-xs"></i>Editar Paciente</a>
                    <br />

                    

                  </div>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                     
                        <li role="presentation" class="active"><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="true">Historial Citas</a>
                        </li>
                    
                        <li role="presentation" class=""><a href="#tab_content7" role="tab" id="profile-tab7" data-toggle="tab" aria-expanded="false">Pagos</a>
                        </li>
                      </ul>
            <div id="myTabContent" class="tab-content">
               <div role="tabpanel" class="tab-pane fade active in" id="tab_content2" aria-labelledby="profile-tab">
                        <div style="text-align: center;">
                        <p>Registro de la Evolución:</p>
                           
                           <div class="x_content">
                             <div class="dashboard-widget-content">
                              
                              <textarea id="nota" name="nota" cols="30" style="width:50%;" rows="2" placeholder="agregar detalle"></textarea>
                               <button type="button" style="border-radius: 20px;" class="btn btn-success btn-xs" onclick="guardarnota()"> Agregar <i class="fa fa-chevron-right"></i> </button>
                             </div>
                           </div> 
                        </div>
               
                           <br>


                     
                <?if($citanota){?>
                 <ul id="listaNotascitas" class="list-unstyled timeline widget;">

                 
                  <?foreach ($citanota AS $id => $array) {
                    $date = strtotime($array['fecha']);
                    switch (date('m', $date)) {
                      case 1:
                      $mesnombre='Ene';
                          break;
                      case 2:
                      $mesnombre='Feb';
                          break;
                      case 3:
                      $mesnombre='Mar';
                          break;
                      case 4:
                      $mesnombre='Abr';
                          break;
                      case 5:
                      $mesnombre='May';
                          break;
                      case 6:
                      $mesnombre='Jun';
                          break;
                      case 12:
                      $mesnombre='Dic';
                          break;
                      case 7:
                      $mesnombre='Jul';
                          break;
                      case 8:
                      $mesnombre='Ago';
                          break;
                      case 9:
                      $mesnombre='Sep';
                          break;
                      case 10:
                      $mesnombre='Oct';
                          break;
                      case 11:
                      $mesnombre='Nov';
                          break;
                  }
                    if($array['observaciones']=='nota'){
                      
                     
                      ?>
                    <li>
                      
                    <div class="row">
                    <div class="col-md-2 col-xs-4" style="position: relative;height: 100px;top:15px;">
                                <div class="fechaHistorial" style="position: absolute;top: 25px;">
                                    <h2><?=date('d',$date)?></h2>
                                    <h2><?=$mesnombre?></h2>
                                </div>
                    </div>
                    <br>
                    <div class="col-md-8 col-xs-8" style="border: 4px solid;border-radius: 15px;border-color: #F3F2F2 ;height:auto;   margin:10px;">
                      <div class="card">
                        <div class="card-body">
                          <h5 class="card-title">Nota</h5>
                          <p class="card-text">Observación: <?=$array['title']?>...</p>
                          <p  style="padding-left: 270px; font: oblique bold 90% cursive;">Realizado por: <?=$array['nomu']?></p>
                        </div>
                      </div>
                    </div>  
                      
                  </div>
                
                </li>
                   <? }else{?>
                   <li> <div class="row">
                       <?php if($tpusuario==2){?>

                       <div class="col-md-2  col-xs-4" style="position: relative;height: 100px;">
                                <div class="fechaHistorial" style="position: absolute;top: 95px;">
                                    <h2><?=date('d',$date)?></h2>
                                    <h2><?=$mesnombre?></h2>
                                </div>
                    </div>
                       <?php }else{?>
                        <div class="col-md-2  col-xs-4" style="position: relative;height: 100px;">
                                <div class="fechaHistorial" style="position: absolute;top: 25px;">
                                    <h2><?=date('d',$date)?></h2>
                                    <h2><?=$mesnombre?></h2>
                                  
                                </div>
                    </div>
                       <?php }?>

                    <?$tipoestado="";
                   
                    if($array['idestado']==0){
                      $tipoestado='<span class="label label-danger cita" >Cancelada</span>';?>
                       
                    <?}else{
                      if($array['idestado']==1){
                        $tipoestado='<span class="label label-warning cita" >Programada</span>';
                      }else{
                        $tipoestado='<span class="label label-success cita" >Finalizada</span>';
                      }
                    }?>
                    <br>
                    
                    
                    <?php if($tpusuario==2){ ?>
                    <div class="col-md-8 col-xs-8" style="border: 4px solid;border-radius: 15px;border-color: #F3F2F2; margin:10px;">
                      <div class="card">
                        <div class="card-body">
                          <h5 class="card-title">Cita <?=$array['title']?> <?echo $tipoestado?></h5>
                            <p class="card-text">Diagnostico: <?=$array['diagnostico']?>...</p>
                            <p class="card-text">Prescripción: <?=$array['prescripcion']?>...</p>
                            <p class="card-text">Indicaciones: <?=$array['indicaciones']?>...</p>
                            <p class="card-text">Anotaciones: <?=$array['anotaciones']?>...</p>
                          <div style="text-align: right;">
                            <a class="cita"  style="padding-left: 415px; font: oblique bold 90% cursive; color: red;"  id="cita<?=$array['id']?>" data-toggle="modal" data-target="#ModalCita"  data-tratamientopago1="<?=$array['title']?>" data-estd="<?=$array['idestado']?>"  data-id="<?=$array['id']?>"  data-md="<?=$array['diagnostico']?>" data-mp="<?=$array['prescripcion']?>" data-mi="<?=$array['indicaciones']?>" data-ma="<?=$array['anotaciones']?>" >Ver</a>
                            <input id="estadocita<?=$array['id']?>" type="hidden" value="<?=$array['idestado']?>" >
                            <p  style="padding-left: 240px; font: oblique bold 90% cursive;">Cita realizado(a) por: <?=$array['nomu']?></p>
                         
                          </div>  
                        </div>
                      </div>
                    </div>
                    <?php }else{?>
                      <div class="col-md-8 col-xs-8" style="border: 4px solid;border-radius: 15px;border-color: #F3F2F2 ;height:auto;   margin:10px;">
                      <div class="card">
                        <div class="card-body">
                        <h5 class="card-title">Cita <?=$array['title']?> <?echo $tipoestado?></h5>
                        <p  style="padding-left: 200px; font: oblique bold 90% cursive;">Cita realizado(a) por: <?=$array['nomu']?></p>
                        </div>
                      </div>
                    </div>  



                      <!--<div class="col-md-4 col-xs-4" style=" border: 4px solid;border-radius: 15px;border-color: #F3F2F2; margin:10px;">
                      <div class="card">
                        <div class="card-body">
                          <h5 class="card-title">Cita <?=$array['title']?> <?echo $tipoestado?></h5>
                          <p  style="padding-left: 100px; font: oblique bold 90% cursive;">Cita realizado(a) por: <?=$array['nomu']?></p>
                         
                        </div>
                       </div>
                      </div>-->
                    
                    <?php }?>




                    
                  </div></li>
                    <?}?>
                    
                 <? }?>
                 </ul>
                  <?}?>
                  <input type="hidden" id="citaprueba" value="<?=$vienedecita?>">
                 
                  <div >
                    <br>
                    <input id="nrocitanotas" type="hidden" value="<?=count($citanota)?>">
                    <?if(count($citanota)>=10){?>
                      <div id="botonVermasnotas" style="text-align: center;">
                    <a onclick="vermasNotas(<?=$idpaciente?>)" class="btn btn-default">Ver más</a>
                 
                    </div>
                      <?}?>
                  
                 </div>
              <br>
            </div>
                  
                         
                        <!--notas-------------------------------------------------->
                    <!--    <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                          <p>Registro de la evolución:</p>
                           
                          <div class="x_content">
                            <div class="dashboard-widget-content">
                              <input id="nota" name="nota" type="text" style="width:50%;" /> 
                              <button type="button"  class="btn btn-success btn-xs " onclick="guardarnota()"> Agregar <i class="fa fa-chevron-right"></i> </button>
                                  <ul class="list-unstyled timeline widget">
                                     
                                    <div id="nnotas">

                                    </div>
                                    <?php if($vNotas){
                                      //var_dump($vNotas);
                                        foreach ($vNotas AS $id => $array) {?>
                                          
                                        <li>
                                          <div class="block">
                                            <div class="block_content">
                                              
                                              <p class="excerpt">  <?=$array['detalle'];?>
                                              </p>
                                              <div class="byline">
                                                <span><?$source = $array['fecha'];
                                                $date = new DateTime($source);
                                                echo $date->format('d-m-Y');
                                                ?></span> por <?=$array['idusuario'];?>
                                              </div>
                                            </div>
                                          </div>
                                        </li>
                                    
                                    <?php } } ?>
                                  </ul>
                          </div>
                        </div>
                        </div>-->

                       
    <!--pagos------------------------------------------------------>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content7" aria-labelledby="profile-tab">
                             <div class="container">
                             <div class="x_titlecc">
                               <h2>Lista de Pagos</h2>
                             </div>
                                          <?include 'pagos.php'?>
                             </div>             
                            
                        </div>
 
<!------------------------------------------------------------------------------------------->
                      </div>
                    
                  </div>
                </div>
              </div>
            </div>
                
          </div>
       
        </div>


      
            </div>
               <div style="height: 100px;"></div>
            <?php include "piep.php" ?> 
          </div>

       

        <!-- Modal Cita -->
        <div class="modal fade" id="ModalCita" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button"  onClick="ocultar('contenido2')"   class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"> Control de Cita  </h4>
              </div>
              <div class="modal-body">
                <div class="item form-group">
                Diagnostico
                <textarea class="form-control" cols="20" id="diagnostico" rows="3"></textarea>
                Prescripción
                <textarea class="form-control" cols="20" id="prescripcion" rows="3"></textarea>
                Indicaciones
                <textarea class="form-control" cols="20" id="indicaciones" rows="2"></textarea>
                Anotaciones
                <textarea class="form-control" cols="20" id="anotaciones" rows="2"></textarea>
                
                <input type="hidden" name="estado" id="estado"/>
                <input type="hidden" name="tratamientopago1" id="tratamientopago1"/>
                                 
                                         
                <br>
                <!--ocultar factura-->
           
                  <!--ocultar factura-->
    <div style="display: none">
                <label class="col-sm-3 control-label">Costo de Consulta </label>
                      <div class="col-sm-1">
                        <input type="text" name="valor" id="valor" value="0"/>
                        <input type="hidden" name="idcita" id="idcita" value="0"/>
                      </div>

                <label class="col-sm-3 control-label"> </label>
                      <div id="facturaDiv" class="col-md-3 col-sm-3 col-xs-12">
                        <div class="checkbox"  >
                            <label>
                              <input id="factura" name="factura" type="checkbox" class=""> Generar Factura
                              <input id="idfactura" name="idfactura" type="hidden" value="0"  > 
                            </label>
                        </div>
                      </div> 
    </div>
                <!---->

                </div>
              </div>


         


    <div class="titulo_boton">
    Pagó de la Cita
    <a style='cursor: pointer;' onClick="muestra_oculta('contenido2')" title="" class="boton_mostrar">Mostrar / Ocultar</a>
    </div>


     <div id="contenido2" style="display: none;">
                        <form id="antoform" class="form-horizontal calender" role="form">
                                                 
                        <input type="hidden" name="tratamientopago2" id="tratamientopago2"/>

                          <div class="form-group">   
                            <label class="col-sm-3 control-label" for="fechap1">Fecha</label>
                            <div class="col-sm-4">
                              <input class="form-control" name="fechap1" id="fechap1" type="date" value="<?php echo date('Y-m-d');?>">
                            </div>
                            
                            <label class="col-sm-2 control-label" for="monto1">Monto</label>
                            <div class="col-sm-3">
                             <input class="form-control" name="monto1" id="monto1" type="number" >
                            </div>
                        </div> 

                        <div class="form-group">
                           <label class="col-sm-3 control-label" for="formapago1">Forma de Pago</label>
                            <div class="col-sm-4">
                             <select class="form-control" name="formapagoid1" id="formapagoid1">
                                    <option selected="selected" value="1">Efectivo</option>
                                    <option  value="2">Tarjeta</option>
                                    
                             </select>
                            </div>
                                   
                         <label class="col-sm-1 control-label" style="padding-left: 3px;" for="formaseguro1">Seguro</label>
                            <div class="col-sm-4" >

                            <select class="form-control" name="formaseguroid1" id="formaseguroid1">
                          <option value="1">Elegir una opción</option>
                          <?php if($seguross){
                                foreach ($seguross AS $id => $array) {?>
                                <option value="<?=$array['idseguro'];?>" <?php if($idseguro==$id){echo"selected='selected'";}?>><?=$array['nombre'];?></option>
                          <?php } } ?>
                        </select>

                          
                            </div>
                        </div> 
                        </form>
     </div>   


<br>
              <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-danger cerrar ">Cancelar</button>
                <button type="button" data-dismiss="modal" class="btn btn-success guardarResultado">Guardar</button>
              </div>
            </div>
          </div>
        </div>



<!--------------------------------------------------------------------------modal citas--->
<div id="CalenderModalNew" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title" id="myModalLabel">Nueva Cita</h4>
            </div>
            <div class="modal-body">
              <div id="testmodal" style="padding: 5px 20px;">
                <form id="antoform" class="form-horizontal calender" role="form">
                <div class="form-group">
                      <label class="col-sm-3 control-label">Tipo de Cita </label>
                      <div class="col-sm-9">
                        <select id="idtipocita" name="idtipocita" class="form-control">
                        <?php if($vtcita){
                        
                                foreach ($vtcita AS $id => $array) {
                                  if($array['idtipocita']!=4){?>
                                    <option value="<?=$array['idtipocita'];?>" <?php if($idtipocita==$id){echo"selected='selected'";}?>><?=$array['tipo'];?></option>
                         
                                  <?}?>
                                  
                          <?php } } ?>
                        </select>
                      </div>
                    </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Motivó </label>
                    <div class="col-sm-9">
                      
                        <input type="text" class="form-control"  id="pacientec" name="pacientec" /> 
                      
                    </div>
                   </div>
                  <div class="form-group" id="camposinpaciente" style="display: none">
                    <label class="col-sm-3 control-label">Paciente</label>
                    <div class="col-sm-9" >
                    
                   
                          <?php if($vpaciente){
                                foreach ($vpaciente AS $id => $array) {?>
                                <input type="text" id="paci" value="<?=$array['idpaciente'];?>">
                                
                          <input type="hidden" id="idpacientec" name="idpacientec" value="<?=$array['idpaciente'];?>"/>
                          <?php } } ?>
                  
                   
                    
                    </div>
                     
                  </div>
                  
                  <div class="form-group" style="display: none">
                      <label class="col-sm-3 control-label">Usuario </label>
                      <div class="col-sm-9">
                        
                        <input type="hidden" id="idusuario" value="<?=$_SESSION['csmart']['idusuario']?>">
                      </div>
                    </div>

                 
                <div class="form-group" >   
                <label class="col-sm-3 control-label">Color </label> 
                <div class="col-sm-9">
                <input id="rojo" style="display: none;" checked="checked" name="coloresc" type="radio" value="FF2968">
                      <label class="labelcolo" for="rojo" style="background: #FF2968; width: 20px;height: 20px;border-radius:15px; border:3px solid;"></label>
                      <input id="verde" style="display: none;" name="coloresc" type="radio" value="FF9500">
                      <label class="labelcolo" for="verde" style="background: #FF9500; width: 20px;height: 20px;border-radius:15px;"></label>
                      <input id="azul" style="display: none;" name="coloresc" type="radio" value="FFD60B">
                      <label class="labelcolo" for="azul" style="background: #FFD60B; width: 20px;height: 20px;border-radius:15px;"></label>
                      <input id="amarillo" style="display: none;" name="coloresc" type="radio" value="1BADF8">
                      <label class="labelcolo" for="amarillo" style="background: #1BADF8 ; width: 20px;height: 20px;border-radius:15px;"></label>
                      <input  id="cielo" style="display: none;" name="coloresc" type="radio" value="CC73E1">
                      <label class="labelcolo" for="cielo" style="background: #CC73E1 ; width: 20px;height: 20px;border-radius:15px;"></label>
                     
                      <input  id="x" style="display: none;" name="coloresc" type="radio" value="AC8E68">
                      <label class="labelcolo" for="cielo" style="background: #AC8E68 ; width: 20px;height: 20px;border-radius:15px;"></label>
                      <input  id="y" style="display: none;" name="coloresc" type="radio" value="32D74B">
                      <label class="labelcolo" for="cielo" style="background: #32D74B ; width: 20px;height: 20px;border-radius:15px;"></label>
                       </div>            
                     
                </div>


                <div class="form-group">
                    <label class="col-sm-3 control-label">Fecha</label>
                    <div class="col-sm-4">
                      <input type="date" class="form-control" value="<?php echo date('Y-m-d');?>" id="fechacita" name="fechacita">
                     </div>

                     <label class="col-sm-1 control-label">Hora</label>

                    <div class="col-sm-2">
                      <select id="horac" name="horac" class="form-control">
                          <option value="<?=$newh?>" ><?=$newh?></option>
                          <option value="00" >00</option>
                          <option value="01" >01</option>
                          <option value="02" >02</option>
                          <option value="03" >03</option>
                          <option value="04" >04</option>
                          <option value="05" >05</option>
                          <option value="06" >06</option>
                          <option value="07" >07</option>
                          <option value="08" >08</option>
                          <option value="09" >09</option>
                          <option value="10" >10</option>
                          <option value="11" >11</option>
                          <option value="12" >12</option>
                          <option value="13" >13</option>
                          <option value="14" >14</option>
                          <option value="15" >15</option>
                          <option value="16" >16</option>
                          <option value="17" >17</option>
                          <option value="18" >18</option>
                          <option value="19" >19</option>
                          <option value="20" >20</option>
                          <option value="21" >21</option>
                          <option value="22" >22</option>
                          <option value="23" >23</option>
                          <option value="24" >24</option>
                         </select>

                    </div>
                  
                    <label style="position: absolute; right: 120px;">:</label>
               
                    <div class="col-sm-2">
                      <select id="minutoc" name="minutoc" class="form-control">
                          <option value="<?=$newm?>" ><?=$newm?></option>
                          <option value="00" >00</option>
                          <option value="10" >10</option>
                          <option value="20" >20</option>
                          <option value="30" >30</option>
                          <option value="40" >40</option>
                          <option value="50" >50</option>
                         </select>
                    </div>

                  </div>
                
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Nota</label>
                    <div class="col-sm-9">
                      <textarea class="form-control" style="height:55px;" id="descrc" name="descrc"></textarea>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-primary antosubmit">Guardar</button>
            </div>
          </div>
        </div>
      </div>

      <form style="display: hidden" action="pacientenuevo.php" method="POST" id="formularioeditar">
																						<input type="hidden" id="idpacienteditar" name="id" value=""/>
																						<input type="hidden" id="modificarpaciente" name="opt" value="">
																						
		</form>
<!----------------------------------------->
        <script type="text/javascript">
                   
          $(document).ready(function() {
      
            setTimeout(function() {
              debugger;
              
              var citaprueba=$("#citaprueba").val();
              var estadocit=$("#estadocita"+citaprueba).val();
              if($("#citaprueba").val()>0&&estadocit==1 ){

                console.log($("#citaprueba"));
                $("#cita"+citaprueba).get(0).click();
                
              }
          },1000);
             
           
          
              
              
              $('#factura').change(function(){
                  if ($('#factura').is(":checked")){
                    $('#idfactura').val('1');
                  }else{
                    $('#idfactura').val('0');
                    
                  }
                  console.log('3');
              });
            
                      
            });
       </script>



      </div>
      <!-- /page content -->
    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="js/bootstrap.min.js"></script>
  
  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>

  <script src="js/custom.js"></script>

  <!-- image cropping -->
  <script src="js/cropping/cropper.min.js"></script>
  <script src="js/cropping/main.js"></script>

  <!-- daterangepicker -->
  <script type="text/javascript" src="js/moment/moment.min.js"></script>
  <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>

  <!-- chart js -->
  <script src="js/chartjs/chart.min.js"></script>

  <!-- moris js -->
  <script src="js/moris/raphael-min.js"></script>
  <script src="js/moris/morris.min.js"></script>

  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>

  

   <script>

      function onClick(element) {
        document.getElementById("img01").src = element.src;
        //$('#myModalFoto').show();
        $("#ampliara").click();
        //document.getElementById("myModalFoto").style.display = "block";
      }
//formato fecha
function formatDate(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();

  var strTime = hours + ':' + minutes ;
  return date.getDate() + "/" + date.getMonth()+1 + "/" + date.getFullYear() + " " + strTime;
}


      function guardarnota(){
        var nombreusuario = $('#nombreusuario1').val();   
        var d = new Date();
        var e = formatDate(d);
        var idpaciente = $('#idpaciente').val();
        var user=$("#nombreusuario").val();
        var detalle = $('#nota').val();
       
        if( detalle == null || detalle.length == 0 || /^\s+$/.test(detalle) ){
          new PNotify({
                title: 'error !!',
                text: 'agregar una nota!',
                type: 'error'
          });
        }else{
        $(".loaderp").fadeIn("slow");
        $.ajax({
          url:'actions/actionpacienteNotas.php',
          type:'POST',
          data: { opt: 'n', val: detalle, idpaciente: idpaciente  }
        }).done(function( data ){
            var num = data;
            var codigo = $('#nnotas').html();
           // $('#nota').html('<li><div class="block"><div class="block_content"><p class="excerpt">  '+detalle+' </p><div class="byline"> </div></div></div></li>')
             new PNotify({
               title: 'Datos almacenados',
               text: 'Todos los datos fueron guardados. Puede continuar.!',
               type: 'info'
             });
          window.setTimeout("location.reload(true);",500);
        });

      } 
        $("#nota").val('');
     }


















      function borrarnota(id){
        var cod = id;
        $.ajax({
          url:'actions/actionnotas.php',
          type:'POST',
          data: { opt: 'b', val: cod  }
        }).done(function( data ){
          
           
            $('#'+id+'').hide();
            
        });

      }
  </script>

  <!-- datepicker -->
  <script type="text/javascript">
    $(document).on("click", ".enlace", function () {
         var myBookId = $(this).data('id');
         $(".modal-body #bookId").val( myBookId );
   
    });

    $(document).on("click", ".cita", function () {
         var myBookId = $(this).data('id');
         $(".modal-body #idcita").val( myBookId );

         var estado = $(this).data('estd');
         $(".modal-body #estado").val( estado );

         var title = $(this).data('tratamientopago1');
         $(".modal-body #tratamientopago1").val( title );

         var diagnostico = $(this).data('md');
         $(".modal-body #diagnostico").val( diagnostico );
         var prescripcion = $(this).data('mp');
         $(".modal-body #prescripcion").val( prescripcion );
         var indicaciones = $(this).data('mi');
         $(".modal-body #indicaciones").val( indicaciones );
         var anotaciones = $(this).data('ma');
         $(".modal-body #anotaciones").val( anotaciones);
       
       
         var valor = $(this).data('valor');
         $(".modal-body #valor").val( valor );
         var fact = $(this).data('fact');
         if(fact==1){
            $(".modal-body #facturaDiv").hide( );
         }
       
    });


    $(document).on("click", ".guardarResultado1", function() {     
            var id = $("#idcita").val();
            if (id) {
                   var diagnostico =  $("#diagnostico").val();
                   var prescripcion = $("#prescripcion").val();
                   var indicaciones = $("#indicaciones").val();
                   var anotaciones = $("#anotaciones").val();
                
                  $.ajax({
                       url: 'actions/actioncita.php?opt=control1',
                       data: 'id='+ id +'&diagnostico='+ diagnostico +'&prescripcion='+ prescripcion +'&indicaciones='+ indicaciones +'&anotaciones='+ anotaciones  ,
                       type: "POST",
                       success: function(json) {
                         $("#diagnostico").val('');
                         $("#prescripcion").val('');
                         $("#indicaciones").val('');
                         $("#anotaciones").val('');
                         location.reload();
                         new PNotify({
                             title: 'Datos Guardados',
                             text: 'El seguimiento y control de la cita fueron guardados.',
                             type: 'info'
                          });
                     }
                   });             
               }
      });









      $(document).on("click", ".cerrar", function() {
          ocultar('contenido2');
      });





    $(document).on("click", ".guardarResultado", function() { 


            var id = $("#idcita").val();
            if (id) {
                   var diagnostico =  $("#diagnostico").val();
                   var prescripcion = $("#prescripcion").val();
                   var indicaciones = $("#indicaciones").val();
                   var anotaciones = $("#anotaciones").val();
                   var valor = $("#valor").val();
                   var factura = $("#idfactura").val();
                   var idpaciente = $("#idpaciente").val();
                  $.ajax({
                       url: 'actions/actioncita.php?opt=control',
                       data: 'id='+ id +'&diagnostico='+ diagnostico +'&prescripcion='+ prescripcion +'&indicaciones='+ indicaciones +'&anotaciones='+ anotaciones +'&valor='+ valor +'&factura='+ factura +'&idpaciente='+ idpaciente ,
                       type: "POST",
                       success: function(json) {
                         $("#diagnostico").val('');
                         $("#prescripcion").val('');
                         $("#indicaciones").val('');
                         $("#anotaciones").val('');
                         $("#valor").val('');
                         new PNotify({
                             title: 'Datos Guardados',
                             text: 'El seguimiento y control de la cita fueron guardados.',
                             type: 'success'
                          });

                        

                          if(validarrr()){
                            var seguro="";
                            var userID=$("#userID").val();
                            var fecha=$("#fechap1").val();
                            var idpaciente = $('#idpaciente').val();
                            var monto =  $("#monto1").val();
                            var formapago = $("#formapagoid1").val();
                            var formaseguro = $("#formaseguroid1").val();
                            var nota = "sin nota";
                            var tratamiento=$("#tratamientopago2").val();
                            var tipopago=1;

                            $.ajax({
                      type: 'POST',
                      url: 'actions/actionUsuario.php',
                      data: {id:formaseguro, opt: 'obtenerseguro'},
                            success: function(data) {   
                             if (data != 0){   
                             seguro=data;
                              }
                            }	
                     }); 

                         $(".loaderp").fadeIn("slow");
                    $.ajax({
                      url: 'actions/actionpagos.php?opt=np',
                      data: 'usuario='+userID+'&fecha='+fecha+ '&paciente='+idpaciente+
                            '&monto='+monto+'&formapago='+formapago+'&nota='+nota+
                            '&tratamiento='+tratamiento+
                            '&tipopago='+tipopago +
                            '&formaseguro='+formaseguro ,
                      type: "POST",
                     dataType:'json',
                      success: function(data) {
                          
                       
                        $(".loaderp").fadeOut("slow");
                           
                        
                        llenartablapago(data,tratamiento,fecha,monto,formapago,nota,seguro)
                        location.reload();

                       
                                             
                    }
                  });
            


                          }else{
                             
                            new PNotify({
                                title: 'Faltan datos',
                                  text: 'Rellene todos los datos !!!',
                                  type: 'info'
                                });

                          }

                       }
                   });             
               }
      });
  

    $(document).ready(function() {

      
      $('#imagenS3').change(function() {
                var filename = $(this).val();
                var lastIndex = filename.lastIndexOf("\\");
                if (lastIndex >= 0) {
                    filename = filename.substring(lastIndex + 1);
                } 
                
                $('#img3').val(filename);
               $("#botonSubir").click();
            });
            
  $('#imagenS3').change(function(e) {
	debugger;
      addImage(e); 
     });
   function addImage(e){
      var file = e.target.files[0],
      imageType = /image.*/;
    
      if (!file.type.match(imageType))
       return;
  
      var reader = new FileReader();
      reader.onload = fileOnload;
      reader.readAsDataURL(file);
     }
  
     function fileOnload(e) {
      var result=e.target.result;
      $('#imagenpaciente').attr("src",result);
     }
      var cb = function(start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
      }

      var optionSet1 = {
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2015',
        dateLimit: {
          days: 60
        },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'MM/DD/YYYY',
        separator: ' to ',
        locale: {
          applyLabel: 'Submit',
          cancelLabel: 'Clear',
          fromLabel: 'From',
          toLabel: 'To',
          customRangeLabel: 'Custom',
          daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
          monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
          firstDay: 1
        }
      };
      $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
      //$('#reportrange').daterangepicker(optionSet1, cb);
      $('#reportrange').on('show.daterangepicker', function() {
        console.log("show event fired");
      });
      $('#reportrange').on('hide.daterangepicker', function() {
        console.log("hide event fired");
      });
      $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
      });
      $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
        console.log("cancel event fired");
      });
      $('#options1').click(function() {
        $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
      });
      $('#options2').click(function() {
        $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
      });
      $('#destroy').click(function() {
        $('#reportrange').data('daterangepicker').remove();
      });
    });
  </script>
  <!-- /datepicker ////////////////////////////////////////////////////////////////////////////////-->
 
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
 
  <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/maphilight/1.4.0/jquery.maphilight.min.js"></script>-->
  <script src="js/jquery.maphilight.js"></script>
<script src="js/jquery.rwdImageMaps.min.js" ></script>




<script>




//$(document).on("click", ".cerrar", function() {     
 // muestra_oculta('contenido2');
  //      });

function muestra_oculta(id){
if (document.getElementById){ //se obtiene el id
var el = document.getElementById(id); 
//se define la variable "el" igual a nuestro div
el.style.display = (el.style.display == 'none') ? 'block' : 'none'; //damos un atributo display:none que oculta el div
}
}



function ocultar(id){
if (document.getElementById){ //se obtiene el id
var el = document.getElementById(id); 
el.style.display = 'none'; //damos un atributo display:none que oculta el div
}
}




      var dientes = Array.from(document.querySelectorAll('.dientepieza'));
      var colores= Array.from(document.querySelectorAll('.dientecolor'));
      
      function agregarDientes(pieza,color){
        debugger;
        dientes.push(pieza);
        colores.push(color);
        console.log(dientes);
      }
     
   var tablea= $("#tablaa").DataTable({
                "searching": true,
                "info": false,
                "lengthChange": false,
                "oLanguage": {
                        "sSearch": "Buscar:",
                        "sLengthMenu": "Mostrar _MENU_ registros pagina",
                        "sZeroRecords": "Nothing found - sorry",
                        "sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
                        "sInfoEmpty": "Showing 0 to 0 of 0 records",
                        "sInfoFiltered": "(filtered from _MAX_ total records)",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                                    }
                      }
              });
   var tablen= $("#tablani").DataTable({
                "searching": true,
                "info": false,
                "lengthChange": false,
                "oLanguage": {
                        "sSearch": "Buscar:",
                        "sLengthMenu": "Mostrar _MENU_ registros pagina",
                        "sZeroRecords": "Nothing found - sorry",
                        "sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
                        "sInfoEmpty": "Showing 0 to 0 of 0 records",
                        "sInfoFiltered": "(filtered from _MAX_ total records)",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                                    }
                      }
              });

   $(document).on("click", ".guardaLesion", function() {
           
           var id = $("#diente0").val();
           var color = $("input[name='colores']:checked").val();
           var userID=$("#userID").val();
          debugger;
                  var idpaciente =  $("#idpaciente").val();
                  var idtipo = $("#idtipoLesion").val();
                  var nota = $("#notad").val();
                 var pieza={value: id};
                 var colorpieza={value: color};
                 if(id>0){
                  $.ajax({
                      url: 'actions/actionlesiones.php?opt=np',
                      data: 'idtipoLesion='+ idtipo +
                      '&idpaciente='+ idpaciente +
                      '&pieza='+ id +
                      '&notad='+ nota +
                      '&color='+ color+
                      '&user='+userID+
                      '&adulto='+ 0  ,
                      type: "POST",
                      success: function(json) {
                        console.log(json);
                       darcolor(json);
                       $("#diente0").val(0);
                        agregarDientes(pieza,colorpieza);
                        $("#nota").val('');
                        
                        //$('.closemodal').click();
                         //alert(json);
           
                        //return false;
                    }
                  });
                 }else{
                  new PNotify({
                                title: 'ocurrio un problema',
                                  text: 'seleccione un diente para continuar',
                                  type: 'warning'
                                });

                 }
                  
                  
             
      });
   
//diente niño////////////////////////////////////////////////////////
      var dientesn = Array.from(document.querySelectorAll('.dientepiezan'));
      var coloresn= Array.from(document.querySelectorAll('.dientecolorn'));
      function agregarDientesn(pieza,color){
        
        dientesn.push(pieza);
        coloresn.push(color);
        console.log(dientesn);
      }
      $("#btncolorn").click(function(){
        console.log($('#btncolorn').is(':disabled'));
      });
     
$(document).on("click", ".guardaLesionn", function() {
     
           var id = $("#dienten").val();
           var color = $("input[name='coloresn']:checked").val();
           var userID=$("#userID").val();
          debugger;
                  var idpaciente =  $("#idpaciente").val();
                  var idtipo = $("#idtipoLesionn").val();
                  var nota = $("#notan").val();
                  var pieza={value: id};
                 var colorpieza={value: color};
                 if(id>0){
                  $.ajax({
                      url: 'actions/actionlesiones.php?opt=np',
                      data: 'idtipoLesion='+ idtipo +
                      '&idpaciente='+ idpaciente +
                      '&pieza='+ id +
                      '&notad='+ nota +
                      '&color='+ color+
                      '&user='+userID+
                      '&adulto='+ 1  ,
                      type: "POST",
                      success: function(json) {
                        darcolorn(json);
                        agregarDientesn(pieza,colorpieza)
                        $("#notan").val('');
                        $("#dienten").val(0);
                    }
                  });
                 }else{
                  new PNotify({
                                title: 'ocurrio un problema',
                                  text: 'seleccione un diente para continuar',
                                  type: 'warning'
                                });

                 }
                 
                  
             
      });

      function eliminarLn(id,diente){
        //debugger;
        if(confirm("Atención! Va a proceder a eliminar este registro. Desea continuar?"))
                {
                  $('#tablani tbody').on( 'click', 'a', function () {
                   tablen.row( $(this).parents('tr') )
                    .remove()
                    .draw();
                  } );
   
                
                      $.ajax({
                      url: 'actions/actionlesiones.php?opt=e',
                      data: 'idlesion='+ id  ,
                      type: "POST",
                      success: function(json) {
                        quitarcolorn(diente);
                        $("#notan").val('');
                        $('#'+diente).data('maphilight', {"stroke":0, "alwaysOn": false});
                        $('#mapasn').maphilight();
                     
                    }
                  });
                }
      }

      function eliminarL(id,diente){
        //debugger;
        if(confirm("Atención! Va a proceder a eliminar este registro. Desea continuar?"))
                {
                  $('#tablani tbody').on( 'click', 'a', function () {
                   tablen.row( $(this).parents('tr') )
                    .remove()
                    .draw();
                  } );
   
                $('#tablaa tbody').on( 'click', 'a', function () {
                  tablea.row( $(this).parents('tr') )
                      .remove()
                      .draw();
                    } );
                  debugger;
                      $.ajax({
                      url: 'actions/actionlesiones.php?opt=e',
                      data: 'idlesion='+ id  ,
                      type: "POST",
                      success: function(json) {
                        quitarcolor(diente);
                        $("#notan").val('');
                        $('#'+diente).data('maphilight', {"stroke":0, "alwaysOn": false});
                        $('#mapas').maphilight();
                     
                    }
                  });
                }
      }
      function ocultardienten(){
        $("#idbotondienten").show('slow');
        $("#divdienten").hide("slow");
  
}

function verdienten(){
     rellenardienten();
     $("#idbotondienten").hide('slow');
      $("#divdienten").show('slow');
      
       //$('#mapasn').rwdImageMaps();
       $('#mapasn').maphilight();
       ocultardiente();
    }
    
    function rellenardienten(){
      
         for (let index = 0; index < dientesn.length; index++) {
          //dientes[index].setAttribute('visible', 'false');
          var obj2=dientesn[index].value;
          var color=coloresn[index].value;
          //console.log(dientes[index].value);
          $('#'+obj2).data('maphilight', {"stroke":0, "alwaysOn": true, "fillColor":color,  "fillOpacity":0.3});
         }
    }


    //////////////////////////////////
    $('.nino').each(function(){
        $('#'+this.id).click(function(){
          var dientes=Array.from(document.querySelectorAll('.nino'));
          var datos;
          
          var diente=this.id;
        for (let index = 0; index < dientes.length; index++) {
          const element = dientes[index];
          
           $('#'+element.id).data('maphilight', {"stroke":false});
        }
        rellenardienten();
       var data = $('#'+diente).mouseout().data('maphilight') || {};
        if(!data.alwaysOn){
                data.alwaysOn=!data.alwaysOn;
                data.stroke = !data.stroke;
                var datos=   $('#'+diente).data('maphilight', data).trigger('stroke.maphilight');
        
              }
              else{
                data.stroke = !data.stroke;
                var datos=   $('#'+diente).data('maphilight', data).trigger('stroke.maphilight');
        
              }
          console.log(data);
            $('#mapasn').maphilight();
            formularn($(this));
            $( "#btncolorn" ).removeClass( "disabled" );
        });
    });


        $(".colorninos").click(function(){
        var radio= Array.from(document.querySelectorAll(".colorninos"));
        debugger;
        for (let index = 0; index < radio.length; index++) {
          const element = radio[index];
                $(element).css('border','0px solid');
            }
        $(this).css('border','3px solid');
            
            console.log();
        });
    

    function formularn(obj){
        //rojo=FF0000 , verde=228B22,azul=0000FF,gris=708090
        $("#dienten").val(obj.attr("id"));
       // alert(obj.attr("id"));
        $('#formulario').show();
       
            //alert(obj.attr("id"));
           
      
        
    }
    
    function quitarcolorn(id){
      debugger;
      for( var i = 0; i < dientesn.length; i++){ 
          if ( dientesn[i].value ==id ) {
            dientesn.splice(i, 1); 
            coloresn.splice(i,1);
        }
        console.log(dientesn);
        }
        }
    function darcolorn(nuevoid){
        var id=$("#dienten").val();
      
        var descripcion=$("#idtipoLesionn option:selected").text();
        var color = $("input[name='coloresn']:checked").val();
       
        var nota=$("#notan").val();
        
        var obj2=$("#dienten").val();
            $('#'+obj2).data('maphilight', {"stroke":0, "alwaysOn": true, "fillColor":color, "fillOpacity":0.3});
            $('#mapasn').maphilight();
            //$( "#btncolorn" ).addClass( "disabled" );

            var colo=""+id+" <label class='dientecolor' value="+color+" style='background:#"+color+";width:20px;height:10px;border-radius:15px;'></label>";
          var link="<a class='dientepieza' value="+id+" onclick='javascript:eliminarLn("+nuevoid+","+id+")' ><i style='font-size:18px;' class='fa fa-trash-o' aria-hidden='true'></i></a>";
          tablen.row.add( [
            colo,
            descripcion,
            nota,
            
            link
           
        ] ).draw( false );
          
    }

//diente adulto///////////////////////////////////////////////////////
$(".coloradul").click(function(){
        var radio= Array.from(document.querySelectorAll(".coloradul"));
        debugger;
        for (let index = 0; index < radio.length; index++) {
          const element = radio[index];
                $(element).css('border','0px solid');
            }
        $(this).css('border','3px solid');
            
            console.log();
        });
   
function ocultardiente(){
  $("#idbotondiente").show('slow');
  $("#divdiente").hide("slow");
 
}
    function verdiente(){
      $("#idbotondiente").hide('slow');
      $("#divdiente").show("slow");
      rellenardiente();
      $('#mapas').maphilight({ fillColor: 'ffffff'});
       //$('#mapas').rwdImageMaps();
       ocultardienten();
    }
   
      
    
      
      $('.adul').each(function(){
        $('#'+this.id).click(function(){
          console.log(this);
          var dientes=Array.from(document.querySelectorAll('.adul'));
          var datos;
          
          var diente=this.id;
        for (let index = 0; index < dientes.length; index++) {
          const element = dientes[index];
          
           $('#'+element.id).data('maphilight', {"stroke":false});
        }
        rellenardiente();
       var data = $('#'+diente).mouseout().data('maphilight') || {};
        if(!data.alwaysOn){
                data.alwaysOn=!data.alwaysOn;
                data.stroke = !data.stroke;
                var datos=   $('#'+diente).data('maphilight', data).trigger('stroke.maphilight');
        
              }
              else{
                data.stroke = !data.stroke;
                var datos=   $('#'+diente).data('maphilight', data).trigger('stroke.maphilight');
        
              }
          
            $('#mapas').maphilight();
            formular($(this));
            $( "#btncolor" ).removeClass( "disabled" );
        });
    });
  

    
    function quitarcolor(id){
      debugger;
      for( var i = 0; i < dientes.length; i++){ 
          if ( dientes[i].value ==id ) {
            dientes.splice(i, 1); 
            colores.splice(i,1);
        }
        console.log(dientes);
          }
    }
    function rellenardiente(){
      //debugger;
         for (let index = 0; index < dientes.length; index++) { 
          var obj2=dientes[index].value;
          var color=colores[index].value;
          $('#'+obj2).data('maphilight', {"stroke":false, "alwaysOn": true, "fillColor":color,  "fillOpacity":0.3});
         }
    }

   

    function formular(obj){
        $("#diente0").val(obj.attr("id"));
    }
    

    function darcolor(nuevoid){
      debugger;
        var id=$("#diente0").val();
       
        
        var descripcion=$("#idtipoLesion option:selected").text();
        var color = $("input[name='colores']:checked").val();
        var nota=$("#notad").val();
     
        var obj2=$("#diente0").val();
            $('#'+obj2).data('maphilight', {"stroke":0, "alwaysOn": true, "fillColor":color, "fillOpacity":0.3});
            $('#mapas').maphilight();
            //$( "#btncolor" ).addClass( "disabled" );
          var colo=""+id+" <label style='background:#"+color+";width:20px;height:10px;border-radius:15px;'></label><input type='hidden' value="+color+" class='dientecolor'> ";
          var link="<a onclick='javascript:eliminarL("+nuevoid+","+id+")'><i style='font-size:18px;' class='fa fa-trash-o' aria-hidden='true'></i></a><input type='hidden' value="+id+" class='dientepieza'>";
          tablea.row.add( [
         
            colo,
            descripcion,
            nota,
            link
           
        ] ).draw( false );



    }
    function ver(id){
        var mostrar= id;
        alert(mostrar);
    }
 
////////////////////////////////////////////////////////////////pagos

var tablapagos= $("#tablapagos").DataTable({
  "searching": true,
                "info": false,
                "lengthChange": true,
                "oLanguage": {
                        "sSearch": "Buscar:",
                        "sLengthMenu": "Mostrar _MENU_ registros pagina",
                        "sZeroRecords": "Nothing found - sorry",
                        "sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
                        "sInfoEmpty": "Showing 0 to 0 of 0 records",
                        "sInfoFiltered": "(filtered from _MAX_ total records)",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                                    }
                      }
});
var tablacreditos= $("#tablacredito").DataTable({
  "searching": true,
                "info": false,
                "lengthChange": true,
                "oLanguage": {
                        "sSearch": "Buscar:",
                        "sLengthMenu": "Mostrar _MENU_ registros pagina",
                        "sZeroRecords": "Nothing found - sorry",
                        "sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
                        "sInfoEmpty": "Showing 0 to 0 of 0 records",
                        "sInfoFiltered": "(filtered from _MAX_ total records)",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                                    }
                      }
});
function verpago(tipo){
  if(tipo==2){

    $("#formularioPagos").hide('slow');
    $("#formularioCreditos").hide('slow');
    $("#forpagocreditos").hide('slow');
   $( "#botonpago" ).removeClass( "btn-default" ).addClass( "btn-success" );
  }
}

function verformulario(tipo){
  if(tipo==1){
    $("#formularioPagos").show('slow');
    $("#forcredito").hide();
    $("#actualizarFormpago").hide();
    $("#guadarFormPago").show();
    $("#forpagocreditos").hide('slow');
    $("#idtipopago").val(1);
    $("#tratamientopago").val('');
    $("#monto").val(0);
    $("#notapago").val('');
    $("#modaldepago").text('Nuevo');
    $("#monto").removeAttr("disabled");
  }else{
    $("#forpagocreditos").hide('slow');
    $("#forcredito").show();
    $("#formularioPagos").show('slow');
    $("#actualizarFormpago").hide();
    $("#guadarFormPago").show();
    $("#idtipopago").val(2);
    $("#tratamientopago").val('');
    $("#monto").val(0);
    $("#notapago").val('');
    $("#modaldepago").text('Nuevo');
    $("#monto").removeAttr("disabled");
  }
}

function verfomcreditopago(id){
  var saldo=$("#d"+id).text();
  $('#idpago').val(id);
  $("#idsaldo").val(saldo);
  $("#modalpagocredito").text('Saldo: '+saldo);
  

  $("#forpagocreditos").show('slow');
  
  $("#formularioPagos").hide('slow');
  $("#formularioCreditos").hide('slow');
 
}
function vertablacreditospago(id){
  $('#idpago').val(id);
  $("#forpagocreditos").hide('slow');
  $("#tablacreditopagos").show('show');
  $("#formularioPagos").hide('slow');
 $("#tablaCredito").empty();
 
 var tabla= $("#tablaCredito");
 debugger;
  var tr;
  var nro=1;
  $(".loaderp").fadeIn("slow");
                    $.ajax({
                      url: 'actions/actionpagocredito.php?opt=todos',
                      data: 'pago='+id ,
                      type: "POST",
                      dataType:'json',
                      success: function(data) {
                        $(".loaderp").fadeOut("slow");
                        if(data!=400){
                          $.each(data, function(i, item) {
                      
                            $("#nombretratamiento").text(item.tratamiento);
                          $("#saldo").text("Saldo: "+item.debe);
                          $("#pagado").text('Se pagó: '+item.pagado);
                          if(item.credito1==1){
                            tr="<tr><td>"+nro+"</td>"+
                          "<td>"+convertirfecha(item.fecha)+"</td>"+
                          "<td>"+item.monto+"</td>"+
                          "<td>"+item.observaciones+"</td>"+  
                          "<td><a style='font-size:20px;' href='javascript:eliminarCredito("+item.idpagoTraramiento+")'><i class='fa fa-trash' aria-hidden='true'></i></a></td>"
                                     
                          "</tr>";
                          }else{
                            tr="<tr><td>"+nro+"</td>"+
                          "<td>"+convertirfecha(item.fecha)+"</td>"+
                          "<td>"+item.monto+"</td>"+
                          "<td>"+item.observaciones+"</td>"+

                            "<td><a style='font-size:20px;' href='javascript:eliminarCredito("+item.idpagoTraramiento+")'><i class='fa fa-trash' aria-hidden='true'></i></a></td>"
                          
                          
                            "</tr>";
                            }
                            
                            $(tabla).append(tr);
                            nro++;
                            
                 
                            });
                                  }else{
                                    $("#cerrarModalcredito").click();
                                    new PNotify({
                                title: 'Sin datos!!!',
                                  text: 'Agregue un pago',
                                  type: 'warning'
                                });
                                  }
                       
                        console.log(data);
                       
                    }
                  });
 
}
function eliminarCredito(id){
  debugger;
  var idpaciente = $('#idpaciente').val();
                if(confirm("Atención! Va a proceder a eliminar este registro. Desea continuar?"))
                {
                  $.ajax({
                      url: 'actions/actionpagocredito.php?opt=eli',
                      data: 'id='+id ,
                      type: "POST",
                    
                      success: function(data) {
                              if(data == 0){
                                new PNotify({
                                title: 'Datos Eliminados',
                                  text: 'Todos los datos fueron guardados. Puede continuar.!',
                                  type: 'info'
                                });
                                window.setTimeout("location.reload(true);",2500);
                          }
                          else{
                                new PNotify({
                                    title: 'Error en formulario',
                                    text: 'No se puedieron guardar los datos, intente de nuevo.',
                                    type: 'error'
                                });
                                  window.setTimeout("location.reload(true);",2500);
                          }                     
                    }
                  });
                }
}

function eliminarCreditoTotal(id){
  var idpaciente = $('#idpaciente').val();
                if(confirm("Atención! Va a proceder a eliminar este registro. Desea continuar?"))
                {
                  $(".loaderp").fadeIn("slow");
                  $.ajax({
                      url: 'actions/actionpagocredito.php?opt=eliminarTotal',
                      data: 'id='+id ,
                      type: "POST",
                    
                      success: function(data) {
                              if(data == 0){
                                new PNotify({
                                title: 'Datos Eliminados',
                                  text: 'Todos los datos fueron guardados. Puede continuar.!',
                                  type: 'info'
                                });
                                window.setTimeout("location.reload(true);",2500);
                          }
                          else{
                                new PNotify({
                                    title: 'Error en formulario',
                                    text: 'No se puedieron guardar los datos, intente de nuevo.',
                                    type: 'error'
                                });
                                  window.setTimeout("location.reload(true);",2500);
                          }                     
                    }
                  });
                }
}

function cerrarpagos(){
  $("#formularioPagos").hide('slow');
  $("#forpagocreditos").hide('slow');

  
}
function agregarcredito(){
                  var userID=$("#userID").val();
                  var fecha=$("#fechapc").val();
                  var idpaciente = $('#idpaciente').val();
                  var monto =  $("#montoc").val();
                  var pago = $("#idpago").val();
                  var nota = $("#notapagoc").val();
               
                  var sald=$("#idsaldo").val();
                  var subtotal=sald-monto;
                 var cambio=Math.abs(subtotal);
                 if(subtotal<=0){
                  monto=monto-Math.abs(subtotal);
                  subtotal=0;
                  
                   $("#idsaldo").val('0');
                   $("#abrirformulariopagos"+pago).hide();
                   new PNotify({
                                title: 'Devolver',
                                  text: cambio,
                                  type: 'success'
                                });
                 }
                 $('#idsaldo').val(subtotal);
                 $(".loaderp").fadeIn("slow");
                  $.ajax({
                      url: 'actions/actionpagocredito.php?opt=np',
                      data: 'usuario='+userID+
                            '&fecha='+fecha+ 
                            '&paciente='+idpaciente+
                            '&monto='+monto+
                            '&nota='+nota+
                            '&credito1='+0+
                            '&idpago='+pago
                            ,
                      type: "POST",
                      success: function(json) {
                        $(".loaderp").fadeOut("slow");
                        $("#d"+pago).text(subtotal);
                        //$("#idtipoLesion").val('0');
                        $("#formularioPagos").hide('slow');
                        $("#forpagocreditos").hide('slow');
                        $("#notapagoc").val('');
                        $("#montoc").val(0);
                        $("#notan").val('');
                        new PNotify({
                                title: 'Datos almacenando',
                                  text: 'Los datos fueron guardados correctamente',
                                  type: 'info'
                                });
                        console.log(json);
                       
                    }
                  });
                 
                
                     
}

////nuevo pago
function pagocredito0(id,fecha,monto,tipopago,tratamiento){
  debugger;
  if(tipopago==1){
          tipo="efectivo";
        }else{
          tipo="tarjeta";
        }
        var usuario=$("#nombreusuario").val();
                  var userID=$("#userID").val();
                  var credito1=1;
                  var idpaciente = $('#idpaciente').val();
                  var pago=(id);
                  var montoc =  $("#montocr").val();
                  var subtotal=monto-montoc;
                  var debe="<span id='d"+id+"'>"+subtotal+"</span>";
                  var nota = $("#notapago").val();
                  var opcion="<a style='font-size:18px;' onclick='javascript:verfomcreditopago("+id+")'"+
                  " data-toggle='modal' data-target='#modalFormularioCredito'"+
                   "title='Agregar pagos'><i class='fa fa-plus' aria-hidden='true'></i></a>"+
                   "<a style='font-size:18px;' title='Ver pagos' data-toggle='modal' data-target='#tablapagoscredito' onclick='javascript:vertablacreditospago("+id+")' ><i class='fa fa-search' aria-hidden='true'></i></a>"+
                   "<a style='font-size:18px;' title='Imprimir' href='pagosImprimir.php?pago="+pago+"'><i class='fa fa-print'></i></a>"+
                   
                   '<a style="font-size: 18px;" data-toggle="modal" data-target="#modalformulariopago" onclick="javascript:actualizarPagoform('+id+',2)"> <i class="fa fa-pencil" aria-hidden="true"></i></a>'+
                                          
                   "<a style='font-size:18px;' title='Eliminar' href='javascript:eliminarCreditoTotal("+pago+")'><i class='fa fa-trash-o'></i></a>"
                      
                  tablacreditos.row.add( [ 
                    tratamiento,
                    fecha,
                    monto,
                    debe,
                    tipo,
                    nota,
                    usuario,
                    opcion
                    ] ).draw( false );
                    //$(".loaderp").fadeIn("slow");
                    if($("#montocr").val()>0){
                   $.ajax({
                      url: 'actions/actionpagocredito.php?opt=np',
                      data: 'usuario='+userID+
                            '&fecha='+fecha+ 
                            '&paciente='+idpaciente+
                            '&monto='+montoc+
                            '&nota='+nota+
                            '&credito1='+1+
                            '&idpago='+pago
                            ,
                      type: "POST",
                      success: function(json) {
                        $(".loaderp").fadeOut("slow");
                        new PNotify({
                                title: 'Datos almacenando',
                                  text: 'Los datos fueron guardados correctamente',
                                  type: 'info'
                                });
                        $("#notan").val('');
                        console.log(json);
                       
                       }
                      }); 
                    }  
}

              $(document).on("click", ".guardapago", function() {
                var seguro ="";
                  var userID=$("#userID").val();
                  var fecha=$("#fechap").val();
                  var idpaciente = $('#idpaciente').val();
                  var monto =  $("#monto").val();
                  var formapago = $("#formapagoid").val();
                  var formaseguro = $("#formaseguroid").val();
                  var nota = $("#notapago").val();
                  var tratamiento=$("#tratamientopago").val();
                  var tipopago=$("#idtipopago").val();
                  $.ajax({
                      type: 'POST',
                      url: 'actions/actionUsuario.php',
                      data: {id:formaseguro, opt: 'obtenerseguro'},
                            success: function(data) {   
                             if (data != 0){   
                             seguro=data;
                              }
                            }	
                     }); 


                  if(validar()){


                    $(".loaderp").fadeIn("slow");
                    $.ajax({
                      url: 'actions/actionpagos.php?opt=np',
                      data: 'usuario='+userID+'&fecha='+fecha+ '&paciente='+idpaciente+
                            '&monto='+monto+'&formapago='+formapago+'&nota='+nota+
                            '&tratamiento='+tratamiento+
                            '&tipopago='+tipopago +
                            '&formaseguro='+formaseguro ,
                      type: "POST",
                     dataType:'json',
                      success: function(data) {
                        $(".loaderp").fadeOut("slow");
                        $("#formularioPagos").hide('slow');
                        if(tipopago==1){
                          new PNotify({
                                title: 'Almacenando Datos',
                                  text: 'Todos los datos fueron almacenados correctamente',
                                  type: 'info'
                                });


                       

                          llenartablapago(data,tratamiento,fecha,monto,formapago,nota,seguro)
                        $("#notapago").val('');
                        }
                        $("#tratamientopago").val('');
                        $("#monto").val(0);
                       
                                             
                    }
                  });
            


                  
                  }else
                  {
                    new PNotify({
                                title: 'Faltan Datos',
                                  text: 'No puede almacenar',
                                  type: 'error'
                                });
                  }
                  
                 
                  
             
      });


    function llenartablapago(data,tratamiento,fecha,monto,tipopago,nota,formaseguro){
        var fechampagos=convertirfecha(fecha);
        var tipo;
        var usuario=$("#nombreusuario").val();
        var opcion="<a style='font-size:18px;' onclick='javascript:eliminarPago("+data+")'><i class='fa fa-trash-o' aria-hidden='true'></i></a>"+
        "<a style='font-size: 18px;' data-toggle='modal' data-target='#modalformulariopago' onclick='javascript:actualizarPagoform("+data+",1)'> <i class='fa fa-pencil' aria-hidden='true'></i></a>"+
        "<a style='font-size:18px;' title='Imprimir' href='pagosImprimir.php?pago="+data+"'><i class='fa fa-print'></i></a>"          

        if(tipopago==1){
          tipo="Efectivo";
        }else{
          tipo="Tarjeta";
        }


if(formaseguro==1){
 formaseguro="SIN SEGURO";
}



              tablapagos.row.add( [ 
                    tratamiento,
                    fechampagos,
                    monto,
                    tipo,
                    nota,
                    formaseguro,
                    opcion
                    ] ).draw( false );
      }

  function eliminarPago(id){
                var idpaciente = $('#idpaciente').val();
                if(confirm("Atención! Va a proceder a eliminar este registro. Desea continuar?"))
                {
                  $(".loaderp").fadeIn("slow");
                  $.ajax({
                      url: 'actions/actionpagos.php?opt=eli',
                      data: 'id='+id ,
                      type: "POST",
                    
                      success: function(data) {
                              if(data == 0){
                                new PNotify({
                                title: 'Datos Eliminados',
                                  text: 'Todos los datos fueron guardados. Puede continuar.!',
                                  type: 'info'
                                });
                                window.setTimeout("location.reload(true);",2500);
                          }
                          else{
                                new PNotify({
                                    title: 'Error en formulario',
                                    text: 'No se puedieron guardar los datos, intente de nuevo.',
                                    type: 'error'
                                });
                                  window.setTimeout("location.reload(true);",2500);
                          }                     
                    }
                  });
                }
  }
function actualizarPagoform(id,tipodepago){
  $("#idpago").val(id);
  $("#formularioPagos").show();
  $("#idtipopago").val(tipodepago);
  $("#modaldepago").text('Actualizar');
  $("#actualizarFormpago").show();
  $("#guadarFormPago").hide();
  if(tipodepago==1){
    $("#monto").removeAttr("disabled");
  }
                $.ajax({
                      url: 'actions/actionpagos.php?opt=uno',
                      data: 'idpago='+id ,
                      type: "POST",
                     dataType:'json',
                      success: function(data) {                     
                        $.each(data, function(i, item) {
                          $("#monto").val(item.monto);
                          $("#notapago").val(item.observaciones);
                          $("#tratamientopago").val(item.tratamiento);
                        });                     
                    }
                  });

}
function actualizarPago(){
                  var id=$("#idpago").val();
                  var userID=$("#userID").val();
                  var fecha=$("#fechap").val();
                  var idpaciente = $('#idpaciente').val();
                  var monto =  $("#monto").val();
                  var formapago = $("#formapagoid").val();
                  var nota = $("#notapago").val();
                  var tratamiento=$("#tratamientopago").val();
                  var tipopago=$("#idtipopago").val();
                  $(".loaderp").fadeIn("slow");
                  $.ajax({
                      url: 'actions/actionpagos.php?opt=up',
                      data: 'usuario='+userID+'&fecha='+fecha+ '&paciente='+idpaciente+
                            '&monto='+monto+'&formapago='+formapago+'&nota='+nota+
                            '&tratamiento='+tratamiento+
                            '&tipopago='+tipopago+
                            '&idpago='+id ,
                      type: "POST",
                     dataType:'json',
                      success: function(data) {
                        
                        if(data != 0){
                          
                                new PNotify({
                                title: 'Datos Actualizados',
                                  text: 'Todos los datos fueron guardados. Puede continuar.!',
                                  type: 'info'
                                });
                                window.setTimeout("location.reload(true);",1500);
                          }
                          else{
                                new PNotify({
                                    title: 'Error en formulario',
                                    text: 'No se puedieron guardar los datos, intente de nuevo.',
                                    type: 'error'
                                });
                                  window.setTimeout("location.reload(true);",1500);
                          }
                       
                                             
                    }
                  });
}
//////////////////Editar
function ireditar(id){
				$("#idpacienteditar").val(id);
				$("#modificarpaciente").val('m');
				$("#formularioeditar").submit();
				//window.setTimeout("document.location.href='pacientenuevo.php?opt=m&id="+id+"';",500);
			}
///////////////////////////

function imprimir(){

  var printContents = document.getElementById('tablapagoscredito').innerHTML;
			var originalContents = document.body.innerHTML;
			document.body.innerHTML = printContents;
			window.print();
			document.body.innerHTML = originalContents;
}
function convertirfecha(fecha){
  var today = new Date(fecha);
var dd = today.getDate()+1;

var mm = today.getMonth()+1; 
var yyyy = today.getFullYear();
    if(dd<10) 
    {
        dd='0'+dd;
    } 

    if(mm<10) 
    {
        mm='0'+mm;
    } 
return today = dd+'-'+mm+'-'+yyyy;
}

function validar(){
var paso=true;
var tratamiento=$("#tratamientopago").val();
var monto=$("#monto").val();

if( tratamiento == null || tratamiento.length == 0 || /^\s+$/.test(tratamiento) ){
  paso=false;

}else{
  if(isNaN(monto)){
    paso=false;
  }else{
   paso= true;
  }
}
  return paso;
}


function validarrr(){
  var porId=document.getElementById("tratamientopago1").value;
 document.getElementById("tratamientopago2").value = porId;

var paso=false;
var tratamiento=$("#tratamientopago2").val();
var monto=$("#monto1").val();

if( ( tratamiento.length > 0 ) && (monto!=0) ){
  paso=true;
}else{
 paso =false;
}
  return paso;
}
//////////////////////////////////citas---modal
$(".labelcolo").click(function(){
        var radio= Array.from(document.querySelectorAll(".labelcolo"));
        debugger;
        for (let index = 0; index < radio.length; index++) {
          const element = radio[index];
                $(element).css('border','0px solid');
            }
        $(this).css('border','3px solid');
            
            console.log();
        });

        $(".antosubmit").on("click", function() {

                var title = $("#pacientec").val();
                if (title) {
                  var idpaciente =  $("#idpacientec").val();
                   var idusuario =  $("#idusuario").val();
                  if(idtipocita==4){
                    idpaciente=1;
                   
                   }
                   
                   var idtipocita = $("#idtipocita").val();
                   var detalle = $("#descrc").val();
                   var colores='#'+$("input[name='coloresc']:checked").val();
                   var start = $('#fechacita').val() + " " + $('#horac').val()+":"+$('#minutoc').val();
                   //var hora = $('#horac').val().charAt(0) + $('#horac').val().charAt(1);
                   ///var min = $('#horac').val().charAt(3) + $('#horac').val().charAt(4);
                   var hora = $('#horac').val().charAt(0) + $('#horac').val().charAt(1);
                   var min = $('#minutoc').val().charAt(0) + $('#minutoc').val().charAt(1);

                   var mediahora=parseInt(min)+30;
                   if(mediahora>=60){
                    hora = parseInt(hora) + 1;
                    min= mediahora - 60;
                   }else{
                    min=parseInt(min)+30;
                   }

                   //hora = parseInt(hora) + 1;
                   var end = $('#fechacita').val() + " " +  hora + ":" + min ;
                   var url = '';
                   $.ajax({
                       url: 'actions/actioncita.php?opt=ja',
                       data: 'title='+ title+
                       '&start='+ start +
                       '&end='+ end +
                       '&url='+ url +
                       '&tipocita=' + idtipocita +
                       '&idpaciente=' + idpaciente +
                       '&detalle=' + detalle + 
                       '&idusuario=' + idusuario + 
                       '&color='+colores+
                       '&allDay='  ,
                       type: "POST",
                       success: function(json) {
                        new PNotify({
                                title: 'Se agrego correctamente',
                                  text: 'todos los cambios fueron guardados!',
                                  type: 'info'
                                });
                                $('.antoclose').click();
                                window.setTimeout("location.reload(true);",2500);

                     }
                   });
                  
               }else{

                  new PNotify({
                                  title: 'Falta!!!',
                                    text: 'Agregar un titulo',
                                    type: 'error'
                                  });
                  }
             

               return false;

           });





















//ver notas
function vermasNotas(paciente){
              var i=1;
              var  nro=$("#nrocitanotas").val();
                $.ajax({
                       url: 'actions/actionpacienteNotas.php?opt=vermas',
                       data: 'nronotas='+ nro+
                       '&paciente='+paciente,
                       
                       type: "POST",
                       dataType:"json",
                       success: function(json) {
                        $.each(json, function(i, item) {
       agregarlista(item.observaciones,item.fecha,item.title,item.idestado,item.maxSup,item.maxInf,item.id);
                        i++;
                        });
                        console.log(json);
                        nro=parseInt(nro)+parseInt(i) ;
                        if(nro>=10){
                          $("#botonVermasnotas").hide();
                        }
                        $("#nrocitanotas").val(nro);
                     }
                     
                     
                   });
           }

function funcionnombremes(mes){
  var nombre='';
 if(mes==1){
    nombre='Ene';
 }
 if(mes==2){
   nombre='Feb';
  }
  if(mes==3){
   nombre='Mar';
  }
  if(mes==4){
   nombre='Abr';
  }
  if(mes==5){
   nombre='May';
  }
  if(mes==6){
   nombre='Jun';
  }
  if(mes==7){
   nombre='Jul';
  }
  if(mes==8){
   nombre='Ago';
  }
  if(mes==9){
   nombre='Sep';
  }
  if(mes==10){
   nombre='Oct';
  }
  if(mes==11){
   nombre='Nov';
  }
  if(mes==12){
   nombre='Dic';
  }
  return nombre;
}

function agregarlista(observaciones,fecha,title,estado,maxsup,maxinf,id){
             var date= new Date(fecha);
          var dia=  date.getDate();
            
           var mes=  date.getMonth()+1;
          nombremes= funcionnombremes(mes);
      if(observaciones=="nota"){
        var lis='<div style="height:30px;"></div>'+
         '<div class="row">'+
                '<div class="col-md-2">'+
                      '<div class="fechaHistorial">'+
                          '<h2>'+dia+'</h2>'+
                        '<h2>'+nombremes+'</h2>'+
                      '</div>'+
                '</div>'+  
                  '<div class="col-md-8" style="border: 2px solid;border-radius: 15px;border-color: #F3F2F2">'+
                    '<div class="card">'+
                      '<div class="card-body">'+
                        '<h5 class="card-title">Notas</h5>'+
                          '<p class="card-text">Observación:'+title+' ...</p>'+
                      '</div>'+
                    '</div>'+
                  '</div>'+             
            '</div>'
     
            $("#listaNotascitas").append(lis);
      }else{
        var tipoestado="";
                   
                   if(estado==0){
                     tipoestado='<span class="label label-danger cita" >Cancelada</span>';
                     
                 }else{
                     if(estado==1){
                       tipoestado='<span class="label label-warning cita" >Programada</span>';
                     }else{
                       tipoestado='<span class="label label-success cita" >Finalizada</span>';
                     }
                   }
             var lis='<div style="height:30px;"></div>'+
             '<div class="row">'+
                    '<div class="col-md-2">'+
                                '<div class="fechaHistorial">'+
                                    '<h2>'+dia+'</h2>'+
                                    '<h2>'+nombremes+'</h2>'+
                                '</div>'+
                    '</div>'+
                    '<div class="col-md-8" style="border: 2px solid;border-radius: 15px;border-color: #F3F2F2">'+
                      '<div class="card">'+
                        '<div class="card-body">'+
                          '<h5 class="card-title">Cita '+title+' '+tipoestado+'</h5>'+
                            '<p class="card-text">diagnostico: '+diagnostico+'...</p>'+
                            '<p class="card-text">Maxilar inferior: '+maxinf+'...</p>'+
                          '<div style="text-align: right;">'+
                            '<a class="cita" id="cita'+id+'" data-toggle="modal" data-target="#ModalCita" data-id="'+id+'"  data-ms="'+maxsup+'" data-mi="'+maxinf+'" data-obs="'+observaciones+'">Ver</a>'+
                          '</div>'+
                        
                        '</div>'+
                      '</div>'+
                    '</div>'+
                  '</div>';
        $("#listaNotascitas").append(lis);
      }
           }
</script>
</body>

</html>
