<!DOCTYPE html>
<html lang="en">
<head>
<?php include "headerm.php";?>
<title>Medicapp </title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/logo.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<!--<script src="js/sigin.js"></script>-->
<!--===============================================================================================-->

<style type="text/css">	
	#top-image {
	background:url('images/fondoclinica.jpg') -25px -50px;
	position:fixed ;
	top:0;
	width:100%;
	z-index:0;
		height:100%;
		background-size: calc(100% + 50px);
	}
</style>

<script type="text/javascript">
	$(document).ready(function() {
		var movementStrength = 25;
		var height = movementStrength / $(window).height();
		var width = movementStrength / $(window).width();
		$("#top-image").mousemove(function(e){
							var pageX = e.pageX - ($(window).width() / 2);
							var pageY = e.pageY - ($(window).height() / 2);
							var newvalueX = width * pageX * -1 - 25;
							var newvalueY = height * pageY * -1 - 50;
							$('#top-image').css("background-position", newvalueX+"px     "+newvalueY+"px");
		});
	});

function SHA1(msg) {
	  function rotate_left(n,s) {
		var t4 = ( n<<s ) | (n>>>(32-s));
		return t4;
	  };
	  function lsb_hex(val) {
		var str="";
		var i;
		var vh;
		var vl;
		for( i=0; i<=6; i+=2 ) {
		  vh = (val>>>(i*4+4))&0x0f;
		  vl = (val>>>(i*4))&0x0f;
		  str += vh.toString(16) + vl.toString(16);
		}
		return str;
	  };
	  function cvt_hex(val) {
		var str="";
		var i;
		var v;
		for( i=7; i>=0; i-- ) {
		  v = (val>>>(i*4))&0x0f;
		  str += v.toString(16);
		}
		return str;
	  };
	  function Utf8Encode(string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";
		for (var n = 0; n < string.length; n++) {
		  var c = string.charCodeAt(n);
		  if (c < 128) {
			utftext += String.fromCharCode(c);
		  }
		  else if((c > 127) && (c < 2048)) {
			utftext += String.fromCharCode((c >> 6) | 192);
			utftext += String.fromCharCode((c & 63) | 128);
		  }
		  else {
			utftext += String.fromCharCode((c >> 12) | 224);
			utftext += String.fromCharCode(((c >> 6) & 63) | 128);
			utftext += String.fromCharCode((c & 63) | 128);
		  }
		}
		return utftext;
	  };
	  var blockstart;
	  var i, j;
	  var W = new Array(80);
	  var H0 = 0x67452301;
	  var H1 = 0xEFCDAB89;
	  var H2 = 0x98BADCFE;
	  var H3 = 0x10325476;
	  var H4 = 0xC3D2E1F0;
	  var A, B, C, D, E;
	  var temp;
	  msg = Utf8Encode(msg);
	  var msg_len = msg.length;
	  var word_array = new Array();
	  for( i=0; i<msg_len-3; i+=4 ) {
		j = msg.charCodeAt(i)<<24 | msg.charCodeAt(i+1)<<16 |
		msg.charCodeAt(i+2)<<8 | msg.charCodeAt(i+3);
		word_array.push( j );
	  }
	  switch( msg_len % 4 ) {
		case 0:
		  i = 0x080000000;
		break;
		case 1:
		  i = msg.charCodeAt(msg_len-1)<<24 | 0x0800000;
		break;
		case 2:
		  i = msg.charCodeAt(msg_len-2)<<24 | msg.charCodeAt(msg_len-1)<<16 | 0x08000;
		break;
		case 3:
		  i = msg.charCodeAt(msg_len-3)<<24 | msg.charCodeAt(msg_len-2)<<16 | msg.charCodeAt(msg_len-1)<<8  | 0x80;
		break;
	  }
	  word_array.push( i );
	  while( (word_array.length % 16) != 14 ) word_array.push( 0 );
	  word_array.push( msg_len>>>29 );
	  word_array.push( (msg_len<<3)&0x0ffffffff );
	  for ( blockstart=0; blockstart<word_array.length; blockstart+=16 ) {
		for( i=0; i<16; i++ ) W[i] = word_array[blockstart+i];
		for( i=16; i<=79; i++ ) W[i] = rotate_left(W[i-3] ^ W[i-8] ^ W[i-14] ^ W[i-16], 1);
		A = H0;
		B = H1;
		C = H2;
		D = H3;
		E = H4;
		for( i= 0; i<=19; i++ ) {
		  temp = (rotate_left(A,5) + ((B&C) | (~B&D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;
		  E = D;
		  D = C;
		  C = rotate_left(B,30);
		  B = A;
		  A = temp;
		}
		for( i=20; i<=39; i++ ) {
		  temp = (rotate_left(A,5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;
		  E = D;
		  D = C;
		  C = rotate_left(B,30);
		  B = A;
		  A = temp;
		}
		for( i=40; i<=59; i++ ) {
		  temp = (rotate_left(A,5) + ((B&C) | (B&D) | (C&D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;
		  E = D;
		  D = C;
		  C = rotate_left(B,30);
		  B = A;
		  A = temp;
		}
		for( i=60; i<=79; i++ ) {
		  temp = (rotate_left(A,5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;
		  E = D;
		  D = C;
		  C = rotate_left(B,30);
		  B = A;
		  A = temp;
		}
		H0 = (H0 + A) & 0x0ffffffff;
		H1 = (H1 + B) & 0x0ffffffff;
		H2 = (H2 + C) & 0x0ffffffff;
		H3 = (H3 + D) & 0x0ffffffff;
		H4 = (H4 + E) & 0x0ffffffff;
	  }
	  var temp = cvt_hex(H0) + cvt_hex(H1) + cvt_hex(H2) + cvt_hex(H3) + cvt_hex(H4);

	  return temp.toLowerCase();
	}


function Login(){
	var tienepermiso;
		if($('#correo').val() == "" || $('#clave').val() == ""){
			new PNotify({
                 title: 'Necesita llenar todos los datos!',
                text: 'Falta de datos!'
             });
		} else {
			var correo = $("#correo").val();
			var pass  = SHA1($("#clave").val());
			
			var lo1=$("#clave").val().length;
			if( lo1>=6){


				$.ajax({
				type: 'POST',
				url: 'actions/actionL.php',
				data: { user:correo, pass: pass, opt: 'login'},
				success: function(data) {
					data = data.replace(/^\s*|\s*$/g,"");
					if (data == 'done'){
				

			 new PNotify({
          title: 'Datos Ingresados',
          text: 'Acceso correcto espere por favor.',
          type: 'info'
          
      })
						
						setTimeout( "window.location.href='agenda.php'", 2000);
					} else  {

						if(data =="inactivo"){
							new PNotify({
                 title: 'Datos en espera',
                text: 'Su Usuario está en proceso de verificación, espere a que los administradores del sistema se comuniquen con usted.',
                type: 'warning'
             });

						}else{
							new PNotify({
                 title: 'Datos Incorrectos',
                text: 'Usuario o Contraseña con incorrectos!',
                type: 'error'
             });

						}
				
					}
				}	
			});




			}else{
				new PNotify({
                   title: 'Señor usuario',
                   text: 'Verifique la clave introducida tiene que ser mayor o igual a 6 dígitos!',
                   type: 'warning'
                 });
			}

		}
	}

</script>


</head>

<body>
	<div class="limiter">
		<div id="top-image" class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form">

					<span class="login100-form-title">
						<img src="images/logo.png" style="border: 0px; width:60%; border-radius: 50%; height:60%;">
					</span>
                       <br>
					<div class="wrap-input100 validate-input" >
			    		<span class="btn-show-pass">
							<i class="zmdi zmdi-account"></i>
						</span>
						<input class="input100" type="text" name="correo" id="correo">
						<span class="focus-input100" data-placeholder="juanperez@mail.com"></span>
					</div>

	

					<div class="wrap-input100 validate-input" >
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type="password" name="clave" id="clave">
						<span class="focus-input100" data-placeholder="clave"></span>
					</div>


					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<a class="login100-form-btn" href="#" onclick="Login();">Ingresar</a>
						</div>
					</div>

					<div>
              <a class="container-login100-form-btn" href="recuperar1.php">¿Olvidó su clave?</a>
            </div>

				<!--	<div class="text-center ">
						<span class="txt1">
                          ¿No tiene una cuenta?
						</span>

						<a class="txt2" href="registro.php">					
                          Regístrese aquí
						</a>
					</div>-->
					
					<br>
					<div>

					<div id="custom_notifications" class="custom-notifications dsp_none">
						<ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group"></ul>
						<div class="clearfix"></div>
					    <div id="notif-group" class="tabbed_notifications"></div>
					</div>


              </div>

				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
	<script type="text/javascript" src="js/notify/pnotify.core.js"></script>


</body>
</html>