<?php 
session_start();
if (!isset($_SESSION['csmart']['idusuario'])) {
Header ("Location: login.php");
}

    include 'headerincidencia.php';
    include 'classes/cmembresia.php'; 
    $omembresia = new Membresia();
  
    $accion   = "Crear";
    $option   = "n";
    $nombre     = "";
    $valor  = "";
    $duracion   = "";
    $idestado   = "";
    $idmonetario   = "";
    $nusuarios   = "";
    $detalle="";
    $idtiempo="";
  
    if(isset($_REQUEST['opt']) && $_REQUEST['opt']== "m"){
      $option = $_REQUEST['opt'];
      $idObj  = $_REQUEST['id'];
    }

    if ($option == "m") {
      $vmembresia = $omembresia->getOne($idObj);
      if($vmembresia){
        $accion   = "Modificar";
              foreach ($vmembresia AS $id => $info){ 
              $idmembresia    = $info["idmembresia"];
              $nombre       = $info["nombre"];
              $valor       = $info["valor"];
              $duracion       = $info["duracion"];
              $idestado       = $info["idestado"];
              $nusuarios       = $info["nusuarios"];
              $detalle       = $info["detalle"];
              $idmonetario       = $info["moneda"];
              $idtiempo       = $info["tiempo"];
            }
      }else{
        header("Location: index.php");
        exit();
      }
    }



 $array_estado = array (
  "0" => "Inactivo",
  "1" => "Activo",
);

$array_monetario = array (
  "1" => "$",
  "2" => "Bs",
);


$array_tiempo = array (
  "1" => "Días",
  "2" => "Meses",
  "3" => "Años",
);


    $vmembresia = $omembresia->getAll();
 ?>  

 <!DOCTYPE html>
<html >

<head>
  
  <?php include "headerm.php";?>

  <script type="text/javascript">

      function guardarformulario(){  
        var form = $("#form").serializeJSON(); 
        $.ajax({
          url:'actions/actionMembresia.php',
          type:'POST',
          data: { valor: form }
        }).done(function( data ){
          if(data > 0){
            new PNotify({
                 title: 'Datos Guardados',
                text: 'Todos los datos fueron guardados. Puede continuar.',
                type: 'success'
             });
            window.setTimeout("document.location.href='membresia.php';",2500);
          }else{
            new PNotify({
                 title: 'Error en datos',
                text: 'Falta información!'
             });
          }
        });
      }

      function cancelar(){
        window.setTimeout("document.location.href='membresia.php';",500);
      }

      function eliminar(id) {
          if (confirm("Atención! Va a proceder a eliminar este registro. Desea continuar?")) {
             var form = "valor"; 
            $.ajax({
              url: 'actions/actionMembresia.php?opt=e&id='+id,
              type:'POST',
              data: { valor1: form, }
            }).done(function( data ){   
              if(data == 0){
                  new PNotify({
                   title: 'Datos Eliminados',
                    text: 'Todos los datos fueron guardados. Puede continuar.!',
                    type: 'success'
                  });
              window.setTimeout("document.location.href='membresia.php';",2500);
              }
              else{
                 new PNotify({
                    title: 'Error en formulario',
                    text: 'No se puedieron guardar los datos, intente de nuevo.',
                    type: 'error'
                 });
                  window.setTimeout("location.reload(true);",2500);
              }
            });
          }
      }

          

      function habilitar(idd){
    //  alert(idd);

          if (confirm("Atención! Va Habilitar Menbresia . Desea continuar?")) {     
                   $.ajax({
                      type: 'POST',
                      url: 'actions/actionMembresia.php',
                       data: { id:idd,
                       opt: 'ha'},
                            success: function(data) {     
                             if (data != 0){   
                              new PNotify({
                              title: 'Usuario',
                              text: 'Se actualizo es estado correctamente!!!',
                              type: 'success'
                              });      
                              } else  {
                                new PNotify({
                              title: 'Usuario',
                              text: 'Nose actualizar el estado !!!',
                              type: 'error'
                               }); 
                              }
                              window.setTimeout("location.reload(true);",2500);
                            }	
                  }); 
             }
        }
        function deshabilitar(idd){
      
          if (confirm("Atención! Va Deshabilitar la membresia a la clinica . Desea continuar?")) {     
                   $.ajax({
                      type: 'POST',
                      url: 'actions/actionMembresia.php',
                       data: { id:idd,
                       opt: 'deshabilitado'},
                            success: function(data) {     
                             if (data != 0){   
                              new PNotify({
                              title: 'Usuario',
                              text: 'Se actualizo es estado correctamente!!!',
                              type: 'success'
                              });      
                              } else  {
                                new PNotify({
                              title: 'Usuario',
                              text: 'Nose actualizar el estado !!!',
                              type: 'error'
                               }); 
                              }
                              window.setTimeout("location.reload(true);",2500);
                            }	
                  }); 
             }
        }

  </script>

</head>


<body class="nav-md">

  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          
          <!-- sidebar menu -->
            <?php include "menu.php" ?>
          <!-- /sidebar menu -->

         
        </div>
      </div>
      <!-- top navigation -->
           <?php include "top_nav.php" ?>
      <!-- /top navigation -->
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">
                 <div class="x_title">
                   <h2>Administración de Membresías</h2>
                   <div class="clearfix"></div>
                 </div>
                <div class="x_content">

                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                      <li id="tab1" role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Listado</a>
                      </li>
                      <li id="tab2" role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false"><?=$accion;?> Membresía</a>
                      </li>
                      
                    </ul>
                    <div id="myTabContent" class="tab-content">
                      <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                        
                        <!-- tab 1 -->
                        <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2>Membresías Registrados  </h2>
                            
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                              Listado de todas la membresias registradas</p>
                            <table id="datatable-buttons" class="table table-striped table-bordered">
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Nombre</th>
                                  <th>Costo</th>
                                  <th>Moneda</th>
                                  <th>Duracion</th>
                                  <th>Tiempo</th>
                                  <th>n° Usuarios</th>
                                  <th>Estado</th>
                                  <th>Acto para descuento</th>
                                  <th>Detalle</th>
                                  <th>Opciones</th>
                                  
                                </tr>
                              </thead>

                              <tbody>
                                <?php
                                    if($vmembresia){
                                      $i=1;
                                      foreach ($vmembresia AS $id => $array) {
                                    ?>
                                      <tr>
                                        <td align="left"><?=$i;?></td>
                                        <td align="left"><?=$array['nombre'];?></td>
                                        <td align="left"><?=$array['valor'];?></td>

                                        <td align="left"><?php if($array['moneda']==1){ ?>
                                          <span class="label label-success">$</span>  
																			<?php }else{ if($array['moneda']==2){?>
																					
                                        <span class="label label-success">Bs</span>
																				<?php }};?>
                                        </td>

                                        <td align="left"><?=$array['duracion'];?></td>


                                      <td align="left"><?php if($array['tiempo']==1){ ?>
                                          <span class="label label-warning">Días</span>  
																			<?php }else{
                                           if($array['tiempo']==2){ ?>
                                        <span class="label label-warning">Meses</span>
																				<?php }else{if($array['tiempo']==3){ ?>
                                          <span class="label label-warning">Años</span>
                                      <?php
                                      }
                                      }
                                    }?>
                                        </td>


                                        
                                        <td align="left"><?=$array['nusuarios'];?></td>
                                        <td align="left"><?php if($array['idestado']==0){ ?> <span class="label label-danger">Inactivo</span> 
																			<?php }else{ ?>
																					<span class="label label-success">Activo</span>
																				<?php };?></td>
                                        

                                        <td align="left"><?php if($array['actodesc']==0){ ?>
                                          <a style="font-size:75%" onClick="habilitar(<?=$id?>);"   class="label label-danger" >Inactivo para descuento</a>  
																			<?php }else{ ?>
																					
                                          <a style="font-size:75%" onClick="deshabilitar(<?=$id?>);"   class="label label-success" >Activo para descuento</a>  
																				<?php };?>
                                        </td>


                                        <td align="left"><?=$array['detalle'];?></td>

                                        <td>
                                          <a href="membresia.php?opt=m&id=<?=$id;?>" title="Modificar" >
                                              <span class="label label-primary" >
                                                <i class="fa fa-pencil" title="Modificar"></i>
                                              </span>
                                          </a>
                                          &nbsp;
                                          <span class="label label-default" onClick="eliminar(<?=$id?>)" >
                                                <i class="fa fa-trash-o" title="Eliminar"></i>  
                                        
                                          </span>   
                                        </td>
                                      </tr>

                                    <?php
                                      $i++;}
                                    }
                                    ?>

                              </tbody>
                              
                            </table>
                          </div>
                        </div>
                      </div>

                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                        




                      <form id="form" class="form-horizontal form-label-left" novalidate>
                    <p>Ingrese la información en la casilla correspondiente  
                    </p><span class="section">Ingresar Informacion</span>

                    <?php
                          if ($option == "m") {
                        ?>
                            <input type="hidden" name="idmembresia" value="<?=$idmembresia?>"/>
                            <input type="hidden" name="opt" id="opt" value="m" />
                        <?php
                          }else{
                        ?>
                            <input type="hidden" name="opt" id="opt" value="n" />
                        <?php
                          }
                        ?>
                    
         
								 
                    <div class="item form-group">
		                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombre">nombre <span class="required">*</span>
		                      </label>
		                      <div class="col-md-6 col-sm-3 col-xs-12">
		                        <input id="nombre" class="form-control col-md-3 col-xs-12"  name="nombre" value="<?=$nombre?>" placeholder="nombre" required="required" type="text">
		                      </div>
		                    </div>

   
		                 <label class="control-label col-md-3 col-sm-3 col-xs-3" for="valor">Costo <span class="required">*</span>
		                  </label>
		                    <div class="col-md-4 col-sm-3 col-xs-3">
		                      <input id="valor" class="form-control col-md-3 col-xs-3"  name="valor" value="<?=$valor?>" placeholder="costo de plan" required="required" type="number">
                        </div>
                        
                        <?php if($idmonetario==""){ ?>
                   
                        <div class="item form-group">
		                      <div class="col-md-2 col-sm-3 col-xs-3">
		                        <select id="idmonetario" name="idmonetario"  class="form-control">
		                          <option>Moneda</option>
		                          <?php if($array_monetario){
		                                foreach ($array_monetario AS $id => $array) {?>
		                                <option value="<?=$id;?>" <?php if($idmonetario==$array){echo"selected='selected'";}?>><?=$array?></option>
		                          <?php } } ?>
		                        </select>
		                      </div>
		                    </div>


                        <?php }else{ ?>

<div class="item form-group">

<div class="col-md-2 col-sm-3 col-xs-3">
  <select id="idmonetario" name="idmonetario" class="form-control" required="required">
    <option value="1" <?php if($idmonetario==1){echo"selected='selected'";}?>>$</option>
    <option value="2" <?php if($idmonetario==2){echo"selected='selected'";}?>>Bs</option>
  
  </select>
  </div>
</div>
<?php } ?>

                   
		                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="duracion">Duración <span class="required">*</span>
		                      </label>
		                      <div class="col-md-4 col-sm-3 col-xs-9">
		                        <input id="duracion" class="form-control col-md-3 col-xs-12"  name="duracion" value="<?=$duracion?>" placeholder="duracion" required="required" type="number">
		                      </div>
		                   
                    <?php if($idtiempo==""){ ?>
                        <div class="item form-group">
                         <div class="col-md-2 col-sm-3 col-xs-3">
                           <select id="idtiempo" name="idtiempo"  class="form-control">
                             <option>Tiempo</option>
                             <?php if($array_tiempo){
                                   foreach ($array_tiempo AS $id => $array) {?>
                                   <option value="<?=$id;?>" <?php if($idtiempo==$array){echo"selected='selected'";}?>><?=$array?></option>
                             <?php } } ?>
                           </select>
                         </div>
                       </div>

                        <?php }else{ ?>

                          <div class="item form-group">
		                   
                          <div class="col-md-2 col-sm-3 col-xs-3">
		                        <select id="idtiempo" name="idtiempo" class="form-control" required="required">
		                          <option value="1" <?php if($idtiempo==1){echo"selected='selected'";}?>>Días</option>
                              <option value="2" <?php if($idtiempo==2){echo"selected='selected'";}?>>Meses</option>
                              <option value="3" <?php if($idtiempo==3){echo"selected='selected'";}?>>Años</option>
		                        </select>
		                        </div>
		                    </div>
                       <?php } ?>



                        <div class="item form-group">
		                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nusuarios">N° Usuarios <span class="required">*</span>
		                      </label>
		                      <div class="col-md-6 col-sm-3 col-xs-12">
		                        <input id="nusuarios" class="form-control col-md-3 col-xs-12"  name="nusuarios" value="<?=$nusuarios?>" placeholder="nusuarios" required="required" type="number">
		                      </div>
		                    </div>



                        <div class="item form-group">
		                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="detalle">Detalle<span class="required">*</span>
		                      </label>
		                      <div class="col-md-6 col-sm-3 col-xs-12">
                            <input id="detalle" class="form-control col-md-3 col-xs-12"  name="detalle" value="<?=$detalle?>" rows="2"  placeholder='detalle' required="required" type="text">
                            
		                      </div>
		                    </div>

                        

                        <?php if($idestado==""){ ?>

                        <div class="item form-group">
		                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="idestado">Estado <span class="required">*</span>
		                      </label>
		                      <div class="col-md-3 col-sm-3 col-xs-12">
		                        <select id="idestado" name="idestado"  class="form-control">
		                          <option>Elegir una opción</option>
		                          <?php if($array_estado){
		                                foreach ($array_estado AS $id => $array) {?>
		                                <option value="<?=$id;?>" <?php if($idestado==$array){echo"selected='selected'";}?>><?=$array?></option>
		                          <?php } } ?>
		                        </select>
		                      </div>
		                    </div>
							
                        <?php }else{ ?>
                     
                      <div class="item form-group">
		                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="idestado">Estado <span class="required">*</span>
		                      </label>
		                     
                          <div class="col-md-3 col-sm-3 col-xs-12">
		                        <select id="idestado" name="idestado" class="form-control" required="required">
		                          <option value="1" <?php if($idestado==1){echo"selected='selected'";}?>>Activo</option>
		                          <option value="0" <?php if($idestado==0){echo"selected='selected'";}?>>Inactivo</option>
		                          
		                        </select>

		                        </div>
		                    </div>
                     
                       <?php } ?>
													
													
								<div class="ln_solid"></div>
				                    <div class="form-group">
				                      <div class="col-md-6 col-md-offset-3">
				                        <input type="button" onClick="cancelar();" class="btn btn-primary" value="Cancelar">
				                        <button  id="send" class="btn btn-success">Guardar</button>
				                      </div>
				                    </div>
				                  </form>
								
   


                      </div>
                     
                    </div>
                  </div>


                </div>
              </div>
            </div>
          </div>

          
        </div>

        <!-- footer content -->
      <center>  <? include "piep.php"; ?> </center>
      </div>
    </div>

  </div>

  <!--<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>-->

  <script src="js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>
  <script src="js/custom.js"></script>
  <!-- form validation -->
  <script src="js/validator/validator.js"></script>
  <!-- PNotify -->
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  
   <!-- Datatables-->
        <script src="js/datatables/jquery.dataTables.min.js"></script>
        <script src="js/datatables/dataTables.bootstrap.js"></script>
        <script src="js/datatables/dataTables.buttons.min.js"></script>
        <script src="js/datatables/buttons.bootstrap.min.js"></script>
        <script src="js/datatables/jszip.min.js"></script>
        <script src="js/datatables/pdfmake.min.js"></script>
        <script src="js/datatables/vfs_fonts.js"></script>
        <script src="js/datatables/buttons.html5.min.js"></script>
        <script src="js/datatables/buttons.print.min.js"></script>
        <script src="js/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="js/datatables/dataTables.responsive.min.js"></script>
        <script src="js/datatables/responsive.bootstrap.min.js"></script>
        <script src="js/datatables/dataTables.scroller.min.js"></script>


  <script>
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
      .on('blur', 'input[required], input.optional, select.required', validator.checkField)
      .on('change', 'select.required', validator.checkField)
      .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
      .on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });


    $('form').submit(function(e) {
      e.preventDefault();
      var submit = true;

      
      // evaluate the form using generic validaing
      if (!validator.checkAll($(this))) {
        submit = false;
      }

      if (submit)
        guardarformulario();
      return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function() {
      $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function() {
      validator.defaults.alerts = (this.checked) ? false : true;
      if (this.checked)
        $('form .alert').remove();
    }).prop('checked', false);
  </script>
  <!-- pace -->
        <script src="js/pace/pace.min.js"></script>
        <script>
          var handleDataTableButtons = function() {
              "use strict";
              0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
                dom: "Bfrtip",
                buttons: [{
                  extend: "csv",
                  classnombre: "btn-sm"
                }, {
                  extend: "excel",
                  classnombre: "btn-sm"
                }, {
                  extend: "pdf",
                  classnombre: "btn-sm"
                }, {
                  extend: "print",
                  classnombre: "btn-sm"
                }],
                responsive: !0
              })
            },
            TableManageButtons = function() {
              "use strict";
              return {
                init: function() {
                  handleDataTableButtons()
                }
              }
            }();
        </script>


         <script type="text/javascript">
          $(document).ready(function() {
            $('#idestadoS').change(function() {
                var filenombre = $(this).val();
                var lastIndex = filenombre.lastIndexOf("\\");
                if (lastIndex >= 0) {
                    filenombre = filenombre.substring(lastIndex + 1);
                } 
                $('#img').val(filenombre);
            });
            <?php
              if($option == "m"){
            ?>
                $("#tab1").removeClass('active');
                $("#tab2").addClass('active');
                $("#tab_content1").removeClass('active in');
                $("#tab_content2").addClass('active in');
                
            <? } ?>

            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({
              keys: true
            });
            $('#datatable-responsive').DataTable();
            
            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true
            });
          });
          TableManageButtons.init();
        </script>
        
</body>

</html>
