<?php
require 'PHPmailer/PHPMailerAutoload.php';
require 'PHPmailer/class.phpmailer.php';
require 'PHPmailer/class.smtp.php';
require 'PHPmailer/class.pop3.php';
function setTokenapp($id){ 
	try {  
		$db = getConnection();
		$sql   = "UPDATE paciente SET token=:tokenapp WHERE idPaciente=:idusuarioapp;";
		$stmt = $db->prepare($sql);
		$stmt->bindParam("idusuarioapp", $id);
		$fecha = date("Y-m-d H:i:s");
		$token = sha1($fecha.$id); 
		$stmt->bindParam("tokenapp", $token);
		$stmt->execute();
		if ( $stmt->rowCount() > 0 ) {
			return $token;;
		}else{
			return false; 
		}
		
		$db = null;
	} catch (PDOException $e){
		return false;
	}
}

function setTokenapp2($id){   
 	try {  
 		$db = getConnection();
 		$sql   = "UPDATE medico SET token=:tokenapp WHERE idMedico=:idusuarioprofesional;";
 		$stmt = $db->prepare($sql);
 		$stmt->bindParam("idusuarioprofesional", $id);
 		$fecha = date("Y-m-d H:i:s");
 		$token = sha1($fecha.$id); 
 		$stmt->bindParam("tokenapp", $token);
 		$stmt->execute();
 		if ( $stmt->rowCount() > 0 ) {
 			return $token;
 		}else{
 			return false; 
 		}
 		$db = null;
 	} catch (PDOException $e){
 		return false;
 	}
}

function setMedicoFavoritoVal($iduser,$idmedico){
	try{
		$sql = "SELECT idMedicoFav FROM medicofav WHERE idPaciente=:id AND idMedico=:id2;";
		$db   = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id",$iduser);
		$stmt->bindParam("id2",$idmedico);
		$stmt->execute();
		if ( $stmt->rowCount() > 0 ) {
			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
				$idMedicoFav 	= trim($data['idMedicoFav']);				
			}
			$sql = "DELETE FROM medicofav WHERE idMedicoFav=:id";
			$db   = getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("id",$idMedicoFav);
			$stmt->execute();
		}else{
			$sql = "INSERT INTO medicofav (idPaciente,idMedico) VALUES (:id,:id2)";
			$db   = getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("id",$iduser);
			$stmt->bindParam("id2",$idmedico);
			$stmt->execute();
		}
		return true;
		$db = null;
	}catch(PDOException $e){
		return false;
	}
}

function getValidateUserapp($id,$token){
 	$sql = "SELECT idPaciente FROM paciente WHERE idPaciente=:idusuarioapp AND token=:tokenapp;";
 	try{
 		$db   = getConnection();
 		$stmt = $db->prepare($sql);
 		$stmt->bindParam("idusuarioapp",$id);
 		$stmt->bindParam("tokenapp",$token);
 		$stmt->execute();
 		if ( $stmt->rowCount() > 0 ) {
 			return true;
 		}else{
 			return false;
 		}
 		$db = null;
 	}catch(PDOException $e){
 		return false;
 	}
 }

 function getValidateUserapp2($id,$token){
	$sql = "SELECT idMedico FROM medico WHERE idMedico=:idusuarioapp AND token=:tokenapp;";
	try{
		$db   = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("idusuarioapp",$id);
		$stmt->bindParam("tokenapp",$token);
		$stmt->execute();
		if ( $stmt->rowCount() > 0 ) {
			return true;
		}else{
			return false;
		}
		$db = null;
	}catch(PDOException $e){
		return false;
	}
}

 function image($imgbase64){
 	// requires php5
 	define('UPLOAD_DIR', '../IMGUSERS/');
 	$image_parts = explode(";base64,", $imgbase64);
     $image_type_aux = explode("image/", $image_parts[0]);
     $image_type = $image_type_aux[1];
     $image_base64 = base64_decode($image_parts[1]);
 	$file = UPLOAD_DIR . 'IMGUSER'.time(). '.png';
 	$file1 = 'IMGUSER'.time(). '.png';
 	$success = file_put_contents($file, $image_base64);
 	if($success){
 		return  $file1;
 	}else{
 		return false;
 	}
 }

 function image2($imgbase64){ 
	// requires php5
	define('UPLOAD_DIR', '../IMGUSERS/');
	$image_parts = explode(";base64,", $imgbase64);
	$image_type_aux = explode("image/", $image_parts[0]);
	$image_type = $image_type_aux[1];
	$image_base64 = base64_decode($image_parts[1]);
	$file = UPLOAD_DIR . 'doc'.time(). '.png';
	$file1 = 'doc'.time(). '.png';
	$success = file_put_contents($file, $image_base64);
	if($success){
		return  $file1;
	}else{
		return false;
	}
}

function getMedicoFavoritoVal($iduser,$idmedico){
	try{
		$sql = "SELECT idMedicoFav FROM medicofav WHERE idPaciente=:id AND idMedico=:id2;";
		$db   = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id",$iduser);
		$stmt->bindParam("id2",$idmedico);
		$stmt->execute();
		$totalrespuestas = 0;
		if ( $stmt->rowCount() > 0 ) {
			return 1;
		}else{
			return 0;
		}
		$db = null;
	}catch(PDOException $e){
		return 0;
	}
}

function getMedicoEspecialidades($id){
	 
	try{
		$sql = "SELECT e.nombre, e.idEspecialidad 
					FROM especialidadmedico AS em
				 JOIN especialidad AS e ON e.idEspecialidad=em.idEspecialidad WHERE em.idMedico=:id;";
		$db   = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id",$id);
		$stmt->execute();
		$i = 0;
		if ( $stmt->rowCount() > 0 ) {
			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
				$array[$i]['id'] 			= trim($data['idEspecialidad']);
				$array[$i]['nombre'] 		= trim($data['nombre']);
				// $array[$i]['descripcion'] 	= trim($data['descripcion']);
				// $array[$i]['principal'] 	= trim($data['principal']);
				// $array[$i]['imagen'] 		= PATHIMAGEN.trim($data['imagen']);
				$i++;
			}
			return $array;
		}else{
			$array = array();
			return $array;
		}
		$db = null;
	}catch(PDOException $e){
		$array = array();
		return $array;
	}
}

function getMedicoDirecciones($id){
	 
	try{
		$sql = "SELECT * FROM direccion  WHERE idMedico=:id;";
		$db   = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id",$id);
		$stmt->execute();
		$i = 0;
		if ( $stmt->rowCount() > 0 ) {
			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
				$array[$i]['id'] 			= trim($data['idDireccion']);
				$array[$i]['nombre'] 		= trim($data['nombre']);
				$array[$i]['detalle'] 	= trim($data['detalle']);
				$array[$i]['latitud'] 	= trim($data['latitud']);
				$array[$i]['longitud'] 	= trim($data['longitud']);
				// $array[$i]['imagen'] 		= PATHIMAGENSEGURO.trim($data['imagen']);
				$i++;
			}
			return $array;
		}else{
			$array = array();
			return $array;
		}
		$db = null;
	}catch(PDOException $e){
		$array = array();
		return $array;
	}
}

function getMedicoSeguros($id){
	 
	try{
		$sql = "SELECT s.nombre, s.idSeguro, s.imagen 
					FROM seguromedico AS sm
				 JOIN seguro AS s ON s.idSeguro=sm.idSeguro WHERE sm.idMedico=:id;";
		$db   = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id",$id);
		$stmt->execute();
		$i = 0;
		if ( $stmt->rowCount() > 0 ) {
			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
				$array[$i]['id'] 			= trim($data['idSeguro']);
				$array[$i]['nombre'] 		= trim($data['nombre']);
				// $array[$i]['descripcion'] 	= trim($data['descripcion']);
				// $array[$i]['principal'] 	= trim($data['principal']);
				$array[$i]['imagen'] 		= PATHIMAGENSEGURO.trim($data['imagen']);
				$i++;
			}
			return $array;
		}else{
			$array = array();
			return $array;
		}
		$db = null;
	}catch(PDOException $e){
		$array = array();
		return $array;
	}
}

function getPacienteEnfermedades($id){
	try{
		$sql = "SELECT e.nombre, e.idEnfermedad 
					FROM enfermedadpaciente AS ep
				 JOIN enfermedad AS e ON e.idEnfermedad=ep.idEnfermedad WHERE ep.idPaciente=:id;";
		$db   = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id",$id);
		$stmt->execute();
		$i = 0;
		if ( $stmt->rowCount() > 0 ) {
			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
				$array[$i]['id'] 			= trim($data['idEnfermedad']);
				$array[$i]['nombre'] 		= trim($data['nombre']);
				$i++;
			}
			return $array;
		}else{
			$array = array();
			return $array;
		}
		$db = null;
	}catch(PDOException $e){
		$array = array();
		return $array;
	}
}

function getTiempoConsultaMedico($idmedico){
	try{
		$sql = "SELECT tiempoConsulta FROM medico WHERE  idMedico=:id2;";
		$db   = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id2",$idmedico);
		$stmt->execute();
		if ( $stmt->rowCount() > 0 ) {
			$data  = $stmt->fetch(PDO::FETCH_ASSOC);
			$tiempo	= trim($data['tiempoConsulta']);
			return $tiempo;
		}else{
			$tiempo=0;
			return $tiempo;
		}
		$db = null;
	}catch(PDOException $e){
		return 0;
	}
}

function getHoraInicioConsultaMedico($idMedico,$idDia){
	try{
		$sql = "SELECT horaInicio FROM horariomedico WHERE idMedico=:id2 AND idDia=:dia;";
		$db   = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id2",$idMedico);
		$stmt->bindParam("dia",$idDia);
		$stmt->execute();
		$i=0;
		if ( $stmt->rowCount() > 0 ) {
			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC))
			{
				$horaInicio[$i]	= trim($data['horaInicio']);
				$i++;
			}
			return $horaInicio;
		}else{
			$horaInicio=0;
			return $horaInicio;
		}
		$db = null;
	}catch(PDOException $e){
		return 0;
	}
}

function getHoraFinConsultaMedico($idMedico,$idDia){
	try{
		$sql = "SELECT horaFin FROM horariomedico WHERE idMedico=:id2 AND idDia=:dia;";
		$db   = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id2",$idMedico);
		$stmt->bindParam("dia",$idDia);
		$stmt->execute();
		$i = 0;
		if ( $stmt->rowCount() > 0 ) {
			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC))
			{
				$horaFin[$i] = trim($data['horaFin']);
				$i++;
			}
			return $horaFin;
		}else{
			$horaFin=0;
			return $horaFin;
		}
		$db = null;
	}catch(PDOException $e){
		return 0;
	}
}

function getHorarioAtencion($id){
	 
	try{
		$sql = "SELECT hm.idMedico,hm.idDia,d.nombre,hm.horaInicio,hm.horaFin,hm.idHorarioMedico FROM horariomedico hm,dia d WHERE hm.idMedico=:id AND hm.idDia=d.idDia order by hm.idDia;";
		$db   = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id",$id);
		$stmt->execute();
		$i = 0;
		if ( $stmt->rowCount() > 0 ) {
			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
				$array[$i]['idMedico'] 			= trim($data['idMedico']);
				$array[$i]['idHorarioMedico'] 			= trim($data['idHorarioMedico']);
				$array[$i]['nombre'] 		= trim($data['nombre']);
				$array[$i]['horaInicio'] 		= trim($data['horaInicio']);
				$array[$i]['horaFin'] 		= trim($data['horaFin']);
				$array[$i]['idDia'] 		= trim($data['idDia']);
				$i++;
			}
			return $array;
		}else{
			$array = array();
			return $array;
		}
		$db = null;
	}catch(PDOException $e){
		$array = array();
		return $array;
	}
}

 function getVerificacionCorreoUsuarioApp($email){
 	$sql = "SELECT idPaciente FROM paciente WHERE correo=:email;";
 	try{
 		$db   = getConnection();
 		$stmt = $db->prepare($sql);
 		$stmt->bindParam("email",$email);
 		$stmt->execute();
 		if ( $stmt->rowCount() > 0 ) {
 			return true;
 		}else{
 			return false;
 		}
 		$db = null;
 	}catch(PDOException $e){
 		return false;
 	}
 }

 function getVerificacionCorreoMedico($email){
	$sql = "SELECT idMedico FROM medico WHERE correo=:email;";
	try{
		$db   = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("email",$email);
		$stmt->execute();
		if ( $stmt->rowCount() > 0 ) {
			return true;
		}else{
			return false;
		}
		$db = null;
	}catch(PDOException $e){
		return false;
	}
}

function sendEmail($email,$name,$body){		
	$fecha = date("Y-m-d H:i:s");
	//ENVIAR CORREO
		$mensaje = file_get_contents("function/template/mail1.html");
		$mensaje = str_replace("[NAME]", utf8_decode($name), $mensaje);
		$mensaje = str_replace("[ATTACHMENT]", utf8_decode($body), $mensaje);
		$mensaje = str_replace("[DATE]", $fecha, $mensaje);
	   
		$asunto = 'Codigo de acceso temporal - DoctorCita'; 
		$mailPHP 	= new PHPMailer(); 
		//indico a la clase que use SMTP 
		$mailPHP->IsSMTP();
		//permite modo debug para ver mensajes de las cosas que van ocurriendo
		 //$mailPHP->SMTPDebug = 3; 
		//Debo de hacer autenticación SMTP
		$mailPHP->SMTPAuth = true;
		// $mailPHP->SMTPSecure = "ssl";
		//indico el servidor para SMTP
		$mailPHP->Host = "mail.clanbolivia.tech"; 
		//indico el puerto que usa 
		$mailPHP->Port = 25;
		//indico un usuario / 
		$mailPHP->Username = "doctorcita@clanbolivia.tech";
		$mailPHP->Password = 'd0ct0rcita';
		$mailPHP->SetFrom('doctorcita@clanbolivia.tech', 'DoctorCita'); 
		$mailPHP->Subject = ($asunto);
		$mailPHP->isHTML(true);
		$mailPHP->MsgHTML($mensaje);
		//indico destinatario
		$mailPHP->AddAddress($email, $email);
		// $mailPHP->addCC($email);
		if(!$mailPHP->Send()){
			return 400;
		}else{
			return 0;
		}
		// if($email){
		// 	$arrData['id'] = $email;	
		// 	$output = json_encode($arrData);
		// 	echo sprintf('%s', $output);
		// }else{
		// 	$arrData['id'] = 0;	
		// 	$output = json_encode($arrData);
		// 	echo sprintf('%s', $output);
		// }
}

function sendEmailGeneral($email,$name,$body,$asunto){		
	$fecha = date("Y-m-d H:i:s");
	//ENVIAR CORREO
		$mensaje = file_get_contents("function/template/mail1.html");
		$mensaje = str_replace("[NAME]", utf8_decode($name), $mensaje);
		$mensaje = str_replace("[ATTACHMENT]", utf8_decode($body), $mensaje);
		$mensaje = str_replace("[DATE]", $fecha, $mensaje);
	   
		// $asunto = 'Codigo de acceso temporal - DoctorCita'; 
		$mailPHP 	= new PHPMailer(); 
		//indico a la clase que use SMTP 
		$mailPHP->IsSMTP();
		//permite modo debug para ver mensajes de las cosas que van ocurriendo
		 //$mailPHP->SMTPDebug = 3; 
		//Debo de hacer autenticación SMTP
		$mailPHP->SMTPAuth = true;
		// $mailPHP->SMTPSecure = "ssl";
		//indico el servidor para SMTP
		$mailPHP->Host = "mail.clanbolivia.tech"; 
		//indico el puerto que usa 
		$mailPHP->Port = 25;
		//indico un usuario / 
		$mailPHP->Username = "doctorcita@clanbolivia.tech";
		$mailPHP->Password = 'd0ct0rcita';
		$mailPHP->SetFrom('doctorcita@clanbolivia.tech', 'DoctorCita'); 
		$mailPHP->Subject = ($asunto);
		$mailPHP->isHTML(true);
		$mailPHP->MsgHTML($mensaje);
		//indico destinatario
		$mailPHP->AddAddress($email, $email);
		// $mailPHP->addCC($email);
		if(!$mailPHP->Send()){
			return 400;
		}else{
			return 0;
		}
		// if($email){
		// 	$arrData['id'] = $email;	
		// 	$output = json_encode($arrData);
		// 	echo sprintf('%s', $output);
		// }else{
		// 	$arrData['id'] = 0;	
		// 	$output = json_encode($arrData);
		// 	echo sprintf('%s', $output);
		// }
}

function setpasswordupdateUsuarioapp($password,$idusuarioapp){  
 	try{
 		$sql = "UPDATE paciente SET clave = :password, recuperarClave=1 WHERE idPaciente=:idusuarioapp;";
 		$db   = getConnection();
 		$stmt = $db->prepare($sql);
 		$stmt->bindParam("password",$password);
 		$stmt->bindParam("idusuarioapp",$idusuarioapp);
 		$stmt->execute();
 		if ( $stmt->rowCount() > 0 ) {
 			return true;
 		}else{
 			return false;
 		}
 		$db = null;
 	}catch(PDOException $e){
 		return false;
 	}
}

function setpasswordupdateProfesional($password,$idusuarioprofesional){ 	 
 	try{
 		$sql = "UPDATE medico SET clave = :password, recuperarClave=1 WHERE idMedico=:idusuarioprofesional;";
 		$db   = getConnection();
 		$stmt = $db->prepare($sql);
 		$stmt->bindParam("password",$password);
 		$stmt->bindParam("idusuarioprofesional",$idusuarioprofesional);
 		$stmt->execute();
 		if ( $stmt->rowCount() > 0 ) {
 			return true;
 		}else{
 			return false;
 		}
 		$db = null;
 	}catch(PDOException $e){
 		return false;
 	}
}

function sendEmail2($name,$body){		
	$fecha = date("Y-m-d H:i:s");
	//ENVIAR CORREO
		$mensaje = file_get_contents("function/template/mail1.html");
		$mensaje = str_replace("[NAME]", utf8_decode($name), $mensaje);
		$mensaje = str_replace("[ATTACHMENT]", utf8_decode($body), $mensaje);
		$mensaje = str_replace("[DATE]", $fecha, $mensaje);
	   
		$asunto = 'Consulta - DoctorCita'; 
		$mailPHP 	= new PHPMailer(); 
		//indico a la clase que use SMTP 
		$mailPHP->IsSMTP();
		//permite modo debug para ver mensajes de las cosas que van ocurriendo
		 //$mailPHP->SMTPDebug = 3; 
		//Debo de hacer autenticación SMTP
		$mailPHP->SMTPAuth = true;
		// $mailPHP->SMTPSecure = "ssl";
		//indico el servidor para SMTP
		$mailPHP->Host = "mail.clanbolivia.tech"; 
		//indico el puerto que usa 
		$mailPHP->Port = 25;
		//indico un usuario / 
		$mailPHP->Username = "doctorcita@clanbolivia.tech";
		$mailPHP->Password = 'd0ct0rcita';
		$mailPHP->SetFrom('doctorcita@clanbolivia.tech', 'DoctorCita'); 
		$mailPHP->Subject = ($asunto);
		$mailPHP->isHTML(true);
		$mailPHP->MsgHTML($mensaje);
		//indico destinatario
		$mailPHP->AddAddress("doctorcita@clanbolivia.tech", "doctorcita@clanbolivia.tech");
		if(!$mailPHP->Send()){
			return 400;
		}else{
			return 0;
		}
}

function sendEmail3($name,$body){		
	$fecha = date("Y-m-d H:i:s");
	//ENVIAR CORREO
		$mensaje = file_get_contents("function/template/mail2.html");
		$mensaje = str_replace("[NAME]", utf8_decode($name), $mensaje);
		$mensaje = str_replace("[ATTACHMENT]", utf8_decode($body), $mensaje);
		$mensaje = str_replace("[DATE]", $fecha, $mensaje);
	   
		$asunto = 'Registro de Medico Nuevo - DoctorCita'; 
		$mailPHP 	= new PHPMailer(); 
		//indico a la clase que use SMTP 
		$mailPHP->IsSMTP();
		//permite modo debug para ver mensajes de las cosas que van ocurriendo
		 //$mailPHP->SMTPDebug = 3; 
		//Debo de hacer autenticación SMTP
		$mailPHP->SMTPAuth = true;
		// $mailPHP->SMTPSecure = "ssl";
		//indico el servidor para SMTP
		$mailPHP->Host = "mail.clanbolivia.tech"; 
		//indico el puerto que usa 
		$mailPHP->Port = 25;
		//indico un usuario / 
		$mailPHP->Username = "doctorcita@clanbolivia.tech";
		$mailPHP->Password = 'd0ct0rcita';
		$mailPHP->SetFrom('doctorcita@clanbolivia.tech', 'DoctorCita'); 
		$mailPHP->Subject = ($asunto);
		$mailPHP->isHTML(true);
		$mailPHP->MsgHTML($mensaje);
		//indico destinatario
		$mailPHP->AddAddress("doctorcita@clanbolivia.tech", "doctorcita@clanbolivia.tech");
		if(!$mailPHP->Send()){
			return 400;
		}else{
			return 0;
		}
}

function sendEmailCitaRechazada($email,$name,$body){		
	$fecha = date("Y-m-d H:i:s");
	//ENVIAR CORREO
		$mensaje = file_get_contents("function/template/mail1.html");
		$mensaje = str_replace("[NAME]", utf8_decode($name), $mensaje);
		$mensaje = str_replace("[ATTACHMENT]", utf8_decode($body), $mensaje);
		$mensaje = str_replace("[DATE]", $fecha, $mensaje);
	   
		$asunto = 'Cita Rechazada - DoctorCita'; 
		$mailPHP 	= new PHPMailer(); 
		//indico a la clase que use SMTP 
		$mailPHP->IsSMTP();
		//permite modo debug para ver mensajes de las cosas que van ocurriendo
		 //$mailPHP->SMTPDebug = 3; 
		//Debo de hacer autenticación SMTP
		$mailPHP->SMTPAuth = true;
		// $mailPHP->SMTPSecure = "ssl";
		//indico el servidor para SMTP
		$mailPHP->Host = "mail.clanbolivia.tech"; 
		//indico el puerto que usa 
		$mailPHP->Port = 25;
		//indico un usuario / 
		$mailPHP->Username = "doctorcita@clanbolivia.tech";
		$mailPHP->Password = 'd0ct0rcita';
		$mailPHP->SetFrom('doctorcita@clanbolivia.tech', 'DoctorCita'); 
		$mailPHP->Subject = ($asunto);
		$mailPHP->isHTML(true);
		$mailPHP->MsgHTML($mensaje);
		//indico destinatario
		$mailPHP->AddAddress($email, $email);
		// $mailPHP->addCC($email);
		if(!$mailPHP->Send()){
			return 400;
		}else{
			return 0;
		}
		// if($email){
		// 	$arrData['id'] = $email;	
		// 	$output = json_encode($arrData);
		// 	echo sprintf('%s', $output);
		// }else{
		// 	$arrData['id'] = 0;	
		// 	$output = json_encode($arrData);
		// 	echo sprintf('%s', $output);
		// }
}

function horariosDisponibles($idMedico,$fecha,$dia){
	try{
			$sql = "SELECT hora FROM `reserva` WHERE fecha=:fecha AND idMedico=:idMedico AND estado!=4";
			$db   = getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("fecha",$fecha);
			$stmt->bindParam("idMedico",$idMedico);
			$stmt->execute();
			$horasReservadas=array();
			$i = 0;
			if ( $stmt->rowCount() > 0 ) {
				while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
					$horasReservadas[$i]    		= trim($data['hora']);
					$i++;
				}
			}else{
			}
			$horas = array();
			$i1 = 0;

			$tiempoConsulta = getTiempoConsultaMedico($idMedico);
			$horaInicio = getHoraInicioConsultaMedico($idMedico,$dia);
			$horaFin = getHoraFinConsultaMedico($idMedico,$dia);
			$x=0;
			while($x<count($horaInicio))
			{
				$hora1 = strtotime( $horaInicio[$x] );
			    $hora2 = strtotime( $horaFin[$x] );
				$horas[$i1]=date("H:i:s",$hora1);
				$i1++;

			    while($hora1 < $hora2)
			    {
				    $min = date("i",strtotime($tiempoConsulta));
				    $segundos_tiempoConsulta = (int)$min*60;
	 
				    $nuevaHora = date("H:i:s",$hora1+$segundos_tiempoConsulta);
				    $horas[$i1] = $nuevaHora;
				    $p=date('H:i:s', strtotime(Time($horaInicio)+Time($tiempoConsulta)));
				    $i1=$i1+1;
				    $hora1 = strtotime( $nuevaHora );
				}
				$x++;
		    } 
			
			unset($horas[$i1-1]);
			$reservas=count($horasReservadas);
			if($reservas>0)
			{
				$disponible = count($horas);
			    $i2 = 0;
			    $j = 0;
			    $horasDisponibles = array();
			    while($i2 < $disponible)
			    {
				    $hora = $horas[$i2];
				    if(count($horasReservadas)>0)
				    {
					    if(in_array($hora, $horasReservadas)!=true)
				        {
					        $horasDisponibles[$j]=$hora;
					        $j=$j+1;
				        }
				    }				
				    $i2=$i2+1;
				}
				if(count($horasDisponibles)>0)
				{
					return true;
				}else{
					return false;
				}
				//return $horasDisponibles;
			}else if($reservas==0)
			{
				return true;
			}
		$db = null;
	} catch(PDOException $e){
		$db = null;
		echo '{
						"errorCode": 2,
						"errorMessage": "Error al ejecutar el servicio web.",
						"msg": "'.$e.'"  
				}';
					
	}
}

function diahabil($fecha,$idmedico){
	try{
		// $fecha = '2019-02-01';
			$sql = "SELECT * FROM vacacionmedico WHERE fechaInicio='$fecha' AND idMedico=:idMedico;";
			$db   = getConnection();
			$stmt = $db->prepare($sql);
			// $stmt->bindParam("fecha",$fecha);
			$stmt->bindParam("idMedico",$idMedico);
			$stmt->execute();
			if ( $stmt->rowCount() > 0 ) {
				return false;
			}else{
				return true;
			}
	}catch(PDOException $e){
		$db = null;
		return false; 
	} 
}

function getFechaI($idMedico)
{
	try{
		// $fecha = '2019-02-01';
			$sql = "SELECT fechaInicio FROM vacacionmedico WHERE idMedico=:idMedico;";
			$db   = getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("idMedico",$idMedico);
			$stmt->execute();
			if ( $stmt->rowCount() > 0 ) {
				$data  = $stmt->fetch(PDO::FETCH_ASSOC);
				return trim($data['fechaInicio']);
			}else{
				return 0;
			}
	}catch(PDOException $e){
		$db = null;
		return false; 
	} 
}

function getFechaF($idMedico)
{
	try{
		// $fecha = '2019-02-01';
			$sql = "SELECT fechaFin FROM vacacionmedico WHERE idMedico=:idMedico;";
			$db   = getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("idMedico",$idMedico);
			$stmt->execute();
			if ( $stmt->rowCount() > 0 ) {
				$data  = $stmt->fetch(PDO::FETCH_ASSOC);
				return trim($data['fechaFin']);
			}else{
				return 0;
			}
	}catch(PDOException $e){
		$db = null;
		return false; 
	} 
}

function deleteSeguroMedico($idMedico){   
	try {  
		$db = getConnection();
		$sql   = "DELETE FROM seguromedico WHERE idMedico=:idMedico;";
		$stmt = $db->prepare($sql);
		$stmt->bindParam("idMedico", $idMedico);
		$stmt->execute();
		if ( $stmt->rowCount() > 0 ) {
			return true;
		}else{
			return false; 
		}
	   
		$db = null;
	} catch (PDOException $e){
		return false;
	}
} 

function setSeguro($idMedico,$idSeguro){   
 	try {  
 		$db = getConnection();
 		$sql   = "INSERT INTO seguromedico (idSeguro,idMedico,estado) 
		 VALUES (:idSeguro,:idMedico,1);";
 		$stmt = $db->prepare($sql);
 		$stmt->bindParam("idMedico", $idMedico);
 		$stmt->bindParam("idSeguro", $idSeguro);
 		$stmt->execute();
 		if ( $stmt->rowCount() > 0 ) {
 			return true;
 		}else{
 			return false; 
 		}	
 		$db = null;
 	} catch (PDOException $e){
 		return false;
 	}
} 

function setEnfermedades($idPaciente,$idEnfermedad){   
	try {  
		$db = getConnection();
		$sql   = "INSERT INTO enfermedadpaciente (idEnfermedad,idPaciente) 
		VALUES (:idEnfermedad,:idPaciente);";
		$stmt = $db->prepare($sql);
		$stmt->bindParam("idEnfermedad", $idEnfermedad);
		$stmt->bindParam("idPaciente", $idPaciente);
		$stmt->execute();
		if ( $stmt->rowCount() > 0 ) {
			return true;
		}else{
			return false; 
		}	
		$db = null;
	} catch (PDOException $e){
		return false;
	}
}

function verificarPermisoHistorial($idPaciente,$idMedico){   
	try {  
		$db = getConnection();
		$sql   = "SELECT idPermisoHistorial,estado
		FROM permisohistorial
		WHERE idPaciente=:idPaciente AND idMedico=:idMedico;";
		$stmt = $db->prepare($sql);
		$stmt->bindParam("idPaciente", $idPaciente);
		$stmt->bindParam("idMedico", $idMedico);
		$stmt->execute();
		if ( $stmt->rowCount() > 0 ) {
			$data  = $stmt->fetch(PDO::FETCH_ASSOC);
			if(trim($data['estado'])==0)
			{
				$sql2  = "UPDATE permisohistorial SET estado=1 WHERE idMedico=:idMedico AND idPaciente=:idPaciente;";
		        $stmt2 = $db->prepare($sql2);
		        $stmt2->bindParam("idPaciente", $idPaciente);
		        $stmt2->bindParam("idMedico", $idMedico);
				$stmt2->execute();
				return true;
			}else{
				return true;
			}
		}else{
			$sql2  = "INSERT INTO permisohistorial (idPaciente,idMedico,estado) 
			VALUES(:idPaciente,:idMedico,1);";
		    $stmt2 = $db->prepare($sql2);
		    $stmt2->bindParam("idPaciente", $idPaciente);
		    $stmt2->bindParam("idMedico", $idMedico);
			$stmt2->execute();
			return true;
		}	
		$db = null;
	} catch (PDOException $e){
		echo $e;
		return false;
	}
}
function consultarPermisoHistorial($idPaciente,$idMedico){   
	try {  
		$db = getConnection();
		$sql   = "SELECT idPermisoHistorial FROM permisohistorial
					WHERE idPaciente=:idPaciente AND idMedico=:idMedico AND estado=1;";
		$stmt = $db->prepare($sql);
		$stmt->bindParam("idPaciente", $idPaciente);
		$stmt->bindParam("idMedico", $idMedico);
		$stmt->execute();
		if ( $stmt->rowCount() > 0 ) {
			return true;
		}else{
			return false;
		}	
		$db = null;
	} catch (PDOException $e){
		echo $e;
		return false;
	}
}

function cantidadDeCitasSinFinalizar($idPaciente,$idMedico){   
	try {  
		$db = getConnection();
		$sql   = "SELECT COUNT(idReserva) as cantidad
		FROM reserva 
		WHERE idPaciente=:idPaciente AND idMedico=:idMedico AND estado<4;";
		$stmt = $db->prepare($sql);
		$stmt->bindParam("idPaciente", $idPaciente);
		$stmt->bindParam("idMedico", $idMedico);
		$stmt->execute();
		if ( $stmt->rowCount() > 0 ) {
			$data  = $stmt->fetch(PDO::FETCH_ASSOC);
			$cantidad = trim($data['cantidad']);
			return $cantidad;
		}	
		$db = null;
	} catch (PDOException $e){
		echo $e;
		return -1;
	}
}

function eliminarPermisoHistorial($idReserva,$idMedico){   
	try {  
		$db = getConnection();
		$sql   = "SELECT idPaciente
		FROM reserva
		WHERE idReserva=:idReserva AND idMedico=:idMedico;";
		$stmt = $db->prepare($sql);
		$stmt->bindParam("idReserva", $idReserva);
		$stmt->bindParam("idMedico", $idMedico);
		$stmt->execute();
		if ( $stmt->rowCount() > 0 ) {
			$data  = $stmt->fetch(PDO::FETCH_ASSOC);
			$idPaciente = trim($data['idPaciente']);
			if(cantidadDeCitasSinFinalizar($idPaciente,$idMedico)>0)
			{
				return true;
			}else{
				$sql   = "UPDATE permisohistorial SET estado = 0 WHERE idPaciente=:idPaciente AND idMedico=:idMedico;";
 		        $stmt = $db->prepare($sql);
 		        $stmt->bindParam("idPaciente", $idPaciente);
		        $stmt->bindParam("idMedico", $idMedico);
				$stmt->execute();
				return true;
			}
		}else{
			return true;
		}	
		$db = null;
	} catch (PDOException $e){
		echo $e;
		return false;
	}
}

function eliminarPermisoHistorialPaciente($idReserva,$idPaciente){   
	try {  
		$db = getConnection();
		$sql   = "SELECT idMedico
		FROM reserva
		WHERE idReserva=:idReserva AND idPaciente=:idPaciente;";
		$stmt = $db->prepare($sql);
		$stmt->bindParam("idReserva", $idReserva);
		$stmt->bindParam("idPaciente", $idPaciente);
		$stmt->execute();
		if ( $stmt->rowCount() > 0 ) {
			$data  = $stmt->fetch(PDO::FETCH_ASSOC);
			$idMedico = trim($data['idMedico']);
			if(cantidadDeCitasSinFinalizar($idPaciente,$idMedico)>0)
			{
				return true;
			}else{
				$sql   = "UPDATE permisohistorial SET estado = 0 WHERE idPaciente=:idPaciente AND idMedico=:idMedico;";
 		        $stmt = $db->prepare($sql);
 		        $stmt->bindParam("idPaciente", $idPaciente);
		        $stmt->bindParam("idMedico", $idMedico);
				$stmt->execute();
				return true;
			}
		}else{
			return true;
		}	
		$db = null;
	} catch (PDOException $e){
		echo $e;
		return false;
	}
}

function getIdHorarioMedico($idMedico,$iddireccion,$idDia,$horaInicio){
	$sql = "SELECT idHorarioMedico FROM horariomedico WHERE idMedico=:idMedico AND iddireccion=:iddireccion AND idDia=:idDia AND horaInicio=:horaInicio;";
	try{
		$db   = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("idMedico",$idMedico);
		$stmt->bindParam("iddireccion",$iddireccion);
		$stmt->bindParam("idDia",$idDia);
		$stmt->bindParam("horaInicio",$horaInicio);
		$stmt->execute();
		if ( $stmt->rowCount() > 0 ) {
			$data  = $stmt->fetch(PDO::FETCH_ASSOC);
			$id = trim($data['idHorarioMedico']);
			return $id;
		}else{
			return 0;
		}
		$db = null;
	}catch(PDOException $e){
		return $e;
	}
}

function getMembresiaActiva($idMedico){
	$fecha = date("Y-m-d");
	$sql = "SELECT COUNT(idMembresiaMedico) as membresia
	FROM membresiamedico 
	WHERE idMedico=:idMedico AND estado=1 AND fechaFin > '$fecha' ;";
	try{
		$db   = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("idMedico",$idMedico);
		$stmt->execute();
		if ( $stmt->rowCount() > 0 ) {
			$data  = $stmt->fetch(PDO::FETCH_ASSOC);
			$id = trim($data['membresia']);
			if($id>0)
			{
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
		$db = null;
	}catch(PDOException $e){
		return $e;
	}
}

function validarCitaPorFecha($idPaciente,$idMedico, $fecha){   
	try {  
		$db = getConnection();
		$sql   = "SELECT idReserva FROM reserva
					WHERE idPaciente=:idPaciente AND idMedico=:idMedico AND fecha=:fecha AND estado<4;";
		$stmt = $db->prepare($sql);
		$stmt->bindParam("idPaciente", $idPaciente);
		$stmt->bindParam("idMedico", $idMedico);
		$stmt->bindParam("fecha", $fecha);
		$stmt->execute();
		if ( $stmt->rowCount() > 0 ) {
			return true;
		}else{
			return false;
		}	
		$db = null;
	} catch (PDOException $e){
		echo $e;
		return false;
	}
}


 ///////////////////////////////////////////////////////////////////
 ///////////////////////////////////////////////////////////////////
 ///////////////////////////////////////////////////////////////////
 ///////////////////////////////////////////////////////////////////


// //VERIFICACION DE EXISTENCIA DE CORREO ELECTRONICO
// function getVerificacionCorreoUsuarioApp($email){
// 	$sql = "SELECT idusuarioapp FROM usuarioapp WHERE correo=:email;";
// 	try{
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("email",$email);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getVerificacionCarnetUsuarioApp($carnet){
// 	$sql = "SELECT idusuarioapp FROM usuarioapp WHERE carnet=:carnet;";
// 	try{
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("carnet",$carnet);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getVerificacionCorreoProfesionalApp($email){

// 	$sql = "SELECT idusuarioprofesional FROM usuarioprofesional WHERE correo=:email;";
// 	try{
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("email",$email);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getVerificacionCarnetProfesionalApp($carnet){

// 	$sql = "SELECT idusuarioprofesional FROM usuarioprofesional WHERE carnet=:carnet;";
// 	try{
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("carnet",$carnet);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function sendMailConfirmarCuenta($email,$token){
// 	$link = "http://legalbo.com/verificacion.php?t=$token"; 
// 	$mensaje = file_get_contents("../template/verificar_registro.html");
// 	$mensaje = str_replace("{{CORREO}}", strtolower(trim($email)), $mensaje);
// 	$mensaje = str_replace("{{LIK}}", $link, $mensaje);
// 	$mensaje = str_replace("{{YEAR}}", date('Y'), $mensaje);
	
// 	$para = strtolower($email);
// 	$asunto = "Bienvenido, complete su registro en Legalbo"; 
// 	$mailPHP 	= new PHPMailer(); 
// 	//indico a la clase que use SMTP 
// 	$mailPHP->IsSMTP();
// 	//permite modo debug para ver mensajes de las cosas que van ocurriendo
// 	//$mailPHP->SMTPDebug = 2; 
// 	//Debo de hacer autenticaci?n SMTP
// 	$mailPHP->SMTPAuth = true;
// 	$mailPHP->SMTPSecure = "TLS";
// 	//indico el servidor de Gmail para SMTP
// 	$mailPHP->Host = "mail.smtp2go.com";
// 	//indico el puerto que usa Gmail
// 	$mailPHP->Port = 2525;
// 	//indico un usuario / clave de un usuario de gmail
// 	$mailPHP->Username = "";
// 	$mailPHP->Password = '';
// 	$mailPHP->SetFrom('', ''); 
// 			$mailPHP->Subject = utf8_decode($asunto);
// 	$mailPHP->MsgHTML($mensaje);
// 	//indico destinatario
// 	$address = $para;
// 	$mailPHP->AddAddress($address, strtolower(trim($email)));
// 	if(!$mailPHP->Send()) {
// 		return false;
// 	} else {
// 		return true;
// 	}
// }
// function setTokenapp($id){ 
// 	try {  
// 		$db = getConnection();
// 		$sql   = "UPDATE usuarioapp SET tokenapp=:tokenapp WHERE idusuarioapp=:idusuarioapp;";
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idusuarioapp", $id);
// 		$fecha = date("Y-m-d H:i:s");
// 		$token = sha1($fecha.$id); 
// 		$stmt->bindParam("tokenapp", $token);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return $token;;
// 		}else{
// 			return false; 
// 		}
		
// 		$db = null;
// 	} catch (PDOException $e){
// 		return false;
// 	}
// }
// function setTokenapp2($id){   
// 	try {  
// 		$db = getConnection();
// 		$sql   = "UPDATE usuarioprofesional SET tokenapp=:tokenapp WHERE idusuarioprofesional=:idusuarioprofesional;";
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idusuarioprofesional", $id);
// 		$fecha = date("Y-m-d H:i:s");
// 		$token = sha1($fecha.$id); 
// 		$stmt->bindParam("tokenapp", $token);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return $token;
// 		}else{
// 			return false; 
// 		}
		
// 		$db = null;
// 	} catch (PDOException $e){
// 		return false;
// 	}
// }
// function getValidateUserapp($id,$token){
// 	$sql = "SELECT idusuarioapp FROM usuarioapp WHERE idusuarioapp=:idusuarioapp AND tokenapp=:tokenapp;";
// 	try{
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idusuarioapp",$id);
// 		$stmt->bindParam("tokenapp",$token);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getValidateCobro($usuario,$secret_key){
// 	$sql = "SELECT idusuario_cobro FROM usuario_cobro WHERE username=:usuario AND secret_key=:secret_key;";
// 	try{
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("usuario",$usuario);
// 		$stmt->bindParam("secret_key",$secret_key);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$idusuario_cobro = trim($data['idusuario_cobro']);				
// 			}
// 			return $idusuario_cobro;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getValidateprofesionalapp($id,$token){
// 	$sql = "SELECT idusuarioprofesional FROM usuarioprofesional WHERE idusuarioprofesional=:idusuarioprofesional AND tokenapp=:tokenapp;";
// 	try{
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idusuarioprofesional",$id);
// 		$stmt->bindParam("tokenapp",$token);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getValidateRespiesta($id){
// 	$sql = "SELECT idpregunta FROM pregunta WHERE idpregunta=:idpregunta;";
// 	try{
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idpregunta",$id);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getValidateTomarPregunta($id){ 
// 	$sql = "SELECT estado FROM pregunta WHERE idpregunta=:idpregunta;";
// 	try{
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idpregunta",$id);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$estado = trim($data['estado']);				
// 			}
// 		}
// 		return $estado;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return 0;
// 	}
// }
// function getValidateCalificacion($id){
// 	$sql = "SELECT idpregunta FROM  pregunta WHERE estado = 0 AND idpregunta=:id;";
// 	try{
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function image($imgbase64){
// 	// requires php5
// 	define('UPLOAD_DIR', '../IMGUSERS/');
// 	$image_parts = explode(";base64,", $imgbase64);
//     $image_type_aux = explode("image/", $image_parts[0]);
//     $image_type = $image_type_aux[1];
//     $image_base64 = base64_decode($image_parts[1]);
// 	$file = UPLOAD_DIR . 'IMGUSER'.time(). '.png';
// 	$file1 = 'IMGUSER'.time(). '.png';
// 	$success = file_put_contents($file, $image_base64);
// 	if($success){
// 		return  $file1;
// 	}else{
// 		return false;
// 	}
// }

// function getCalificacionAbogado($id){
// 	try{
// 		$sql = "SELECT sum(cpe.calificacion) AS totalpuntaje,count(cpe.idpregunta) AS totalrespuestas 
// 					FROM calificacion_pregunta_express AS cpe INNER JOIN pregunta AS rp ON rp.idpregunta = cpe.idpregunta
// 					WHERE rp.idusuarioprofesional=:idprofesional;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idprofesional",$id);
// 		$stmt->execute();
// 		$total = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$totalpuntaje   	= trim($data['totalpuntaje']);
// 				$totalrespuestas 	= trim($data['totalrespuestas']);
// 				if($totalpuntaje>0){
// 					$total 	= $totalpuntaje/$totalrespuestas;
// 				}else{
// 					$total 	= 0;
// 				}
				
// 			}
// 		}
// 		$total = round($total, 2, PHP_ROUND_HALF_DOWN); 
// 		return $total;
// 		$db = null;
// 	}catch(PDOException $e){ 
// 		return 0;
// 	}
// }
// function idpr($id){
// 	try{
// 		$sql = "SELECT calificacion FROM calificacion_pregunta_express WHERE idpregunta=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		$calificacion = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$calificacion 	= trim($data['calificacion']);					
// 			}
// 		}
// 		return $calificacion;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return 0;
// 	}
// }
// function getCalificacionRespuesta2($id){
// 	try{
// 		$sql = "SELECT c.calificacion FROM calificacion_pregunta_express  AS c
// 					INNER JOIN pregunta AS r ON r.idpregunta = c.idpregunta
// 					WHERE r.idpregunta=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		$calificacion = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$calificacion 	= trim($data['calificacion']);					
// 			}
// 		}
// 		return $calificacion;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return 0;
// 	}
// }
// function getComentarioRespuesta($id){
// 	try{
// 		$sql = "SELECT comentario FROM calificacion_pregunta_express WHERE idpregunta=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		$comentario = '';
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$comentario 	= trim($data['comentario']);			
// 			}
// 		}
// 		return $comentario;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return 0;
// 	}
// }
// function getContestadasAbogado($id){ 
	 
// 	try{
// 		$sql = "SELECT count(idpregunta) AS totalrespuestas FROM pregunta WHERE idusuarioprofesional=:id and estado>4;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		$totalrespuestas = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$totalrespuestas 	= trim($data['totalrespuestas']);				
// 			}
// 		}
// 		return $totalrespuestas;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return 0;
// 	}
// }

// function getEspecialidadesProfesional_ligth($id){
	 
// 	try{
// 		$sql = "SELECT e.nombre,e.imagen
// 					FROM detalleprofesionalespecialidad AS d
// 				INNER JOIN especialidad AS e ON e.idespecialidad=d.idespecialidad WHERE d.idusuarioprofesional=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		$i = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				// $array[$i]['id'] 			= trim($data['idespecialidad']);
// 				$array[$i]['nombre'] 		= trim($data['nombre']);
// 				// $array[$i]['descripcion'] 	= trim($data['descripcion']);
// 				// $array[$i]['principal'] 	= trim($data['principal']);
// 				$array[$i]['imagen'] 		= PATHIMAGEN.trim($data['imagen']);
// 				$i++;
// 			}
// 			return $array;
// 		}else{
// 			$array = array();
// 			return $array;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		$array = array();
// 		return $array;
// 	}
// }
// function getExamenesProfesionalapp($id){
	 
// 	try{ 
// 		$sql = "SELECT MAX(i.calificacion) AS calificacion,i.idexamenpro,e.nombre,e.icono FROM intentoexamen AS i 
// 				INNER JOIN examenpro AS e ON e.idexamenpro = i.idexamenpro 
// 				WHERE i.idusuarioprofesional=:id GROUP BY  i.idexamenpro;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		$i = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$array[$i]['id'] 			= trim($data['idexamenpro']);
// 				$array[$i]['calificacion'] 	= trim($data['calificacion']);
// 				$array[$i]['nombre'] 		= trim($data['nombre']);
// 				$array[$i]['icono'] 		= PATHIMAGEN.trim($data['icono']);
// 				$i++;
// 			}
// 			return $array;
// 		}else{
// 			$array = array();
// 			return $array;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		$array = array();
// 		return $array;
// 	}
// }
// function getExamenesProfesionalapp_light($id){
	 
// 	try{ 
// 		$sql = "SELECT MAX(i.calificacion) AS calificacion,e.nombre,e.icono FROM intentoexamen AS i 
// 				INNER JOIN examenpro AS e ON e.idexamenpro = i.idexamenpro 
// 				WHERE i.idusuarioprofesional=:id GROUP BY  i.idexamenpro;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		$i = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				// $array[$i]['id'] 			= trim($data['idexamenpro']);
// 				// $array[$i]['calificacion'] 	= trim($data['calificacion']);
// 				$array[$i]['nombre'] 		= trim($data['nombre']);
// 				$array[$i]['icono'] 		= PATHIMAGEN.trim($data['icono']);
// 				$i++;
// 			}
// 			return $array;
// 		}else{
// 			$array = array();
// 			return $array;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		$array = array();
// 		return $array;
// 	}
// }
// function setAbogadoFavoritoVal($iduser,$idabogado){
// 	try{
// 		$sql = "SELECT idabogado_favorito FROM abogado_favorito WHERE idusuarioapp=:id AND idusuarioprofesional=:id2;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$iduser);
// 		$stmt->bindParam("id2",$idabogado);
// 		$stmt->execute();
// 		$totalrespuestas = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$idabogado_favorito 	= trim($data['idabogado_favorito']);				
// 			}
// 			$sql = "DELETE FROM abogado_favorito WHERE idabogado_favorito=:id";
// 			$db   = getConnection();
// 			$stmt = $db->prepare($sql);
// 			$stmt->bindParam("id",$idabogado_favorito);
// 			$stmt->execute();
// 		}else{
// 			$sql = "INSERT INTO abogado_favorito (idusuarioapp,idusuarioprofesional) VALUES (:id,:id2)";
// 			$db   = getConnection();
// 			$stmt = $db->prepare($sql);
// 			$stmt->bindParam("id",$iduser);
// 			$stmt->bindParam("id2",$idabogado);
// 			$stmt->execute();
// 		}
// 		return true;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }

// function getLastPreguntaAbogado($idabogado){
// 	try{
// 		/*$sql = "SELECT p.idpregunta,p.titulo,p.creacion,p.contenido
// 				FROM respuestapregunta AS r
// 				INNER JOIN pregunta AS p ON p.idpregunta =r.idpregunta
// 				WHERE r.idusuarioprofesional=:id  ORDER BY p.creacion DESC LIMIT 0,10;";*/
// 		$sql = "SELECT idpregunta,titulo,creacion,contenido
// 				FROM  pregunta 
// 				WHERE idusuarioprofesional=:id AND estado>3 ORDER BY creacion DESC LIMIT 0,10;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$idabogado);
// 		$stmt->execute();
// 		$array = array();
// 		$i = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$array[$i]['id_p'] 				= trim($data['idpregunta']);				
// 				$array[$i]['titulo_p'] 			= trim($data['titulo']);				
// 				$array[$i]['contenido_p'] 		= trim($data['contenido']);				
// 				$array[$i]['calificacion_p'] 	= getCalificacionRespuesta2($data['idpregunta']);				
// 				$array[$i]['fecha_p'] 			= trim($data['creacion']);	
// 				$i++;
// 			}
// 		}
// 		return $array;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return array();
// 	}
// }
// function getFirmaAbogado($idabogado){
// 	try{
// 		$sql = "SELECT d.idfirmas,f.nombre,f.logo
// 				FROM detallefirmaprofesional AS d INNER JOIN firma AS f ON f.idfirmas=d.idfirmas
// 				WHERE d.idusuarioprofesional=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$idabogado);
// 		$stmt->execute();
// 		$array = array();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$array['firma_id'] 		= trim($data['idfirmas']);				
// 				$array['firma_nombre'] 	= trim($data['nombre']);				
// 				$array['firma_logo'] 	= PATHIMAGEN.trim($data['logo']);				
// 			}
// 		}
// 		return $array;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return array();
// 	}
// }
// function getPlanAbogado($idabogado){
// 	try{
// 		$sql = "SELECT idplan FROM membresia WHERE :fecha <= fechavencimiento and idusuarioprofesional = :id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$fecha = date('Y-m-d');
// 		$stmt->bindParam("fecha",$fecha);
// 		$stmt->bindParam("id",$idabogado);
// 		$stmt->execute();
// 		$array = array();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				return	 trim($data['idplan']);									
// 			}
// 		}else{
// 			return 0;
// 		}
		
// 		$db = null;
// 	}catch(PDOException $e){
// 		return array();
// 	}
// }
// function getConfirmacionCalificacion($idrespuesta){
// 	try{
// 		$sql = "SELECT idcalificacion_pregunta_express FROM calificacion_pregunta_express WHERE idpregunta=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$idrespuesta);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }

// function getComentarios($id){ 
	 
// 	try{
// 		$sql = "SELECT count(c.idcalificacion_pregunta_express ) AS cantidad
// 			FROM calificacion_pregunta_express AS c INNER JOIN pregunta AS r ON r.idpregunta = c.idpregunta
// 			WHERE c.comentario IS NOT NULL AND c.comentario <> '' AND r.idusuarioprofesional=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		$cantidad = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$cantidad 	= trim($data['cantidad']);				
// 			}
// 		}
// 		return $cantidad;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return 0;
// 	}
// }
// function getComentarios2($id){ 
	 
// 	try{
// 		$sql = "SELECT c.idcalificacion_pregunta_express,c.comentario,c.calificacion
// 			FROM calificacion_pregunta_express AS c INNER JOIN pregunta AS r ON r.idpregunta = c.idpregunta
// 			WHERE c.comentario IS NOT NULL AND c.comentario <> '' AND r.idusuarioprofesional=:id  LIMIT 0,10;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		$i = 0;
// 		$array = array();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$array[$i]['id'] 			= trim($data['idcalificacion_pregunta_express']);
// 				$array[$i]['comentario'] 	= trim($data['comentario']);
// 				$array[$i]['calificacion'] 	= trim($data['calificacion']);
// 				$i++;			
// 			}
// 		}
// 		return $array;
// 		$db = null;
// 	}catch(PDOException $e){
// 		$array = array();
// 		return $array;
// 	}
// }

// function getVerficarRespuestaPregunta($id){
// 	try{
// 		$sql = "SELECT r.idpregunta,u.nombre,u.apellido,u.idusuarioprofesional
// 					FROM respuesta AS r
// 					INNER JOIN usuarioprofesional AS u ON u.idusuarioprofesional =r.idusuarioprofesional
// 					WHERE r.idpregunta=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		$array = array();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$array['idprofesional'] = trim($data['idusuarioprofesional']);				
// 				$array['p_nombre'] 		= trim($data['nombre']);				
// 				$array['p_apellido'] 	= trim($data['apellido']);				
// 			}
// 		}
// 		return $array;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return array();
// 	}
// }
// function getDetallesRespuestasPD($id){
// 	try{
// 		$sql = "SELECT r.idpregunta,u.nombre,u.apellido,u.idusuarioprofesional
// 					FROM pregunta AS r
// 					INNER JOIN usuarioprofesional AS u ON u.idusuarioprofesional =r.idusuarioprofesional
// 					WHERE r.idpregunta=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		$array = array();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$array['idprofesional'] = trim($data['idusuarioprofesional']);				
// 				$array['p_nombre'] 		= trim($data['nombre']);				
// 				$array['p_apellido'] 	= trim($data['apellido']);				
// 			}
// 		}
// 		return $array;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return array();
// 	}
// }
// function getConfigPDC(){
// 	try{
// 		$sql = "SELECT cantidadpreguntasdirectas FROM configuracion;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->execute();
// 		$array = array();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$cantidadpd = trim($data['cantidadpreguntasdirectas']);				
// 			}
// 		}
// 		return $cantidadpd; 
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getConfigPEC(){
// 	try{
// 		$sql = "SELECT cantidadpreguntaexpress FROM configuracion;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->execute();
// 		$array = array();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$cantidadpe = trim($data['cantidadpreguntaexpress']);				
// 			}
// 		}
// 		return $cantidadpe; 
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }

// function getConfigPDCUapp($id){
// 	try{
// 		$sql = "SELECT count(iddetalle_preguntadirecta) AS cantidad FROM detalle_preguntadirecta WHERE idpreguntadirecta=:id AND tipo_respuesta=1;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);		
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		$array = array();
// 		$cantidad = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$cantidad = trim($data['cantidad']);				
// 			}
// 		}
// 		return $cantidad;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getConfigPECUapp($id){
// 	try{
// 		$sql = "SELECT count(iddetallepreguntaexpress) AS cantidad FROM detalle_preguntaexpress WHERE idpregunta=:id AND tipo_respuesta=1;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);		
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		$array = array();
// 		$cantidad = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$cantidad = trim($data['cantidad']);				
// 			}
// 		}
// 		return $cantidad;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getConfigPDCUapp2($id){
// 	try{
// 		$sql = "SELECT count(iddetalle_preguntadirecta) AS cantidad FROM detalle_preguntadirecta WHERE idpreguntadirecta=:id AND tipo_respuesta=2;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);		
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		$array = array();
// 		$cantidad = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$cantidad = trim($data['cantidad']);				
// 			}
// 		}
// 		return $cantidad;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getConfigPECUapp2($id){
// 	try{
// 		$sql = "SELECT count(iddetallepreguntaexpress) AS cantidad FROM detalle_preguntaexpress WHERE idpregunta=:id AND tipo_respuesta=2;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);		
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		$array = array();
// 		$cantidad = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$cantidad = trim($data['cantidad']);				
// 			}
// 		}
// 		return $cantidad;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
 
// function getProfesionalInfo($id){
// 	try{
// 		$sql = "SELECT p.idpreguntadirecta,u.nombre,u.apellido,u.idusuarioprofesional FROM preguntadirecta AS p 
// 					INNER JOIN usuarioprofesional AS u ON u.idusuarioprofesional=p.idusuarioprofesional
// 					WHERE p.idpreguntadirecta=:id";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		$array = array();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$array['idprofesional'] = trim($data['idusuarioprofesional']);				
// 				$array['p_nombre'] 		= trim($data['nombre']);				
// 				$array['p_apellido'] 	= trim($data['apellido']);				
// 			}
// 		}
// 		return $array;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return array();
// 	}
// }
// function getProfesionalInfo2($id){
// 	try{
// 		$sql = "SELECT nombre,apellido,idusuarioprofesional,fotopersonal FROM  usuarioprofesional 
// 					WHERE idusuarioprofesional=:id";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		$array = array();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$array['idprofesional'] = trim($data['idusuarioprofesional']);				
// 				$array['p_nombre'] 		= trim($data['nombre']);				
// 				$array['p_apellido'] 	= trim($data['apellido']);				
// 				$array['p_foto'] 		= PATHIMGPROFILE.trim($data['fotopersonal']);
// 				$array['calif_prof'] 	= getCalificacionAbogado($data['idusuarioprofesional']);					
// 			}
// 		}
// 		return $array;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return array();
// 	}
// }
// function getDetallePreguntaDirecta($idpregunta){
// 	try{
// 		$sql = "SELECT iddetalle_preguntadirecta,contenido,datetime,tipo_respuesta FROM detalle_preguntadirecta 
// 					WHERE idpreguntadirecta=:id";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$idpregunta);
// 		$stmt->execute();
// 		$array = array();
// 		$i = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$array[$i]['iddetalle'] = trim($data['iddetalle_preguntadirecta']);				
// 				$array[$i]['contenido'] = trim($data['contenido']);				
// 				$array[$i]['datetime'] 	= trim($data['datetime']);
// 				if($data['tipo_respuesta'] == 2){
// 					$array[$i]['profesional_info'] 	= getProfesionalInfo($idpregunta);
// 				}
// 				$i++;
// 			}
// 		}
// 		return $array;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return array();
// 	}	
// }
// function getDetalleExpress($idpregunta){
// 	try{
// 		$sql = "SELECT iddetallepreguntaexpress,contenido,datetime,tipo_respuesta,id,idpregunta FROM detalle_preguntaexpress 
// 					WHERE idpregunta=:id";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$idpregunta);
// 		$stmt->execute();
// 		$array = array();
// 		$i = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$array[$i]['iddetalle'] = trim($data['iddetallepreguntaexpress']);				
// 				$array[$i]['contenido'] = trim($data['contenido']);				
// 				$array[$i]['datetime'] 	= trim($data['datetime']);
// 				$array[$i]['id'] 	= trim($data['id']);
// 				if($data['tipo_respuesta'] == 2){
// 					$array[$i]['profesional_info'] 	= getProfesionalInfo2($data['id']);
// 				}
// 				$i++;
// 			}
// 		}
// 		return $array;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return array();
// 	}	
// }
// function getDetalleRespuestaCotizacion($idcotizacion){
// 	try{
// 		$sql = "SELECT idrespuesta_cotizacion,contenido,precio FROM respuesta_cotizacion WHERE idcotizacion=:id";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$idcotizacion);
// 		$stmt->execute();
// 		$array = array();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$array['iddetalle'] = trim($data['idrespuesta_cotizacion']);				
// 				$array['contenido'] = trim($data['contenido']);				
// 				$array['precio'] 	= trim($data['precio']);
// 			}
// 		}
// 		return $array;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return array();
// 	}	
// }

// function getValidateCotizacionRespuesta($id){
// 	$sql = "SELECT idrespuesta_cotizacion FROM respuesta_cotizacion WHERE idcotizacion=:id;";
// 	try{
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }

// function getPlanProfesional($id){ 
// 	try{
// 		$sql = "SELECT idmembresia,fechacompra,fechavencimiento,idplan,nombreplan,especialidades,ubicacion,
// 				creacion,biblioteca FROM plamprofesional WHERE idusuarioprofesional=:id AND fechavencimiento >= :date";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$id);
// 		$date = date('Y-m-d');
// 		$stmt->bindParam("date",$date);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$array['id'] 				= trim($data['idmembresia']);
// 				$array['fecha_compra'] 		= trim($data['fechacompra']);
// 				$array['fecha_expiracion'] 	= trim($data['fechavencimiento']);
// 				$array['id_plan'] 			= trim($data['idplan']);
// 				$array['plan'] 				= trim($data['nombreplan']);
// 				// $array['n_especialidades'] 	= trim($data['especialidades']);
// 				// $array['geolocalizacion'] 	= trim($data['ubicacion']);
// 				// $array['documentacion'] 	= trim($data['biblioteca']);
// 				$array['fecha_creacion'] 	= trim($data['creacion']);
// 			}
// 		}else{
// 			$array['id'] 				= Null;
// 			$array['fecha_compra'] 		= Null;
// 			$array['fecha_expiracion'] 	= Null;
// 			$array['id_plan'] 			= Null;
// 			$array['plan'] 				= 'FREE'; 
// 			// $array['n_especialidades'] 	= 0;
// 			// $array['geolocalizacion'] 	= 0;
// 			// $array['documentacion'] 	= 0;
// 			$array['fecha_creacion'] 	= Null;
			
// 		}
// 		return $array;
// 		$db = null;
// 	}catch(PDOException $e){
// 		$array = array();
// 		return $array;
// 	}
// }
 
// function setActualizarestadopreguntadirecta($id){   
// 	try {  
// 		$db = getConnection();
// 		$sql   = "UPDATE preguntadirecta SET estado=5 WHERE idpreguntadirecta=:id";
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id", $id);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false; 
// 		}
		
// 		$db = null;
// 	} catch (PDOException $e){
// 		return false;
// 	}
// } 
// function setActualizarestadopreguntadirecta2($id,$estado){   
// 	try {  
// 		$db = getConnection();
// 		$sql   = "UPDATE preguntadirecta SET estado=:estado WHERE idpreguntadirecta=:id";
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id", $id);
// 		$stmt->bindParam("estado", $estado);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false; 
// 		}
		
// 		$db = null;
// 	} catch (PDOException $e){
// 		return false;
// 	}
// } 
// function setActualizarProfesionalPE($idprofesional,$idpreguntae){   
// 	try {  
// 		$db = getConnection();
// 		$sql   = "UPDATE pregunta SET idusuarioprofesional=:idusuarioprofesional WHERE idpregunta=:idpreguntae";
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idusuarioprofesional", $idprofesional);
// 		$stmt->bindParam("idpreguntae", $idpreguntae);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false; 
// 		}
		
// 		$db = null;
// 	} catch (PDOException $e){
// 		return false;
// 	}
// } 
// function setActualizarestadopreguntaexpress($id){   
// 	try {  
// 		$db = getConnection();
// 		$sql   = "UPDATE pregunta SET estado=5 WHERE idpregunta=:id";
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id", $id);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false; 
// 		}
		
// 		$db = null;
// 	} catch (PDOException $e){
// 		return false;
// 	}
// }
// function setProfesionalAcreditacionPreguntaExpress($idpregunta,$idprofesional){
// 		$valorpregunta 	= getExpressvalorreservado($idpregunta);
// 		$procentaje 	= getProcentajeGananciaLegalappPE();
// 		if($procentaje){
// 			if($procentaje>0){
// 				$ganancialp 	= ($valorpregunta * ($procentaje/100));
// 				$gananciapro 	=  $valorpregunta - $ganancialp;
// 			}else{
// 				$procentaje 	= 1;
// 				$ganancialp 	= 0;
// 				$gananciapro	= $valorpregunta;
// 			}
// 		}else{
// 			$procentaje 	= 0;
// 			$ganancialp 	= 0;
// 			$gananciapro	= $valorpregunta;
// 		}
		
// 		setLogProfesionalApp($idprofesional,$gananciapro,1,$idpregunta); 
// 		setLogLegalApp($ganancialp,1,$idpregunta,$procentaje,$valorpregunta); 
// 		setActualizarGananciaProfesional($idprofesional,$gananciapro);
// }
// function setActualizarestadopreguntaexpress2($id,$estado){   
// 	try {  
// 		$db = getConnection();
// 		$sql   = "UPDATE pregunta SET estado=:estado,datetimerespuesta= now() WHERE idpregunta=:id";
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id", $id);
// 		$stmt->bindParam("estado", $estado);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false; 
// 		}
		
// 		$db = null;
// 	} catch (PDOException $e){
// 		return false;
// 	}
// } 
// function getValidatePreguntaCompleja($idprofesional,$idpreguntae){
// 	$sql = "SELECT idlogpreguntacompleja FROM logpreguntacompleja WHERE idusuarioprofesional=:idprofesional AND idpregunta=:idpreguntae";
// 	try{
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idprofesional",$idprofesional);
// 		$stmt->bindParam("idpreguntae",$idpreguntae);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getIntentoExamen($idprofesional,$idexamen){
// 	try{ 
// 		$sql = "SELECT idintentoexamen,creacion,calificacion FROM intentoexamen 
// 					WHERE idusuarioprofesional=:idprofesional AND idexamenpro=:idexamen 
// 					AND calificacion=(SELECT MAX( calificacion ) FROM intentoexamen WHERE  idusuarioprofesional=:idprofesional2 AND idexamenpro=:idexamen2);";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idprofesional",$idprofesional);
// 		$stmt->bindParam("idprofesional2",$idprofesional);
// 		$stmt->bindParam("idexamen",$idexamen);
// 		$stmt->bindParam("idexamen2",$idexamen);
// 		$stmt->execute();
// 		$array = array();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$array['idintento'] 		= trim($data['idintentoexamen']);				
// 				$array['nota_obtenida'] 	= trim($data['calificacion']);				
// 				$array['fecha_intento'] 	= trim($data['creacion']);				
// 			}
// 		}
// 		return $array;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return array();
// 	}
// }

// function getValidatePreguntaIntentoExamen($idintento,$idpregunta){
// 	$sql = "SELECT idrespuestasexamenpro FROM respuestasexamenpro WHERE idintentoexamen=:idintento AND idpreguntasexamen=:idpregunta;";
// 	try{
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idintento",$idintento);
// 		$stmt->bindParam("idpregunta",$idpregunta);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }

// function getRespuestaPreguntaExamen($idpregunta){
// 	try{
// 		$sql = "SELECT respuesta FROM preguntasexamen WHERE idpreguntasexamen = :idpregunta;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idpregunta",$idpregunta);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$respuesta = trim($data['respuesta']);
// 			}
// 		}
// 		return $respuesta;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}	
// }
// function getDetallePreguntas($idexamen){
// 	try{ 
// 		$sql = "SELECT idpreguntasexamen,pregunta,orden FROM preguntasexamen WHERE idexamenpro=:idexamen ORDER BY orden DESC";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idexamen",$idexamen);
// 		$stmt->execute();
// 		$array = array();
// 		$i = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$array[$i]['idpregunta'] 	= trim($data['idpreguntasexamen']);				
// 				$array[$i]['pregunta'] 		= trim($data['pregunta']);				
// 				$array[$i]['orden'] 		= trim($data['orden']);	
// 				$i++;
// 			}
// 		}
// 		return $array;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return array();
// 	}
// }

// function getExpressPendientes(){ 
	 
// 	try{
// 		$sql = "SELECT count(idpregunta) AS cantidad FROM pregunta  WHERE estado=2;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->execute();
// 		$cantidad = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$cantidad 	= trim($data['cantidad']);				
// 			}
// 		}
// 		return $cantidad;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return 0;
// 	}
// }
// function getExpressvalorreservado($idpreguntae){ 
	 
// 	try{
// 		$sql = "SELECT cantidad FROM log_debito_usuarioapp WHERE idpreguntae=:idpreguntae;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idpreguntae",$idpreguntae);
// 		$stmt->execute();
// 		$cantidad = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$cantidad 	= trim($data['cantidad']);				
// 			}
// 		}
// 		return $cantidad;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return 0;
// 	}
// }

// function getReplicasPendientes($id){ 
	 
// 	try{
// 		$sql = "SELECT count(idpregunta) AS cantidad FROM pregunta  WHERE (estado=4 OR estado=6) AND idusuarioprofesional=:id; ;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		$cantidad = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$cantidad 	= trim($data['cantidad']);
				
// 				// $sql2 = "SELECT count(idpreguntadirecta) AS cantidad FROM preguntadirecta WHERE (estado=6) AND idusuarioprofesional=:id; ;";
// 				// $db   = getConnection();
// 				// $stmt2 = $db2->prepare($sql2);
// 				// $stmt2->bindParam("id",$id);
// 				// $stmt2->execute();
// 				// $cantidad2 = 0;
// 				// if ( $stmt2->rowCount() > 0 ) {
// 				// 	while ($data2  = $stmt2->fetch(PDO::FETCH_ASSOC)){
// 				// 		$cantidad2 	= trim($data['cantidad']);				
// 				// 	}
// 				// }				
// 			}
// 		}
// 		return $cantidad;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return 0;
// 	}
// }

// function getDirectasPendientes($id){ 
	 
// 	try{
// 		$sql = "SELECT count(idpreguntadirecta) AS cantidad FROM preguntadirecta  WHERE (estado=2 OR estado=6) AND idusuarioprofesional=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		$cantidad = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$cantidad 	= trim($data['cantidad']);				
// 			}
// 		}
// 		return $cantidad;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return 0;
// 	}
// }

// function getCotizacionPendientes($id){ 
	 
// 	try{
// 		$sql = "SELECT count(idcotizaciones) AS cantidad FROM cotizacionusuario  WHERE estado=0 AND idusuarioprofesional=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$id);
// 		$stmt->execute();
// 		$cantidad = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$cantidad 	= trim($data['cantidad']);				
// 			}
// 		}
// 		return $cantidad;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return 0;
// 	}
// }

 function getverificardesp($idPaciente,$idEnfermedad){ 
	 
 	try{
 		$sql = "SELECT iddetalleprofesionalespecialidad FROM detalleprofesionalespecialidad WHERE  idusuarioprofesional=:idusuarioprofesional AND idespecialidad=:idespecialidad;";
 		$db   = getConnection();
 		$stmt = $db->prepare($sql);
 		$stmt->bindParam("idusuarioprofesional",$idprofesional);
 		$stmt->bindParam("idespecialidad",$idespecialidad);
 		$stmt->execute();
 		$cantidad = 0;
 		if ( $stmt->rowCount() > 0 ) {
 			return true;
 		}else{
 			return false;
 		}
 		$db = null;
 	}catch(PDOException $e){
 		return false;
 	}
 }

// function setEspecificaciones($idprofesional,$idespecialidad,$principal){   
// 	try {  
// 		$db = getConnection();
// 		$sql   = "INSERT INTO detalleprofesionalespecialidad (idusuarioprofesional,idespecialidad,principal) VALUES (:idusuarioprofesional,:idespecialidad,:principal);";
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idusuarioprofesional", $idprofesional);
// 		$stmt->bindParam("idespecialidad", $idespecialidad);
// 		$stmt->bindParam("principal", $principal);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false; 
// 		}
		
// 		$db = null;
// 	} catch (PDOException $e){
// 		return false;
// 	}
// } 

 function deleteEnfermedadUsuario($idPaciente){   
 	try {  
 		$db = getConnection();
 		$sql   = "DELETE FROM enfermedadpaciente WHERE idPaciente=:idPaciente;";
 		$stmt = $db->prepare($sql);
 		$stmt->bindParam("idPaciente", $idPaciente);
 		$stmt->execute();
 		if ( $stmt->rowCount() > 0 ) {
 			return true;
 		}else{
 			return false; 
 		}
		
 		$db = null;
 	} catch (PDOException $e){
 		return false;
 	}
 } 


function validarcuentaexistente($correo){ 
	try{
		$db = getConnection();
		$sql   = "SELECT * FROM paciente WHERE correo=:correo";
		$stmt = $db->prepare($sql);
		$correo 	=  strtolower(trim($correo));
		$stmt->bindParam("correo",$correo);
		$stmt->execute();
		if ( $stmt->rowCount() > 0 ) {				
			while ($response  = $stmt->fetch(PDO::FETCH_ASSOC)){
					
						$id			  			= trim($response['idPaciente']);
						$arrUser['idPaciente']   = trim($response['idPaciente']);
						$arrUser['u_nombre'] 	= trim($response['nombre']);
						$arrUser['u_apellido'] 	= trim($response['apellido']);
						$arrUser['u_correo'] 	= trim($response['correo']);
						$result = mb_substr($response['imagen'], 0, 5);
						if( $response['imagen'] == ''){
							$arrUser['u_imagen'] 	= trim($response['imagen']);
						}else if( $result == 'https'){
							$arrUser['u_imagen'] 	=  trim($response['imagen']);							
						}else{
							$arrUser['u_imagen'] 	= PATHIMAGEN.trim($response['imagen']);							
						}
						$arrUser['u_estado']	= trim($response['estado']);
						// $token 					= setTokenapp($id); 
						$arrUser['token_session']= trim($response['token']);
			}			
			
		}else{
			$arrUser = false;
		}
		return $arrUser;
		$db = null;
	}catch(PDOException $e){
		return false;
	}
}

function validarcuentaexistenteMedico($correo){ 
	try{
		$db = getConnection();
		$sql   = "SELECT * FROM medico WHERE correo=:correo";
		$stmt = $db->prepare($sql);
		$correo 	=  strtolower(trim($correo));
		$stmt->bindParam("correo",$correo);
		$stmt->execute();
		if ( $stmt->rowCount() > 0 ) {				
			while ($response  = $stmt->fetch(PDO::FETCH_ASSOC)){
						$id			  			= trim($response['idMedico']);
						$arrUser['idMedico']   = trim($response['idMedico']);
						$arrUser['m_nombre'] 	= trim($response['nombre']);
						$arrUser['m_apellido'] 	= trim($response['apellido']);
						$arrUser['m_correo'] 	= trim($response['correo']);
						$arrUser['m_estado']	= trim($response['estado']);
						$arrUser['m_membresiaActiva']	= getMembresiaActiva($id);
						$token 					= setTokenapp2($id); 
						$arrUser['token_session']= trim($token);
						$arrUser['tokenpush']	= trim($response['tokenpush']);

						$result = mb_substr($response['imagen'], 0, 5);
						if( $response['imagen'] == ''){
							$arrUser['m_imagen'] 	= trim($response['imagen']);
						}else if( $result == 'https'){
							$arrUser['m_imagen'] 	=  trim($response['imagen']);							
						}else{
							$arrUser['m_imagen'] 	= PATHIMAGEN.trim($response['imagen']);							
						}

			}			
			
		}else{
			$arrUser = false;
		}
		return $arrUser;
		$db = null;
	}catch(PDOException $e){
		return false;
	}
}


// function setRegistroUsuarioApp2($data){ 
// 	try { 
// 		$vcorreo 	= getVerificacionCorreoUsuarioApp(strtolower(trim($data['correo'])));
// 		if(!$vcorreo){
// 			$db = getConnection();
// 			$sql   = "INSERT INTO usuarioapp (nombre,apellido,correo,foto,tokenfacebook,tokenapp,idestadousuario,carnet) 
// 						VALUES (:nombre,:apellido,:correo,:foto,:tokenfacebook,:tokenapp,2,0);";
// 			$stmt = $db->prepare($sql);
// 			$stmt->bindParam("nombre", $data['nombre']);
// 			$stmt->bindParam("apellido", $data['apellido']);
// 			$stmt->bindParam("foto", $data['foto']);
// 			$correo 	=  strtolower(trim($data['correo']));
// 			$stmt->bindParam("correo",$correo);
// 			$stmt->bindParam("tokenfacebook", $data['tokenfacebook']);
// 			$fecha = date("Y-m-d H:i:s");
// 			$token = sha1($fecha.$data['correo']); 
// 			$stmt->bindParam("tokenapp", $token);
// 			$stmt->execute();
// 			if ( $stmt->rowCount() > 0 ) {
// 				$resultmail = true;//sendMailConfirmarCuenta($email,$token);
// 				if(!$resultmail) {
// 					return  	'{
// 								"errorCode": 6,
// 								"errorMessage": "Error para enviar correo.",
// 								"msg": ""
// 							}';
// 				} else {
// 					$validacion = validarcuentaexistente($data['correo']);
// 					return  '{
// 								"errorCode": 0,
// 								"errorMessage": "Servicio ejecutado con exito.",
// 								"msg": '.json_encode($validacion).'
// 							}';
// 				}
// 			}else{
// 				return '{
// 						"errorCode": 2,
// 						"errorMessage": "Error al ejecutar el servicio web.",
// 						"msg": ""
// 					}'; 
// 			}
// 		}else{
// 			return  	'{
// 						"errorCode": 1,
// 						"errorMessage": "Correo ya registrado.",
// 						"msg": ""
// 					}';	
// 		}
// 		$db = null;
// 	} catch (PDOException $e){
// 		$db = null;
// 		return  	'{
// 					"errorCode": 2,
// 					"errorMessage": "Error al ejecutar el servicio web.",
// 					"msg": ""
// 				}'; 
// 	}
// }

// function cancelartransaccionespasadas($idusuario){   
// 	try {  
// 		$db = getConnection();
// 		$sql   = "UPDATE  logtransaccionalapp SET estado=0 WHERE idusuarioapp=:idusuarioapp;";
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idusuarioapp", $idusuario);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false; 
// 		}
		
// 		$db = null;
// 	} catch (PDOException $e){
// 		return false;
// 	}
// } 
// function cancelartransaccionespasadasp($idprofesional){   
// 	try {  
// 		$db = getConnection();
// 		$sql   = "UPDATE  logtransaccionalprofesional SET estado=0 WHERE idusuarioprofesional=:idusuarioprofesional;";
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idusuarioprofesional", $idprofesional);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false; 
// 		}
		
// 		$db = null;
// 	} catch (PDOException $e){
// 		return false;
// 	}
// } 

// /*function validarsaldousuarioapp($id_transac){ 
// 	try{
// 		$db = getConnection();
// 		$sql   = "SELECT idusuarioapp,idplan,valortransac FROM logtransaccionalapp WHERE idlogtransaccionalapp=:id_transac";
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id_transac",$id_transac);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {				
// 			while ($response  = $stmt->fetch(PDO::FETCH_ASSOC)){					
// 				$idusuarioapp	= trim($response['idusuarioapp']);
// 				$idplan			= trim($response['idplan']);
// 				$valor			= trim($response['valortransac']);
// 				$sql2   = "SELECT costo FROM plan WHERE idplan=:idplan";
// 				$stmt2 = $db->prepare($sql2);
// 				$stmt2->bindParam("idplan",$idplan);
// 				$stmt2->execute();
// 				if ( $stmt2->rowCount() > 0 ) {				
// 					while ($response2  = $stmt2->fetch(PDO::FETCH_ASSOC)){					
// 						$valor		= trim($response2['costo']);
// 					}							
// 				}
// 				$sql3   = "UPDATE  usuarioapp SET saldo = saldo + :valor WHERE idusuarioapp=:idusuarioapp;";
// 				$stmt3 = $db->prepare($sql3);
// 				$stmt3->bindParam("valor", $valor);
// 				$stmt3->bindParam("idusuarioapp", $idusuarioapp);
// 				$stmt3->execute();
// 			}			
			
// 		}
// 		return true;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }*/

// function validarsaldousuarioapp($valor_transac_directa,$valor_transac_express,$idusuarioapp,$idtransaccion){ 
// 	try{
// 		$db = getConnection();
// 		if($valor_transac_directa > 0){
// 			$sql   = "UPDATE  usuarioapp SET saldo = saldo + :valor WHERE idusuarioapp=:idusuarioapp;";
// 			$stmt = $db->prepare($sql);
// 			$stmt->bindParam("valor", $valor_transac_directa);
// 			$stmt->bindParam("idusuarioapp", $idusuarioapp);
// 			$stmt->execute();
// 		}
// 		if($valor_transac_express > 0 ){
// 			$sql 	= "SELECT idpregunta FROM pregunta WHERE estadopago=0 AND estado=1 AND creacion BETWEEN CURDATE()- INTERVAL 7 DAY and CURDATE() + INTERVAL 1 DAY ORDER BY creacion ASC;";
// 			$db   	= getConnection();
// 			$stmt2 	= $db->prepare($sql);
// 			$stmt2->bindParam("id",$id);
// 			$stmt2->execute();
// 			$totalacumulado =  $valor_transac_express;
// 			$precio_p 		= getPrecioPreguntaExpress();
// 			if ( $stmt2->rowCount() > 0 ) {
// 				while ($data  = $stmt2->fetch(PDO::FETCH_ASSOC)){
// 					$idpregunta 	= trim($data['idpregunta']);
// 					if($totalacumulado >= $precio_p){						
// 						$sql   = "UPDATE  pregunta SET estadopago = 1, estado = 2, datetimepago = now() WHERE idpregunta=:idpregunta;";
// 						$stmt3 = $db->prepare($sql);
// 						$stmt3->bindParam("idpregunta", $idpregunta);
// 						$stmt3->execute();
// 						setLogDebitoUsuario($idusuarioapp,$precio_p,$idpregunta);
// 						setLogTrasaccPregunta($idtransaccion,$idpregunta,$precio_p);
// 						$totalacumulado = $totalacumulado - $precio_p; 
// 					}
					
// 				}
// 			}
// 			if($totalacumulado > 0){
// 				$sql   = "UPDATE  usuarioapp SET saldo = saldo + :valor WHERE idusuarioapp=:idusuarioapp;";
// 				$stmt = $db->prepare($sql);
// 				$stmt->bindParam("valor", $totalacumulado);
// 				$stmt->bindParam("idusuarioapp", $idusuarioapp);
// 				$stmt->execute();
// 			}
// 		}		
// 		return true;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }


// function getestadopago($idpregunta){ 
	 
// 	try{
// 		$sql = "SELECT estadopago FROM pregunta WHERE idpregunta=:id";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$idpregunta);
// 		$stmt->execute();
// 		$cantidad = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$pago_usuario     = trim($data2['estadopago']);
// 				if($pago_usuario == 1){
// 					return 1;
// 				}else{
// 					return 0;
// 				}
// 			}
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getestadoPreguntaExpress($idpregunta){ 
	 
// 	try{
// 		$sql = "SELECT estado FROM pregunta WHERE idpregunta=:id";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$idpregunta);
// 		$stmt->execute();
// 		$cantidad = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				return trim($data2['estado']);
				
// 			}
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getPrecioPreguntaExpress(){ 
	 
// 	try{
// 		$sql = "SELECT preciopregunta FROM configuracion WHERE idconfiguracion=1;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->execute();
// 		$cantidad = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$precio     = trim($data2['preciopregunta']);
// 				return $precio;
// 			}
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }

// function getSaldoUsuario($iduserapp){ 
	 
// 	try{
// 		$sql = "SELECT saldo FROM usuarioapp WHERE idusuarioapp=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$iduserapp);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$saldo     = trim($data2['saldo']);
// 				return $saldo;
// 			}
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }


// function setActualizarSaldo($iduserapp,$descuento){ 
	 
// 	try{
// 		$sql = "UPDATE usuarioapp SET  saldo = (saldo-:descuento) WHERE idusuarioapp=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$iduserapp);
// 		$stmt->bindParam("descuento",$descuento);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }

// function setActualizarSaldoPositivo($iduserapp,$aumento){ 
	 
// 	try{
// 		$sql = "UPDATE usuarioapp SET  saldo = (saldo+:aumento) WHERE idusuarioapp=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$iduserapp);
// 		$stmt->bindParam("aumento",$aumento);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// } 

// function setLogProfesionalApp($idprofesional,$precio_p,$tipo,$idpregunta){ 
	 
// 	try{
// 		$sql = "INSERT INTO log_ganancia_profesional (idusuarioprofesional,tipo,idpregunta,valor) VALUES (:idusuarioprofesional,:tipo,:idpregunta,:valor);";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idusuarioprofesional",$idprofesional);
// 		$stmt->bindParam("valor",$precio_p);
// 		$stmt->bindParam("tipo",$tipo);
// 		$stmt->bindParam("idpregunta",$idpregunta);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function setActualizarGananciaProfesional($idprofesional,$ganancia){ 
	 
// 	try{
// 		$sql = "UPDATE usuarioprofesional SET  ganancia = (ganancia+:ganancia) WHERE idusuarioprofesional=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$idprofesional);
// 		$stmt->bindParam("ganancia",$ganancia);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }

// function setActualizarEstadoPagoPregunta($idpregunta){ 
	 
// 	try{
// 		$sql = "UPDATE pregunta SET estadopago =1 WHERE idpregunta=:idpreguntae;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idpreguntae",$idpregunta);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }

// function setActualizarEstadoPagoPreguntaD($idpregunta){ 
	 
// 	try{
// 		$sql = "UPDATE preguntadirecta SET pago_usuario =1 WHERE idpreguntadirecta=:idpreguntadirecta;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idpreguntadirecta",$idpregunta);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function setActualizarviewpreguntadirectaU($idpregunta,$estado){ 
	 
// 	try{
// 		$sql = "UPDATE preguntadirecta SET view_user =:estado WHERE idpreguntadirecta=:idpreguntadirecta;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idpreguntadirecta",$idpregunta);
// 		$stmt->bindParam("estado",$estado);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function setActualizarviewpreguntaexpressU($idpregunta,$estado){ 
	 
// 	try{
// 		$sql = "UPDATE pregunta SET view_user =:estado WHERE idpregunta=:idpregunta;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idpregunta",$idpregunta);
// 		$stmt->bindParam("estado",$estado);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function setActualizarviewpreguntadirectaU2($idpregunta){ 
	 
// 	try{
// 		$sql = "UPDATE preguntadirecta SET view_user =0 WHERE idpreguntadirecta=:idpreguntadirecta;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idpreguntadirecta",$idpregunta);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function setActualizarviewpreguntadirectaP($idpregunta,$estado){ 
	 
// 	try{
// 		$sql = "UPDATE preguntadirecta SET  view_prof =:estado WHERE idpreguntadirecta=:idpreguntadirecta;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idpreguntadirecta",$idpregunta);
// 		$stmt->bindParam("estado",$estado);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function setActualizarviewpreguntaexpressP($idpregunta,$estado){ 
	 
// 	try{
// 		$sql = "UPDATE pregunta SET view_prof =:estado WHERE idpregunta=:idpregunta;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idpregunta",$idpregunta);
// 		$stmt->bindParam("estado",$estado);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function setActualizarviewpreguntadirectaP2($idpregunta){ 
	 
// 	try{
// 		$sql = "UPDATE preguntadirecta SET view_prof =0 WHERE idpreguntadirecta=:idpreguntadirecta;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idpreguntadirecta",$idpregunta);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function setLogDebitoUsuario($iduserapp,$descuento,$idpregunta){ 
	 
// 	try{
// 		$sql = "INSERT INTO log_debito_usuarioapp (idusuarioapp,idpreguntae,cantidad) VALUES (:idusuarioapp,:idpreguntae,:cantidad);";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idusuarioapp",$iduserapp);
// 		$stmt->bindParam("idpreguntae",$idpregunta);
// 		$stmt->bindParam("cantidad",$descuento);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){ 
// 		return false;
// 	}
// }
// function setLogTrasaccPregunta($idtrasaccion,$idpregunta,$precio_p){ 	 
// 	try{
// 		$sql = "INSERT INTO logtrasaccionpregunta (idpregunta,idtransaccion,precio_p) VALUES (:idpreguntae,:idtransaccion,:precio_p);";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idtransaccion",$idtrasaccion);
// 		$stmt->bindParam("idpreguntae",$idpregunta);
// 		$stmt->bindParam("precio_p",$precio_p);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){ 
// 		return false;
// 	}
// }
// function setLogDebitoUsuarioPD($iduserapp,$descuento,$idpregunta){ 
	 
// 	try{
// 		$sql = "INSERT INTO log_debito_usuarioapp_pd (idusuarioapp,idpreguntadirecta,valor) VALUES (:idusuarioapp,:idpreguntadirecta,:valor);";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idusuarioapp",$iduserapp);
// 		$stmt->bindParam("idpreguntadirecta",$idpregunta);
// 		$stmt->bindParam("valor",$descuento);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }


// function getPreguntasExpressPendientesPago($iduserapp){ 
	 
// 	try{
// 		$sql = "SELECT count(dpregunta) AS cantidad FROM pregunta
// 					WHERE estadopago = 0 AND p.idusuarioApp=:id";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$iduserapp);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$cantidad     = trim($data2['cantidad']);
// 				return $cantidad;
// 			}
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }

// function getPreguntasDirectasPendientesPago($iduserapp){ 
	 
// 	try{
// 		$sql = "SELECT count(idpreguntadirecta) AS cantidad FROM preguntadirecta WHERE idusuarioapp=:id AND pago_usuario=0;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$iduserapp);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$cantidad     = trim($data2['cantidad']);
// 				return $cantidad;
// 			}
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }

// function getTotalPagosPreguntasDirectas($iduserapp){ 
	 
// 	try{
// 		$sql = "SELECT idusuarioprofesional FROM preguntadirecta WHERE idusuarioapp=:id AND pago_usuario=0;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$iduserapp);
// 		$stmt->execute();
// 		$cantidad = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$cantidad     = $cantidad + getPrecioPreguntaExpressProfesional(trim($data2['idusuarioprofesional']));				
// 			}
// 			return $cantidad;
// 		}else{
// 			return 0;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getDetallePagosPreguntasDirectas($iduserapp){ 
	 
// 	try{
// 		$sql = "SELECT p.idusuarioprofesional, DATE_FORMAT(p.creacionPreguntaDir, '%d/%l/%Y') AS 'creacionPreguntaDir' FROM preguntadirecta AS  p  WHERE p.idusuarioapp=:id AND p.pago_usuario=0;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$iduserapp);
// 		$stmt->execute();
// 		$i = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$array[$i]['precio_unitario']   =  getPrecioPreguntaExpressProfesional(trim($data2['idusuarioprofesional']));				
// 				$array[$i]['concepto']   =  'Pago consulta directa generada en la fecha: '.trim($data2['creacionPreguntaDir']);				
// 				$i++;
// 			}
// 			return $array;
// 		}else{
// 			return 0;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getPrecioPreguntaExpressProfesional($idprofesional){ 
	 
// 	try{
// 		$sql = "SELECT valorpregunta FROM usuarioprofesional WHERE idusuarioprofesional=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$idprofesional);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$valorpregunta     = trim($data2['valorpregunta']);
// 				return $valorpregunta;
// 			}
// 		}else{
// 			return 0;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return 0;
// 	}
// }

// function getCorreoPorIdpregunta($idpregunta){ 
	 
// 	try{
// 		$sql = "SELECT u.correo FROM pregunta p join usuarioapp u on p.idusuarioApp = u.idusuarioApp 
// 		WHERE p.idpregunta=:idpregunta;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idpregunta",$idpregunta);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$correo     = trim($data2['correo']);
// 				return $correo;
// 			}
// 		}else{
// 			return "";
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getCorreoUsuario($iduser){ 	 
// 	try{
// 		$sql = "SELECT correo FROM usuarioapp WHERE idusuarioApp=:iduser;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("iduser",$iduser);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$correo     = trim($data2['correo']);
// 				return $correo;
// 			}
// 		}else{
// 			return "";
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getQuestionoCompleja($idpregunta){ 
	 
// 	try{
// 		$sql = "SELECT titulo FROM pregunta WHERE idpregunta=:idpregunta;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idpregunta",$idpregunta);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$titulo     = trim($data2['titulo']);
// 				return $titulo;
// 			}
// 		}else{
// 			return "";
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getIdusuarioapp($carnet){ 
	 
// 	try{
// 		$sql = "SELECT idusuarioapp FROM usuarioapp WHERE carnet=:carnet;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("carnet",$carnet);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$idusuarioapp     = trim($data2['idusuarioapp']);
// 				return $idusuarioapp;
// 			}
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getIdprofesionalapp($carnet){ 
	 
// 	try{
// 		$sql = "SELECT idusuarioprofesional FROM usuarioprofesional WHERE carnet=:carnet;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("carnet",$carnet);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$idusuarioprofesional     = trim($data2['idusuarioprofesional']);
// 				return $idusuarioprofesional;
// 			}
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }

// function getIdTrascc($idusuarioprofesional){ 
	 
// 	try{
// 		$sql = "SELECT idlogtransaccional FROM logtransaccionalprofesional WHERE idusuarioprofesional=:idusuarioprofesional AND estado = 1;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idusuarioprofesional",$idusuarioprofesional);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$idlogtransaccional     = trim($data2['idlogtransaccional']);
// 				return $idlogtransaccional;
// 			}
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getPlan($idtrascc){ 
	 
// 	try{
// 		$sql = "SELECT idplan FROM logtransaccionalprofesional WHERE idlogtransaccional=:idtrascc;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idtrascc",$idtrascc);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$idplan     = trim($data2['idplan']);
// 				return $idplan;
// 			}
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }

// function getMesesPlan($idplan){ 
	 
// 	try{
// 		$sql = "SELECT mesesactivo FROM plan WHERE idplan=:idplan;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idplan",$idplan);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$mesesactivo     = trim($data2['mesesactivo']);
// 				return $mesesactivo;
// 			}
// 		}else{
// 			return 0;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return 0;
// 	}
// }

// function validarplanProfesional($idplan,$idprofesional,$idlogtransaccional){ 
// 	try{
// 		$db = getConnection();
// 		$sql3   = "INSERT INTO membresia (fechacompra,fechavencimiento,estado,idplan,idusuarioprofesional,idlogtransaccional) 
// 					VALUES (:fechacompra,:fechavencimiento,1,:idplan,:idusuarioprofesional,:idlogtransaccional);";
// 		$stmt3 = $db->prepare($sql3);
// 		$stmt3->bindParam("idplan", $idplan);
// 		$date = date ('Y-m-d H:i:s');
// 		$stmt3->bindParam("fechacompra", $date);
// 		$meses = getMesesPlan($idplan);
// 		$fechaf 	= strtotime ( "+ $meses months" , strtotime ( $date ) ) ;
// 		$fechaf 	= date ( 'Y-m-d H:i:s' , $fechaf );
// 		$stmt3->bindParam("fechavencimiento", $fechaf);
// 		$stmt3->bindParam("idusuarioprofesional", $idprofesional);
// 		$stmt3->bindParam("idlogtransaccional", $idlogtransaccional);
// 		$stmt3->execute();
// 		return true;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }

// function getValidateTokenCobro($token){ 
	 
// 	try{
// 		$sql 	= "SELECT idcredencial FROM credencial WHERE value_key = :value AND life_time >= :date;";
// 		$db   	= getConnection();
// 		$stmt 	= $db->prepare($sql);
// 		$stmt->bindParam("value",$token);
// 		$date 	= date ('Y-m-d H:i:s');
// 		$stmt->bindParam("date",$date);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			//return false;
// 			return true;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		//return false;
// 		return true;
// 	}
// }


// function getestadopagoPD($idpregunta){ 
	 
// 	try{
// 		$sql = "SELECT pago_usuario FROM preguntadirecta WHERE idpreguntadirecta=:id";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$idpregunta);
// 		$stmt->execute();
// 		$cantidad = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$pago_usuario     = trim($data2['pago_usuario']);
// 				if($pago_usuario == 1){
// 					return 1;
// 				}else{
// 					return 0;
// 				}
// 			}
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getestadoPreguntaDirecta($idpregunta){ 
	 
// 	try{
// 		$sql = "SELECT estado FROM preguntadirecta WHERE idpreguntadirecta=:id";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$idpregunta);
// 		$stmt->execute();
// 		$cantidad = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				return trim($data2['estado']);
				
// 			}
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getPrecioPreguntaDirecta($idpreguntadirecta){ 
	 
// 	try{
// 		$sql = "SELECT valor FROM log_debito_usuarioapp_pd WHERE idpreguntadirecta=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$idpreguntadirecta);
// 		$stmt->execute();
// 		$valor = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$valor     = trim($data2['valor']);
// 				return $valor;
// 			}
// 		}else{
// 			return 0;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getPrecioServiciosProfesional($idprofesional){ 
	 
// 	try{
// 		$sql = "SELECT valorpregunta FROM usuarioprofesional WHERE idusuarioprofesional= :id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id",$idprofesional);
// 		$stmt->execute();
// 		$valor = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$valor     = trim($data2['valorpregunta']);
// 				return $valor;
// 			}
// 		}else{
// 			return 0;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }

// function getProcentajeGananciaLegalappPD(){ 
	 
// 	try{
// 		$sql = "SELECT porcentajeganancia FROM configuracion WHERE idconfiguracion=1;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->execute();
// 		$cantidad = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$procentaje     = trim($data2['porcentajeganancia']);
// 				return $procentaje;
// 			}
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getProcentajeGananciaLegalappPE(){ 
	 
// 	try{
// 		$sql = "SELECT porcentajegananciaex FROM configuracion WHERE idconfiguracion=1;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->execute();
// 		$cantidad = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$procentaje     = trim($data2['porcentajegananciaex']);
// 				return $procentaje;
// 			}
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }

// function setLogLegalApp($monto,$tipo,$idpregunta,$porcentaje,$valor_neto){ 
	 
// 	try{
// 		$sql = "INSERT INTO log_ganancia_legalapp (valor,idpregunta,tipo,porcentaje,valor_neto) VALUES (:monto,:idpregunta,:tipo,:porcentaje,:valor_neto);";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("monto",$monto);
// 		$stmt->bindParam("tipo",$tipo);
// 		$stmt->bindParam("idpregunta",$idpregunta);
// 		$stmt->bindParam("valor_neto",$valor_neto);
// 		$stmt->bindParam("porcentaje",$porcentaje);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }


// function quitarplanprofesionalapp($idprofesional,$idlogtransaccional){
// 	try{
// 		$db = getConnection();
// 		$sql3   = "UPDATE membresia SET estado = 2 WHERE idlogtransaccional=:idlogtransaccional AND idusuarioprofesional=:idusuarioprofesional;";
// 		$stmt3 = $db->prepare($sql3);
// 		$stmt3->bindParam("idusuarioprofesional", $idprofesional);
// 		$stmt3->bindParam("idlogtransaccional", $idlogtransaccional);
// 		$stmt3->execute();
// 		return true;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return $e;
// 	}
// }

// function quitarsaldousuarioapp($idusuarioapp,$valortransac){
// 	try{
// 		$db = getConnection();
// 		$sql3   = "UPDATE usuarioapp SET saldo = saldo-:valortransac WHERE idusuarioapp=:idusuarioapp;";
// 		$stmt3 = $db->prepare($sql3);
// 		$stmt3->bindParam("valortransac", $valortransac);
// 		$stmt3->bindParam("idusuarioapp", $idusuarioapp);
// 		$stmt3->execute();
// 		return true;
// 		$db = null;
// 	}catch(PDOException $e){
// 		return $e;
// 	}
// }
// function quitarsaldousuarioapp2($idusuarioapp,$valortransac,$idtransaccion){
// 	try{
// 		$totaltrasac 	= $valortransac;
// 		$totaldiscoint 	= 0;
// 		$sql = "SELECT idlogtrasaccionpregunta,idpregunta,precio_p FROM logtrasaccionpregunta WHERE idtransaccion=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id", $idtransaccion);
// 		$stmt->execute();
// 		$cantidad = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$precio_p     	= trim($data2['precio_p']);
// 				$idpregunta   	= trim($data2['idpregunta']);
// 				$totaldiscoint 	= $totaldiscoint+$precio_p;
// 				$sql2   = "UPDATE pregunta set estado = 1 WHERE idpregunta=:idpregunta";
// 				$stmt2 = $db->prepare($sql2);
// 				$stmt2->bindParam("idpregunta", $idpregunta);
// 				$stmt2->execute();
// 			}
// 			$residuo = $totaltrasac-$totaldiscoint;
// 			if($residuo){
// 				$sql3   = "UPDATE  usuarioapp SET saldo = saldo - :valor WHERE idusuarioapp=:idusuarioapp;";
// 				$stmt3 = $db->prepare($sql3);
// 				$stmt3->bindParam("valor", $residuo);
// 				$stmt3->bindParam("idusuarioapp", $idusuarioapp);
// 				$stmt3->execute();
// 			}
// 		}else{
// 			if($valortransac){
// 				$sql3   = "UPDATE  usuarioapp SET saldo = saldo - :valor WHERE idusuarioapp=:idusuarioapp;";
// 				$stmt3 = $db->prepare($sql3);
// 				$stmt3->bindParam("valor", $valortransac);
// 				$stmt3->bindParam("idusuarioapp", $idusuarioapp);
// 				$stmt3->execute();
// 			}
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }

// function getCurrectSaldoUsuarioapp($idusuarioapp){ 
	 
// 	try{
// 		$sql = "SELECT saldo FROM usuarioapp WHERE idusuarioapp=:idusuarioapp;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("idusuarioapp", $idusuarioapp);
// 		$stmt->execute();
// 		$cantidad = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$saldo     = trim($data2['saldo']);
// 				return $saldo;
// 			}
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getCurrentStateTrasacc($id){ 
	 
// 	try{
// 		$sql = "SELECT estado FROM logtransaccionalprofesional WHERE idlogtransaccional=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id", $id);
// 		$stmt->execute();
// 		$estado = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$estado     = trim($data2['estado']);
// 				return $estado;
// 			}
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getCurrentPreguntaCompleja($id){ 
	 
// 	try{
// 		$sql = "SELECT COUNT(idlogpreguntacompleja) AS cantidad_compleja FROM logpreguntacompleja WHERE idpregunta=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id", $id);
// 		$stmt->execute();
// 		$estado = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$cantidad_compleja     = trim($data2['cantidad_compleja']);
// 				return $cantidad_compleja;
// 			}
// 		}else{
// 			return 0;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return 0;
// 	}
// }


// function setpasswordupdateUsuarioapp($password,$idusuarioapp){ 
	 
// 	try{
// 		$sql = "UPDATE usuarioapp SET password = :password WHERE idusuarioapp=:idusuarioapp;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("password",$password);
// 		$stmt->bindParam("idusuarioapp",$idusuarioapp);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function setpasswordupdateProfesional($password,$idusuarioprofesional){ 
	 
// 	try{
// 		$sql = "UPDATE usuarioprofesional SET password = :password WHERE idusuarioprofesional=:idusuarioprofesional;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("password",$password);
// 		$stmt->bindParam("idusuarioprofesional",$idusuarioprofesional);
// 		$stmt->execute();
// 		if ( $stmt->rowCount() > 0 ) {
// 			return true;
// 		}else{
// 			return false;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return false;
// 	}
// }
// function getcurrentstatepaymentexpress($id){ 
	 
// 	try{
// 		$sql = "SELECT estado FROM pregunta WHERE idpregunta=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id", $id);
// 		$stmt->execute();
// 		$estado = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$estado     = trim($data2['estado']);
// 				return $estado;
// 			}
// 		}else{
// 			return 0;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return 0;
// 	}
// }
// function getcurrentstatepaymentdirecta($id){ 
	 
// 	try{
// 		$sql = "SELECT estado FROM preguntadirecta WHERE idpreguntadirecta=:id;";
// 		$db   = getConnection();
// 		$stmt = $db->prepare($sql);
// 		$stmt->bindParam("id", $id);
// 		$stmt->execute();
// 		$estado = 0;
// 		if ( $stmt->rowCount() > 0 ) {
// 			while ($data2  = $stmt->fetch(PDO::FETCH_ASSOC)){
// 				$estado     = trim($data2['estado']);
// 				return $estado;
// 			}
// 		}else{
// 			return 0;
// 		}
// 		$db = null;
// 	}catch(PDOException $e){
// 		return 0;
// 	}
// }

// function getquestionexpressstatus($id){
// 	switch ($id) {
// 		case 0:
// 			return "Cancelada";
// 			break;
// 		case 1:
// 			return "Por Pagar";
// 			break;
// 		case 2:
// 			return "Por Responder";
// 			break;
// 		case 3:
// 			return "Sin Respuesta";
// 			break;
// 		case 4:
// 			return "Recibiendo Respuesta";
// 			break;
// 		case 5:
// 			return "Contestada";
// 			break;
// 		case 6:
// 			return "Por Responder Apelacion";
// 			break;
// 		case 7:
// 			return "Finalizada Completamente";
// 			break;
// 		default:
// 			return "Estado no encontrado";
// 			break;
// 	}
	
// }
// function getquestiondirectastatus($id){
// 	switch ($id) {
// 		case 0:
// 			return "Cancelada";
// 			break;
// 		case 1:
// 			return "Por Pagar";
// 			break;
// 		case 2:
// 			return "Por Responder";
// 			break;
// 		case 3:
// 			return "Sin Respuesta";
// 			break;
// 		case 4:
// 			return "Recibiendo Respuesta";
// 			break;
// 		case 5:
// 			return "Contestada";
// 			break;
// 		case 6:
// 			return "Por Responder Apelacion";
// 			break;
// 		case 7:
// 			return "Finalizada Completamente";
// 			break;
// 		case 8:
// 			return "Rechazada";
// 			break;
// 		default:
// 			return "Estado no encontrado";
// 			break;
// 	}
	
// }



