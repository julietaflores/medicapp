<!DOCTYPE html>
<html lang="en">
<head>
<?php include "headerm.php";
  include 'data/dataBase.php';
  include_once 'classes/cclinica.php';
?>
<title>Clinica Smart | </title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
    <link rel="icon" type="image/png" href="images/logo.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">

</head>



<style type="text/css">	
	#top-image {
	background:url('images/fondoclinica.jpg') -25px -50px;
	position:fixed ;
	top:0;
	width:100%;
	z-index:0;
		height:100%;
		background-size: calc(100% + 50px);
	}
</style>


<script type="text/javascript">	

$(document).ready(function() {
		var movementStrength = 25;
		var height = movementStrength / $(window).height();
		var width = movementStrength / $(window).width();
		$("#top-image").mousemove(function(e){
							var pageX = e.pageX - ($(window).width() / 2);
							var pageY = e.pageY - ($(window).height() / 2);
							var newvalueX = width * pageX * -1 - 25;
							var newvalueY = height * pageY * -1 - 50;
							$('#top-image').css("background-position", newvalueX+"px     "+newvalueY+"px");
		});
	});

	function registrar(codigo){ 
    if($("#username").val().trim()=='' || $("#email").val().trim()==''  ||  $("#telefono").val().trim()=='' ||    $("#nclinica").val().trim()=='' ||  $("#clave").val().trim()==''  )
	{ 
		new PNotify({
                title: 'Estimado usuario',
                text: 'Todos los campos son requerido.',
                type: 'error'
        });     
	}else{  
		var email=$("#email").val();
		 var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		 if(regex.test(email) ){ 

			var lo1=$("#clave").val().length;
			if( lo1>=6){

				$.ajax({
                      type: 'POST',
                      url: 'actions/actionUsuario.php',
                      data: {email:email,opt: 'verificarcorreo'},
                     success: function(data) { 
                          if (data > 0){         
							  new PNotify({
								title: 'Estimado Usuario',
                                text: 'Éste correo ya tiene una cuenta creada. Haga clic en olvidé mi contraseña o solicite una reactivación.',
                                type: 'error'
                              });              
                            } else {
                               
				var nombre = $("#username").val(); 
				var email = $("#email").val(); 
				var telefono = $("#telefono").val(); 
				var clinica = $("#nclinica").val(); 
				var clave = $("#clave").val(); 
				$.ajax({
                      type: 'POST',
                      url: 'actions/actionUsuario.php',
                      data: {nombre:nombre,email:email,clave:clave,telefono:telefono,clinica:clinica,cod:codigo,opt: 'registro'},
                            success: function(data) {   
                             if (data == 0){         
								new PNotify({
						title: 'Usuario Creado correctamente',
						text: 'Verifique su correo electrónico para confirmar el acceso a la plataforma.',
						type: 'info'
             });       

			 $.ajax({
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        url: "ws/api_rest.php/getAvisoNuevoUsuarioA",
                        dataType: "json",
                        data: { },
                        success: function (data){
                           if(data==1){
                            }else{
                            if(data==400){
                            }
                           }
                        }
                      });

					  $.ajax({
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        url: "ws/api_rest.php/getAvisoNuevoUsuarioU/"+email+"/"+nombre,
                        dataType: "json",
                        data: { },
                        success: function (data){
                           if(data==1){
							new PNotify({
                 title: 'Usuario',
                text: 'Tienes un correo verifica tu cuanta !!!',
                type: 'info'
             });       
                            }else{
                            if(data==400){
							//	alert("no se pudo enviar el correo al usuario");
							new PNotify({
                 title: 'Usuario',
                text: 'No se pudo enviar el correo !!!',
                type: 'error'
             });       
                            }
                           }
                        }
                      });


                                            setTimeout( "window.location.href='login.php'", 5000);
                              } else  {
								new PNotify({
                 title: 'Usuario no creado',
                text: 'Los datos no se pudieron crear !!!',
                type: 'error'
             });     

                              }
                            }	
                     }); 
					

					
                          }
                        }	
                     }); 





			}else{
				new PNotify({
                   title: 'Señor usuario',
                   text: 'Verifique la clave introducida tiene que ser mayor o igual a 6 dígitos.',
                   type: 'warning'
                 });
			}




	      }else{
			new PNotify({
                   title: 'Señor usuario',
                   text: 'En campo correo tiene que tener todos las caracteristicas de un correo como ser "@", ".com", "etc."',
                   type: 'warning'
                 });
          }




  
    }
   }			

</script>

<body>
	
	<div class="limiter">
	<div id="top-image" class="container-login100">
	<!--	<div class="container-login100">-->
			<div class="wrap-login100">
				<form class="login100-form validate-form">
					<span class="login100-form-title">
						<img src="images/logo.png" style="border: 0px; width:50%; border-radius: 50%; height:50%;">
					</span>
				  <br>
					<div class="wrap-input100 validate-input" >
			    		<span class="btn-show-pass">
							<i class="zmdi zmdi-account"></i>
						</span>
						<input class="input100" type="text" name="username" id="username">
						<span class="focus-input100" data-placeholder="Nombre completo"></span>
					</div>

	
					<div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
					<span class="btn-show-pass">
							<i class="zmdi zmdi-email"></i>
						</span>
					<input class="input100" type="text" name="email" id="email">
						<span class="focus-input100" data-placeholder="Correo electrónico"></span>
					</div>


					<div class="wrap-input100 validate-input" >
						<span class="btn-show-pass">
							<i class="zmdi zmdi-phone"></i>
						</span>
						<input class="input100" type="number" name="telefono" id="telefono">
						<span class="focus-input100" data-placeholder="Teléfono"></span>
					</div>


					<div class="wrap-input100 validate-input" >
						<span class="btn-show-pass">
							<i class="zmdi zmdi-hospital-alt"></i>
						</span>
						<input class="input100" type="text" name="nclinica" id="nclinica">
						<span class="focus-input100" data-placeholder="Nombre de su Clínica"></span>
					</div>

					<div class="wrap-input100 validate-input" >
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type="password" name="clave" id="clave">
						<span class="focus-input100" data-placeholder="Contraseña"></span>
					</div>

				
					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<?if(isset($_REQUEST['codigo'])&&$_REQUEST['codigo']!=1){
								function my_simple_crypt( $string, $action = 'e' ) {
									$secret_key = 'my_simple_secret_key';
									$secret_iv = 'my_simple_secret_iv';
								
									$output = false;
									$encrypt_method = "AES-256-CBC";
									$key = hash( 'sha256', $secret_key );
									$iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
								
									if( $action == 'e' ) {
										$output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
									}
									else if( $action == 'd' ){
										$output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
									}
								
									return $output;
								}
								$encrypted = my_simple_crypt( $_REQUEST['codigo'], 'e' );
								$decrypted = my_simple_crypt( $_REQUEST['codigo'], 'd' );
							  
								$oclinica= new clinica();
								$vclinica=false;
								if($decrypted){
									$vclinica=$oclinica->getOne($decrypted);
								}
								if($vclinica){
									foreach ($vclinica as $id => $value) {?>
										<a class="login100-form-btn" href="#" onclick="registrar('<?=$value['idclinica']?>');">Regístrate</a>
										<label>Clinica referida: <?=$value['nombre']?> </label>
									<?}
								}else{?>
									<a class="login100-form-btn" href="#" onclick="registrar(0);">Regístrarse</a>
								
								<?}
							}else{?>
								<a class="login100-form-btn" href="#" onclick="registrar(0);">Regístrarse</a>
								
							<?}
							?>
						</div>
					</div>

					<br>
					<div>
					<p style="font-size: 10px; text-align: center;" class="pull-center"><a style="font-size: 10px; text-align: center;"  href="terminosycondiciones.php">Terminos y Condiciones</a></p>
					<p style="font-size: 10px" class="pull-center"><a style="font-size: 10px"  href="https://clanbolivia.tech">Clan Bolivia</a> &nbsp;<strong>-</strong>&nbsp; Clinica App | Derechos reservados 2019</p>
              </div>

				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
	<script type="text/javascript" src="js/notify/pnotify.core.js"></script>

</body>
</html>