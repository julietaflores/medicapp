<?php 
		 session_start();
		 if (!isset($_SESSION['csmart']['idusuario'])) {
		 Header ("Location: login.php");
		 }
		 include 'headercondicion.php';
		include 'classes/cpaciente.php';
		$opaciente = new paciente();
		$accion 	= "Crear";
		$option 	= "n";
		$idpaciente 	= "";
	        $nombre 	= "";
	        $apellido 	= "";
	        $identificacion 	= "";
	        $genero 	= "";
	        $imagen 	= "";
	        $direccion 	= "";
	        $telefono1 	= "";
	        $telefono2 	= "";
	        $correo 	= "";
	        $fecha_nac 	= "";
	        $fecha_mod 	= "";
	        $fecha_cre 	= "";
	        $notas 	= "";
	        $idestado 	= "";
	        $iddepartamento 	= "";
	        $responsable 	= "";
	        $telefono_resp 	= "";
	        $medico_fam 	= "";
	        $ocupacion 	= "";
	        
	    if(isset($_REQUEST['opt']) && $_REQUEST['opt']== "m"){
		$option = $_REQUEST['opt'];
		$idObj	= $_REQUEST['id'];
	}


	
	if ($option == "m") {
		$vpaciente = $opaciente->getOne($idObj);


		if($vpaciente){
			$accion 	= "Modificar";
			
			   foreach ($vpaciente AS $id => $info){ 
			   	  
			   	  $idpaciente		= $info["idpaciente"];
		        $nombre		= $info["nombre"];
		        $apellido		= $info["apellido"];
		        $identificacion		= $info["identificacion"];
		        $genero		= $info["genero"];
		        $imagen		= $info["imagen"];
		        $direccion		= $info["direccion"];
		        $telefono1		= $info["telefono1"];
		        $telefono2		= $info["telefono2"];
		        $correo		= $info["correo"];
		        $fecha_nac		= $info["fecha_nac"];
		        $fecha_mod		= $info["fecha_mod"];
		        $fecha_cre		= $info["fecha_cre"];
		        $notas		= $info["notas"];
		        $idestado		= $info["idestado"];
		        $iddepartamento		= $info["iddepartamento"];
		        $responsable		= $info["responsable"];
		        $telefono_resp		= $info["telefono_resp"];
		        $medico_fam		= $info["medico_fam"];
		        $ocupacion		= $info["ocupacion"];
		         }
		}else{
			header("Location: paciente.php");
			exit();
		}
	}

 $vpaciente 		= $opaciente->getAll1();
 $vpaciente 		= $opaciente->paginadorphp(0);
 

?>
<!DOCTYPE HTML><html>
<head>
 	 <?php include 'header.php'; ?>
	
		<script type="text/javascript">	
			function irperfil(id){
				$('#idpacientes').val(id);
			    $("#formoculto").submit();
			}

			function ireditar(id){
				$("#idpacienteditar").val(id);
				$("#modificarpaciente").val('m');
				$("#formularioeditar").submit();
			}
		
			$(window).load(function() {
					$(".loaderp").fadeOut("slow");
		    });
	    </script>
</head>

<body class="nav-md">
<div class="loaderp" style="position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('images/cargando.webp') 50% 50% no-repeat rgb(249,249,249);
    opacity: .8;"></div>
<div  id="btn3" class="loader" style="display: none"></div>	

  <div class="container body">


    <div class="main_container">

      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          
          <!-- sidebar menu -->
            <?php include "menu.php" ?>
          <!-- /sidebar menu -->

         
        </div>
      </div>

      <!-- top navigation -->
           <?php include "top_nav.php" ?>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">

        <div class="">
          
         
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Administración de Paciente</h2>
                    
                   <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input id="buscadorNoticias" type="text" class="form-control" placeholder="Buscar...">
                  <span class="input-group-btn">
                                            <a class="btn btn-default" onclick="javascript:botonBuscarpaciente()">Ir!</a>
                                        </span>
                </div>
              </div>
            </div><div class="clearfix"></div>
				</div>
				<?if(!$vpaciente){?>
					<div class="col-md-12" style="height: 400px; text-align: center">
					<h2>Bienvenido Registra tu primer Paciente</h2>
					<a class="btn btn-success " href="pacientenuevo.php">Nuevo Paciente</a>
				  </div>
					<?}else{?>		
                <div  class="x_content" >

                

                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                       <li id="tab2" role="presentation" class="active"><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="true">Pacientes</a>
                      </li>
                      
                    </ul>
                    <div id="myTabContent" class="tab-content">
                      
<!--paciente cartas------------------------------------------------------------------------------------------>
                      <div role="tabpanel" class="tab-pane active fade in" id="tab_content2" aria-labelledby="profile-tab">
                        <table id="Cartapacientes">
							<thead>
								<tr>
									<th></th>
								</tr>
							</thead>
							<tbody id="tablaPacientes" style="display:flex;flex-wrap: wrap;">
							
							<?php
						if($vpaciente){
							foreach ($vpaciente AS $id => $array) {   ?>
					
				
								<tr>
									<td>
									<div class="col-md-10 col-sm-10 col-xs-12  animated fadeInDown">
						                      <div class="well profile_view">
						                        <div class="col-sm-12">
						                          <div class="left col-xs-7">
						                            <h2><?=$array['nombre'];?> <?=$array['apellido'];?></h2>
						                            <!-- <p><strong>About: </strong> Web Designer / UI. </p> -->
						                            <ul class="list-unstyled">
						                              <li><i class="fa fa-phone"></i>  <?=$array['telefono1'];?> </li>
						                              <li><i class="fa fa-phone"></i>  <?=$array['telefono2'];?></li>
													 <li><i class="fa fa-email"></i>  <?=$array['correo'];?></li>

						                            </ul>
						                            <br>
						                          </div>
						                          <div class="right col-xs-5 text-center">
													  <?if($array['imagen']==''){?>
														<img style="border-radius: 50%; border: 2px solid; border-color: #58D3F7;width: 80px;height: 80px; " src="images/user.png" alt="" class="img-circle img-responsive">
						                          
														<?}else{
															if(file_exists("images/paciente/".$array['idpaciente']."/".$array['imagen'])){?>
																<img style="border-radius: 50%; border: 2px solid; border-color: #58D3F7;width: 80px;height: 80px;" src="images/paciente/<?=$array['idpaciente'];?>/<?=$array['imagen'];?>" alt="" class="img-circle img-responsive">
						                         
															
															<?}else{?>
																<img style="border-radius: 50%; border: 2px solid; border-color: #58D3F7;width: 80px;height: 80px;" src="images/user.png" alt="" class="img-circle img-responsive">
						                          
																<?}?>
														
															<?}?>
						                          </div>
						                        </div>
						                        <div class="col-xs-12 bottom text-center">
						                          
						                          <div class="col-xs-12 col-sm-6 emphasis">
						                            <button type="button" style="border-radius: 20px;" class="btn btn-success btn-xs" onclick="ireditar(<?=$array['idpaciente'];?>)"> 
						                            <i class="fa fa-edit"></i> Editar </button>

						                            <a id="perfil"  class="btn btn-primary btn-xs" onclick="irperfil(<?=$array['idpaciente'];?>)"> <i class="fa fa-user">
																					</i> Ver Perfil </a>
																					
						                          </div>
						                        </div>
						                      </div>
						                    </div>

														
										<?php }
											}	?>
										

                       
									
								
									</td>
								</tr>
					
							</tbody>
						</table>
						</div>
						<input type="hidden" name="" id="numeropacientes" value="<?=count($vpaciente)?>">
						<?if(count($vpaciente)>=10){?>
							<div>
							<a id="btnnumeropacientes" class="btn btn-default" onclick="javascript:maspacientes()">Mas Pacientes</a>
                    
							</div>
						<?}?>
                    </div>
                  </div>


				</div>
						<?}?>
              </div>
            </div>
          </div>
        </div>
		<form style="display: hidden" action="pacienteperfil.php" method="POST" id="formoculto">
																						<input type="hidden" id="idpacientes" name="id" value=""/>
																						
		</form>
		<form style="display: hidden" action="pacientenuevo.php" method="POST" id="formularioeditar">
																						<input type="hidden" id="idpacienteditar" name="id" value=""/>
																						<input type="hidden" id="modificarpaciente" name="opt" value="">																				
		</form>

		<div style="height: 100px;"></div>
        <?php include "piep.php" ?>

      </div>
    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="js/bootstrap.min.js"></script>
  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>
  <script src="js/custom.js"></script>
  <!-- form validation -->
  <script src="js/validator/validator.js"></script>
  <!-- PNotify -->
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  <script type="text/javascript" src="js/notify/pnotify.buttons.js"></script>
  <script type="text/javascript" src="js/notify/pnotify.nonblock.js"></script>
  
   <!-- Datatables-->
        <script src="js/datatables/jquery.dataTables.min.js"></script>
        <script src="js/datatables/dataTables.bootstrap.js"></script>
        <script src="js/datatables/dataTables.buttons.min.js"></script>
        <script src="js/datatables/buttons.bootstrap.min.js"></script>
        <script src="js/datatables/jszip.min.js"></script>
        <script src="js/datatables/pdfmake.min.js"></script>
        <script src="js/datatables/vfs_fonts.js"></script>
        <script src="js/datatables/buttons.html5.min.js"></script>
        <script src="js/datatables/buttons.print.min.js"></script>
        <script src="js/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="js/datatables/dataTables.responsive.min.js"></script>
        <script src="js/datatables/responsive.bootstrap.min.js"></script>
        <script src="js/datatables/dataTables.scroller.min.js"></script>

<!-- form wizard -->
  <script type="text/javascript" src="js/wizard/jquery.smartWizard.js"></script>
  
  <script>

 

	/* FOR DEMO ONLY */

	var table= $('#Cartapacientes').DataTable({
		"searching": true,
		"lengthChange": false,
		"info":false,
		"paging": false,
		"language": {
    "search": "Buscar:",
		"paginate": {
      "previous": "anterior",
			"next":"siguiente"
    }
  },
		pageLength : 999,
	
	
	});
	$('#buscadorNoticias').on( 'keyup', function () {
    table.search( this.value ).draw();
		
});
$('#Cartapacientes_filter').hide();

$('#borrar').click(function() {
    table.search('').draw();
    $("#buscadorNoticias").val("");
		
});
//paginador pacientes/////////////////////////////////////////////////////////////
function maspacientes(){
	debugger;
	var numero=parseInt($("#numeropacientes").val());

				$.ajax({
                      url: 'actions/actionpaciente.php?opt=maspacientes',
                      data: 'numeropa='+numero,
                            
                      type: "POST",
                      dataType:'json',
                      success: function(json) {
                        $.each(json, function(i, item) {
							construirtabla(item.nombre,item.apellido,item.telefono1,item.telefono2,item.correo,item.imagen,item.idpaciente);
							
                                      
						});
						numero=numero+1;
						$("#numeropacientes").val(numero);
                         
                        console.log(json);
                      
                    }
                  });
}
function construirtabla(nombre,apellido,telefono1,telefono2,correo,imagen,idpaciente){
	debugger;
	var ruta="";

		if(imagen=="user.png"){
			ruta="images/"+imagen;
		}else{
			
			ruta="images/paciente/"+idpaciente+"/"+imagen;
			if(!ImageExist(ruta)){
				ruta="images/user.png";
			}
		}
var prueba="<div class='col-md-10 col-sm-10 col-xs-12  animated fadeInDown'>"+
		"<div class='well profile_view'>"+
		"<div class='col-sm-12'>"+           
		"<div class='left col-xs-7'>"+
		"<h2>"+nombre+" "+apellido+"</h2>"+
		"<ul class='list-unstyled'>"+
			"<li><i class='fa fa-phone'></i>  "+telefono1+" </li>"+
			"<li><i class='fa fa-phone'></i>  "+telefono2+"</li>"+
			"<li><i class='fa fa-email'></i>  "+correo+"</li>"+
		"</ul><br></div>"+
		
		
		"<div class='right col-xs-5 text-center'>"+
		"<img style='width:80px;height: 62px;' src='"+ruta+"' alt='' class='img-circle img-responsive'>"+
	
			"</div></div>"+
			"<div class='col-xs-12 bottom text-center'>"+
			"<div class='col-xs-12 col-sm-6 emphasis'>"+
			 "<button type='button' class='btn btn-success btn-xs' onclick='ireditar("+idpaciente+")'> "+
				"<i class='fa fa-edit'></i> Editar </button>"+
					"<a id='perfil' class='btn btn-primary btn-xs' onclick='irperfil("+idpaciente+")'> <i class='fa fa-user'>"+
						"</i> Ver Perfil </a>"+							
						  "</div>"+
						   "</div>"+
						     "</div>"+
						       "</div>"

					table.row.add( [ 
								prueba	
							] ).draw( false );
}
function ImageExist(url) 
{
	debugger;
   var img = new Image();
   var prueba;
   img.src = url;
   prueba=img.height;
   if(prueba>0){
	   return true
   }
   return false
}
function construirtablaBuscar(nombre,apellido,telefono1,telefono2,correo,imagen,idpaciente){
	var ruta="";
if(imagen==""){
	ruta="images/"+imagen;
}else{
	ruta="images/paciente/"+idpaciente+"/"+imagen;
			if(!ImageExist(ruta)){
				ruta="images/user.png";
			}
}
var fila="<tr><td><div class='col-md-10 col-sm-10 col-xs-12  animated fadeInDown'>"+
		"<div class='well profile_view'>"+
		"<div class='col-sm-12'>"+           
		"<div class='left col-xs-7'>"+
		"<h2>"+nombre+" "+apellido+"</h2>"+
		"<ul class='list-unstyled'>"+
			"<li><i class='fa fa-phone'></i>  "+telefono1+" </li>"+
			"<li><i class='fa fa-phone'></i>  "+telefono2+"</li>"+
			"<li><i class='fa fa-email'></i>  "+correo+"</li>"+
		"</ul><br></div>"+
		
		
		"<div class='right col-xs-5 text-center'>"+
		"<img style='width:80px;height: 62px;' src='"+ruta+"' alt='' class='img-circle img-responsive'>"+
	
			"</div></div>"+
			"<div class='col-xs-12 bottom text-center'>"+
			"<div class='col-xs-12 col-sm-6 emphasis'>"+
			 "<button type='button' class='btn btn-success btn-xs' onclick='ireditar("+idpaciente+")'> "+
				"<i class='fa fa-edit'></i> Editar </button>"+
					"<a id='perfil' class='btn btn-primary btn-xs' onclick='irperfil("+idpaciente+")'> <i class='fa fa-user'>"+
						"</i> Ver Perfil </a>"+							
						  "</div>"+
						   "</div>"+
						     "</div>"+
							   "</div></td></tr>";
	 var btn = document.createElement("TR");
      btn.innerHTML=fila;
      document.getElementById("tablaPacientes").appendChild(btn);
	
	
}

function botonBuscarpaciente(){
	var filas=0;
	table.destroy();
	$("#tablaPacientes tr").remove();
	var palabra=$("#buscadorNoticias").val();
	$.ajax({
                      url: 'actions/actionpaciente.php?opt=botonbuscarpaciente',
                      data: 'palabra='+palabra,
                            
                      type: "POST",
                      dataType:'json',
                      success: function(json) {
                        $.each(json, function(i, item) {
							construirtablaBuscar(item.nombre,item.apellido,item.telefono1,item.telefono2,item.correo,item.imagen,item.idpaciente);
							
                                 filas++;     
						});
						console.log(filas);
						
						$("#btnnumeropacientes").hide();
						construirDatatable();
						
				  }
				});
}
function construirDatatable(){
	 table= $('#Cartapacientes').DataTable({
		"searching": true,
		"lengthChange": false,
		"info":false,
		"language": {
    "search": "Buscar:",
		"paginate": {
      "previous": "anterior",
			"next":"siguiente"
    }
  },
		pageLength : 999,
	
	
	});
	$('#buscadorNoticias').on( 'keyup', function () {
    table.search( this.value ).draw();
		
});
$('#Cartapacientes_filter').hide();

$('#borrar').click(function() {
    table.search('').draw();
    $("#buscadorNoticias").val("");
		
});
}
///////////////////////////////////////////






function reporte(){
      debugger;
      if($("#tablaReportes tr").length>0){
        tabla.buttons().remove('1-3');
       tabla.destroy();

      }
          $("#tablaReportes tr").remove();
          $("#tablafoot tr").remove();
          
          var totalIngresos=0.0;
          var totalGastos=0.0;
          var clinica=$("#clinicaid").val() ;
          var inicio=$("#fechainicio").val() ;
          var fin=$("#fechafin").val();
                  

    }
//
    $('#vfields').change(function() {
      $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function() {
      validator.defaults.alerts = (this.checked) ? false : true;
      if (this.checked)
        $('form .alert').remove();
    }).prop('checked', false);

  </script>
  <!-- pace -->
        <script src="js/pace/pace.min.js"></script>
        <script>
          var handleDataTableButtons = function() {
              "use strict";
              0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
                dom: "Bfrtip",
                buttons: [{
                  extend: "csv",
                  className: "btn-sm"
                }, {
                  extend: "excel",
                  className: "btn-sm"
                }, {
                  extend: "pdf",
                  className: "btn-sm"
                }, {
                  extend: "print",
                  className: "btn-sm"
                }],
                responsive: !0
              })
            },
            TableManageButtons = function() {
              "use strict";
              return {
                init: function() {
                  handleDataTableButtons()
                }
              }
            }();
        </script>
         <script type="text/javascript">
          $(document).ready(function() {
          	// Smart Wizard
		      $('#wizard_verticle').smartWizard({
		        transitionEffect: 'slide'
		      });
            <?php
              if($option == "m"){
            ?>
                $("#tab1").removeClass('active');
                $("#tab2").addClass('active');
                $("#tab_content1").removeClass('active in');
                $("#tab_content2").addClass('active in');
                
            <? } ?>

            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({
              keys: true
            });
            $('#datatable-responsive').DataTable();
            
            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true
            });
          });
          TableManageButtons.init();
		</script>
		
</body>

</html>

