<?php 
session_start();
if (!isset($_SESSION['csmart']['idusuario'])) {
Header ("Location: login.php");
}


    include 'headercondicion.php';
    include 'classes/ctipoUsuario.php';
    $oTipo  = new tipoUsuario();



    $accion   = "Crear";
    $idestado  = 0;
    $idtipoUsuario  = "";
    $imagen       = "";
    $telefono       = "";
    $correo       = "";
    $nombre       = "";
    $idusuario   =0;
    $notas       = "verificar datos";
    $idclinica  = $_SESSION['csmart']['clinica'];

   


      $vTipo     = $oTipo->getAll();
      $vUserr     = $oUser->getAllxclinica1($_SESSION['csmart']['clinica'],$_SESSION['csmart']['idusuario']);  
      if($vUserr!=""){
        $vcan= count($vUserr);
      }else{
        $vcan= 0;
      }

      $tpusuario=$_SESSION['csmart']['permisos'];
 ?>  

 <!DOCTYPE html>
<html >
<head>
  <?php include "header.php";?>
  <script type="text/javascript">
      function ireditar(id){
				$("#idusuarioe").val(id);
				$("#formularioeditar").submit();
			}

      function guardarpermisos(idusud){
        var pe=0,pe1=0,pe2=0,pe3 =0;
        for (x=0;x<permisosm.length;x++){
               if(permisosm[x]=='0'){
                  pe=1;
               }else{
                 if(permisosm[x]=='1'){
                   pe1=1;
                  }else{
                    if(permisosm[x]=='2'){
                   pe2=1;
                 }else{
                  if(permisosm[x]=='3'){
                   pe3=1;
                  }
                 } 
                 } 
               }
        }
			           	$.ajax({
                      type: 'POST',
                      url: 'actions/actionUsuario.php',
                      data: {idusud:idusud,pe:pe,pe1:pe1,pe2:pe2,pe3:pe3, opt: 'registrarpermiso'},
                            success: function(data) {  
                             if (data != 0){   
                              new PNotify({
                               title: 'Usuarios',
                               text: 'Todos permisos seleccionados fueron guardados correctamente!',
                               type: 'info'
                              });

                             }else{
                              new PNotify({
                               title: 'Usuarios',
                               text: 'No se guardaron los permisos!',
                               type: 'error'
                              });
                            }
                          }	
                     }); 
				}



        function eliminar(id) {
          if (confirm("Atención! Va a proceder a eliminar este registro. Desea continuar?")) {
             var form = "valor"; 
            $.ajax({
              url: 'actions/actionUsuario.php?opt=e&id='+id,
              type:'POST',
              data: { valor1: form, }
            }).done(function( data ){   
              if(data == 0){
                  new PNotify({
                   title: 'Datos Eliminados',
                    text: 'Todos los datos fueron guardados. Puede continuar.!',
                    type: 'info'
                  });
                  window.setTimeout("location.reload(true);",2500);
              }
              else{
                    new PNotify({
                     title: 'Error en formulario',
                    text: 'No se puedieron guardar los datos, intente de nuevo.',
                    type: 'error'
                 });
                  window.setTimeout("location.reload(true);",2500);
              }
            });
          }
      }

      function guardarm(){
        var toper= permisosm.length;

if(toper==0){
 if (confirm("Atención! Esta seguro que no quiere dar ningún permiso a este usuario?")) {
        var fsizen ="";
        var i5="";
        var file_data = $('#imagenS').prop('files')[0];   
        var form_data = new FormData();                  
        form_data.append('file', file_data);
        if(file_data){ 
          var fsize = $('#imagenS')[0].files[0].size; //get file size
          var fsizen = $('#imagenS')[0].files[0].name;
          if(fsize>1048576) 
          {
            new PNotify({
                 title: 'Error en Imagen!',
                 text: 'Excede el tamaño máximo permitido. Tamaño permitido menor a 1 Mb.'
             });
          }
        }
           if(fsizen!=""){
             i5 = fsizen;         
           }else{
             i5="user.png";
           }
        var i1 = $("#name").val();
				var i2 = $("#clave").val();
				var i3 = $("#telefono").val();
				var i4 = $("#correo").val();
				var i6 = $("#notas").val();
        var i7 = $("#idtipoUsuario").val();
        var i9 = <?=$idclinica?>;
      var lo1=$("#clave").val().length;
      var lo2=$("#password2").val().length;


      if(lo1!="" && lo2!=""){
        if((lo1>=6) && (lo2>=6)){
            if($("#clave").val()==$("#password2").val()){
              var email=$("#correo").val();
	          	var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		            if(regex.test(email) ){
				              $.ajax({
                      type: 'POST',
                      url: 'actions/actionUsuario.php',
                      data: {email:email,opt: 'verificarcorreo'},
                      success: function(data) { 
                          if (data > 0){         
							                  new PNotify({
							                 	title: 'Señor Usuario',
                                text: 'Este correo ya tiene una cuenta creada. !!!',
                                type: 'error'
                              });              
                            } else {
                              $.ajax({
                                url:'actions/actionUsuario.php',
                                type:'POST',
                                data: {name:i1,clave:i2,telefono:i3,correo:i4,
                                img:i5,notas:i6,idtipoUsuario:i7,idclinica:i9, opt: 'nn' }
                                }).done(function( data ){
                               if(data > 0){                     
                                       if(fsizen!=""){
                                        $(".loaderp").fadeIn("fast");
                                     $.ajax({
                                     url: 'processUpload.php?id='+data + '&tipo=usuario', // point to server-side PHP script 
                                     dataType: 'text',  // what to expect back from the PHP script, if anything
                                     cache: false,
                                     contentType: false,
                                     processData: false,
                                     data: form_data,                         
                                     type: 'post',
                                     success: function(val){
                                      guardarpermisos(data);
                                      $(".loaderp").fadeOut("slow");

                                      window.setTimeout("document.location.href='fusuariopnua.php';",2500);
                                        new PNotify({
                                   title: 'Datos Guardados',
                                   text: 'Todos los datos fueron guardados. Puede continuar.',
                                   type: 'info'
                                   });


                                   
                                      }
                                     });  
                                           }else{

                                            guardarpermisos(data);
                                            $(".loaderp").fadeIn("fast");
                                     
                                            window.setTimeout("document.location.href='fusuariopnua.php';",2500);
                                        new PNotify({
                                   title: 'Datos Guardados',
                                   text: 'Todos los datos fueron guardados. Puede continuar.',
                                   type: 'info'
                                   });


                                           
                                           }
         
          }else{
            new PNotify({
                 title: 'Error en datos',
                text: 'Falta información!'
             });
          }
          
        });
							   

                
                            }
                        }	
                     }); 

     }else{
               new PNotify({
                   title: 'Señor usuario',
                   text: 'En campo correo tiene que tener todos las caracteristicas de un correo como ser "@", ".com", "etc."!!!',
                   type: 'warning'
                 });
     }

            }else{
               new PNotify({
                title: 'Error',
                text: 'las claves no coinciden',
                type: 'error'
               });
            }
      }else{
        new PNotify({
                   title: 'Señor usuario',
                   text: 'Verifique la clave introducida tiene que ser mayor o igual a 6 dígitos !!!',
                   type: 'warning'
                 });
        }


      }else{

        new PNotify({
                   title: 'Señor usuario',
                   text: 'Tienen que rellenar los campos donde introduce la contraseña !!!',
                   type: 'warning'
                 });
      }




		



 }
}else{
        var fsizen ="";
        var i5="";
        var file_data = $('#imagenS').prop('files')[0];   
        var form_data = new FormData();                  
        form_data.append('file', file_data);
        if(file_data){ 
          var fsize = $('#imagenS')[0].files[0].size; //get file size
          var fsizen = $('#imagenS')[0].files[0].name;
          if(fsize>1048576) 
          {
            new PNotify({
                 title: 'Error en Imagen!',
                 text: 'Excede el tamaño máximo permitido. Tamaño permitido menor a 1 Mb.'
             });
          }
        }
         
           if(fsizen!=""){
             i5 = fsizen;         
           }else{
             i5="user.png";
           }
  
        var i1 = $("#name").val();
				var i2 = $("#clave").val();
				var i3 = $("#telefono").val();
				var i4 = $("#correo").val();
				var i6 = $("#notas").val();
        var i7 = $("#idtipoUsuario").val();
        var i9 = <?=$idclinica?>;
        var lo1=$("#clave").val().length;
        var lo2=$("#password2").val().length;

        if(lo1!="" && lo2!=""){

          if((lo1>=6) && (lo2>=6)){
            if($("#clave").val()==$("#password2").val()){
              var email=$("#correo").val();
	          	var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		            if(regex.test(email) ){
				              $.ajax({
                      type: 'POST',
                      url: 'actions/actionUsuario.php',
                      data: {email:email,opt: 'verificarcorreo'},
                      success: function(data) { 
                          if (data > 0){         
							                  new PNotify({
							                 	title: 'Señor Usuario',
                                text: 'Este correo ya tiene una cuenta creada. !!!',
                                type: 'error'
                              });              
                            } else {
                              $.ajax({
                                url:'actions/actionUsuario.php',
                                type:'POST',
                                data: {name:i1,clave:i2,telefono:i3,correo:i4,
                                img:i5,notas:i6,idtipoUsuario:i7,idclinica:i9, opt: 'nn' }
                                }).done(function( data ){
                               if(data > 0){
                                
                                       if(fsizen!=""){
                                        $(".loaderp").fadeIn("fast");
                                     $.ajax({
                                     url: 'processUpload.php?id='+data + '&tipo=usuario', // point to server-side PHP script 
                                     dataType: 'text',  // what to expect back from the PHP script, if anything
                                     cache: false,
                                     contentType: false,
                                     processData: false,
                                     data: form_data,                         
                                     type: 'post',
                                     success: function(val){
                                      guardarpermisos(data);
                                      $(".loaderp").fadeOut("slow");
                                      window.setTimeout("document.location.href='fusuariopnua.php';",2500);
                                        new PNotify({
                                   title: 'Datos Guardados',
                                   text: 'Todos los datos fueron guardados. Puede continuar.',
                                   type: 'info'
                                   });    
                                      }
                                     });  
                                           }else{
                                            guardarpermisos(data);
                                            $(".loaderp").fadeIn("fast");
                                            window.setTimeout("document.location.href='fusuariopnua.php';",2500);
                                        new PNotify({
                                   title: 'Datos Guardados',
                                   text: 'Todos los datos fueron guardados. Puede continuar.',
                                   type: 'info'
                                   });

                                  
                                           }
         
          }else{
            new PNotify({
                 title: 'Error en datos',
                text: 'Falta información!'
             });
          }
          
        });
							   

                
                            }
                        }	
                     }); 

     }else{
               new PNotify({
                   title: 'Señor usuario',
                   text: 'En campo correo tiene que tener todos las caracteristicas de un correo como ser "@", ".com", "etc."!!!',
                   type: 'warning'
                 });
     }

            }else{
               new PNotify({
                title: 'Error',
                text: 'las claves no coinciden',
                type: 'error'
               });
            }
      }else{
        new PNotify({
                   title: 'Señor usuario',
                   text: 'Verifique la clave introducida tiene que ser mayor o igual a 6 dígitos !!!',
                   type: 'warning'
                 });
        }


        }else{
          new PNotify({
                   title: 'Señor usuario',
                   text: 'Tienen que rellenar los campos donde introduce la contraseña !!!',
                   type: 'warning'
                 });


        }
			
}

      }

      function cancelar(){
        window.setTimeout("document.location.href='index.php';",500);
      }

        $(window).load(function() {
					$(".loaderp").fadeOut("slow");
				});

  </script>

</head>
<body class="nav-md">

  <?php if($tpusuario==2){?>

    <div class="loaderp" style="position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('images/cargando.webp') 50% 50% no-repeat rgb(249,249,249);
    opacity: .8;"></div>
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
            <?php include "menu.php" ?>
        </div>
      </div>

           <?php include "top_nav.php" ?>
      <div class="right_col" role="main">
        <div class="">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Administración de Perfil</h2>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                    <li id="tab1" role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Listado</a>
                      </li>
                      <li id="tab2" role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false"><?=$accion;?> Usuario</a>
                      </li>
                    </ul>



                    <div id="myTabContent" class="tab-content">
                      <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                  <?php if($vcan==0){?>
                   
              
                      <div class="jumbotron" id="info1">
                    <div class="row">
				            <div class="col-md-12 col-sm-12 col-xs-12">
               
               <strong><h2>No tienes Usuarios Registrados</h2></strong>
                    </div>
				                        
				            </div>
                  </div>
                

                  <?php }else{?>

                    <div class="x_content">
                            <table id="datatable-buttons" class="table table-striped table-bordered">
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Nombre</th>
                                  <th>Telefono</th>
                                  <th>Correo</th>
                                  <th>Tipo</th>
                                  <th>Estado </th>
                                  <th>Opciones</th>            
                                </tr>
                              </thead>
                              <tbody>
                                <?php
                                    if($vUserr){
                                      $i=1;
                                      foreach ($vUserr AS $id => $array) {
                                    ?>
                                      <tr>
                                        <td align="left"><?=$i;?></td>
                                        <td align="left" ><?=$array['nombre'];?></td>
                                        <td align="left"><?=$array['telefono'];?></td>
                                        <td align="left"><?=$array['correo'];?></td>   
                                        <td align="left"><?=$array['tipo'];?></td>
                                        <td align="center">                         
                                        <?php if($array['idestado']==0){ ?>  <a style="font-size:75%" onClick="habilitar(<?=$id?>);"   class="label label-danger" >Inactivo</a>  
																			<?php }else{ ?>
                                        <a style="font-size:75%" class="label label-success" >Activo</a>  
																				<?php };?>       
                                        </td>
                                        <td align="center">
                                          <a onclick="ireditar(<?=$id?>)" title="Perfil" >
																	        	<span class="label label-primary" >
																			       <i class="fa fa-pencil" title="Modificar"></i>
																		        </span> 
																	        </a>
                                          &nbsp;
                                          <span class="label label-default" onClick="eliminar(<?=$id?>)" >
                                                <i class="fa fa-trash-o" title="Eliminar"></i>  
                                          </span>   
                                        </td>
                                      </tr>
                                    <?php
                                      $i++;}
                                    }
                                    ?>
                              </tbody> 
                            </table>
                          </div>


                  <?php }?>
                         




                        </div>
                      </div>
                      </div>


                      <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                        <form id="form" class="form-horizontal form-label-left" novalidate>
                          <span class="section">Datos de tu Perfil</span>
                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">
                      </label>
                      <div class="col-md-5">

                          <?php if ($imagen =="") { ?>
                            <img src="images/user.png" alt="" class="img-circle img-responsive">
                          <?php }else{ ?>


                            <?php
                                    if(file_exists("images/usuario/".$idusuario."/".$imagen)){
                                      $aqui="images/usuario/".$idusuario."/".$imagen;
                                      
                                    } else {
                                      $aqui="images/user.png";
                                    
                                    }
                                    ?>



                            <img src="<?=$aqui?>" alt="" class="img-circle img-responsive" style="border-radius: 50%;
                            height:150px;
                            width: 150px;
                            border: 2px solid;
                            border-color: #58D3F7;">
                         <?php } ?>
                          </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="imagenS"> Foto perfil <span class="required">*</span>
                      </label>
                      <div class="col-md-5"> 
                        <input name="imagenS" class="fileupload " id="imagenS" type="file" />
                        <input name="img" id="img" type="hidden" value="<?=$imagen?>" />
                     </div>
                                
                    </div>
                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nombre <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="name" class="form-control col-md-7 col-xs-12"  name="name" value="<?=$nombre?>" placeholder="nombre" required="required" type="text">
                      </div>
                    </div>

             
                    
                    <div class="item form-group">
                      <label for="password" class="control-label col-md-3 col-sm-3 col-xs-12">Clave <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="clave" type="password" name="clave"  class="form-control col-md-7 col-xs-12" required="required">
                      </div>
                    </div>
                    <div class="item form-group">
                      <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">Repetir clave <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="password2" type="password" name="password2"  class="form-control col-md-7 col-xs-12" required="required">
                      </div>
                    </div>
                    


                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="correo">Email <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="email" id="correo" name="correo" required="required" value="<?=$correo?>" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telephone">Telefono <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="number" id="telefono" name="telefono" required="required" value="<?=$telefono?>"  class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>

              
                    <!--    <input type="hidden" id="nclinica" name="nclinica" required="required" value="<?=$nclinica?>" class="form-control col-md-7 col-xs-12" disabled>-->
                    



                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Tipo de Usuario <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <select id="idtipoUsuario" name="idtipoUsuario" class="form-control">
                          <option>Elegir una opción</option>
                          <?php if($vTipo){
                                foreach ($vTipo AS $id => $array) {?>
                                <option value="<?=$array['idtipoUsuario'];?>" <?php if($idtipoUsuario==$id){echo"selected='selected'";}?>><?=$array['nombre'];?></option>
                          <?php } } ?>
                        </select>
                      </div>
                    </div>


         

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="notas">Permisos del Menu <span class="required">*</span>
                      </label>


                      <div class="row"> 
                          <div class="col-md-3"> 
                          
                          <input   type="checkbox" name="permiso" id="permiso"   value="0"> Agenda<br>
                          <input   type="checkbox" name="permiso1" id="permiso1"     value="1" > Panel de Control<br>
                         
                                
                          </div>  

                          <div class="col-md-3"> 
                          
                          <input   type="checkbox" name="permiso2"  id="permiso2"   value="2" > Mi Clínica<br>
                          <input   type="checkbox" name="permiso3"  id="permiso3"   value="3" > Reportes<br>
                              
                       
                          </div>  
                      </div>
                       
                     
                      </div>

                
                
                        <input type="hidden" id="notas" name="notas"  value="<?=$notas?>" class="form-control col-md-7 col-xs-12" >
                    






                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-md-offset-3">
                        <input type="button" onClick="cancelar();"  style="border-radius: 20px;" class="btn btn-primary" value="Cancelar">
                        <input type="button" onClick="guardarm();"  style="border-radius: 20px;" class="btn btn-success" value="Guardar">
                        
                      </div>
                    </div>
                  </form>
           
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <form style="display: hidden" action="fusuariopa.php" method="POST" id="formularioeditar">
								<input type="hidden" id="idusuarioe" name="idusuarioe" value=""/>																	
		    </form>
        <div style="height: 100px;"></div>
       <? include "piep.php"; ?> 


      </div>
    </div>
  </div>

    <?php }else{?>
    
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
            <?php include "menu.php" ?>
        </div>
      </div>

      <?php include "top_nav.php" ?>
      <div class="right_col" role="main">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
              <div class="jumbotron" id="info1">
                    <div class="row">
				            <div class="col-md-12 col-sm-12 col-xs-12">
               
               <strong><h2>Usted no tiene acceso a esta Página</h2></strong>
                    </div>
				                        
				            </div>
                  </div>
                </div>
              </div>
            </div>
         
          <div style="height: 100px;"></div>
       <? include "piep.php"; ?> 
       </div>
     </div>  
  </div>

    <?php }?>


  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>
  <script src="js/custom.js"></script>
  <!-- form validation -->
  <script src="js/validator/validator.js"></script>
  <!-- PNotify -->
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  
   <!-- Datatables-->
        <script src="js/datatables/jquery.dataTables.min.js"></script>
        <script src="js/datatables/dataTables.bootstrap.js"></script>
        <script src="js/datatables/dataTables.buttons.min.js"></script>
        <script src="js/datatables/buttons.bootstrap.min.js"></script>
        <script src="js/datatables/jszip.min.js"></script>
        <script src="js/datatables/pdfmake.min.js"></script>
        <script src="js/datatables/vfs_fonts.js"></script>
        <script src="js/datatables/buttons.html5.min.js"></script>
        <script src="js/datatables/buttons.print.min.js"></script>
        <script src="js/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="js/datatables/dataTables.responsive.min.js"></script>
        <script src="js/datatables/responsive.bootstrap.min.js"></script>
        <script src="js/datatables/dataTables.scroller.min.js"></script>


  <!-- pace -->
        <script src="js/pace/pace.min.js"></script>
        <script type="text/javascript">

$(document).ready(function() {
           $("#datatable-buttons").DataTable({
                dom: "Bfrtip",
                buttons: [{
                  extend: "csv",
                  exportOptions: {
                       columns: [ 0, 1, 2, 3, 4,5 ] //Your Colume value those you want
                           }
                }, {
                  extend: "excel",
                  exportOptions: {
                       columns: [ 0, 1, 2, 3, 4 ,5] //Your Colume value those you want
                           }
                }, {
                  extend: "pdf",
                  exportOptions: {
                       columns: [ 0, 1, 2, 3, 4 ,5] //Your Colume value those you want
                           }
                }, {
                  extend: "print",
                  exportOptions: {
                       columns: [ 0, 1, 2, 3, 4 ,5] //Your Colume value those you want
                           }         
                }],      
              });
            });
        </script>
        
         <script type="text/javascript">

          $('input:radio[name="permiso0"]').change(function() {
					  if (this.value == '0') {
			        alert("2");
					  }
					});

          var permisosm=new Array();
          
          var checkbox = document.getElementById('permiso');
          var checkbox1 = document.getElementById('permiso1');
          var checkbox2 = document.getElementById('permiso2');
          var checkbox3 = document.getElementById('permiso3');
          checkbox.addEventListener("change", validaCheckbox, false);
          checkbox1.addEventListener("change", validaCheckbox1, false);
          checkbox2.addEventListener("change", validaCheckbox2, false);
          checkbox3.addEventListener("change", validaCheckbox3, false);



          function validaCheckbox(){
           var checked = checkbox.checked;
           if(checked){
              permisosm.push(this.value);
            }else{
                 for (x=0;x<permisosm.length;x++){
                   if(permisosm[x]==this.value){
                    permisosm.splice(x, 1);
                   }
                 }
            }
          }

          function validaCheckbox1(){
           var checked = checkbox1.checked;
           if(checked){
              permisosm.push(this.value);
            
            }else{
              //permisosm.pop();
                 for (x=0;x<permisosm.length;x++){
                   if(permisosm[x]==this.value){
                    permisosm.splice(x, 1);
                   }
                 }
         
            
            }
          }
          function validaCheckbox2(){
           var checked = checkbox2.checked;
           if(checked){
              permisosm.push(this.value);
       
            }else{
              //permisosm.pop();
                 for (x=0;x<permisosm.length;x++){
                   if(permisosm[x]==this.value){
                    permisosm.splice(x, 1);
                   }
                 }
            
            
            }
          }
        
          function validaCheckbox3(){
           var checked = checkbox3.checked;
           if(checked){
              permisosm.push(this.value);
          
            }else{
              //permisosm.pop();
                 for (x=0;x<permisosm.length;x++){
                   if(permisosm[x]==this.value){
                    permisosm.splice(x, 1);
                   }
                 }
     
            
            }
          }

/*
          var checked = checkbox.checked;
          var permisosm=new Array();
          $('input:checkbox[name="permiso"]').change(function() {
					  if (this.value == '0') {
              permisosm.push(this.value);
					  }else{
              if(this.value == '1'){
                permisosm.push(this.value);
              }else{
                if(this.value == '2'){
                  permisosm.push(this.value);
                }else{
                  if(this.value == '3'){
                    permisosm.push(this.value);
                  }else{
                    if(this.value == '4'){
                      permisosm.push(this.value);
                    }else{
                  
                    }
                  }
                }
              }
            }
					});
*/
          $(document).ready(function() {
            $('#imagenS').change(function() {
                var filename = $(this).val();
                var lastIndex = filename.lastIndexOf("\\");
                if (lastIndex >= 0) {
                    filename = filename.substring(lastIndex + 1);
                } 
                $('#img').val(filename);
            });
            TableManageButtons.init();        
          });
  
        </script>
</body>

</html>
