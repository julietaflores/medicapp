<?php

session_start();
   if (!isset($_SESSION['csmart']['idusuario'])) {
   Header ("Location: login.php");
   }
   include 'headercondicion.php';
    include 'classes/cpaciente.php';
   
   
    include 'classes/cpagos.php';
    include 'classes/cpagocredito.php';
   
    $opagos = new pago();
    $opagocredito=new pagocredito();
    $opaciente = new paciente();

    $nombre='';
    $apellido='';
    $telefono="";
    $correo='';
    $pagado='';
    $debe="";
    $tratamiento='';
//$pago=$_REQUEST['pago'];
//$vpagocredigo = $opagocredito->getprueball($pago);
//var_dump($vpagocredigo);
        /*foreach ($vpagocredigo AS $id => $info){ 
                            
            
            $nombre		= $info["nombrepaciente"];
            $apellido		= $info["apellidopa"];
            $telefono		= $info["telefono1"];
            $correo		= $info["correopa"];
            $pagado		= $info["pagado"];
            $debe		= $info["debe"];
            $tratamiento		= $info["tratamiento"];
            
            }*/


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reporte Creditos</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">

  <?php include 'header.php'; ?>
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  <script type="text/javascript"> </script>
</head>
<body class="nav-md">

  <div class="container body">


    <div class="main_container">

      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          
          <!-- sidebar menu -->
            <?php include "menu.php" ?>
          <!-- /sidebar menu -->

         
        </div>
      </div>

      <!-- top navigation -->
           <?php include "top_nav.php" ?>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">

        <div class="">
          
         
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                  <div id="imprimirpagoss">
                  
                    
                <div class="x_title">
                  <h2>Reporte Creditos</h2>
                    <input type="hidden" id="usuarioid" value="<?=$_SESSION['csmart']['idusuario']?>">
                    <input type="hidden" id="clinicaid" value="<?=$_SESSION['csmart']['clinica']?>">
                  <div class="clearfix"></div>
                </div>
               
                <div class="x_content">

           <div class="row">
            <div class="col-md-12 col-xs-12">
              

                          

            </div>

            <div class="col-md-12 col-xs-12">
              <div class="x_panel">
               
                <div class="x_content">
                  <br />
                 
                  		
                  	<div class="row">
                  		<div class="col-sm-4 ">
                        <!--<label for=""><? //date_default_timezone_set ("America/La_Paz"); echo "The time in " . date_default_timezone_get() . " is " . date("H:i:s");?></label>-->
                      <label for="">Fecha Inicio:</label>			
                            <input id="fechainicio" type="date" value="<?php echo date('Y-m-d');?>">
		                      
                      </div>
                      <div class="col-sm-4 ">
                        <label for="">Fecha Fin:</label>			
                          <input id="fechafin" type="date" value="<?php echo date('Y-m-d');?>">
		                      
                      </div>
                      <div class="col-sm-4 ">
                        <a class="btn btn-default" onclick="javascript:reporte()">Generar</a>
		                      
                  		</div>
                  	</div>

							<br>							
							<br>

                    		
		                    <h2>Detalle </h2>
		                    <div class="item form-group">
		                    	<table id="reportesT" class="table table-striped">
				                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>Fecha</th>
                                            <th>Paciente</th>
                                            <th>Detalle</th>
                                            <th>Total</th>     
                                            <th>Pago</th>                     
                                            <th>Debe</th>
                                        </tr>
                                    </thead>
                              <tbody id="tablaReportes">
                              
                              </tbody>
				                    <tfoot id="tablafoot">
                            </tfoot>
				                    
                        </table>
                     
                        
                        
		                    </div>
		           

                </div>
              </div>
            </div>


            
              </div>	
              </div>		 
              </div>        
              </div>
            </div>
          </div>
        </div>

        <!-- footer content -->
        <center> <?php include "piep.php" ?> </center>
        <!-- /footer content -->

      </div>
      <!-- /page content -->
    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="js/moment/moment.min.js"></script>
  <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>
  
  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>
  <script src="js/custom.js"></script>
  <!-- form validation -->
  <script src="js/validator/validator.js"></script>
  <!-- PNotify -->
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  <script src="js/datatables/jquery.dataTables.min.js"></script>
        <script src="js/datatables/dataTables.bootstrap.js"></script>
        <script src="js/datatables/dataTables.buttons.min.js"></script>
        <script src="js/datatables/buttons.bootstrap.min.js"></script>
        <script src="js/datatables/jszip.min.js"></script>
        <script src="js/datatables/pdfmake.min.js"></script>
        <script src="js/datatables/vfs_fonts.js"></script>
        <script src="js/datatables/buttons.html5.min.js"></script>
        <script src="js/datatables/buttons.print.min.js"></script>
        <script src="js/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="js/datatables/dataTables.responsive.min.js"></script>
        <script src="js/datatables/responsive.bootstrap.min.js"></script>
        <script src="js/datatables/dataTables.scroller.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
  <script>
    var tabla;

    function reporte(){
     var nro=1;
     var pago="";
      if($("#tablaReportes tr").length>0){
        tabla.buttons().remove("1-3");
       tabla.destroy();

      }
          $("#tablaReportes tr").remove();
          $("#tablafoot tr").remove();
          
          var total=0.0;
          var totalpago=0.0;
          var totaldebe=0.0;
          var clinica=$("#clinicaid").val() ;
          var inicio=$("#fechainicio").val() ;
          var fin=$("#fechafin").val();
                   $.ajax({
                      url: 'actions/actionpagos.php?opt=reporteCreditos',
                      data: 'fechainicio='+inicio+
                            '&fechafin='+fin+
                            '&clinica='+clinica,
                      type: "POST",
                      dataType:'json',
                      success: function(json) {
                        if(json==400){
                          alert('no existe datos');
                        }else{
                          $.each(json, function(i, item) {
                            total=parseFloat(total)+parseFloat(item.total);
                            
                          pago=parseFloat(item.total)-parseFloat(item.debe);
                          totalpago=parseFloat(totalpago)+parseFloat(pago);
                          totaldebe=parseFloat(totaldebe)+parseFloat(item.debe);
                          var fila="<tr><td>"+nro+"</td><td>"+convertirfecha(item.fechaPago)+"</td><td>"+item.nombrePa+"</td><td>"+item.tratamiento+"</td><td>"+item.total+"</td><td>"+pago+"</td><td>"+item.debe+"</td></tr>";
                          var btn = document.createElement("TR");
                          btn.innerHTML=fila;
                          document.getElementById("tablaReportes").appendChild(btn);
                          nro++;
                        });
                        var btn1 = document.createElement("TR");
                          btn1.innerHTML="<tr><th></th><th></th><th></th><th></th><th>Total: "+total+"</th><th>Total: "+totalpago+"</th><th>Total: "+totaldebe+"</th></tr>";
                          document.getElementById("tablafoot").appendChild(btn1);
                        construirtabla();
                        console.log(json);
                        }
                       
                      
                    }
                  });

    }

    
function construirtabla(){

 tabla=$("#reportesT").DataTable({
    dom: 'Bfrtip',
    columnDefs: [
    {
        targets: [4,5,6],
        className: 'dt-right'
       
    }
  ],
        buttons: [
            'print','excel', 'pdf'
        ], "order": [[ 1, "asc" ]]
                                });
}

function convertirfecha(fecha){
  var today = new Date(fecha);
var dd = today.getDate();

var mm = today.getMonth()+1; 
var yyyy = today.getFullYear();
    if(dd<10) 
    {
        dd='0'+dd;
    } 

    if(mm<10) 
    {
        mm='0'+mm;
    } 
return today = dd+'-'+mm+'-'+yyyy;
}

   function imprimir(){
  //$("#tablapagoscredito").print();
debugger;

  var printContents = document.getElementById('imprimirpagoss').innerHTML;
  
			var originalContents = document.body.innerHTML;
			document.body.innerHTML = printContents;
           
			window.print();
      window.location.reload(true);
			document.body.innerHTML = originalContents;
            
}
  </script>
 
   

 
 <div style="height: 100px;"></div>
</body>
</html>