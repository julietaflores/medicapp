<?php
   session_start();
   if (!isset($_SESSION['csmart']['idusuario'])) {
   Header ("Location: login.php");
   }
    include 'headercondicion.php';
    include 'classes/cpaciente.php';   
    include 'classes/cpagos.php';
  
    $opagos = new pago();
    $opaciente = new paciente();


    $fechaInicio = date("Y-m-") . "01";
    $fechaFin = date("Y-m-t");
    $pagos= $opagos->getAlltotalconseguroconpaciente();
  
    $seguros    =  $oUser->getAllseguros1();
    $idseguro="";

?>


<!DOCTYPE HTML><html>
<head>

<?php include 'header.php'; ?>

</head>
<body class="nav-md">

<div class="container body">
  <div class="main_container">  
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
            <?php include "menu.php" ?>
        </div>
      </div>
           <?php include "top_nav.php" ?>
    <div class="right_col" role="main">
      <div class="">
  
 
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        
      <div class="x_title">
          <h2>Administración de Pagos con Seguro</h2>    
           <div class="title_right"></div>
          <div class="clearfix"></div>
        </div>




        <div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
		 <div class="x_panel">
                        
     
                        <div class="x_title">
				                  <h2>Reporte  <small>por fechas</small></h2>
				                  	<div class="col-md-2 col-sm-2 col-xs-12">
				                        <input type="text" class="form-control has-feedback-right" id="fecha" name="fecha" value="<?=$fechaInicio?>"  >
			                              <span class="fa fa-calendar-o form-control-feedback right" aria-hidden="true"></span>
			                               
			                          </div>
			                        <div class="col-md-2 col-sm-2 col-xs-12">
				                        <input type="text" class="form-control has-feedback-right" id="fecha2" name="fecha2" value="<?=$fechaFin?>" >
			                              <span class="fa fa-calendar-o form-control-feedback right" aria-hidden="true"></span>
			                               
                                </div>
                                
                                <label class="col-md-1 control-label"  for="formaseguro">Seleccione un Seguro</label>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                      
                                <select class="form-control" name="formaseguroid" id="formaseguroid">
                                <option value="0">Todos los Seguros</option>
                                 <?php if($seguros){
                                  foreach ($seguros AS $id => $array) {?>
                                  <option value="<?=$array['idseguro'];?> " <?php if($idseguro==$id){echo"selected='selected'";}?>><?=$array['nombre'];?></option>

                                 <?php } } ?>
                            </select>
                        
			                          </div>

				                  	<button type="button" id="consultar" name="consultar" class="btn btn-info btn-xs">Consultar</button>
				                  <ul class="nav navbar-right panel_toolbox">
				                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				                    </li>
				                    <li><a class="close-link"><i class="fa fa-close"></i></a>
				                    </li>
				                  </ul>
				                  <div class="clearfix"></div>
				                </div>
                

                        

  
                
                
                  <div class="x_content">
                   
                    <table id="reporteT" class="table table-striped table-bordered">
                      <thead>
                    <tr>
                      <th>#</th>
                      <th>Paciente</th>
                        <th>Tratamiento</th>
                        <th>Forma de Pago</th>
                        <th>Fecha</th>
                        <th>Monto</th>
                        <th>Seguro	</th>
                        <th>Notas</th>
                         
                    </tr>
                  </thead>
                  <tbody id="tablaReportes1">        
        </table>
        </div>
                
            
				              </div>
				            </div>
                  </div>
                


      </div> 
    </div>
  </div>
  </div>
<div style="height: 100px;"></div>
<?php include "piep.php" ?>

   
  </div>
 </div>
</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>



  <script src="js/bootstrap.min.js"></script>
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>
  <script src="js/custom.js"></script>
  <!-- form validation -->
  <script src="js/validator/validator.js"></script>
  <!-- PNotify -->
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  <script src="js/chartjs/chart.min.js"></script>
  
   <!-- Datatables-->
        <script src="js/datatables/jquery.dataTables.min.js"></script>
        <script src="js/datatables/dataTables.bootstrap.js"></script>
        <script src="js/datatables/dataTables.buttons.min.js"></script>
        <script src="js/datatables/buttons.bootstrap.min.js"></script>
        <script src="js/datatables/jszip.min.js"></script>
        <script src="js/datatables/pdfmake.min.js"></script>
        <script src="js/datatables/vfs_fonts.js"></script>
        <script src="js/datatables/buttons.html5.min.js"></script>
        <script src="js/datatables/buttons.print.min.js"></script>
        <script src="js/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="js/datatables/dataTables.responsive.min.js"></script>
        <script src="js/datatables/responsive.bootstrap.min.js"></script>
		<script src="js/datatables/dataTables.scroller.min.js"></script>
		<script src="js/pace/pace.min.js"></script>
<!-- daterangepicker -->
  <script type="text/javascript" src="js/moment/moment.min.js"></script>
  <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>


 <script type="text/javascript" >
  var tabla;


$(document).ready(function() {


$('#fecha').daterangepicker({
		        singleDatePicker: true,
		        calender_style: "picker_3"
		      }, function(start, end, label) {
		        console.log(start.toISOString(), end.toISOString(), label);
		      });
          	$('#fecha2').daterangepicker({
		        singleDatePicker: true,
		        calender_style: "picker_3"
		      }, function(start, end, label) {
		        console.log(start.toISOString(), end.toISOString(), label);
		      });

});



  $('#consultar').click(function(){
      if($("#tablaReportes1 tr").length>0){
        tabla.buttons().remove('1-4');
       tabla.destroy();
      }
      $("#tablaReportes1 tr").remove();
		 construirtabla();
  });






   function construirtabla(){
    var fecha1 = $('#fecha').val();
    var fecha2 = $('#fecha2').val();
    var selectBox = document.getElementById("formaseguroid");
      var idseguro = selectBox.options[selectBox.selectedIndex].value;
          		$.ajax({
                	url: "actions/actionpagos.php?opt=d1seguro",
                    type:"POST",
                    dataType: 'json',
                    data: {fecha1:fecha1,fecha2:fecha2,idseguro:idseguro },
                    success: function(data) { 
                        if(data!=400){
							var nro=1;
							var est="";
						   $.each(data, function(i, item) {
                                if(item.idformaPago =='1'){
                                     est="Efectivo";
								}else{
									 est="Tarjeta";
								}
								var fechafin =moment(item.fechaPago).format('DD-MM-YYYY'); 


                            var fila="<tr><td>"+nro+"</td> <td>"+item.nombrePa+"</td> <td>"+item.tratamiento+"</td> <td>"+est+"</td> <td>"+fechafin+"</td> <td>"+item.monto+"</td> <td>"+item.nombreseguro+"</td> <td>"+item.observaciones+"</td>   </tr>";
                          
                         var btn = document.createElement("TR");
                          btn.innerHTML=fila;
                          document.getElementById("tablaReportes1").appendChild(btn);
                          nro++;             
                        });

                        construirt();       
               
						}else{
							alert('no hay datos');
						}
                      

                    }
                }) ;
}

 

function construirt(){
  tabla=$("#reporteT").DataTable({
	dom: "Bfrtip",
                buttons: [{
                  extend: "csv",
                  exportOptions: {
                       columns: [ 0, 1, 2, 3, 4,5,6,7 ] //Your Colume value those you want
                           }
                }, {
                  extend: "excel",
                  exportOptions: {
                       columns: [ 0, 1, 2, 3, 4 ,5,6,7] //Your Colume value those you want
                           }
                }, {
                  extend: "pdf",
                  exportOptions: {
                       columns: [ 0, 1, 2, 3, 4 ,5,6,7] //Your Colume value those you want
                           }
                }, {
                  extend: "print",
                  exportOptions: {
                       columns: [ 0, 1, 2, 3, 4 ,5,6,7] //Your Colume value those you want
                           }         
                }]
		});

}


 </script>

 

</body>
</html>