<?php 
session_start();
if (!isset($_SESSION['csmart']['idusuario']) && !isset($_SESSION['csmart']['clinica'])) {
Header ("Location: login.php");
}


    include 'data/dataBase.php';
    include 'classes/cmembresia.php'; 
    include 'classes/cmembresiaClinica.php'; 
    $omembresia = new Membresia();
    $omembresiaClinica = new MembresiaClinica(); 


    $vmembresiaClinica = $omembresiaClinica->getAll1p($_SESSION['csmart']['idusuario']);

    if($vmembresiaClinica){
        foreach ($vmembresiaClinica AS $id => $data){ 
        $idmembresiaClinica= $data['idmembresiasClinica'];
        $nombrem= $data['nombrem'];
        $idusuario		=  $data['idusuario'];
             }
    }

 ?>  

 <!DOCTYPE html>
<html >


<style type="text/css">	
	#top-image {
	background:url('images/fondoclinica.jpg') -25px -50px;
	position:fixed ;
	top:0;
	width:100%;
	z-index:0;
		height:100%;
		background-size: calc(100% + 50px);
	}

</style>



<head>
  
  <?php include "headerm.php";?>
  <script type="text/javascript">	
$(document).ready(function() {
setTimeout(function(){
  setTimeout( "window.location.href='logout.php'", 17000);
});
});

$(document).ready(function() {
		var movementStrength = 25;
		var height = movementStrength / $(window).height();
		var width = movementStrength / $(window).width();
		$("#top-image").mousemove(function(e){
							var pageX = e.pageX - ($(window).width() / 2);
							var pageY = e.pageY - ($(window).height() / 2);
							var newvalueX = width * pageX * -1 - 25;
							var newvalueY = height * pageY * -1 - 50;
							$('#top-image').css("background-position", newvalueX+"px     "+newvalueY+"px");
		});
	});


	    function recordatorio(){ 

        var nombre = '<?php echo $nombrem;?>'
	             $.ajax({
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        url: "ws/api_rest.php/getrecordatorioAdmin/"+<?=$_SESSION['csmart']['idusuario']?>+"/"+nombre,
                        dataType: "json",
                        data: { },
                        success: function (data){
                           if(data==1){
							                new PNotify({
                              title: 'Usuario',
                              text: 'Se envio un correo al Administrador ya ellos se comunicaran con usted gracias por utilizar la plaataforma !!!',
                              type: 'success'
                              });       
                            }else{
                            if(data==400){
							                new PNotify({
                              title: 'Usuario',
                              text: 'Nose pudo enviar el correo !!!',
                              type: 'error'
                               });       
                             }
                            }
                            window.setTimeout("document.location.href='logout.php';",2500);
                          }
                });
                
                
      }			
</script>
</head>

<body>


<div id="top-image" class="container">
    <div class="main_container">
      <div class="right_col" role="main">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
             
          
   <div class="col-md-3 col-sm-3 col-xs-3">
   </div>
   
   <div class="col-md-6 col-sm-6 col-xs-6">
   <div class="contenttt" style="background: white;">
   
   <div style="display: table;margin: auto;width: 100%;font-size: 20px;text-align: center;">
     <div style="display: table-cell;vertical-align:middle ;width: 100%;font-size: 15px;padding: 0 10px;">
      <div class="field-items">
        <div class="fiel"> Solicitud Pendiente</div>
      </div>
     </div>
   </div>
   
   <div style="display: table;margin: auto;width: 100%;font-size: 16px;text-align: center;">
     <div style="display: table-cell;vertical-align:middle ;width: 100%;font-size: 15px;padding: 0 10px;">
      <div class="field-items">
        <div class="fiel2">Su Membresía <strong> <?=$nombrem?> </strong> esta en estado <strong>Pendiente</strong> espere a que los Administradores de la Plataforma se comuniquen con usted !!!</div>
      </div>  
     </div>
   </div>
   
   
   
    <div class="fieldd">
      <div style="display: table;margin: auto;width: 90%;font-size: 16px;text-align: center;">
        <div style="display: table-cell;vertical-align: middle;width: 20%;font-size: 20px;padding: 0 10px;">
          <a class="btnLog"  onclick="recordatorio();">Comunicarse con el Administrador</a>
        </div>
      </div>
    </div>
   
    </div>
   </div>

   <div class="col-md-3 col-sm-3 col-xs-3">
   </div>    



            </div>   
        </div>
      </div>
    </div>
  </div>



  <script src="js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>
  <script src="js/custom.js"></script>
  <!-- form validation -->
  <script src="js/validator/validator.js"></script>
  <!-- PNotify -->
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  
  <script type="text/javascript" src="js/moment/moment.min.js"></script>
  <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>

  
</body>

</html>
