<?php 
session_start();
if (!isset($_SESSION['csmart']['idusuario']) && !isset($_SESSION['csmart']['clinica'])) {
Header ("Location: login.php");
}

    include 'headercondicion.php';
    include 'classes/cmembresia.php'; 
    include 'classes/creferidos.php';
    $omembresia = new Membresia();
  	$oreferidos = new referido();
    $accion   = "Crear";
    $option   = "n";
    $idmembresia		= "";
    $fechaCompra		= "";
    $idformaPago		= "";
    $idestado		= "";  
    $idclinica		=  "";
    $valor		=  "";
    $idusuario		=  ""	;
    $fechafin;
    $fechaCompra = date("Y-m-d");
  
    if(isset($_REQUEST['opt']) && $_REQUEST['opt']== "m"){
      $option = $_REQUEST['opt'];
      $idObj  = $_REQUEST['id'];
    }


    if ($option == "m") {
      $vmembresiaClinica = $omembresiaClinica->getOne($idObj);
      if($vmembresiaClinica){
        $accion 	= "Modificar";	
           foreach ($vmembresiaClinica AS $id => $data){ 
            $idmembresiaClinica= $data['idmembresiaClinica'];
          $idmembresia		= $data['idmembresia'];
          $fechaCompra		= $data['fechaCompra'];
          $idformaPago		= $data['idformaPago'];
          $idestado		= $data['idestado'];  
          $idclinica		=  $data['idclinica'];
          $valor		=  $data['valor'];
          $idusuario		=  $data['idusuario'];
          $fechafin= $data['fechafin'];
               }
      }else{
        header("Location: logout.php");
      }
    }
    $descuento=0;
    $membresiaUsu= $omembresiaClinica->getAll1($_SESSION['csmart']['idusuario']);
    foreach ($membresiaUsu as $id => $value) {
      $fechafin=$value['fechafin'];
    }
    $vmembresia     = $omembresia->getAllnogratis();
    $cvv= $omembresiaClinica->getTotalpendiente($_SESSION['csmart']['idusuario']);
    $vmembresiaClinica     = $omembresiaClinica->getAllmc($_SESSION['csmart']['idusuario']);
    $rre=round(12/count($vmembresia));
    $contarreferidos=$oreferidos->contardescuento($_SESSION['csmart']['clinica'],$fechafin);
    if($contarreferidos){
      $descuento=count($contarreferidos);
      $descuento=$descuento*10;
    }
    
    if($descuento>80){
      $descuento=8*10;
    }
 $array_estado = array (
  "0" => "Inactivo",
  "1" => "Activo",
);

$array_forma_de_pago = array (
  "1" => "Tigo Money",
  "2" => "Paypal",
  "3" => "Tarjeta",
);
 ?>  

 <!DOCTYPE html>
<html >
<head>
  <?php include "headerm.php";?>
  
  <script type="text/javascript">
    function prsolicitar(id){
      var idmembresia= $("#idmembresia"+id).val();
      var mem= $("#nombre"+id).val();
      var valor= $("#valor"+id).val();
      var formadepgo=0;
      var estado=0;
      var clinica=<?=$_SESSION['csmart']['clinica']?>;
//var valor = document.getElementById('valor').value; 
var idu =<?=$_SESSION['csmart']['idusuario']?>; 

  $.ajax({
          type: 'POST',
          url: 'actions/actionMembresiaClinica.php',
           data: { idmembresia:idmembresia, idclinica:clinica, valorr:valor, idusuario:idu,
           opt: 'n1'},
                success: function(data) {     
                 if (data != 0){   
                  new PNotify({
                  title: 'Usuario',
                  text: 'Se guardaron los datos correctamente!!!',
                  type: 'success'
                  });      
                  } else  {
                    new PNotify({
                  title: 'Usuario',
                  text: 'Nose pudo guardar los datos !!!',
                  type: 'error'
                   }); 

                  }
                }	
         }); 

var est=0;

//alert(idu +" "+mem);
   $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "ws/api_rest.php/getSolicitarMembresia/"+<?=$_SESSION['csmart']['idusuario']?>+"/"+mem+"/"+est,
            dataType: "json",
            data: { },
            success: function (data){
               if(data==1){
                  new PNotify({
                  title: 'Usuario',
                  text: 'Se envio un correo al Administrador ya ellos se comunicaran con usted gracias por utilizar la plaataforma !!!',
                  type: 'success'
                  });       
                }else{
                if(data==400){
                  new PNotify({
                  title: 'Usuario',
                  text: 'Nose pudo enviar el correo !!!',
                  type: 'error'
                   });       
                 }
                }
                window.setTimeout("document.location.href='membresiaClinica.php';",2500);
              }
     });



    }



    function eliminar1(id){
				$.ajax({
              type: 'POST',
              url: 'actions/actionMembresiaClinica.php',
              data: {id:id, opt: 'eliminarc'},
                success: function(data) { 
                 if (data != 0){
                      new PNotify({
                      title: 'Error Eliminados',
                      text: 'Los datos no fueron eliminados.!',
                      type: 'error'
                  });
               }
             }	
          });            
			}




   function solicitar(id){   
      if (confirm("Atención! Tas Seguro que quieres Solicitar esta Membresía ?")){
             var can=<?=$cvv?>;
             if( can>0){
               if (confirm("Estas seguro que quieres cambiar de Membresía ?")) {     
                    eliminar1(<?=$_SESSION['csmart']['clinica']?>);
                       new PNotify({
                       title: 'Datos Eliminados',
                       text: 'Todos los datos fueron guardados. Puede continuar.!',
                       type: 'success'
                      });
                       
                       prsolicitar(id);
                       
                       new PNotify({
                        title: 'Usuario',
                        text: 'datos procesados con exito!!!',
                        type: 'success'
                       }); 
                }
          }else{
                prsolicitar(id);
          }
      }
    }





</script>



  <script type="text/javascript">    
      function cancelar(){
        window.setTimeout("document.location.href='membresiaClinica.php';",500);
      }

      function eliminar(id,mem) {
          if (confirm("Atención! Va a proceder a eliminar este registro. Desea continuar?")) {
             var form = "valor"; 
            $.ajax({
              url: 'actions/actionMembresiaClinica.php?opt=e&id='+id,
              type:'POST',
              data: { valor1: form, }
            }).done(function( data ){   
              if(data == 0){
                  new PNotify({
                   title: 'Datos Eliminados',
                    text: 'Todos los datos fueron guardados. Puede continuar.!',
                    type: 'success'
                  });
              }
              else{
                 new PNotify({
                    title: 'Error en formulario',
                    text: 'No se puedieron guardar los datos, intente de nuevo.',
                    type: 'error'
                 });
              }
            });

            var est=2;
            $.ajax({
                type: "GET",
                 contentType: "application/json; charset=utf-8",
                  url: "ws/api_rest.php/getSolicitarMembresia/"+<?=$_SESSION['csmart']['idusuario']?>+"/"+mem+"/"+est,
                  dataType: "json",
                  data: { },
                    success: function (data){
                      if(data==1){
							            new PNotify({
                            title: 'Usuario',
                            text: 'Se envio un correo al Administrador ya ellos se comunicaran con usted gracias por utilizar la plaataforma !!!',
                            type: 'success'
                          });       
                      }else{
                          if(data==400){
							                new PNotify({
                              title: 'Usuario',
                              text: 'Nose pudo enviar el correo !!!',
                              type: 'error'
                              });       
                          }
                      }
                      window.setTimeout("location.reload(true);",2500);
                    }
                 });
          }
      }

  </script>

</head>
<body class="nav-md">
<div  id="btn3" class="loader" style="display: none"></div>	
  <div class="container body">
    <div class="main_container">
       <div class="col-md-3 left_col">
         <div class="left_col scroll-view">
            <?php include "menu.php" ?>
         </div>
       </div>
           <?php include "top_nav.php" ?>
      <div class="right_col" role="main">
        <div class="">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_panel">



                 <div class="x_title">
                 <div class="col-md-6">
                  <h2>Administración de Membresías de la Clinica</h2>
                 </div>
                 <div class="col-md-6">
                  <h2>Descuento de: <?=$descuento?></h2>
                 </div>
                   <div class="clearfix"></div>
                   </div>
                 <div class="x_content">
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                      <li id="tab1" role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Listado de tus Membresías</a>
                      </li>
                      <li id="tab2" role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false"><?=$accion;?> Membresía para la Clinica</a>
                      </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">


                      <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          
                          <div class="x_title">
                             <h2>Membresías Compradas  </h2>
                             <div class="clearfix"></div>
                          </div>

                          <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                              Listado de todas tus Membresias Compradas</p>
                            <table id="datatable-buttons" class="table table-striped table-bordered">
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Fecha de Solicitud</th>
                                  <th>Membresía</th>
                                  <th>Costo</th>
                                  <th>Estado</th>
                                  <th>Opciones</th>
                                </tr>
                              </thead>

                              <tbody>
                                <?php
                                    if($vmembresiaClinica){
                                      $i=1;
                                      foreach ($vmembresiaClinica AS $id => $array) {
                                    ?>
                                      <tr>
                                        <td align="left"><?=$i;?></td>
                                        <?php
                                	setlocale(LC_TIME, 'spanish');
                                 $fechas = strftime("%d de %B del %Y " , strtotime( $array['fechaCompra']));
                                ?>
                                        <td align="left"><?=$fechas;?></td>
                                     
                                        <td align="left"><?=$array['nombrem'];?></td>



                                        <td align="left"><?=$array['valor'];?></td>

                                    


                                        
                                        <td align="left"><?php if($array['idestado']==0){ ?> <span class="label label-danger">Pendiente</span> 
																			<?php }else{ ?>
																					<span class="label label-success">Comprada</span>
																				<?php };?></td>
                                        <td>
                                       
                                        <a style="font-size:75%" onClick="eliminar(<?=$id?>,<?=$id['nombrem']?>)"   class="label label-primary" >Cancelar</a>   
                                        </td>
                                      </tr>

                                    <?php
                                      $i++;}
                                    }
                                    ?>

                              </tbody>
                              
                            </table>
                          </div>
                        </div>
                      </div>
                      </div>





                      <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                        




                        
                      <form id="form" class="form-horizontal form-label-left" novalidate>
                    
                 <div class="ln_solid"></div>



           

                 <div class="">
                 <div  class="carousel slide" data-ride="">
                 <div class="" role="">              
                <!-- <div id="myCarousel" class="carousel slide" data-ride="carousel">-->

                <?php
                    if($vmembresia){
                     
                       foreach ($vmembresia AS $id => $array) {
                         ?>
<div class="item">
           

   
           <div class="col-md-<?=$rre?> col-xs-<?=$rre?>"> 
           <div class="center-block" style="position: relative;">
           <div class="contentt">
              <div style="display: table;margin: auto;width: 100%;font-size: 20px;text-align: center;">
                <div style="display: table-cell;vertical-align:middle ;width: 100%;font-size: 15px;padding: 0 10px;">
                  <div class="field-items">
                  <?if($array['actodesc']==1){
                    if($descuento>0){?>
                     
                      <div class="fieldescuento" >
                      <s><?=$array['valor'];?></s>
                      <h1><?=$array['valor']-$descuento?></h1>

                      <div class="field-items">
          <div class="fielm"> <?php if($array['moneda']==1){ ?>
                                          <span class="label label-success">$</span>  
																			<?php }else{ if($array['moneda']==2){?>
																					
                                        <span class="label label-success">Bs</span>
																				<?php }};?></div>
          
         </div>

                      <div style="position:absolute;right:0px;top:0px;">
                      <img style="width: 80px;" src="images/icons/descuento.png" alt="">
                      <p style="font-size:14px;position:absolute;transform:rotate(9deg);top:28px;right:5px;color: white "> DESC:<?=$descuento?></p>
                      </div>
                       
                    </div>
                    <?}else{?>
                    
                      <div class="fiel"><?=$array['valor'];?></div>
                      <div class="field-items">
          <div class="fielm"> <?php if($array['moneda']==1){ ?>
                                          <span class="label label-success">$</span>  
																			<?php }else{ if($array['moneda']==2){?>
																					
                                        <span class="label label-success">Bs</span>
																				<?php }};?></div>
          
         </div>
                      
                   <? }
                    ?>
                    
                    
                  <?}else{?>
                     <div class="fiel"><?=$array['valor'];?></div>

                     <div class="field-items">
          <div class="fielm"> <?php if($array['moneda']==1){ ?>
                                          <span class="label label-success">$</span>  
																			<?php }else{ if($array['moneda']==2){?>
																					
                                        <span class="label label-success">Bs</span>
																				<?php }};?></div>
          
         </div>
                 <? }
                  ?>
                   
                  </div>
                </div> 
              </div>
            
            <div style="display: table;margin: auto;width: 90%;font-size: 16px;text-align: center;">
              <div style="display: table-cell;vertical-align: middle;width: 100%;font-size: 15px;padding: 0 10px;">
               <div class="field-items">
                 <div class="fiel1" ><?=$array['nombre'];?></div>
                 <input type="text" id="nombre<?=$array['idmembresia'];?>" value="<?=$array['nombre'];?>"  style="display:none;"/>
                 <input type="text" id="idmembresia<?=$array['idmembresia'];?>" value="<?=$array['idmembresia'];?>"  style="display:none;"/>
                      <?php if($array['actodesc']==1){
                        ?>
                      <input type="text" id="valor<?=$array['idmembresia'];?>" value="<?=$array['valor']-$descuento;?>" style="display:none;" />
                      <?php
                      } 
                        else{
                        ?>
                     <input type="text" id="valor<?=$array['idmembresia'];?>" value="<?=$array['valor'];?>" style="display:none;" />
                  <?php
                      } 
                       ?>
               
                 <input type="hidden" id="descuento" value="<?=$descuento?>">
               </div>
              </div>
            </div>
            
            
            <div style="display: table;margin: auto;width: 100%;font-size: 16px;text-align: center;">
              <div style="display: table-cell;vertical-align:middle ;width: 100%;font-size: 15px;padding: 0 10px;">
               <div class="field-items">
                 <div class="fiel2">Detalle</div>
               </div> 
              </div>
            </div>
            
            
          
   <div class="field field-name-field-content-setlists-desc-mle field-type-text-long field-label-hidden">
   <!--<div class="fieldd">-->
   
   <!--<div class="field-item even">-->

     <div style="display: table;margin: auto;width: 90%;font-size: 16px;text-align: left;">
     <!--  <div style="display: table-cell;vertical-align: middle;width: 20%;font-size: 20px;padding: 0 10px;">
        <img src="images/tv.png">
       </div>-->
        <div style="display: table-cell">
         <strong><?=$array['detalle']?></strong><br>
        </div>
     </div>
   <!--</div>-->
  
   
   <!-- </div>-->
   </div>
   
            
            
            
             <div class="fieldd">
               <div style="display: table;margin: auto;width: 90%;font-size: 16px;text-align: center;">
                 <div style="display: table-cell;vertical-align: middle;width: 20%;font-size: 20px;padding: 0 10px;">
                   <a class="btnLog"  onclick="solicitar(<?=$array['idmembresia'];?>);">Solicitar</a>
                 </div>
               </div>
             </div>
            
             </div>
            </div>
            </div>
         
            
           
         </div> 
	 
       
        <?}
      }
     ?>



      </div>
     
    </div>
     </div>





                 	  <div class="ln_solid"></div>		           				
     </form>




                      </div>
                     
                    </div>
                  </div>


                </div>






              </div>
            </div>
          </div>

          
        </div>

        <!-- footer content -->
      <center>  <? include "piep.php"; ?> </center>
      </div>
    </div>

  </div>

  <script src="js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>
  <script src="js/custom.js"></script>
  <!-- form validation -->
  <script src="js/validator/validator.js"></script>
  <!-- PNotify -->
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  
  <script type="text/javascript" src="js/moment/moment.min.js"></script>
  <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>
 

   <!-- Datatables-->
        <script src="js/datatables/jquery.dataTables.min.js"></script>
        <script src="js/datatables/dataTables.bootstrap.js"></script>
        <script src="js/datatables/dataTables.buttons.min.js"></script>
        <script src="js/datatables/buttons.bootstrap.min.js"></script>
        <script src="js/datatables/jszip.min.js"></script>
        <script src="js/datatables/pdfmake.min.js"></script>
        <script src="js/datatables/vfs_fonts.js"></script>
        <script src="js/datatables/buttons.html5.min.js"></script>
        <script src="js/datatables/buttons.print.min.js"></script>
        <script src="js/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="js/datatables/dataTables.responsive.min.js"></script>
        <script src="js/datatables/responsive.bootstrap.min.js"></script>
        <script src="js/datatables/dataTables.scroller.min.js"></script>


  <script>
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
      .on('blur', 'input[required], input.optional, select.required', validator.checkField)
      .on('change', 'select.required', validator.checkField)
      .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
      .on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });




    /* FOR DEMO ONLY */
    $('#vfields').change(function() {
      $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function() {
      validator.defaults.alerts = (this.checked) ? false : true;
      if (this.checked)
        $('form .alert').remove();
    }).prop('checked', false);
  </script>
  <!-- pace -->
        <script src="js/pace/pace.min.js"></script>
        <script>
          var handleDataTableButtons = function() {
              "use strict";
              0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
                dom: "Bfrtip",
                buttons: [{
                  extend: "csv",
                  classnombre: "btn-sm"
                }, {
                  extend: "excel",
                  classnombre: "btn-sm"
                }, {
                  extend: "pdf",
                  classnombre: "btn-sm"
                }, {
                  extend: "print",
                  classnombre: "btn-sm"
                }],
                responsive: !0
              })
            },
            TableManageButtons = function() {
              "use strict";
              return {
                init: function() {
                  handleDataTableButtons()
                }
              }
            }();
        </script>


         <script type="text/javascript">


          $(document).ready(function() {

            $('#fechaCompra').daterangepicker({
				                singleDatePicker: true,
				                calender_style: "picker_4"
				              }, function(start, end, label) {
				                console.log(start.toISOString(), end.toISOString(), label);
				              });
				              

            $('#idestadoS').change(function() {
                var filenombre = $(this).val();
                var lastIndex = filenombre.lastIndexOf("\\");
                if (lastIndex >= 0) {
                    filenombre = filenombre.substring(lastIndex + 1);
                } 
                $('#img').val(filenombre);
            });
            <?php
              if($option == "m"){
            ?>
                $("#tab1").removeClass('active');
                $("#tab2").addClass('active');
                $("#tab_content1").removeClass('active in');
                $("#tab_content2").addClass('active in');
                
            <? } ?>

            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({
              keys: true
            });
            $('#datatable-responsive').DataTable();
            
            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true
            });
          });
          TableManageButtons.init();
        </script>
        
</body>

</html>
