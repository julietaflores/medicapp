<?php 
  session_start();
  if (!isset($_SESSION['csmart']['idusuario'])) {
    Header ("Location: login.php");
    }
  
    include 'headercondicion.php';
    include 'classes/ccita.php';
    include 'classes/ctipocita.php';
    include 'classes/cpaciente.php';
    include 'classes/cclinica.php';
    $oTipocita  = new tipocita();
    $opaciente  = new paciente();
    $oclinica = new clinica();
      

    $vUser = $oUser->getOne1($_SESSION['csmart']['idusuario']);
      if($vUser){
         foreach ($vUser AS $id => $info){ 
            $imagen       = $info["imagen"];
            $nom       = $info["nombre"];
            $_SESSION['csmart']['iperfil']=$imagen;
            $_SESSION['usuario']=$nom;   
         }
    }

 $vpermisos = $oUser->getperimisos1($_SESSION['csmart']['idusuario']);

    $ocita = new cita();
    $accion   = "Crear";
    $option   = "n";
    $idcita   = "";
    $idusuario  = "";
    $fecha  = "";
    $fecha_prog   = "";
    $idestado   = "";
    $comentario   = "";
    $idtipocita   = "";
          $idpaciente   = "";
          
      if(isset($_REQUEST['opt']) && $_REQUEST['opt']== "m"){
    $option = $_REQUEST['opt'];
    $idObj  = $_REQUEST['id'];
  }

  if ($option == "m") {
    $vcita = $ocita->getOne($idObj);
    if($vcita){
      $accion   = "Modificar";
      
         foreach ($vcita AS $id => $info){ 
            
            $idcita   = $info["idcita"];
            $idusuario    = $info["idusuario"];
            $fecha    = $info["fecha"];
            $fecha_prog   = $info["fecha_prog"];
            $idestado   = $info["idestado"];
            $comentario   = $info["comentario"];
            $idtipocita   = $info["idtipocita"];
            $idpaciente   = $info["idpaciente"];
             }
    }else{
      header("Location: cita.php");
      exit();
    }
  }
 
  $vcitas    =$ocita->getAll($_SESSION['csmart']['clinica']);
  $vtcita    =$oTipocita->getAll();
  $vUsuarios =$oUser->getAllxClinica($_SESSION['csmart']['clinica']);
  $vpaciente=$opaciente->getAll();
  $con= $opaciente->getallcont();
  $vpac   = $opaciente->getTotalPacientes($_SESSION['csmart']['clinica']);

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <?php include "header.php" ?>

  <link href="css/calendar/fullcalendar.css" rel="stylesheet">
  <link href="css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/js/select2.min.js"></script>
</head>
<body class="nav-md">
<div  id="btn3" class="loader" style="display: none"></div>	

      <input type="hidden" id="sessio" value="<?=$_SESSION['csmart']['clinica']?>">
  <div class="container body">
    <div class="main_container">

      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">        
          <?php include "menu.php"; ?>
        </div>
      </div>

    
        <?php include "top_nav.php"; ?>

      <div class="right_col" role="main">
        <div class="row"> 
          <div class="page-title">
            <div class="title_left">
              <h3> Agenda </h3>
            </div>
          </div>
  
          <div class="row">
            <div class="col-md-12">
              <div class="x_panel">
                <div class="x_title">
                  <h3>Agenda de Citas</h3>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                 
                </div>
                <div class="x_content">
                  <div id='calendar'></div>
                </div>
              </div>
            </div>
          </div>
          <div style="height: 100px;"></div>
         <?php include "piepa.php" ?>  
         
        </div>
      </div>

 
      <div id="CalenderModalNew" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title" id="myModalLabel">Nueva Cita</h4>
            </div>
            <div class="modal-body">
              <div id="testmodal" style="padding: 5px 20px;">
                <form id="antoform" class="form-horizontal calender" role="form">
                <div class="form-group">
                      <label class="col-sm-3 control-label">Tipo de Cita </label>
                      <div class="col-sm-9">
                        <select id="idtipocita" name="idtipocita" class="form-control">
                          <?php if($vtcita){
                                foreach ($vtcita AS $id => $array) {?>

                                <option value="<?=$array['idtipocita'];?>" <?php if($idtipocita==$id){echo"selected='selected'";}?>><?=$array['tipo'];?></option>
                          <?php } } ?>
                        </select>
                      </div>
                    </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Motivó </label>
                    <div class="col-sm-9">
                      
                        <input type="text" class="form-control"  id="paciente" name="paciente" /> 
                      
                          <input type="hidden" id="idpaciente" name="idpaciente" value="6"/>
                    </div>
                   </div>
                  <div class="form-group" id="camposinpaciente">
                    <label class="col-sm-3 control-label">Paciente</label>
                  
                    
                <?php if($con==0){?>
                  <div class="col-md-9 col-sm-9">
                  <a href="pacientenuevo.php"   type="button" class="btn btn-default " >Registrar nuevo Paciente</a>
                  </div>
                <?php }else{?>
                  <div class="col-md-6 col-sm-6">
                   <select id="paci" name="paci" class="form-control">
                          <?php if($vpaciente){
                                foreach ($vpaciente AS $id => $array) {?>

                                <option value="<?=$array['idpaciente'];?>"><?=$array['nombre']?> <?=$array['apellido']?></option>
                          <?php } } ?>
                    </select>
                    <input type="hidden" id="nombrepaciente" value="">
                    </div>
                    <div class="col-md-3 col-sm-3">
                    <a href="pacientenuevo.php"   >Nuevo Paciente</a>
                    </div>
                  <?php }?>

                
                    
                     
                  </div>
                  
                  <div class="form-group">
                      <label class="col-sm-3 control-label">Usuario </label>
                      <div class="col-sm-9">
                        <select id="idusuario" name="idusuario" class="form-control">
                          <?php if($vUsuarios){
                                foreach ($vUsuarios AS $id => $array) {?>

                                <option value="<?=$array['idusuario'];?>" <?php if($idusuario==$id){echo"selected='selected'";}?>><?=$array['nombre'];?> <?=$array['apellido'];?></option>
                          <?php } } ?>
                        </select>
                      </div>
                    </div>

                    
                <div class="form-group" >   
                <label class="col-sm-3 control-label">Color </label> 
                <div class="col-sm-9">
                <input id="rojo" style="display: none;" checked="checked" name="coloresc" type="radio" value="FF2968">
                      <label class="labelcolo" for="rojo" style="background: #FF2968; width: 20px;height: 20px;border-radius:15px; border:3px solid;"></label>
                      <input id="verde" style="display: none;" name="coloresc" type="radio" value="FF9500">
                      <label class="labelcolo" for="verde" style="background: #FF9500; width: 20px;height: 20px;border-radius:15px;"></label>
                      <input id="azul" style="display: none;" name="coloresc" type="radio" value="FFD60B">
                      <label class="labelcolo" for="azul" style="background: #FFD60B; width: 20px;height: 20px;border-radius:15px;"></label>
                      <input id="amarillo" style="display: none;" name="coloresc" type="radio" value="1BADF8">
                      <label class="labelcolo" for="amarillo" style="background: #1BADF8 ; width: 20px;height: 20px;border-radius:15px;"></label>
                      <input  id="cielo" style="display: none;" name="coloresc" type="radio" value="CC73E1">
                      <label class="labelcolo" for="cielo" style="background: #CC73E1 ; width: 20px;height: 20px;border-radius:15px;"></label>
                     
                      <input  id="x" style="display: none;" name="coloresc" type="radio" value="AC8E68">
                      <label class="labelcolo" for="cielo" style="background: #AC8E68 ; width: 20px;height: 20px;border-radius:15px;"></label>
                      <input  id="y" style="display: none;" name="coloresc" type="radio" value="32D74B">
                      <label class="labelcolo" for="cielo" style="background: #32D74B ; width: 20px;height: 20px;border-radius:15px;"></label>                             
                    </div>                                 
                </div>


                  <div class="form-group">
                    <label class="col-sm-3 control-label">Fecha</label>
                    <div class="col-sm-4">
                      <input type="date" class="form-control" value="<?php echo date('Y-m-d');?>" id="fecha" name="fecha">
                     </div>
                     <label class="col-sm-1 control-label">Hora</label>
                    <div class="col-sm-2">
                      <input type="hidden" id="horaCompleta">
                      <select  id="hhora" name="hhora" class="form-control">
                          <option value="00" >00</option>
                          <option value="01" >01</option>
                          <option value="02" >02</option>
                          <option value="03" >03</option>
                          <option value="04" >04</option>
                          <option value="05" >05</option>
                          <option value="06" >06</option>
                          <option value="07" >07</option>
                          <option value="08" >08</option>
                          <option value="09" >09</option>
                          <option value="10" >10</option>
                          <option value="11" >11</option>
                          <option value="12" >12</option>
                          <option value="13" >13</option>
                          <option value="14" >14</option>
                          <option value="15" >15</option>
                          <option value="16" >16</option>
                          <option value="17" >17</option>
                          <option value="18" >18</option>
                          <option value="19" >19</option>
                          <option value="20" >20</option>
                          <option value="21" >21</option>
                          <option value="22" >22</option>
                          <option value="23" >23</option>
                          <option value="24" >24</option>
                         </select>

                    </div>
                    <label style="position: absolute; right: 120px;">:</label>
               
               <div class="col-sm-2">
                 <select id="minutoc" name="minutoc" class="form-control">
                     <option value="00" >00</option>
                     <option value="10" >10</option>
                     <option value="20" >20</option>
                     <option value="30" >30</option>
                     <option value="40" >40</option>
                     <option value="50" >50</option>
                    </select>
               </div>
                  </div>


                  <div class="form-group">
                    <label class="col-sm-3 control-label">Nota</label>
                    <div class="col-sm-9">
                      <textarea class="form-control" style="height:55px;" id="descr" name="descr"></textarea>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" style="border-radius: 20px;" class="btn btn-danger antoclose" data-dismiss="modal">Cerrar</button>
              <button type="button"  style="border-radius: 20px;" class="btn btn-primary antosubmit">Guardar</button>
            </div>
          </div>
        </div>
      </div>
      <!----------------------------->

      <div id="CalenderModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title" id="myModalLabel2">Eliminar de la agenda</h4>
            </div>
            <div class="modal-body">

              <div id="testmodal2" style="padding: 5px 20px;">
                
                  <div class="form-group">
                    
                    <label id="modal2paciente" class="col-sm-9 control-label"></label><br>
                    <label id="modal2nota" class="col-sm-9 control-label"></label><br>
                    <label id="modal2fecha" class="col-sm-9 control-label"></label>
                    <input type="hidden" class="form-control" id="idcita" name="idcita">
                  </div>
                  
                
              </div>
            </div>
            <div class="modal-footer">
              <a onclick="irpaciente()" type="button" class="btn btn-default " >detalle cita</a>
              <button type="button" class="btn btn-danger antosubmit2">Eliminar</button>
            </div>
          </div>
        </div>
      </div>

      <div id="fc_create" data-toggle="modal" data-target="#CalenderModalNew"></div>
      <div id="fc_edit" data-toggle="modal" data-target="#CalenderModalEdit"></div>

    </div>

  </div>
  <form style="display: hidden" action="pacienteperfil.php" method="POST" id="formoculto">
                <input type="hidden" id="idpacientes" name="id" value=""/>
                <input type="hidden" id="citaid" name="cita" value=""/>
  </form>

  

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="js/bootstrap.min.js"></script>

  <script src="js/nprogress.js"></script>
  
  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>

  <script src="js/custom.js"></script>

  <script src="js/moment/moment.min.js"></script>
  <script type="text/javascript" src="js/moment/moment.min.js"></script>
  <!-- importo la libreria moments -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.0/moment.min.js"></script>
<!-- importo todos los idiomas -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.0/moment-with-locales.min.js"></script>
  <script src="js/calendar/fullcalendar.min.js"></script>
  <script src='js/calendar/lang/es.js'></script>
  <!-- Autocomplete 

   <script src="js/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>-->
   <script src="js/jquery-ui-1.9.2.custom.js" type="text/javascript"></script>
   <link href="css/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
   <link href="css/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
  <script src="js/pace/pace.min.js"></script>
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script> 
  <script type="text/javascript" src="js/notify/pnotify.buttons.js"></script>
  <script type="text/javascript" src="js/notify/pnotify.nonblock.js"></script> 
  <script>
    function irpaciente(){

     
      $("#formoculto").submit();


}
    $(document).ready(function() {
      $("#paci").select2({
        dropdownParent: $("#CalenderModalNew"),
        width: '100%'
      });
      
      $("#idusuario").select2({
        dropdownParent: $("#CalenderModalNew"),
        width: '100%'
      });
});
  var color="";


$("#paci").change(function(){
  $("#idpaciente").val($("#paci").val());
});

$(".labelcolo").click(function(){
        var radio= Array.from(document.querySelectorAll(".labelcolo"));
        for (let index = 0; index < radio.length; index++) {
          const element = radio[index];
                $(element).css('border','0px solid');
            }
        $(this).css('border','3px solid');
            
            console.log();
        });

    $(document).ready(function() {
     
    


  if(<?=$vpermisos?>==0){
      
      $.ajax({
                    type: 'POST',
                    url: 'actions/actionUsuario.php',
                    data: {idusud:<?=$_SESSION['csmart']['idusuario']?>,pe:1,pe1:1,pe2:1,pe3:1,pe4:1, opt: 'registrarpermiso'},
                          success: function(data) {  
                           if (data != 0){   
                            window.setTimeout("document.location.href='agenda.php';",500);
                           
                          }	
                        }	
                   });  
     
    }


$("#idpaciente").val($("#paci").val());
      $( "#paciente" ).autocomplete({
            source: function( request, response ) {
                        var dato=0;
                        dato = request.term.replace(' ','');
                        $.ajax({
                                url: "actions/actionpaciente.php?opt=j",
                                type:"POST",
                                dataType: "json",
                                data: {
                                        maxRows: 10,
                                        q: dato
                                },
                                success: function(data) {
                                        console.log("entro");
                                        console.log(data);
                                        response( $.map( data, function( item ) {
                                                return {
                                                        label: item.nombre,
                                                        value: item.nombre,
                                                        id: item.idpaciente
                                                        
                                               }
                                        }));
                                }
                        });
              }
                ,
            minLength: 2,
            select: function( event, ui ) {
                    $("#idpaciente").val(ui.item.id);  
            }
        });
        function log( message ) {
            $( "#log" ).html(''); //se tiene q limpiar el div
            $( "<div>" ).text( message ).prependTo( "#log" );
            $( "#log" ).scrollTop( 0 );
                                    
        }
        var zone = "05:30";  //Change this to your timezone

          $.ajax({
            url: 'actions/actioncita.php',
                type: 'POST', // Send post data
                data: 'opt=j',
                async: false,
                success: function(s){
                  json_events = s;
                }
          });


    });
    
    $(document).ready(function() {

      var date = new Date();
      var d = date.getDate();
      var m = date.getMonth();
      var y = date.getFullYear();
      var started;
      var categoryClass;
      var clini=$("#sessio").val();
      var titulocompleto="";

      var calendar = $('#calendar').fullCalendar({
        
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
        }, defaultView: 'agendaWeek',
       
        selectable: true,
        selectHelper: true,
        editable: true,
        events: 'events.php',
       
       

        eventRender: function(event, element, view) {
          if (event.allDay === 'true') {
           event.allDay = true;
          } else {
           event.allDay = false;
          }
         },
         select: function(start, end, allDay) {

           $('#fc_create').click();
           fecha = start.format("YYYY-MM-DD");
           hora = start.format("H:mm");
           ended = end;
           color=$("input[name='coloresc']:checked").val();
           $('#fecha').val(fecha);
           $('#horaCompleta').val(hora);
           var horacompleta=(hora);
           var hora00 = horacompleta.charAt(0) + horacompleta.charAt(1);
           var min00=horacompleta.charAt(3)+ horacompleta.charAt(4);
           if(hora00.charAt(1)==':')
              {
                  hora00='0'+ hora00.charAt(0);
                  min00=horacompleta.charAt(2)+horacompleta.charAt(3);
                    }
             $("#hhora").val(hora00);   
             $("#minutoc").val(min00);    
            console.log(horacompleta);
            console.log(min00);
        },
        eventClick: function(calEvent, jsEvent, view) {
 
          $('#fc_edit').click();
          $('#idcita').val(calEvent.id);
          var cit=calEvent.id;
          $.ajax({
                       url: 'actions/actioncita.php?opt=uno',
                       data: 'cita=' + cit  ,
                       type: "POST",
                       dataType:"json",
                       success: function(data) {
                          $.each(data, function(i, item) {
                            console.log(item);
                            if(item.idtipocita!=4){
                            $('#myModalLabel2').text(item.title);
                            $('#modal2paciente').text("Paciente: "+item.nombre+" "+item.apellido);
                            $('#idpacientes').val(item.idpaciente);
                            $('#citaid').val(cit);
                            }else{
                            $('#myModalLabel2').text(item.title);
                            $('#modal2paciente').text("Doctor: "+item.nombreusuario);
                           
                            }
                            $('#modal2nota').text("Descripción: "+item.comentario);

                            moment.locale('es');
                            var dateTime = moment(item.start);
                            var full = dateTime.format('dddd D, MMMM YYYY'+' '+'H:mm a');
                            $('#modal2fecha').text("Fecha: "+full);                      
            });
                   
                        
                     }
                   });


          $(".antosubmit2").on("click", function() {
            var id = $("#idcita").val();
            $.ajax({
                       url: 'actions/actioncita.php?opt=jr',
                       data: 'id=' + id  ,
                       type: "POST",
                       success: function(json) {
                        calendar.fullCalendar('removeEvents', id);
                     }
                   });

            $('.antoclose2').click();
            return false;
          });
          calendar.fullCalendar('unselect');
        },
         eventDrop: function(event, delta) {
        var start = event.start.format("YYYY-MM-DD H(:mm)");
        var end = (event.end == null) ? start : event.end.format("YYYY-MM-DD H(:mm)");
         $.ajax({
           url: 'actions/actioncita.php?opt=jm',
           data: 'start='+ start +'&end='+ end +'&id='+ event.id ,
           type: "POST",
             success: function(json) {
              new PNotify({
                                title: 'Actualizado correctamente',
                                  text: 'Todos los cambios fueron guardados !!!',
                                  type: 'info'
                                });
             }
         });
         },
         eventResize: function(event, delta) {
        var start = event.start.format("YYYY-MM-DD H(:mm)");
        var end = (event.end == null) ? start : event.end.format("YYYY-MM-DD H(:mm)");
       

         $.ajax({
           url: 'actions/actioncita.php?opt=jm',
           data: 'start='+ start +'&end='+ end +'&id='+ event.id ,
           type: "POST",
             success: function(json) {
               console.log(event.id+" "+ start+" "+end);

              new PNotify({
                                title: 'Actualizado correctamente',
                                  text: 'Todos los cambios fueron guardados !!!',
                                  type: 'info'
                                });
             }
         });
         }
      });

function recargarEventos(){
  $.ajax({
        url:'actions/actioncita.php?opt=todos',
        data:'clinica='+clini,
        type:'POST',
        dataType : 'json',
        success:function(data){
          $.each(data, function(i, item) {
            calendar.fullCalendar('removeEvents', item.id);
                console.log(item);
                if(item.idtipocita==4){
                  titulocompleto=item.title+" "+item.nombreusu;
                }else{
                  titulocompleto=item.title+" "+item.nombrepa;
                }
               
                calendar.fullCalendar('renderEvent',
                   {
                     id:item.id,
                     title: titulocompleto,

                     start: item.start,
                     end: item.end,
                     color: item.color,//'#378006',
                     allDay: ''
                   } 
                   );
            });
         
          }
      });
}


      $(".fc-month-button").click(function(){
        debugger;
        recargarEventos();
       
        });
 ///       
        $(".fc-left").click(function(){
          
          recargarEventos();
         
        });
  
        
 ////////////todos//////////////////////////  
      $.ajax({
        url:'actions/actioncita.php?opt=todos',
        data:'clinica='+clini,
        type:'POST',
        dataType : 'json',
        success:function(data){
          $.each(data, function(i, item) {
                console.log(item);
                if(item.idtipocita==4){
                  titulocompleto=item.title+" "+item.nombreusu;
                }else{
                  titulocompleto=item.title+" "+item.nombrepa;
                }
                calendar.fullCalendar('renderEvent',
                   {
                     id:item.id,
                     title: titulocompleto,

                     start: item.start,
                     end: item.end,
                     color: item.color,//'#378006',
                     allDay: ''
                   }   // make the event "stick"  
                   );
            });
          }
      });
      ////////////////////////////////
      $("#idtipocita").change(function(){
          if(this.value=='4'){
            $("#camposinpaciente").hide('slow');
            console.log($('#idusuario').text());
          }else{
            console.log($('#idpaciente').text());
            $("#camposinpaciente").show('slow');
           
          }
      });
     
///////////////////agregar nuevo//////////////////////////////////////
      $(".antosubmit").on("click", function() {
                var title = $("#paciente").val();
                var detalle = $("#descr").val();
                var nombrecompleto=$("#paci option:selected").text();
                var idcitas='';
                if (title) {
                    var idpaciente =  $("#idpaciente").val();
                   var idusuario =  $("#idusuario").val();
                   var idtipocita = $("#idtipocita").val();
                   if(idtipocita==4 || idpaciente==''){
                    idpaciente=1;
                    idtipocita=4;
                    nombrecompleto="<?=$_SESSION['usuario']?>";
                   }
                    detalle = $("#descr").val();
                   var colores='#'+$("input[name='coloresc']:checked").val();
                   var start = $('#fecha').val() + " " + $('#hhora').val()+":"+$('#minutoc').val();
                   var start1 = $('#fecha').val() + " " + $('#hhora').val()+":"+$('#minutoc').val();
                   var hora = $('#hhora').val().charAt(0) + $('#hhora').val().charAt(1);
                   var min = $('#minutoc').val().charAt(0) + $('#minutoc').val().charAt(1);
                   var mediahora=parseInt(min)+30;
                   if(mediahora>=60){
                    hora = parseInt(hora) + 1;
                    min= mediahora - 60;
                   }else{
                    min=parseInt(min)+30;
                   }
                   var end = $('#fecha').val() + " " +  hora + ":" + min ;
                   var url = '';
                  if($("#fecha").val()>=fechahoy()){
                   $.ajax({
                       url: 'actions/actioncita.php?opt=ja',
                       data: 'title='+ title+
                       '&start='+ start +
                       '&end='+ end +
                       '&url='+ url +
                       '&tipocita=' + idtipocita +
                       '&idpaciente=' + idpaciente +
                       '&detalle=' + detalle + 
                       '&idusuario=' + idusuario + 
                       '&color='+colores+
                       '&allDay='  ,
                       type: "POST",
                       success: function(json) {
                        idcitas=json;
                        calendar.fullCalendar('renderEvent',
                            {
                              id:idcitas,
                              title: nombrecompleto,
                              start: start,
                              end: end,
                              color:colores,
                              allDay: ''
                            });
                        new PNotify({
                                title: 'Se agrego correctamente',
                                  text: 'todos los cambios fueron guardados!',
                                  type: 'info'
                                });
                         $("#paciente").val('');
                         $("#descr").val('');
                         $('.antoclose').click();
                     }
                   });
                       
                  }else{
                    if(confirm("Atención! Esta guardando una fecha anterior"))
                        { 
                          $.ajax({
                       url: 'actions/actioncita.php?opt=ja',
                       data: 'title='+ title+
                       '&start='+ start +
                       '&end='+ end +
                       '&url='+ url +
                       '&tipocita=' + idtipocita +
                       '&idpaciente=' + idpaciente +
                       '&detalle=' + detalle + 
                       '&idusuario=' + idusuario + 
                       '&color='+colores+
                       '&allDay='  ,
                       type: "POST",
                       success: function(json) {
                        idcitas=json;
                        calendar.fullCalendar('renderEvent',
                            {
                              id:idcitas,
                              title: nombrecompleto,
                              start: start,
                              end: end,
                              color:colores,
                              allDay: ''
                            });
                        new PNotify({
                                title: 'Se agrego correctamente',
                                  text: 'todos los cambios fueron guardados!',
                                  type: 'info'
                                });
                         $("#paciente").val('');
                         $("#descr").val('');
                         $('.antoclose').click();
                     }
                   });
                        }
                  } 


                 

                   
               }else{
                new PNotify({
                                title: 'Falta!!!',
                                  text: 'Agregar un título',
                                  type: 'error'
                                });
               }
               $("#title").val('');
               calendar.fullCalendar('unselect');
               return false;
           });
    });
   function fechahoy(){
    var today = new Date();
      var dd = today.getDate();

      var mm = today.getMonth()+1; 
      var yyyy = today.getFullYear();
      if(dd<10) 
      {
          dd='0'+dd;
      } 

      if(mm<10) 
      {
          mm='0'+mm;
      } 
      today = yyyy+'-'+mm+'-'+dd;
      return today;
        }
  </script>
 
</body>

</html>
