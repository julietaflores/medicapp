<?php 
session_start();
if (!isset($_SESSION['csmart']['idusuario'])) {
Header ("Location: login.php");
}
include 'headercondicion.php';
   
   
    include 'classes/ctipoUsuario.php';
  
   
    $oTipo  = new tipoUsuario();

    $accion   = "Crear";
    $idestado  = 0;
    $idtipoUsuario  = "";
    $imagen       = "";
    $telefono       = "";
    $correo       = "";
    $nombre       = "";
    $idusuario   =0;


      $vUser = $oUser->getOne1($_SESSION['csmart']['idusuario']);
      if($vUser){
           foreach ($vUser AS $id => $info){ 
              $idusuario    =      $info["idusuario"];
              $notas       = $info["notas"];
              $idclinica  = $info["idclinica"];
              $nclinica  = $info["nombrec"];
            }
      }
      $vTipo     = $oTipo->getAll();

      $vUserr     = $oUser->getAllxclinica1($_SESSION['csmart']['clinica'],$_SESSION['csmart']['idusuario']);  
 ?>  

 <!DOCTYPE html>
<html >

<head>
  
  <?php include "header.php";?>

  <script type="text/javascript">


function eliminar(id) {
          if (confirm("Atención! Va a proceder a eliminar este registro. Desea continuar?")) {
             var form = "valor"; 
            $.ajax({
              url: 'actions/actionUsuario.php?opt=e&id='+id,
              type:'POST',
              data: { valor1: form, }
            }).done(function( data ){   
              if(data == 0){
                  new PNotify({
                   title: 'Datos Eliminados',
                    text: 'Todos los datos fueron guardados. Puede continuar.!',
                    type: 'success'
                  });
                  window.setTimeout("document.location.href='fusuariopnua.php';",500);
              }
              else{
                    new PNotify({
                     title: 'Error en formulario',
                    text: 'No se puedieron guardar los datos, intente de nuevo.',
                    type: 'error'
                 });
                ;
              }
            });
          }
      }

      function ireditar(id){
				$("#idusuarioe").val(id);
				$("#formularioeditar").submit();
			}

      function cancelar(){
        window.setTimeout("document.location.href='index.php';",500);

      }

  </script>

</head>


<body class="nav-md">

  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <!-- sidebar menu -->
            <?php include "menu.php" ?>
          <!-- /sidebar menu -->
        </div>
      </div>
      <!-- top navigation -->
           <?php include "top_nav.php" ?>
      <!-- /top navigation -->
      <!-- page content -->
      <div class="right_col" role="main">

        <div class="">
          
         
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Administración de Perfil</h2>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                    <li id="tab1" role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Listado</a>
                      </li>
                    
                    </ul>



                    <div id="myTabContent" class="tab-content">
                      <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                        <!-- tab 1 -->
                        <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title">
                       
                            
                            <div class="clearfix"></div>
                            
                            <div class="jumbotron" id="info1">
                      <div class="row">
                        <div class="col-md-8 col-sm-8 col-xs-6">
                        <h1>Tu Membresía es: "<strong><?= $nombredemem?></strong>"</h1>

<?php
        setlocale(LC_TIME, 'spanish');
       $fechas = strftime("%d de %B del %Y" , strtotime( $fechafin));
      ?>
    
    <p>La fecha en que se vence tu membresía es el <strong><?= $fechas?></strong></p>
                          <p>Ya no tiene usuarios para registrar, si deseas cambiar de Membresía presiona el botón </p>
                          <a href="membresiaClinica.php" class="btn btn-primary">Ver Membresías</a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-6"> 
                          <img src="images/referal.png" alt="">
                        </div>
                      </div>
                  </div>
                          </div>
                          <div class="x_content">
                           
                            <table id="datatable-buttons" class="table table-striped table-bordered">
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Nombre</th>
                                  <th>Telefono</th>
                                  <th>Correo</th>
                                  <th>Tipo</th>
                                  <th>Estado </th>
                                  <th>Opciones</th>
                                  
                                </tr>
                              </thead>

                              <tbody>
                                <?php
                                    if($vUserr){
                                      $i=1;
                                      foreach ($vUserr AS $id => $array) {
                                    ?>
                                      <tr>
                                        <td align="left"><?=$i;?></td>
                                        <td align="left" ><?=$array['nombre'];?></td>
                                        <td align="left"><?=$array['telefono'];?></td>
                                        <td align="left"><?=$array['correo'];?></td>
                                        <td align="left"><?=$array['tipo'];?></td>
                                        <td align="left">
                                     
                                        <?php if($array['idestado']==0){ ?>  <a style="font-size:75%" onClick="habilitar(<?=$id?>);"   class="label label-danger" >Inactivo</a>  
																			<?php }else{ ?>
                                        <a style="font-size:75%" class="label label-success" >Activo</a>  
																				<?php };?> 
          
                                        </td>


                                        <td align="center">
                                         

                                          <a onclick="ireditar(<?=$id?>)" title="Perfil" >
																	        	<span class="label label-primary" >
																			       <i class="fa fa-pencil" title="Modificar"></i>
																		        </span> 
																	        </a>

                                          &nbsp;
                                          <span class="label label-default" onClick="eliminar(<?=$id?>)" >
                                                <i class="fa fa-trash-o" title="Eliminar"></i>  
                                          </span>   
                                          
                                        </td>
                                      </tr>

                                    <?php
                                      $i++;}
                                    }
                                    ?>

                              </tbody>
                              
                            </table>
                          </div>
                        </div>


                      </div>
                      </div>


                  </div>


                </div>
              </div>
            </div>
          </div>
        </div>

        <form style="display: hidden" action="fusuariopa1.php" method="POST" id="formularioeditar">
								<input type="hidden" id="idusuarioe" name="idusuarioe" value=""/>																	
		    </form>



        <!-- footer content -->
      <center>  <? include "piep.php"; ?> </center>
        <!-- /footer content -->

      </div>
      <!-- /page content -->
    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>
  <script src="js/custom.js"></script>
  <!-- form validation -->
  <script src="js/validator/validator.js"></script>
  <!-- PNotify -->
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  
   <!-- Datatables-->
        <script src="js/datatables/jquery.dataTables.min.js"></script>
        <script src="js/datatables/dataTables.bootstrap.js"></script>
        <script src="js/datatables/dataTables.buttons.min.js"></script>
        <script src="js/datatables/buttons.bootstrap.min.js"></script>
        <script src="js/datatables/jszip.min.js"></script>
        <script src="js/datatables/pdfmake.min.js"></script>
        <script src="js/datatables/vfs_fonts.js"></script>
        <script src="js/datatables/buttons.html5.min.js"></script>
        <script src="js/datatables/buttons.print.min.js"></script>
        <script src="js/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="js/datatables/dataTables.responsive.min.js"></script>
        <script src="js/datatables/responsive.bootstrap.min.js"></script>
        <script src="js/datatables/dataTables.scroller.min.js"></script>


  <script>
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('#form')
      .on('blur', 'input[required], input.optional, select.required', validator.checkField)
      .on('change', 'select.required', validator.checkField)
      .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
      .on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('#form').submit(function(e) {
      e.preventDefault();
      var submit = true;

      
      // evaluate the form using generic validaing
      if (!validator.checkAll($(this))) {
        submit = false;
      }

      if (submit)
        guardarformulario();
      return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function() {
      $('#form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function() {
      validator.defaults.alerts = (this.checked) ? false : true;
      if (this.checked)
        $('#form .alert').remove();
    }).prop('checked', false);
  </script>
  <!-- pace -->
        <script src="js/pace/pace.min.js"></script>
        <script>
          var handleDataTableButtons = function() {
              "use strict";
              0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
                dom: "Bfrtip",
                buttons: [{
                  extend: "csv",
                  className: "btn-sm"
                }, {
                  extend: "excel",
                  className: "btn-sm"
                }, {
                  extend: "pdf",
                  className: "btn-sm"
                }, {
                  extend: "print",
                  className: "btn-sm"
                }],
                responsive: !0
              })
            },
            TableManageButtons = function() {
              "use strict";
              return {
                init: function() {
                  handleDataTableButtons()
                }
              }
            }();
        </script>
         <script type="text/javascript">
          $(document).ready(function() {
            $('#imagenS').change(function() {
                var filename = $(this).val();
                var lastIndex = filename.lastIndexOf("\\");
                if (lastIndex >= 0) {
                    filename = filename.substring(lastIndex + 1);
                } 
                $('#img').val(filename);
            });
            <?php
              if($option == "m"){
            ?>
                $("#tab1").removeClass('active');
                $("#tab2").addClass('active');
                $("#tab_content1").removeClass('active in');
                $("#tab_content2").addClass('active in');
                
            <? } ?>

            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({
              keys: true
            });
            $('#datatable-responsive').DataTable();
            
            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true
            });
          });
          TableManageButtons.init();
        </script>
</body>

</html>
