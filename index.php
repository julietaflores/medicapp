<?php 
  session_start();
  if (!isset($_SESSION['csmart']['idusuario']) ) {
    Header ("Location: logout.php");
    }

    include 'headercondicion.php';
    include 'classes/ccita.php';
    include 'classes/cpaciente.php';
    include 'classes/cnotas.php';
    include 'classes/cgasto.php';
    include 'classes/cfactura.php';
    include 'classes/cnoticias.php';
    include 'classes/ctrabajoslab.php';
    $onoticia=new noticia();
    $otrabajoslab=new TrabajosLab();
    date_default_timezone_set('UTC'); 

   $vnoticia=$onoticia->getAll();
    $oCita  = new cita();
    $oPac  = new paciente();
    $oNota  = new notas();
    $ogasto  = new gasto();
    $ofactura = new factura();

    $fecha = date("Y-m-d");
    $fechaFin = date("Y-m-t");

    //CITA DE HOY
    $vcita = $oCita->getAllfecha($fecha,$_SESSION['csmart']['clinica']);
    $vcitaXusuario = $oCita->getCitasXusuario($fecha,$_SESSION['csmart']['idusuario']);
    
    //USUARIOS INACTIVOS
    $vcitaInactivos = $oCita->getUsuarioInactivos($fecha,$_SESSION['csmart']['clinica']);
    
    $fecha = date("Y-m-") . "01";
    
    //CITAS DEL MES
    $vcitas = $oCita->getCitasXusuarioXrango($fecha,$fechaFin,$_SESSION['csmart']['idusuario']);

    //TOTAL DE PACIENTES
    $vpac   = $oPac->getTotalPacientes($_SESSION['csmart']['clinica']);
    //PACIENTES NUEVOS ESTE MES
    $vpacNew   = $oPac->getTotalPacientesXfecha($_SESSION['csmart']['clinica'], $fecha, $fechaFin);
    
    $dia = date("d"); $mes = date("m");


    //CUMPLEANEROS DEL DIA
    $vcumple   = $oPac->getAllCumpleaneros($dia,$mes);
    //TOTAL DE NOTAS
    $vNotas   = $oNota->getAllusuario($_SESSION['csmart']['idusuario']);
    //TOTAL DE GASTOS
    $vGasto = $ogasto -> getGastosXmesXid($_SESSION['csmart']['clinica'],$fecha,$fechaFin);
    //DETALLE DE GASTOS
    $vGastoDetalle = $ogasto -> getGastosDetalleXmesXid($_SESSION['csmart']['clinica'],$fecha,$fechaFin);
    //TOTAL DE FACTURACION
    $vFact = $ofactura -> getFacturasXmesXid($_SESSION['csmart']['clinica'],$fecha,$fechaFin);
    $Trabajoslab=$otrabajoslab->getAll($_SESSION['csmart']['clinica']);
 ?>   




<!DOCTYPE html>
<html>
<head>
<?php include "header.php";?>
</head>
<body class="nav-md"   >
<div  id="btn3" class="loader" style="display: none"></div>	
<?php  if($_SESSION['csmart']['permisos']!='1'){  ?>
  <script  type="text/javascript">
       
  $(document).ready(function() {
<?php if(!$vpac){?>
    new PNotify({
      title: 'Quiere agregar a su primer paciente?',
      text: 'Le ayudamos, haga <a style="color:white" href="pacientenuevo.php">click aqui</a>!!!',
      type: 'info',
       hide: false,
       
      })
  
  <?}?>
  

  });
  </script>


      <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <!-- sidebar menu -->
            <?php include "menu.php" ?>
          <!-- /sidebar menu -->
        </div>
      </div>

      <!-- top navigation -->
        <?php include 'top_nav.php'; ?>
      
      <!-- /top navigation -->


      <!-- page content -->
      <div class="right_col" role="main">

        <!-- top tiles -->
        <div class="row tile_count">
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-clock-o"></i> Citas Hoy</span>
              <div class="count green"><?php echo $vcitaXusuario; ?></div>
              <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> que la semana pasada</span> -->
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-clock-o"></i> Total citas mes</span>
              <div class="count green"><?php echo $vcitas; ?></div>
              <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> que el mes pasado</span> -->
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-user"></i> Pacientes Activos</span>
              <div class="count">0</div>
              
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-user"></i> Nuevos Pacientes</span>
              <div class="count"><?php echo $vpacNew; ?></div>
              
            </div>
          </div>
          

          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-hand-o-down"></i> Total Gastos </span>
              <div class="count"><? if($vGasto){echo $vGasto;}else{echo 0;}?></div>
              <!-- <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>0% </i> más</span> -->
            </div>
          </div>
          

        </div>
      
              
                  <div class="bs-glyphicons">
                      <ul class="bs-glyphicons-list">

                        <li style="width: 25%;"><a href="pacientenuevo.php">
                        <span class="glyphicon glyphicon-copy" aria-hidden="true"></span>
                          <span class="glyphicon-class">Nuevo Paciente</span>
                        </a>
                         
                        </li>

                        <li style="width: 25%;">
                          <a href="gasto.php">
                          <span class="glyphicon glyphicon-piggy-bank" aria-hidden="true"></span>
                          <span class="glyphicon-class">Registrar Gastos</span>
                          </a>
                         
                        </li>

                        <li style="width: 25%;">
                          <a href="agenda.php">
                          <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                          <span class="glyphicon-class">Marcar cita</span>
                          </a>
                          
                        </li>


                        <li style="width: 25%;">
                          <a href="incidencia.php">
                          <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span>
                          <span class="glyphicon-class">Soporte</span>
                          </a>
                        
                        </li>

                        

                      </ul>
                    </div>
        <div class="row">


          <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="x_panel">
              <div class="x_title">
                <h2>Agenda de Hoy </h2> &nbsp; 
                  
                <ul class="nav navbar-right panel_toolbox">
                  <li><a href="agenda.php" class="btn btn-default btn-success btn-xs">Nueva Cita</a></li>
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <form style="display: hidden" action="pacienteperfil.php" method="POST" id="formoculto">
                <input type="hidden" id="idpacientes" name="id" value=""/>
                <input type="hidden" id="idcita" name="cita" value=""/>
              </form>
              <div class="x_content">
                <ul class="list-unstyled msg_list">
                  <?php if($vcita){
                                foreach ($vcita AS $id => $array) {?>
                  <li>
<!--------------------------------------------------------------------->
                    <a href="javascript:irpaciente(<?=$array['idpaciente']?>,<?=$array['id']?>) ">
                    <label id="cargaexterna"></label>
                      <span class="image">
                        <?if($array['imagen']=="user.png"){?>
                            <img style="width: 44px; height: :44px;" src="images/user.png" alt="img" />
                               
                       <? }else{
															if(file_exists("images/paciente/".$array['idpaciente']."/".$array['imagen'])){?>
																<img style="width:44px;height: 44px;" src="images/paciente/<?=$array['idpaciente'];?>/<?=$array['imagen'];?>" alt="" class="img-circle img-responsive">
						                         
															
															<?}else{?>
																<img style="width: 44px;height: 44px;" src="images/user.png" alt="" class="img-circle img-responsive">
						                          
																<?}?>
														
															<?}?>
                                  </span>
                      <span>
                                    <span><?=$array['nombre']?>|<?=$array['title'];?> | <?=$array['tipo'];?></span>
                      <span style="font-size: 15px;" class="time"><i class="fa fa-clock-o"> </i> <?php $date = date_create($array['start']); echo date_format($date, 'H:i'); ?> Hrs.</span>
                      </span>
                      <span class="message">
                                    <?=$array['comentario'];?>
                                </span>
                    </a>
                  </li>
                   <?php } } ?>
                </ul>
              </div>
              
            </div>
          </div>

         
           <!-- Start to do list -->
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> Notas <small>importantes</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="">
                      
                      <ul class="to_do">
                        <?php if($vNotas){
                                foreach ($vNotas AS $id => $array) {?>
                                
                                <!-- <div > -->
                                <li id="<?=$array['idnota'];?>">
                                  <p>
                                    <button type="button" class="btn btn-xs" onclick="borrarnota(<?=$array['idnota'];?>)"> 
                                <i class="fa fa-square-o"></i> </button> <?=$array['detalle'];?> </p>
                                </li>
                                <!-- </div> -->
                        <?php } } ?>
                        
                         <div id="nnotas">

                         </div>
                      </ul>
                    
                      <input id="nota" name="nota" type="text" /> 
                      <button type="button" class="btn btn-success btn-xs" onclick="guardarnota()"> 
                                 Agregar <i class="fa fa-chevron-right"></i> </button>
                    </div>
                  </div>
                </div>
              </div>
         

        </div>


        <div class="row">
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="x_panel">
              <div class="x_title">
                <h2>Actividades Recientes </h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <div class="dashboard-widget-content">

                  <ul class="list-unstyled timeline widget">
                    <?if($vnoticia){
                       foreach ($vnoticia AS $id => $array) {?>
                          
                   
                    <li>
                      <div class="block">
                        <div class="block_content">
                          <h2 class="title">
                                            <a><?=$array['titulo']?></a>
                                        </h2>
                          <div class="byline">
                            <span><?=$array['fecha']?></span> por <a><?=$array['usuario']?></a>
                          </div>
                          <p class="excerpt"><?=$array['detalle']?>
                          <?if($array['importancia']==3){?>
                              <a href="http://blog.tuclinicapp.com/#/tutorial/<?=$array['idNoticia']?>" target="_blank">Leer más</a>

                          <?}else{?>
                                    <a href='http://blog.tuclinicapp.com/#/noticia/<?=$array['idNoticia']?>' target="_blank">Leer más</a>

                          <?}?>
                         
                          </p>
                        </div>
                      </div>
                    </li>  
                    <? }} ?>
                  </ul>
                </div>
              </div>
            </div>
          </div>


           <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="x_panel tile fixed_height_320">
              <div class="x_titlec">
                <h2>Cumpleañeros en los próximos días</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <ul class="quick-list" style="width: 100% !important">
                <!--<?php if($vcumple){
                        foreach ($vcumple AS $id => $array) {?>
                          <li><i class="fa fa-birthday-cake"></i><?=$array['nombre'];?> | 
                          <?
                           $source = $array['fecha_nac'];
                           $date = new DateTime($source);
                         
                           echo $date->format('d-m-Y');
                            ?>
                          </li>
                       
                    
                        <?php } }else{

                          ?>
                          <p class="excerpt">No hay Cumpleañeros este día.</p>
                          <?php

                        } ?>-->
                      </ul>
              </div>
            </div>
          </div>

          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="x_panel tile fixed_height_320">
              <div class="x_title">
                <h2>Trabajos Pendientes</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                
                <ul>
                  <?php if($Trabajoslab){
                        foreach ($Trabajoslab AS $id => $array) {
                          if($array['idestado']==1){?>
                              <li><?=$array['nombrepaciente'];?> | <?=$array['detalle'];?> |
                          
                          <?
                          $source = $array['fechaEntrega'];
                          $date = new DateTime($source);
                          echo $date->format('d-m-Y'); ?> 
                          </li>
                          <?}?>    
                         
                        <?php } }else{

                          ?>
                          <p class="excerpt">No hay Trabajos pendientes.</p>
                          <?php

                        } ?>

                 </ul>
              </div>
            </div>
          </div>
          <!---------------------------------------------------------------------------------->
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="x_panel tile fixed_height_320 overflow_hidden">
              <div class="x_title">
                <h2>Distribución de Gastos</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

                <table class="" style="width:100%">
                  <tr>
                    <th>
                      <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        <p class="">Tipo de gasto</p>
                      </div>
                      <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                        <p class="">Porcentaje %</p>
                      </div>
                    </th>
                  </tr>
                  <tr>
                   <td>
                      <table class="tile_info">
                        <?php if($vGastoDetalle){
                          foreach ($vGastoDetalle AS $id => $array) {?>
                            <tr>
                              <td>
                                <p><i class="fa fa-square green"></i><?=$array['gasto'] ?> </p>
                              </td>
                              <td><?=round(($array['total']*100)/$vGasto,2); ?> %  </td>
                            </tr>
                        <?php }
                        } ?>
                        
                      </table>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
          </div>
          <div style="height: 100px;"></div>
         <?php include "piep.php" ?> 
        </div>
    
      </div>
    

    </div>

  </div>

  



  <script src="js/bootstrap.min.js"></script>
  <!-- gauge js -->
  <script type="text/javascript" src="js/gauge/gauge.min.js"></script>
  <script type="text/javascript" src="js/gauge/gauge_demo.js"></script>
  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="js/moment/moment.min.js"></script>
  <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>
  <!-- chart js -->
  <script src="js/chartjs/chart.min.js"></script>

  <script src="js/custom.js"></script>
  <script type="text/javascript" src="js/flot/jquery.flot.js"></script>
  <script type="text/javascript" src="js/flot/jquery.flot.pie.js"></script>
  <script type="text/javascript" src="js/flot/jquery.flot.orderBars.js"></script>
  <script type="text/javascript" src="js/flot/jquery.flot.time.min.js"></script>
  <script type="text/javascript" src="js/flot/date.js"></script>
  <script type="text/javascript" src="js/flot/jquery.flot.spline.js"></script>
  <script type="text/javascript" src="js/flot/jquery.flot.stack.js"></script>
  <script type="text/javascript" src="js/flot/curvedLines.js"></script>
  <script type="text/javascript" src="js/flot/jquery.flot.resize.js"></script>
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  <script type="text/javascript" src="js/notify/pnotify.buttons.js"></script>
  <script type="text/javascript" src="js/notify/pnotify.nonblock.js"></script>
  <script>
    $(document).ready(function() {
      // [17, 74, 6, 39, 20, 85, 7]
      //[82, 23, 66, 9, 99, 6, 2]
      var data1 = [
        [gd(2012, 1, 1), 17],
        [gd(2012, 1, 2), 74],
        [gd(2012, 1, 3), 6],
        [gd(2012, 1, 4), 39],
        [gd(2012, 1, 5), 20],
        [gd(2012, 1, 6), 85],
        [gd(2012, 1, 7), 7]
      ];

      var data2 = [
        [gd(2012, 1, 1), 82],
        [gd(2012, 1, 2), 23],
        [gd(2012, 1, 3), 66],
        [gd(2012, 1, 4), 9],
        [gd(2012, 1, 5), 119],
        [gd(2012, 1, 6), 6],
        [gd(2012, 1, 7), 9]
      ];
      $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
        data1, data2
      ], {
        series: {
          lines: {
            show: false,
            fill: true
          },
          splines: {
            show: true,
            tension: 0.4,
            lineWidth: 1,
            fill: 0.4
          },
          points: {
            radius: 0,
            show: true
          },
          shadowSize: 2
        },
        grid: {
          verticalLines: true,
          hoverable: true,
          clickable: true,
          tickColor: "#d5d5d5",
          borderWidth: 1,
          color: '#fff'
        },
        colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
        xaxis: {
          tickColor: "rgba(51, 51, 51, 0.06)",
          mode: "time",
          tickSize: [1, "day"],
          //tickLength: 10,
          axisLabel: "Date",
          axisLabelUseCanvas: true,
          axisLabelFontSizePixels: 12,
          axisLabelFontFamily: 'Verdana, Arial',
          axisLabelPadding: 10
        },
        yaxis: {
          ticks: 8,
          tickColor: "rgba(51, 51, 51, 0.06)",
        },
        tooltip: false
      });

      function gd(year, month, day) {
        return new Date(year, month - 1, day).getTime();
      }
    });
  </script>

  <!-- worldmap -->
  <script type="text/javascript" src="js/maps/jquery-jvectormap-2.0.3.min.js"></script>
  <script type="text/javascript" src="js/maps/gdp-data.js"></script>
  <script type="text/javascript" src="js/maps/jquery-jvectormap-world-mill-en.js"></script>
  <script type="text/javascript" src="js/maps/jquery-jvectormap-us-aea-en.js"></script>
  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>
  <script>
      function guardarnota(){
        var detalle = $('#nota').val();

        if(detalle!=""){
          $.ajax({
          url:'actions/actionnotas.php',
          type:'POST',
          data: { opt: 'n', val: detalle  }
        }).done(function( data ){
            var num = data;
            var codigo = $('#nnotas').html();
            $('#nnotas').html(codigo + '<li id="'+num+'"><p><button type="button" class="btn btn-xs" onclick="borrarnota('+num+')"><i class="fa fa-square-o"></i> </button>'+detalle +'  </p></li>');
        });

        }else{

          new PNotify({
      title: 'Usuario: <?=$_SESSION['usuario'] ?>',
      text: 'Debes llenar el campo nota!!!',
      type: 'warning',
       hide: false,
      })


        }
      
        $("#nota").val('');
      }

      function borrarnota(id){
        var cod = id;
        $.ajax({
          url:'actions/actionnotas.php',
          type:'POST',
          data: { opt: 'b', val: cod  }
        }).done(function( data ){

            $('#'+id+'').hide();
            
        });

      }
  </script>
  <script>
    $(function() {
      $('#world-map-gdp').vectorMap({
        map: 'world_mill_en',
        backgroundColor: 'transparent',
        zoomOnScroll: false,
        series: {
          regions: [{
            values: gdpData,
            scale: ['#E6F2F0', '#149B7E'],
            normalizeFunction: 'polynomial'
          }]
        },
        onRegionTipShow: function(e, el, code) {
          el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
        }
      });
    });
  </script>
  <!-- skycons -->
  <script src="js/skycons/skycons.min.js"></script>
  <script>
    var icons = new Skycons({
        "color": "#73879C"
      }),
      list = [
        "clear-day", "clear-night", "partly-cloudy-day",
        "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
        "fog"
      ],
      i;

    for (i = list.length; i--;)
      icons.set(list[i], list[i]);

    icons.play();
  </script>

  <!-- dashbord linegraph -->
  <script>
  //$(document).ready(function() {
    Chart.defaults.global.legend = {
       enabled: false
    }; 

      var valores2 = $.ajax({
                url: "actions/actiongasto.php?opt=d",
                                type:"POST",
                                dataType: 'json',
                                data: {
                                        maxRows: 10,
                                        q: '0'
                                },
                                success: function(data) {
                                       
                                  var data1 = data;


                                  var canvasDoughnut = new Chart(document.getElementById("canvas1"), {
                                    type: 'doughnut',
                                    tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                                    data: data1
                                  });
                                  }
                        }) ;

 // });
    

    
    
  </script>

  <script type="text/javascript">
    $(document).ready(function() {

      var cb = function(start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
      }

      var optionSet1 = {
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2015',
        dateLimit: {
          days: 60
        },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'MM/DD/YYYY',
        separator: ' to ',
        locale: {
          applyLabel: 'Submit',
          cancelLabel: 'Clear',
          fromLabel: 'From',
          toLabel: 'To',
          customRangeLabel: 'Custom',
          daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
          monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
          firstDay: 1
        }
      };
      $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
      $('#reportrange').daterangepicker(optionSet1, cb);
      $('#reportrange').on('show.daterangepicker', function() {
        console.log("show event fired");
      });
      $('#reportrange').on('hide.daterangepicker', function() {
        console.log("hide event fired");
      });
      $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
      });
      $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
        console.log("cancel event fired");
      });
      $('#options1').click(function() {
        $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
      });
      $('#options2').click(function() {
        $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
      });
      $('#destroy').click(function() {
        $('#reportrange').data('daterangepicker').remove();
      });
    });
  </script>

  <?php }?>
  <script> 
    function irpaciente(paciente,citas){
      $('#idpacientes').val(paciente);
      $('#idcita').val(citas);
      $("#formoculto").submit();
    }
  </script>
</body>

</html>
